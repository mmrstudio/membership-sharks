<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'shark_db');

/** MySQL database username */
define('DB_USER', 'shark_db_user');

/** MySQL database password */
define('DB_PASSWORD', 'glsoalf3258');

/** MySQL hostname */
define('DB_HOST', 'mariadb-au1.nexusdigital.net.au');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z>3Y[(IZlW:^~iSB8~$w23M+&LQA?Ar:8J.8%||mSxp}?>1}J#*IhD*lqSLu]i<T');
define('SECURE_AUTH_KEY',  '2][ec(DX+ZS*z{1xQ~4|>s k*Z3<_(_;f$;kxZJoN5Y07X.8M5M*q`*9^]}`y>,N');
define('LOGGED_IN_KEY',    '1m*mB.)bE[_7OIl.PE^^4hK3WCG_>p(BSdNDh@5WHCIIDEf*hvJ1te+,g04;|#G*');
define('NONCE_KEY',        '&]SY{{$.EL3^tRuijD,-Bk.EwAi/1M8*8^KKL>/WBJ0i<)aeSNL}GBT;Yb|@Q`J ');
define('AUTH_SALT',        '$gc1`FRE&Zn0L^|U .,RMM~L!!x9M%ysOLUm4<DUhEu*X8T:hJfI]qDXEdc10B>k');
define('SECURE_AUTH_SALT', '8?vS&Jh8D*U9>FyUfeQZ)q*4i-CohGb*Fj7n_e<Ah/!Tv:no&kR/PVNIa?xlMFlk');
define('LOGGED_IN_SALT',   '=5^HI2NC@9lM@5p5&,[p$~)m,8xeCZz:pP `X_ gH+RN*7EA+k{y-xJG%G9[4X}l');
define('NONCE_SALT',       'eQi)}&F.jDwHfyLibmeka2rI:2v8[gwyLQb>Nre#8D&6@02Lm#>~ul]{SmDn)fi?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
