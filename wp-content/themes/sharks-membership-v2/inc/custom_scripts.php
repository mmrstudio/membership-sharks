<?php // Function Partial :  Custom functions for footer of html doc

// Enqueue minified JS in the footer for all pages
function turbo_enqueue_js_global() {
	if(!is_admin()){
		wp_deregister_script( 'jquery' ); // De-register default jQuery for our version
		wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js',  array(),  '', true);
		wp_enqueue_script('turbo_js_min', get_template_directory_uri() . '/assets/js/uglify.js',  array('jquery'), '', true);
	}
 }
add_action( 'wp_enqueue_scripts', 'turbo_enqueue_js_global' );
//add_action('wp_footer', 'turbo_enqueue_js',1);


// Enqueue JS for just the package summary page
function turbo_enqueue_js_packages() {
  if(!is_admin() && (is_page_template('templates/package-summary-page.php') || is_page_template('templates/package-summary-page-renew.php')) ){
	wp_enqueue_script('jquery_mixitup', get_template_directory_uri() . '/assets/js/jquery.mixitup.min.js', array('jquery'),  '', true);
	wp_enqueue_script('jquery_filtering', get_template_directory_uri() . '/assets/js/filtering.js', array('jquery'), '', true);
	wp_enqueue_script('jquery_load_package', get_template_directory_uri() . '/assets/js/load-package.js', array('jquery'), '', true);
  }
  if (is_page_template('templates/home-page.php'))
  {
  wp_enqueue_script('jquery_mixitup', get_template_directory_uri() . '/assets/js/jquery.mixitup.min.js', array('jquery'),  '', true);
  }

}
add_action('wp_enqueue_scripts', 'turbo_enqueue_js_packages' );


// Enqueue JS for just the package summary page
function turbo_enqueue_js_help_me_choose() {
  if(!is_admin() &&  is_page_template( 'templates/help-me-choose-page.php' ) ){
	wp_enqueue_script('jquery_load_package', get_template_directory_uri() . '/assets/js/load-package.js', array('jquery'), '', true);
	wp_enqueue_script('jquery_help_me_choose', get_template_directory_uri() . '/assets/js/help_me_choose.js', array('jquery'), '', true);
  }
}
add_action('wp_enqueue_scripts', 'turbo_enqueue_js_help_me_choose' );


// Enqueue minified css stylesheet (style.css is only used for the banner)
function turbo_enqueue_css() {
	wp_register_style( 'turbo_style', get_template_directory_uri() . '/assets/css/main.css', array(), null, 'all');
	wp_enqueue_style( 'turbo_style' ); // Enqueue it!
}
add_action( 'wp_enqueue_scripts', 'turbo_enqueue_css' );

//add_action('wp_footer', 'turbo_enqueue_css',1);


// Analytics - get value from options page, only echo if there is a value
function turbo_google_analytics() { ?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'XXX', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
<?php }
// add_action('wp_footer', 'turbo_google_analytics');

?>
