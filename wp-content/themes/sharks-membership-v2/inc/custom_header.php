<?php // Function Partial :  Custom functions for header of html doc

// Load IE Scripts and Favicons
function turbo_ie_favicon_scripts() { ?>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/respond.min.js"></script>
<![endif]-->

<?php }
add_action('wp_head', 'turbo_ie_favicon_scripts'); // Add IE Scripts


// Turbo Critical CSS
function turbo_critical_css(){ ?>
<style><?php //echo file_get_contents( get_template_directory_uri() ."/assets/css/critical.css"); ?></style>
<?php }

//add_action('wp_head', 'turbo_critical_css',1);


//	Add slug to body
function turbo_body_class( $classes ) {
	global $post;
	if( is_home() ) {
		$key = array_search( 'blog', $classes );
		if($key > -1) {
			unset( $classes[$key] );
		};
	} elseif( is_page() ) {
		$classes[] = sanitize_html_class( $post->post_name );
	} elseif(is_singular()) {
		$classes[] = sanitize_html_class( $post->post_name );
	};
	return $classes;
}
add_filter('body_class', 'turbo_body_class' );

?>
