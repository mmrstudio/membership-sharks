<?php
/**
 * The template for displaying a default page
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main main--sub-page <?php echo get_field("additional_classes") ?>" role="main">

<?php // Get Rows by looping over the ACF flexible fields on this page
// Get template part for each of the content rows based on layout name

while (the_flexible_field('content_rows')) {
    get_template_part('parts/'. get_row_layout());
} ?>

</section><!-- main -->

<?php get_footer(); ?>
