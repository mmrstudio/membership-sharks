<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php wp_title(); ?></title>
<meta name=viewport content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" />
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png" />
<?php wp_head(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48785601-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body <?php body_class(); ?>>

<!--Start of HappyFox Live Chat Script-->

<script>

window.HFCHAT_CONFIG = {

EMBED_TOKEN: "d2c28900-51d0-11e5-bcb8-cdaf82f8250c",

ACCESS_TOKEN: "faafe89b9fd045d19864c87ae7f9c03e",

HOST_URL: "https://happyfoxchat.com",

ASSETS_URL: "https://d1l7z5ofrj6ab8.cloudfront.net/visitor"

};



(function() {

var scriptTag = document.createElement('script');

scriptTag.type = 'text/javascript';

scriptTag.async = true;

scriptTag.src = window.HFCHAT_CONFIG.ASSETS_URL + '/js/widget-loader.js';



var s = document.getElementsByTagName('script')[0];

s.parentNode.insertBefore(scriptTag, s);

})();

</script>

<!--End of HappyFox Live Chat Script-->

	<div class="header-extend">
		<section class="header">
			<div class="container">

				<div class="logo logo--header">
					<a href="<?php bloginfo('url'); ?>"></a>
				</div>

				<div class="member-tally member-tally--header">
					<p class="member-tally__title">Member</br><span class="highlight">Tally</span></p>
					<?php
						// get membership count from options panel
						$membership_count = false;
						$membership_total = 0;
						$membership_total = get_field('membership_total', 'option');

						if( $membership_total ) :
							$membership_count = str_pad($membership_total, 5, STR_PAD_LEFT);
							$membership_count = str_split($membership_count);
						endif;

						// Apply animate if First visit and total is greater than 0
						$animate_class = false;
						if(FIRST_VISIT && $membership_total > 0) :
							$animate_class = 'animate';
						endif;
					?>
					<div class="member-tally__total counter <?php echo $animate_class; ?>" data-membership-count="<?php echo ($membership_total > 0) ? $membership_total : '0'; ?>">
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[0] : '0'; ?></div>
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[1] : '0'; ?></div>
						<div class="digit digit-comma">,</div>
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[2] : '0'; ?></div>
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[3] : '0'; ?></div>
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[4] : '0'; ?></div>
					</div>
				</div>

				<ul class="social social--header">
					<li><a class="social__item social__icon--facebook" href="<?php the_field('facebook_link','options'); ?>" target="_blank"></a></li>
					<li><a class="social__item social__icon--instagram" href="<?php the_field('instagram_link','options'); ?>" target="_blank"></a></li>
					<li><a class="social__item social__icon--twitter" href="<?php the_field('twitter_link','options'); ?>" target="_blank"></a></li>
				</ul>

                    <div class="member-login">
                      <a href="https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login" onclick="window.open(this.href, 'newwindow', 'width=900, height=750'); return false;" class="btn">Member Login</a>
                    </div>

               <div class="nav">
               	<?php //wp_list_pages("depth=2&title_li="); ?>
				<?php // wp nav menu function defaults
				$nav_menu_large = array(
					'theme_location'  => 'large-screen-menu',
					'menu'            => '',
					'container'       => false,
					'container_class' => 'main-nav--large',
					'container_id'    => '',
					'menu_class'      => '',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => false,
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul class="main-nav--large" id="%1$s">%3$s</ul>',
					'depth'           => 3,
					'walker'          => '',
				);

				wp_nav_menu( $nav_menu_large);
				?>

			<a href="#mobile-menu" class="menu-toggle"><span></span><p class="menubtn">Menu</p><p class="closebtn">Close</p></a>
			<nav id="mobile-menu">

				<?php // wp nav menu function defaults
				$nav_menu_small = array(
					'theme_location'  => 'small-screen-menu',
					'menu'            => '',
					'container'       => false,
					'container_class' => 'small-screen-menu',
					'container_id'    => '',
					'menu_class'      => '',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => false,
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
					'depth'           => 3,
					'walker'          => '',
				);

				wp_nav_menu( $nav_menu_small );
				?>
			</nav>

		</section><!-- header -->
	</div>
<?php if (get_the_title() == 'Home') : ?>
	<div>
		<div>
<?php else: ?>
	<div class="container-background">
		<div class="container container-base">
<?php endif; ?>
