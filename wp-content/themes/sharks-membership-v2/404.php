<?php
/**
 * The template for displaying a 404
 *
 * @package Turbo Starter Theme
 */

get_header(); ?>

<section id="main" class="main main--404" role="main">

	<div class="container">
		<h1 class="heading--featured"> 404 page not found</h1>
		<p>The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.</p>

		<p>Please try the following:</p>
		<ul>
			<li>Check your spelling</li>
			<li>Return to the <a href="<?php echo home_url(); ?>/">home page</a></li>
			<li>Click the <a href="javascript:history.back()">Back</a> button</li>
		</ul>

	</div><!-- .row__container -->
</section><!-- #main -->

<?php get_footer(); ?>
