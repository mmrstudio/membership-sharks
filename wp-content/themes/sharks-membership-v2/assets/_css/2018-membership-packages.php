<?php
/**
 * Template Name: 2018 Membership Packages page
 *
 * 2018 Membership Packages page template
 *

 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main main--sub-page" role="main">

	<?php // Get Rows by looping over the ACF flexible fields on this page
	// Get template part for each of the content rows based on layout name
	while (the_flexible_field('content_rows')) {
		get_template_part('parts/'. get_row_layout());
	} ?>


	<section class="membership-packs-page">
		<!-- <div class="container">
			<div class="membership-packs-page__list"> -->

		<div class="all-membership__sidebar">

			<div class="all-membership__sidebar__category">
				<h3>location</h3>
				<input class="hidden" type="checkbox" id="btn-NSW">
				<label for="btn-NSW" class="checkbox-button"><span>NSW</span></label>

				<input class="hidden" type="checkbox" id="btn-Interstate">
				<label for="btn-Interstate" class="checkbox-button"><span>Interstate</span></label>

				<input class="hidden" type="checkbox" id="btn-International">
				<label for="btn-International" class="checkbox-button"><span>International</span></label>
			</div>

			<div class="all-membership__sidebar__category">
				<h3>access</h3>
				<input class="hidden" type="checkbox" id="btn-full-season">
				<label for="btn-full-season" class="checkbox-button"><span>full season</span></label>

				<input class="hidden" type="checkbox" id="btn-6-games">
				<label for="btn-6-games" class="checkbox-button"><span>6 games</span></label>

				<input class="hidden" type="checkbox" id="btn-3-games">
				<label for="btn-3-games" class="checkbox-button"><span>3 games</span></label>

				<input class="hidden" type="checkbox" id="btn-2-games">
				<label for="btn-2-games" class="checkbox-button"><span>2 games</span></label>

				<input class="hidden" type="checkbox" id="btn-1-game">
				<label for="btn-1-game" class="checkbox-button"><span>1 game</span></label>

				<input class="hidden" type="checkbox" id="btn-no-games">
				<label for="btn-no-games" class="checkbox-button"><span>no games</span></label>
			</div>

			<div class="all-membership__sidebar__category">
				<h3>seating</h3>
				<input class="hidden" type="checkbox" id="btn-reserved">
				<label for="btn-reserved" class="checkbox-button"><span>reserved</span></label>

				<input class="hidden" type="checkbox" id="btn-general-season">
				<label for="btn-general-season" class="checkbox-button"><span>general season</span></label>
			</div>
		</div><!--  END div.all-membership__sidebar-->

		<div class="all-membership__main">
		<?php
		$args = array( 'post_type' => 'packages', 'posts_per_page' => 9 );
		$the_query = new WP_Query( $args );

		if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

			<div class="help_me-row__packages--group_details">
				<div class="bg">
					<div class="title">
						<h3><?php echo get_the_title($group->ID) ?></h3>
					</div>
					<div class="details">
						<div class="descriptor"><?php echo get_field('price_descriptor',$group->ID); ?></div>
						<div class="price"><?php echo get_field('price',$group->ID); ?></div>
						<div class="subtext"><?php echo get_field('price_subtext',$group->ID); ?></div>
					</div>
					<div class="action">
						<a href="<?php echo get_permalink($group->ID) ?>" class="btn">MORE INFO</a>
					</div>
					<div class="image">
						<?php $image = wp_get_attachment_image_src(get_field('panel_image',$group->ID),'panel');  ?>
						<img src="<?php echo $image[0]; ?>" />
					</div>
				</div>
			</div>

		<?php endwhile;
		wp_reset_postdata();
		else:  ?>
			<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
		</div>


			<!-- </div><!-- -END membership-packs-page__list -->

		<!-- </div> -->
	</section><!-- .member-benefits -->
</section>

<?php get_footer();
