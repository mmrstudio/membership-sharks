// Js to load the package details into the side panel

var loadPackageinit = function(){
	// load the package & open panel
	$('a.js-trigger-slideout').click(function(event) {
		// prevent default link
		event.preventDefault();
		// clear html
		$("#package-results").html('');
		// get link value
		var packageLink = $(this).attr('href');
		// add class to panel to make visible
		$('.cd-panel').addClass('is-visible');
		// add class to body to make blocker visible (via CSS)
		$('body').addClass('js-blocker-on');

		// to to the next link, then search over the data for the result (container) we want
		$.get(packageLink, function(data) {
			// Store the data we need (each list item) as a result
			result = $(".package-detail", data);
			// replace the holder div with the results
			$("#package-results").html(result);
			$('.package-item').equalHeights();
		});
	});

	//close the  panel
	$('.cd-panel').on('click', function(event){
		// if we don't click outside of the content or click close
		if( $(event.target).is('.cd-panel') || $(event.target).is('.cd-panel-close') ) {
			// prevent default click
			event.preventDefault();
			// remove visable class from the pannel to trigger animation
			$('.cd-panel').removeClass('is-visible');
			// remove the body class so the blocker dispersals
			$('body').removeClass('js-blocker-on');
		}
	});

};


jQuery(document).ready(function($) {


	// load packages
	loadPackageinit();

	// refine options toggle (the filter form is hidden with css on small screens)
	$('.filter-controls h3').click(function() {
		$('.filter-form').addClass('is-visible');
	});

	// filter form - click anywhere anchor to filer section
	$('.filter-form input').click(function() {
		// only if we're on screens less that 1260px
		if (window.matchMedia("(max-width: 1260px)").matches) {
			$('html,body').animate({
				scrollTop:  $("#filter-container").offset().top
			}, 300);
		}
		//return false;
	});

});
