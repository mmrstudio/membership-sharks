!function(e, t) {
    "function" == typeof define && define.amd ? define([ "jquery" ], t) : "object" == typeof exports ? module.exports = t(require("jquery")) : e.jquery_mmenu_all_min_js = t(e.jQuery);
}(this, function(e) {
    return !function(e) {
        function t() {
            e[i].glbl || (a = {
                $wndw: e(window),
                $docu: e(document),
                $html: e("html"),
                $body: e("body")
            }, s = {}, o = {}, r = {}, e.each([ s, o, r ], function(e, t) {
                t.add = function(e) {
                    e = e.split(" ");
                    for (var i = 0, n = e.length; n > i; i++) t[e[i]] = t.mm(e[i]);
                };
            }), s.mm = function(e) {
                return "mm-" + e;
            }, s.add("wrapper menu panels panel nopanel current highest opened subopened navbar hasnavbar title btn prev next listview nolistview inset vertical selected divider spacer hidden fullsubopen"),
            s.umm = function(e) {
                return "mm-" == e.slice(0, 3) && (e = e.slice(3)), e;
            }, o.mm = function(e) {
                return "mm-" + e;
            }, o.add("parent child"), r.mm = function(e) {
                return e + ".mm";
            }, r.add("transitionend webkitTransitionEnd click scroll keydown mousedown mouseup touchstart touchmove touchend orientationchange"),
            e[i]._c = s, e[i]._d = o, e[i]._e = r, e[i].glbl = a);
        }
        var i = "mmenu", n = "5.7.1";
        if (!(e[i] && e[i].version > n)) {
            e[i] = function(e, t, i) {
                this.$menu = e, this._api = [ "bind", "initPanels", "update", "setSelected", "getInstance", "openPanel", "closePanel", "closeAllPanels" ],
                this.opts = t, this.conf = i, this.vars = {}, this.cbck = {}, "function" == typeof this.___deprecated && this.___deprecated(),
                this._initMenu(), this._initAnchors();
                var n = this.$pnls.children();
                return this._initAddons(), this.initPanels(n), "function" == typeof this.___debug && this.___debug(),
                this;
            }, e[i].version = n, e[i].addons = {}, e[i].uniqueId = 0, e[i].defaults = {
                extensions: [],
                initMenu: function() {},
                initPanels: function() {},
                navbar: {
                    add: !0,
                    title: "Menu",
                    titleLink: "panel"
                },
                onClick: {
                    setSelected: !0
                },
                slidingSubmenus: !0
            }, e[i].configuration = {
                classNames: {
                    divider: "Divider",
                    inset: "Inset",
                    panel: "Panel",
                    selected: "Selected",
                    spacer: "Spacer",
                    vertical: "Vertical"
                },
                clone: !1,
                openingInterval: 25,
                panelNodetype: "ul, ol, div",
                transitionDuration: 400
            }, e[i].prototype = {
                init: function(e) {
                    this.initPanels(e);
                },
                initPanels: function(e) {
                    e = e.not("." + s.nopanel), e = this._initPanels(e), this.opts.initPanels.call(this, e),
                    this.trigger("initPanels", e), this.trigger("update");
                },
                update: function() {
                    this.trigger("update");
                },
                setSelected: function(e) {
                    this.$menu.find("." + s.listview).children().removeClass(s.selected), e.addClass(s.selected),
                    this.trigger("setSelected", e);
                },
                openPanel: function(t) {
                    var n = t.parent(), o = this;
                    if (n.hasClass(s.vertical)) {
                        var r = n.parents("." + s.subopened);
                        if (r.length) return void this.openPanel(r.first());
                        n.addClass(s.opened), this.trigger("openPanel", t), this.trigger("openingPanel", t),
                        this.trigger("openedPanel", t);
                    } else {
                        if (t.hasClass(s.current)) return;
                        var a = this.$pnls.children("." + s.panel), l = a.filter("." + s.current);
                        a.removeClass(s.highest).removeClass(s.current).not(t).not(l).not("." + s.vertical).addClass(s.hidden),
                        e[i].support.csstransitions || l.addClass(s.hidden), t.hasClass(s.opened) ? t.nextAll("." + s.opened).addClass(s.highest).removeClass(s.opened).removeClass(s.subopened) : (t.addClass(s.highest),
                        l.addClass(s.subopened)), t.removeClass(s.hidden).addClass(s.current), o.trigger("openPanel", t),
                        setTimeout(function() {
                            t.removeClass(s.subopened).addClass(s.opened), o.trigger("openingPanel", t), o.__transitionend(t, function() {
                                o.trigger("openedPanel", t);
                            }, o.conf.transitionDuration);
                        }, this.conf.openingInterval);
                    }
                },
                closePanel: function(e) {
                    var t = e.parent();
                    t.hasClass(s.vertical) && (t.removeClass(s.opened), this.trigger("closePanel", e),
                    this.trigger("closingPanel", e), this.trigger("closedPanel", e));
                },
                closeAllPanels: function() {
                    this.$menu.find("." + s.listview).children().removeClass(s.selected).filter("." + s.vertical).removeClass(s.opened);
                    var e = this.$pnls.children("." + s.panel), t = e.first();
                    this.$pnls.children("." + s.panel).not(t).removeClass(s.subopened).removeClass(s.opened).removeClass(s.current).removeClass(s.highest).addClass(s.hidden),
                    this.openPanel(t);
                },
                togglePanel: function(e) {
                    var t = e.parent();
                    t.hasClass(s.vertical) && this[t.hasClass(s.opened) ? "closePanel" : "openPanel"](e);
                },
                getInstance: function() {
                    return this;
                },
                bind: function(e, t) {
                    e = "init" == e ? "initPanels" : e, this.cbck[e] = this.cbck[e] || [], this.cbck[e].push(t);
                },
                trigger: function() {
                    var e = this, t = Array.prototype.slice.call(arguments), i = t.shift();
                    if (i = "init" == i ? "initPanels" : i, this.cbck[i]) for (var n = 0, s = this.cbck[i].length; s > n; n++) this.cbck[i][n].apply(e, t);
                },
                _initMenu: function() {
                    this.conf.clone && (this.$orig = this.$menu, this.$menu = this.$orig.clone(!0),
                    this.$menu.add(this.$menu.find("[id]")).filter("[id]").each(function() {
                        e(this).attr("id", s.mm(e(this).attr("id")));
                    })), this.opts.initMenu.call(this, this.$menu, this.$orig), this.$menu.attr("id", this.$menu.attr("id") || this.__getUniqueId()),
                    this.$pnls = e('<div class="' + s.panels + '" />').append(this.$menu.children(this.conf.panelNodetype)).prependTo(this.$menu),
                    this.$menu.parent().addClass(s.wrapper);
                    var t = [ s.menu ];
                    this.opts.slidingSubmenus || t.push(s.vertical), this.opts.extensions = this.opts.extensions.length ? "mm-" + this.opts.extensions.join(" mm-") : "",
                    this.opts.extensions && t.push(this.opts.extensions), this.$menu.addClass(t.join(" "));
                },
                _initPanels: function(t) {
                    var i = this, n = this.__findAddBack(t, "ul, ol");
                    this.__refactorClass(n, this.conf.classNames.inset, "inset").addClass(s.nolistview + " " + s.nopanel),
                    n.not("." + s.nolistview).addClass(s.listview);
                    var r = this.__findAddBack(t, "." + s.listview).children();
                    this.__refactorClass(r, this.conf.classNames.selected, "selected"), this.__refactorClass(r, this.conf.classNames.divider, "divider"),
                    this.__refactorClass(r, this.conf.classNames.spacer, "spacer"), this.__refactorClass(this.__findAddBack(t, "." + this.conf.classNames.panel), this.conf.classNames.panel, "panel");
                    var a = e(), l = t.add(t.find("." + s.panel)).add(this.__findAddBack(t, "." + s.listview).children().children(this.conf.panelNodetype)).not("." + s.nopanel);
                    this.__refactorClass(l, this.conf.classNames.vertical, "vertical"), this.opts.slidingSubmenus || l.addClass(s.vertical),
                    l.each(function() {
                        var t = e(this), n = t;
                        t.is("ul, ol") ? (t.wrap('<div class="' + s.panel + '" />'), n = t.parent()) : n.addClass(s.panel);
                        var o = t.attr("id");
                        t.removeAttr("id"), n.attr("id", o || i.__getUniqueId()), t.hasClass(s.vertical) && (t.removeClass(i.conf.classNames.vertical),
                        n.add(n.parent()).addClass(s.vertical)), a = a.add(n);
                    });
                    var d = e("." + s.panel, this.$menu);
                    a.each(function(t) {
                        var n, r, a = e(this), l = a.parent(), d = l.children("a, span").first();
                        if (l.is("." + s.panels) || (l.data(o.child, a), a.data(o.parent, l)), l.children("." + s.next).length || l.parent().is("." + s.listview) && (n = a.attr("id"),
                        r = e('<a class="' + s.next + '" href="#' + n + '" data-target="#' + n + '" />').insertBefore(d),
                        d.is("span") && r.addClass(s.fullsubopen)), !a.children("." + s.navbar).length && !l.hasClass(s.vertical)) {
                            l.parent().is("." + s.listview) ? l = l.closest("." + s.panel) : (d = l.closest("." + s.panel).find('a[href="#' + a.attr("id") + '"]').first(),
                            l = d.closest("." + s.panel));
                            var c = !1, u = e('<div class="' + s.navbar + '" />');
                            if (i.opts.navbar.add && a.addClass(s.hasnavbar), l.length) {
                                switch (n = l.attr("id"), i.opts.navbar.titleLink) {
                                  case "anchor":
                                    c = d.attr("href");
                                    break;

                                  case "panel":
                                  case "parent":
                                    c = "#" + n;
                                    break;

                                  default:
                                    c = !1;
                                }
                                u.append('<a class="' + s.btn + " " + s.prev + '" href="#' + n + '" data-target="#' + n + '" />').append(e('<a class="' + s.title + '"' + (c ? ' href="' + c + '"' : "") + " />").text(d.text())).prependTo(a);
                            } else i.opts.navbar.title && u.append('<a class="' + s.title + '">' + i.opts.navbar.title + "</a>").prependTo(a);
                        }
                    });
                    var c = this.__findAddBack(t, "." + s.listview).children("." + s.selected).removeClass(s.selected).last().addClass(s.selected);
                    c.add(c.parentsUntil("." + s.menu, "li")).filter("." + s.vertical).addClass(s.opened).end().each(function() {
                        e(this).parentsUntil("." + s.menu, "." + s.panel).not("." + s.vertical).first().addClass(s.opened).parentsUntil("." + s.menu, "." + s.panel).not("." + s.vertical).first().addClass(s.opened).addClass(s.subopened);
                    }), c.children("." + s.panel).not("." + s.vertical).addClass(s.opened).parentsUntil("." + s.menu, "." + s.panel).not("." + s.vertical).first().addClass(s.opened).addClass(s.subopened);
                    var u = d.filter("." + s.opened);
                    return u.length || (u = a.first()), u.addClass(s.opened).last().addClass(s.current),
                    a.not("." + s.vertical).not(u.last()).addClass(s.hidden).end().filter(function() {
                        return !e(this).parent().hasClass(s.panels);
                    }).appendTo(this.$pnls), a;
                },
                _initAnchors: function() {
                    var t = this;
                    a.$body.on(r.click + "-oncanvas", "a[href]", function(n) {
                        var o = e(this), r = !1, a = t.$menu.find(o).length;
                        for (var l in e[i].addons) if (e[i].addons[l].clickAnchor.call(t, o, a)) {
                            r = !0;
                            break;
                        }
                        var d = o.attr("href");
                        if (!r && a && d.length > 1 && "#" == d.slice(0, 1)) try {
                            var c = e(d, t.$menu);
                            c.is("." + s.panel) && (r = !0, t[o.parent().hasClass(s.vertical) ? "togglePanel" : "openPanel"](c));
                        } catch (u) {}
                        if (r && n.preventDefault(), !r && a && o.is("." + s.listview + " > li > a") && !o.is('[rel="external"]') && !o.is('[target="_blank"]')) {
                            t.__valueOrFn(t.opts.onClick.setSelected, o) && t.setSelected(e(n.target).parent());
                            var p = t.__valueOrFn(t.opts.onClick.preventDefault, o, "#" == d.slice(0, 1));
                            p && n.preventDefault(), t.__valueOrFn(t.opts.onClick.close, o, p) && t.close();
                        }
                    });
                },
                _initAddons: function() {
                    var t;
                    for (t in e[i].addons) e[i].addons[t].add.call(this), e[i].addons[t].add = function() {};
                    for (t in e[i].addons) e[i].addons[t].setup.call(this);
                },
                _getOriginalMenuId: function() {
                    var e = this.$menu.attr("id");
                    return e && e.length && this.conf.clone && (e = s.umm(e)), e;
                },
                __api: function() {
                    var t = this, i = {};
                    return e.each(this._api, function(e) {
                        var n = this;
                        i[n] = function() {
                            var e = t[n].apply(t, arguments);
                            return void 0 === e ? i : e;
                        };
                    }), i;
                },
                __valueOrFn: function(e, t, i) {
                    return "function" == typeof e ? e.call(t[0]) : void 0 === e && void 0 !== i ? i : e;
                },
                __refactorClass: function(e, t, i) {
                    return e.filter("." + t).removeClass(t).addClass(s[i]);
                },
                __findAddBack: function(e, t) {
                    return e.find(t).add(e.filter(t));
                },
                __filterListItems: function(e) {
                    return e.not("." + s.divider).not("." + s.hidden);
                },
                __transitionend: function(t, i, n) {
                    var s = !1, o = function(n) {
                        if (void 0 !== n) {
                            if (!e(n.target).is(t)) return !1;
                            t.unbind(r.transitionend), t.unbind(r.webkitTransitionEnd);
                        }
                        s || i.call(t[0]), s = !0;
                    };
                    t.on(r.transitionend, o), t.on(r.webkitTransitionEnd, o), setTimeout(o, 1.1 * n);
                },
                __getUniqueId: function() {
                    return s.mm(e[i].uniqueId++);
                }
            }, e.fn[i] = function(n, s) {
                return t(), n = e.extend(!0, {}, e[i].defaults, n), s = e.extend(!0, {}, e[i].configuration, s),
                this.each(function() {
                    var t = e(this);
                    if (!t.data(i)) {
                        var o = new e[i](t, n, s);
                        o.$menu.data(i, o.__api());
                    }
                });
            }, e[i].support = {
                touch: "ontouchstart" in window || navigator.msMaxTouchPoints || !1,
                csstransitions: function() {
                    if ("undefined" != typeof Modernizr && void 0 !== Modernizr.csstransitions) return Modernizr.csstransitions;
                    var e = document.body || document.documentElement, t = e.style, i = "transition";
                    if ("string" == typeof t[i]) return !0;
                    var n = [ "Moz", "webkit", "Webkit", "Khtml", "O", "ms" ];
                    i = i.charAt(0).toUpperCase() + i.substr(1);
                    for (var s = 0; s < n.length; s++) if ("string" == typeof t[n[s] + i]) return !0;
                    return !1;
                }(),
                csstransforms: function() {
                    return "undefined" == typeof Modernizr || void 0 === Modernizr.csstransforms || Modernizr.csstransforms;
                }(),
                csstransforms3d: function() {
                    return "undefined" == typeof Modernizr || void 0 === Modernizr.csstransforms3d || Modernizr.csstransforms3d;
                }()
            };
            var s, o, r, a;
        }
    }(e), function(e) {
        var t = "mmenu", i = "offCanvas";
        e[t].addons[i] = {
            setup: function() {
                if (this.opts[i]) {
                    var s = this.opts[i], o = this.conf[i];
                    r = e[t].glbl, this._api = e.merge(this._api, [ "open", "close", "setPage" ]), ("top" == s.position || "bottom" == s.position) && (s.zposition = "front"),
                    "string" != typeof o.pageSelector && (o.pageSelector = "> " + o.pageNodetype), r.$allMenus = (r.$allMenus || e()).add(this.$menu),
                    this.vars.opened = !1;
                    var a = [ n.offcanvas ];
                    "left" != s.position && a.push(n.mm(s.position)), "back" != s.zposition && a.push(n.mm(s.zposition)),
                    this.$menu.addClass(a.join(" ")).parent().removeClass(n.wrapper), e[t].support.csstransforms || this.$menu.addClass(n["no-csstransforms"]),
                    e[t].support.csstransforms3d || this.$menu.addClass(n["no-csstransforms3d"]), this.setPage(r.$page),
                    this._initBlocker(), this["_initWindow_" + i](), this.$menu[o.menuInjectMethod + "To"](o.menuWrapperSelector);
                    var l = window.location.hash;
                    if (l) {
                        var d = this._getOriginalMenuId();
                        d && d == l.slice(1) && this.open();
                    }
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("offcanvas slideout blocking modal background opening blocker page no-csstransforms3d"),
                s.add("style"), o.add("resize");
            },
            clickAnchor: function(e, t) {
                if (!this.opts[i]) return !1;
                var n = this._getOriginalMenuId();
                return n && e.is('[href="#' + n + '"]') ? (this.open(), !0) : r.$page ? (n = r.$page.first().attr("id"),
                !(!n || !e.is('[href="#' + n + '"]') || (this.close(), 0))) : void 0;
            }
        }, e[t].defaults[i] = {
            position: "left",
            zposition: "back",
            blockUI: !0,
            moveBackground: !0
        }, e[t].configuration[i] = {
            pageNodetype: "div",
            pageSelector: null,
            noPageSelector: [],
            wrapPageIfNeeded: !0,
            menuWrapperSelector: "body",
            menuInjectMethod: "prepend"
        }, e[t].prototype.open = function() {
            if (!this.vars.opened) {
                var e = this;
                this._openSetup(), setTimeout(function() {
                    e._openFinish();
                }, this.conf.openingInterval), this.trigger("open");
            }
        }, e[t].prototype._openSetup = function() {
            var t = this, a = this.opts[i];
            this.closeAllOthers(), r.$page.each(function() {
                e(this).data(s.style, e(this).attr("style") || "");
            }), r.$wndw.trigger(o.resize + "-" + i, [ !0 ]);
            var l = [ n.opened ];
            a.blockUI && l.push(n.blocking), "modal" == a.blockUI && l.push(n.modal), a.moveBackground && l.push(n.background),
            "left" != a.position && l.push(n.mm(this.opts[i].position)), "back" != a.zposition && l.push(n.mm(this.opts[i].zposition)),
            this.opts.extensions && l.push(this.opts.extensions), r.$html.addClass(l.join(" ")),
            setTimeout(function() {
                t.vars.opened = !0;
            }, this.conf.openingInterval), this.$menu.addClass(n.current + " " + n.opened);
        }, e[t].prototype._openFinish = function() {
            var e = this;
            this.__transitionend(r.$page.first(), function() {
                e.trigger("opened");
            }, this.conf.transitionDuration), r.$html.addClass(n.opening), this.trigger("opening");
        }, e[t].prototype.close = function() {
            if (this.vars.opened) {
                var t = this;
                this.__transitionend(r.$page.first(), function() {
                    t.$menu.removeClass(n.current).removeClass(n.opened), r.$html.removeClass(n.opened).removeClass(n.blocking).removeClass(n.modal).removeClass(n.background).removeClass(n.mm(t.opts[i].position)).removeClass(n.mm(t.opts[i].zposition)),
                    t.opts.extensions && r.$html.removeClass(t.opts.extensions), r.$page.each(function() {
                        e(this).attr("style", e(this).data(s.style));
                    }), t.vars.opened = !1, t.trigger("closed");
                }, this.conf.transitionDuration), r.$html.removeClass(n.opening), this.trigger("close"),
                this.trigger("closing");
            }
        }, e[t].prototype.closeAllOthers = function() {
            r.$allMenus.not(this.$menu).each(function() {
                var i = e(this).data(t);
                i && i.close && i.close();
            });
        }, e[t].prototype.setPage = function(t) {
            var s = this, o = this.conf[i];
            t && t.length || (t = r.$body.find(o.pageSelector), o.noPageSelector.length && (t = t.not(o.noPageSelector.join(", "))),
            t.length > 1 && o.wrapPageIfNeeded && (t = t.wrapAll("<" + this.conf[i].pageNodetype + " />").parent())),
            t.each(function() {
                e(this).attr("id", e(this).attr("id") || s.__getUniqueId());
            }), t.addClass(n.page + " " + n.slideout), r.$page = t, this.trigger("setPage", t);
        }, e[t].prototype["_initWindow_" + i] = function() {
            r.$wndw.off(o.keydown + "-" + i).on(o.keydown + "-" + i, function(e) {
                return r.$html.hasClass(n.opened) && 9 == e.keyCode ? (e.preventDefault(), !1) : void 0;
            });
            var e = 0;
            r.$wndw.off(o.resize + "-" + i).on(o.resize + "-" + i, function(t, i) {
                if (1 == r.$page.length && (i || r.$html.hasClass(n.opened))) {
                    var s = r.$wndw.height();
                    (i || s != e) && (e = s, r.$page.css("minHeight", s));
                }
            });
        }, e[t].prototype._initBlocker = function() {
            var t = this;
            this.opts[i].blockUI && (r.$blck || (r.$blck = e('<div id="' + n.blocker + '" class="' + n.slideout + '" />')),
            r.$blck.appendTo(r.$body).off(o.touchstart + "-" + i + " " + o.touchmove + "-" + i).on(o.touchstart + "-" + i + " " + o.touchmove + "-" + i, function(e) {
                e.preventDefault(), e.stopPropagation(), r.$blck.trigger(o.mousedown + "-" + i);
            }).off(o.mousedown + "-" + i).on(o.mousedown + "-" + i, function(e) {
                e.preventDefault(), r.$html.hasClass(n.modal) || (t.closeAllOthers(), t.close());
            }));
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "scrollBugFix";
        e[t].addons[i] = {
            setup: function() {
                var s = this, a = this.opts[i];
                if (this.conf[i], r = e[t].glbl, e[t].support.touch && this.opts.offCanvas && this.opts.offCanvas.blockUI && ("boolean" == typeof a && (a = {
                    fix: a
                }), "object" != typeof a && (a = {}), a = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], a),
                a.fix)) {
                    var l = this.$menu.attr("id"), d = !1;
                    this.bind("opening", function() {
                        this.$pnls.children("." + n.current).scrollTop(0);
                    }), r.$docu.on(o.touchmove, function(e) {
                        s.vars.opened && e.preventDefault();
                    }), r.$body.on(o.touchstart, "#" + l + "> ." + n.panels + "> ." + n.current, function(e) {
                        s.vars.opened && (d || (d = !0, 0 === e.currentTarget.scrollTop ? e.currentTarget.scrollTop = 1 : e.currentTarget.scrollHeight === e.currentTarget.scrollTop + e.currentTarget.offsetHeight && (e.currentTarget.scrollTop -= 1),
                        d = !1));
                    }).on(o.touchmove, "#" + l + "> ." + n.panels + "> ." + n.current, function(t) {
                        s.vars.opened && e(this)[0].scrollHeight > e(this).innerHeight() && t.stopPropagation();
                    }), r.$wndw.on(o.orientationchange, function() {
                        s.$pnls.children("." + n.current).scrollTop(0).css({
                            "-webkit-overflow-scrolling": "auto"
                        }).css({
                            "-webkit-overflow-scrolling": "touch"
                        });
                    });
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e;
            },
            clickAnchor: function(e, t) {}
        }, e[t].defaults[i] = {
            fix: !0
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "autoHeight";
        e[t].addons[i] = {
            setup: function() {
                if (this.opts.offCanvas) {
                    var s = this.opts[i];
                    if (this.conf[i], r = e[t].glbl, "boolean" == typeof s && s && (s = {
                        height: "auto"
                    }), "string" == typeof s && (s = {
                        height: s
                    }), "object" != typeof s && (s = {}), s = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], s),
                    "auto" == s.height || "highest" == s.height) {
                        this.$menu.addClass(n.autoheight);
                        var o = function(t) {
                            if (this.vars.opened) {
                                var i = parseInt(this.$pnls.css("top"), 10) || 0, o = parseInt(this.$pnls.css("bottom"), 10) || 0, r = 0;
                                this.$menu.addClass(n.measureheight), "auto" == s.height ? (t = t || this.$pnls.children("." + n.current),
                                t.is("." + n.vertical) && (t = t.parents("." + n.panel).not("." + n.vertical).first()),
                                r = t.outerHeight()) : "highest" == s.height && this.$pnls.children().each(function() {
                                    var t = e(this);
                                    t.is("." + n.vertical) && (t = t.parents("." + n.panel).not("." + n.vertical).first()),
                                    r = Math.max(r, t.outerHeight());
                                }), this.$menu.height(r + i + o).removeClass(n.measureheight);
                            }
                        };
                        this.bind("opening", o), "highest" == s.height && this.bind("initPanels", o), "auto" == s.height && (this.bind("update", o),
                        this.bind("openPanel", o), this.bind("closePanel", o));
                    }
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("autoheight measureheight"), o.add("resize");
            },
            clickAnchor: function(e, t) {}
        }, e[t].defaults[i] = {
            height: "default"
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "backButton";
        e[t].addons[i] = {
            setup: function() {
                if (this.opts.offCanvas) {
                    var s = this, o = this.opts[i];
                    if (this.conf[i], r = e[t].glbl, "boolean" == typeof o && (o = {
                        close: o
                    }), "object" != typeof o && (o = {}), o = e.extend(!0, {}, e[t].defaults[i], o),
                    o.close) {
                        var a = "#" + s.$menu.attr("id");
                        this.bind("opened", function(e) {
                            location.hash != a && history.pushState(null, document.title, a);
                        }), e(window).on("popstate", function(e) {
                            r.$html.hasClass(n.opened) ? (e.stopPropagation(), s.close()) : location.hash == a && (e.stopPropagation(),
                            s.open());
                        });
                    }
                }
            },
            add: function() {
                return window.history && window.history.pushState ? (n = e[t]._c, s = e[t]._d, void (o = e[t]._e)) : void (e[t].addons[i].setup = function() {});
            },
            clickAnchor: function(e, t) {}
        }, e[t].defaults[i] = {
            close: !1
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "columns";
        e[t].addons[i] = {
            setup: function() {
                var s = this.opts[i];
                if (this.conf[i], r = e[t].glbl, "boolean" == typeof s && (s = {
                    add: s
                }), "number" == typeof s && (s = {
                    add: !0,
                    visible: s
                }), "object" != typeof s && (s = {}), "number" == typeof s.visible && (s.visible = {
                    min: s.visible,
                    max: s.visible
                }), s = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], s), s.add) {
                    s.visible.min = Math.max(1, Math.min(6, s.visible.min)), s.visible.max = Math.max(s.visible.min, Math.min(6, s.visible.max)),
                    this.$menu.addClass(n.columns);
                    for (var o = this.opts.offCanvas ? this.$menu.add(r.$html) : this.$menu, a = [], l = 0; l <= s.visible.max; l++) a.push(n.columns + "-" + l);
                    a = a.join(" ");
                    var d = function(e) {
                        p.call(this, this.$pnls.children("." + n.current));
                    }, c = function() {
                        var e = this.$pnls.children("." + n.panel).filter("." + n.opened).length;
                        e = Math.min(s.visible.max, Math.max(s.visible.min, e)), o.removeClass(a).addClass(n.columns + "-" + e);
                    }, u = function() {
                        this.opts.offCanvas && r.$html.removeClass(a);
                    }, p = function(t) {
                        this.$pnls.children("." + n.panel).removeClass(a).filter("." + n.subopened).removeClass(n.hidden).add(t).slice(-s.visible.max).each(function(t) {
                            e(this).addClass(n.columns + "-" + t);
                        });
                    };
                    this.bind("open", c), this.bind("close", u), this.bind("initPanels", d), this.bind("openPanel", p),
                    this.bind("openingPanel", c), this.bind("openedPanel", c), this.opts.offCanvas || c.call(this);
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("columns");
            },
            clickAnchor: function(t, s) {
                if (!this.opts[i].add) return !1;
                if (s) {
                    var o = t.attr("href");
                    if (o.length > 1 && "#" == o.slice(0, 1)) try {
                        var r = e(o, this.$menu);
                        if (r.is("." + n.panel)) for (var a = parseInt(t.closest("." + n.panel).attr("class").split(n.columns + "-")[1].split(" ")[0], 10) + 1; a !== !1; ) {
                            var l = this.$pnls.children("." + n.columns + "-" + a);
                            if (!l.length) {
                                a = !1;
                                break;
                            }
                            a++, l.removeClass(n.subopened).removeClass(n.opened).removeClass(n.current).removeClass(n.highest).addClass(n.hidden);
                        }
                    } catch (d) {}
                }
            }
        }, e[t].defaults[i] = {
            add: !1,
            visible: {
                min: 1,
                max: 3
            }
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "counters";
        e[t].addons[i] = {
            setup: function() {
                var o = this, a = this.opts[i];
                this.conf[i], r = e[t].glbl, "boolean" == typeof a && (a = {
                    add: a,
                    update: a
                }), "object" != typeof a && (a = {}), a = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], a),
                this.bind("initPanels", function(t) {
                    this.__refactorClass(e("em", t), this.conf.classNames[i].counter, "counter");
                }), a.add && this.bind("initPanels", function(t) {
                    var i;
                    switch (a.addTo) {
                      case "panels":
                        i = t;
                        break;

                      default:
                        i = t.filter(a.addTo);
                    }
                    i.each(function() {
                        var t = e(this).data(s.parent);
                        t && (t.children("em." + n.counter).length || t.prepend(e('<em class="' + n.counter + '" />')));
                    });
                }), a.update && this.bind("update", function() {
                    this.$pnls.children("." + n.panel).each(function() {
                        var t = e(this), i = t.data(s.parent);
                        if (i) {
                            var r = i.children("em." + n.counter);
                            r.length && (t = t.children("." + n.listview), t.length && r.html(o.__filterListItems(t.children()).length));
                        }
                    });
                });
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("counter search noresultsmsg");
            },
            clickAnchor: function(e, t) {}
        }, e[t].defaults[i] = {
            add: !1,
            addTo: "panels",
            update: !1
        }, e[t].configuration.classNames[i] = {
            counter: "Counter"
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "dividers";
        e[t].addons[i] = {
            setup: function() {
                var s = this, a = this.opts[i];
                if (this.conf[i], r = e[t].glbl, "boolean" == typeof a && (a = {
                    add: a,
                    fixed: a
                }), "object" != typeof a && (a = {}), a = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], a),
                this.bind("initPanels", function(t) {
                    this.__refactorClass(e("li", this.$menu), this.conf.classNames[i].collapsed, "collapsed");
                }), a.add && this.bind("initPanels", function(t) {
                    var i;
                    switch (a.addTo) {
                      case "panels":
                        i = t;
                        break;

                      default:
                        i = t.filter(a.addTo);
                    }
                    e("." + n.divider, i).remove(), i.find("." + n.listview).not("." + n.vertical).each(function() {
                        var t = "";
                        s.__filterListItems(e(this).children()).each(function() {
                            var i = e.trim(e(this).children("a, span").text()).slice(0, 1).toLowerCase();
                            i != t && i.length && (t = i, e('<li class="' + n.divider + '">' + i + "</li>").insertBefore(this));
                        });
                    });
                }), a.collapse && this.bind("initPanels", function(t) {
                    e("." + n.divider, t).each(function() {
                        var t = e(this), i = t.nextUntil("." + n.divider, "." + n.collapsed);
                        i.length && (t.children("." + n.subopen).length || (t.wrapInner("<span />"), t.prepend('<a href="#" class="' + n.subopen + " " + n.fullsubopen + '" />')));
                    });
                }), a.fixed) {
                    var l = function(t) {
                        t = t || this.$pnls.children("." + n.current);
                        var i = t.find("." + n.divider).not("." + n.hidden);
                        if (i.length) {
                            this.$menu.addClass(n.hasdividers);
                            var s = t.scrollTop() || 0, o = "";
                            t.is(":visible") && t.find("." + n.divider).not("." + n.hidden).each(function() {
                                e(this).position().top + s < s + 1 && (o = e(this).text());
                            }), this.$fixeddivider.text(o);
                        } else this.$menu.removeClass(n.hasdividers);
                    };
                    this.$fixeddivider = e('<ul class="' + n.listview + " " + n.fixeddivider + '"><li class="' + n.divider + '"></li></ul>').prependTo(this.$pnls).children(),
                    this.bind("openPanel", l), this.bind("update", l), this.bind("initPanels", function(t) {
                        t.off(o.scroll + "-dividers " + o.touchmove + "-dividers").on(o.scroll + "-dividers " + o.touchmove + "-dividers", function(t) {
                            l.call(s, e(this));
                        });
                    });
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("collapsed uncollapsed fixeddivider hasdividers"),
                o.add("scroll");
            },
            clickAnchor: function(e, t) {
                if (this.opts[i].collapse && t) {
                    var s = e.parent();
                    if (s.is("." + n.divider)) {
                        var o = s.nextUntil("." + n.divider, "." + n.collapsed);
                        return s.toggleClass(n.opened), o[s.hasClass(n.opened) ? "addClass" : "removeClass"](n.uncollapsed),
                        !0;
                    }
                }
                return !1;
            }
        }, e[t].defaults[i] = {
            add: !1,
            addTo: "panels",
            fixed: !1,
            collapse: !1
        }, e[t].configuration.classNames[i] = {
            collapsed: "Collapsed"
        };
        var n, s, o, r;
    }(e), function(e) {
        function t(e, t, i) {
            return t > e && (e = t), e > i && (e = i), e;
        }
        function i(i, n, s) {
            var a, l, d, c, u, p = this, h = {}, f = 0, v = !1, g = !1, m = 0, y = 0;
            switch (this.opts.offCanvas.position) {
              case "left":
              case "right":
                h.events = "panleft panright", h.typeLower = "x", h.typeUpper = "X", g = "width";
                break;

              case "top":
              case "bottom":
                h.events = "panup pandown", h.typeLower = "y", h.typeUpper = "Y", g = "height";
            }
            switch (this.opts.offCanvas.position) {
              case "right":
              case "bottom":
                h.negative = !0, c = function(e) {
                    e >= s.$wndw[g]() - i.maxStartPos && (f = 1);
                };
                break;

              default:
                h.negative = !1, c = function(e) {
                    e <= i.maxStartPos && (f = 1);
                };
            }
            switch (this.opts.offCanvas.position) {
              case "left":
                h.open_dir = "right", h.close_dir = "left";
                break;

              case "right":
                h.open_dir = "left", h.close_dir = "right";
                break;

              case "top":
                h.open_dir = "down", h.close_dir = "up";
                break;

              case "bottom":
                h.open_dir = "up", h.close_dir = "down";
            }
            switch (this.opts.offCanvas.zposition) {
              case "front":
                u = function() {
                    return this.$menu;
                };
                break;

              default:
                u = function() {
                    return e("." + r.slideout);
                };
            }
            var b = this.__valueOrFn(i.node, this.$menu, s.$page);
            "string" == typeof b && (b = e(b));
            var w = new Hammer(b[0], this.opts[o].vendors.hammer);
            w.on("panstart", function(e) {
                c(e.center[h.typeLower]), s.$slideOutNodes = u(), v = h.open_dir;
            }).on(h.events + " panend", function(e) {
                f > 0 && e.preventDefault();
            }).on(h.events, function(e) {
                if (a = e["delta" + h.typeUpper], h.negative && (a = -a), a != m && (v = a >= m ? h.open_dir : h.close_dir),
                m = a, m > i.threshold && 1 == f) {
                    if (s.$html.hasClass(r.opened)) return;
                    f = 2, p._openSetup(), p.trigger("opening"), s.$html.addClass(r.dragging), y = t(s.$wndw[g]() * n[g].perc, n[g].min, n[g].max);
                }
                2 == f && (l = t(m, 10, y) - ("front" == p.opts.offCanvas.zposition ? y : 0), h.negative && (l = -l),
                d = "translate" + h.typeUpper + "(" + l + "px )", s.$slideOutNodes.css({
                    "-webkit-transform": "-webkit-" + d,
                    transform: d
                }));
            }).on("panend", function(e) {
                2 == f && (s.$html.removeClass(r.dragging), s.$slideOutNodes.css("transform", ""),
                p[v == h.open_dir ? "_openFinish" : "close"]()), f = 0;
            });
        }
        function n(t, i, n, s) {
            var l = this;
            t.each(function() {
                var t = e(this), i = t.data(a.parent);
                if (i && (i = i.closest("." + r.panel), i.length)) {
                    var n = new Hammer(t[0], l.opts[o].vendors.hammer);
                    n.on("panright", function(e) {
                        l.openPanel(i);
                    });
                }
            });
        }
        var s = "mmenu", o = "drag";
        e[s].addons[o] = {
            setup: function() {
                if (this.opts.offCanvas) {
                    var t = this.opts[o], r = this.conf[o];
                    d = e[s].glbl, "boolean" == typeof t && (t = {
                        menu: t,
                        panels: t
                    }), "object" != typeof t && (t = {}), "boolean" == typeof t.menu && (t.menu = {
                        open: t.menu
                    }), "object" != typeof t.menu && (t.menu = {}), "boolean" == typeof t.panels && (t.panels = {
                        close: t.panels
                    }), "object" != typeof t.panels && (t.panels = {}), t = this.opts[o] = e.extend(!0, {}, e[s].defaults[o], t),
                    t.menu.open && i.call(this, t.menu, r.menu, d), t.panels.close && this.bind("initPanels", function(e) {
                        n.call(this, e, t.panels, r.panels, d);
                    });
                }
            },
            add: function() {
                return "function" != typeof Hammer || Hammer.VERSION < 2 ? void (e[s].addons[o].setup = function() {}) : (r = e[s]._c,
                a = e[s]._d, l = e[s]._e, void r.add("dragging"));
            },
            clickAnchor: function(e, t) {}
        }, e[s].defaults[o] = {
            menu: {
                open: !1,
                maxStartPos: 100,
                threshold: 50
            },
            panels: {
                close: !1
            },
            vendors: {
                hammer: {}
            }
        }, e[s].configuration[o] = {
            menu: {
                width: {
                    perc: .8,
                    min: 140,
                    max: 440
                },
                height: {
                    perc: .8,
                    min: 140,
                    max: 880
                }
            },
            panels: {}
        };
        var r, a, l, d;
    }(e), function(e) {
        var t = "mmenu", i = "dropdown";
        e[t].addons[i] = {
            setup: function() {
                if (this.opts.offCanvas) {
                    var a = this, l = this.opts[i], d = this.conf[i];
                    if (r = e[t].glbl, "boolean" == typeof l && l && (l = {
                        drop: l
                    }), "object" != typeof l && (l = {}), "string" == typeof l.position && (l.position = {
                        of: l.position
                    }), l = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], l), l.drop) {
                        if ("string" != typeof l.position.of) {
                            var c = this.$menu.attr("id");
                            c && c.length && (this.conf.clone && (c = n.umm(c)), l.position.of = '[href="#' + c + '"]');
                        }
                        if ("string" == typeof l.position.of) {
                            var u = e(l.position.of);
                            if (u.length) {
                                this.$menu.addClass(n.dropdown), l.tip && this.$menu.addClass(n.tip), l.event = l.event.split(" "),
                                1 == l.event.length && (l.event[1] = l.event[0]), "hover" == l.event[0] && u.on(o.mouseenter + "-dropdown", function() {
                                    a.open();
                                }), "hover" == l.event[1] && this.$menu.on(o.mouseleave + "-dropdown", function() {
                                    a.close();
                                }), this.bind("opening", function() {
                                    this.$menu.data(s.style, this.$menu.attr("style") || ""), r.$html.addClass(n.dropdown);
                                }), this.bind("closed", function() {
                                    this.$menu.attr("style", this.$menu.data(s.style)), r.$html.removeClass(n.dropdown);
                                });
                                var p = function(s, o) {
                                    var a = o[0], c = o[1], p = "x" == s ? "scrollLeft" : "scrollTop", h = "x" == s ? "outerWidth" : "outerHeight", f = "x" == s ? "left" : "top", v = "x" == s ? "right" : "bottom", g = "x" == s ? "width" : "height", m = "x" == s ? "maxWidth" : "maxHeight", y = null, b = r.$wndw[p](), w = u.offset()[f] -= b, $ = w + u[h](), _ = r.$wndw[g](), k = d.offset.button[s] + d.offset.viewport[s];
                                    if (l.position[s]) switch (l.position[s]) {
                                      case "left":
                                      case "bottom":
                                        y = "after";
                                        break;

                                      case "right":
                                      case "top":
                                        y = "before";
                                    }
                                    null === y && (y = _ / 2 > w + ($ - w) / 2 ? "after" : "before");
                                    var C, x;
                                    return "after" == y ? (C = "x" == s ? w : $, x = _ - (C + k), a[f] = C + d.offset.button[s],
                                    a[v] = "auto", c.push(n["x" == s ? "tipleft" : "tiptop"])) : (C = "x" == s ? $ : w,
                                    x = C - k, a[v] = "calc( 100% - " + (C - d.offset.button[s]) + "px )", a[f] = "auto",
                                    c.push(n["x" == s ? "tipright" : "tipbottom"])), a[m] = Math.min(e[t].configuration[i][g].max, x),
                                    [ a, c ];
                                }, h = function(e) {
                                    if (this.vars.opened) {
                                        this.$menu.attr("style", this.$menu.data(s.style));
                                        var t = [ {}, [] ];
                                        t = p.call(this, "y", t), t = p.call(this, "x", t), this.$menu.css(t[0]), l.tip && this.$menu.removeClass(n.tipleft + " " + n.tipright + " " + n.tiptop + " " + n.tipbottom).addClass(t[1].join(" "));
                                    }
                                };
                                this.bind("opening", h), r.$wndw.on(o.resize + "-dropdown", function(e) {
                                    h.call(a);
                                }), this.opts.offCanvas.blockUI || r.$wndw.on(o.scroll + "-dropdown", function(e) {
                                    h.call(a);
                                });
                            }
                        }
                    }
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("dropdown tip tipleft tipright tiptop tipbottom"),
                o.add("mouseenter mouseleave resize scroll");
            },
            clickAnchor: function(e, t) {}
        }, e[t].defaults[i] = {
            drop: !1,
            event: "click",
            position: {},
            tip: !0
        }, e[t].configuration[i] = {
            offset: {
                button: {
                    x: -10,
                    y: 10
                },
                viewport: {
                    x: 20,
                    y: 20
                }
            },
            height: {
                max: 880
            },
            width: {
                max: 440
            }
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "fixedElements";
        e[t].addons[i] = {
            setup: function() {
                if (this.opts.offCanvas) {
                    var n = this.opts[i];
                    this.conf[i], r = e[t].glbl, n = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], n);
                    var s = function(e) {
                        var t = this.conf.classNames[i].fixed;
                        this.__refactorClass(e.find("." + t), t, "slideout").appendTo(r.$body);
                    };
                    s.call(this, r.$page), this.bind("setPage", s);
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("fixed");
            },
            clickAnchor: function(e, t) {}
        }, e[t].configuration.classNames[i] = {
            fixed: "Fixed"
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "iconPanels";
        e[t].addons[i] = {
            setup: function() {
                var s = this, o = this.opts[i];
                if (this.conf[i], r = e[t].glbl, "boolean" == typeof o && (o = {
                    add: o
                }), "number" == typeof o && (o = {
                    add: !0,
                    visible: o
                }), "object" != typeof o && (o = {}), o = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], o),
                o.visible++, o.add) {
                    this.$menu.addClass(n.iconpanel);
                    for (var a = [], l = 0; l <= o.visible; l++) a.push(n.iconpanel + "-" + l);
                    a = a.join(" ");
                    var d = function(t) {
                        t.hasClass(n.vertical) || s.$pnls.children("." + n.panel).removeClass(a).filter("." + n.subopened).removeClass(n.hidden).add(t).not("." + n.vertical).slice(-o.visible).each(function(t) {
                            e(this).addClass(n.iconpanel + "-" + t);
                        });
                    };
                    this.bind("openPanel", d), this.bind("initPanels", function(t) {
                        d.call(s, s.$pnls.children("." + n.current)), t.not("." + n.vertical).each(function() {
                            e(this).children("." + n.subblocker).length || e(this).prepend('<a href="#' + e(this).closest("." + n.panel).attr("id") + '" class="' + n.subblocker + '" />');
                        });
                    });
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("iconpanel subblocker");
            },
            clickAnchor: function(e, t) {}
        }, e[t].defaults[i] = {
            add: !1,
            visible: 3
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "navbars";
        e[t].addons[i] = {
            setup: function() {
                var s = this, o = this.opts[i], a = this.conf[i];
                if (r = e[t].glbl, void 0 !== o) {
                    o instanceof Array || (o = [ o ]);
                    var l = {};
                    e.each(o, function(r) {
                        var d = o[r];
                        "boolean" == typeof d && d && (d = {}), "object" != typeof d && (d = {}), void 0 === d.content && (d.content = [ "prev", "title" ]),
                        d.content instanceof Array || (d.content = [ d.content ]), d = e.extend(!0, {}, s.opts.navbar, d);
                        var c = d.position, u = d.height;
                        "number" != typeof u && (u = 1), u = Math.min(4, Math.max(1, u)), "bottom" != c && (c = "top"),
                        l[c] || (l[c] = 0), l[c]++;
                        var p = e("<div />").addClass(n.navbar + " " + n.navbar + "-" + c + " " + n.navbar + "-" + c + "-" + l[c] + " " + n.navbar + "-size-" + u);
                        l[c] += u - 1;
                        for (var h = 0, f = 0, v = d.content.length; v > f; f++) {
                            var g = e[t].addons[i][d.content[f]] || !1;
                            g ? h += g.call(s, p, d, a) : (g = d.content[f], g instanceof e || (g = e(d.content[f])),
                            p.append(g));
                        }
                        h += Math.ceil(p.children().not("." + n.btn).length / u), h > 1 && p.addClass(n.navbar + "-content-" + h),
                        p.children("." + n.btn).length && p.addClass(n.hasbtns), p.prependTo(s.$menu);
                    });
                    for (var d in l) s.$menu.addClass(n.hasnavbar + "-" + d + "-" + l[d]);
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("close hasbtns");
            },
            clickAnchor: function(e, t) {}
        }, e[t].configuration[i] = {
            breadcrumbSeparator: "/"
        }, e[t].configuration.classNames[i] = {};
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "navbars", n = "breadcrumbs";
        e[t].addons[i][n] = function(i, n, s) {
            var o = e[t]._c, r = e[t]._d;
            o.add("breadcrumbs separator");
            var a = e('<span class="' + o.breadcrumbs + '" />').appendTo(i);
            this.bind("initPanels", function(t) {
                t.removeClass(o.hasnavbar).each(function() {
                    for (var t = [], i = e(this), n = e('<span class="' + o.breadcrumbs + '"></span>'), a = e(this).children().first(), l = !0; a && a.length; ) {
                        a.is("." + o.panel) || (a = a.closest("." + o.panel));
                        var d = a.children("." + o.navbar).children("." + o.title).text();
                        t.unshift(l ? "<span>" + d + "</span>" : '<a href="#' + a.attr("id") + '">' + d + "</a>"),
                        l = !1, a = a.data(r.parent);
                    }
                    n.append(t.join('<span class="' + o.separator + '">' + s.breadcrumbSeparator + "</span>")).appendTo(i.children("." + o.navbar));
                });
            });
            var l = function() {
                a.html(this.$pnls.children("." + o.current).children("." + o.navbar).children("." + o.breadcrumbs).html());
            };
            return this.bind("openPanel", l), this.bind("initPanels", l), 0;
        };
    }(e), function(e) {
        var t = "mmenu", i = "navbars", n = "close";
        e[t].addons[i][n] = function(i, n) {
            var s = e[t]._c, o = e[t].glbl, r = e('<a class="' + s.close + " " + s.btn + '" href="#" />').appendTo(i), a = function(e) {
                r.attr("href", "#" + e.attr("id"));
            };
            return a.call(this, o.$page), this.bind("setPage", a), -1;
        };
    }(e), function(e) {
        var t = "mmenu", i = "navbars", n = "next";
        e[t].addons[i][n] = function(n, s) {
            var o, r, a = e[t]._c, l = e('<a class="' + a.next + " " + a.btn + '" href="#" />').appendTo(n), d = function(e) {
                e = e || this.$pnls.children("." + a.current);
                var t = e.find("." + this.conf.classNames[i].panelNext);
                o = t.attr("href"), r = t.html(), l[o ? "attr" : "removeAttr"]("href", o), l[o || r ? "removeClass" : "addClass"](a.hidden),
                l.html(r);
            };
            return this.bind("openPanel", d), this.bind("initPanels", function() {
                d.call(this);
            }), -1;
        }, e[t].configuration.classNames[i].panelNext = "Next";
    }(e), function(e) {
        var t = "mmenu", i = "navbars", n = "prev";
        e[t].addons[i][n] = function(n, s) {
            var o = e[t]._c, r = e('<a class="' + o.prev + " " + o.btn + '" href="#" />').appendTo(n);
            this.bind("initPanels", function(e) {
                e.removeClass(o.hasnavbar).children("." + o.navbar).addClass(o.hidden);
            });
            var a, l, d = function(e) {
                if (e = e || this.$pnls.children("." + o.current), !e.hasClass(o.vertical)) {
                    var t = e.find("." + this.conf.classNames[i].panelPrev);
                    t.length || (t = e.children("." + o.navbar).children("." + o.prev)), a = t.attr("href"),
                    l = t.html(), r[a ? "attr" : "removeAttr"]("href", a), r[a || l ? "removeClass" : "addClass"](o.hidden),
                    r.html(l);
                }
            };
            return this.bind("openPanel", d), this.bind("initPanels", function() {
                d.call(this);
            }), -1;
        }, e[t].configuration.classNames[i].panelPrev = "Prev";
    }(e), function(e) {
        var t = "mmenu", i = "navbars", n = "searchfield";
        e[t].addons[i][n] = function(i, n) {
            var s = e[t]._c, o = e('<div class="' + s.search + '" />').appendTo(i);
            return "object" != typeof this.opts.searchfield && (this.opts.searchfield = {}),
            this.opts.searchfield.add = !0, this.opts.searchfield.addTo = o, 0;
        };
    }(e), function(e) {
        var t = "mmenu", i = "navbars", n = "title";
        e[t].addons[i][n] = function(n, s) {
            var o, r, a = e[t]._c, l = e('<a class="' + a.title + '" />').appendTo(n), d = function(e) {
                if (e = e || this.$pnls.children("." + a.current), !e.hasClass(a.vertical)) {
                    var t = e.find("." + this.conf.classNames[i].panelTitle);
                    t.length || (t = e.children("." + a.navbar).children("." + a.title)), o = t.attr("href"),
                    r = t.html() || s.title, l[o ? "attr" : "removeAttr"]("href", o), l[o || r ? "removeClass" : "addClass"](a.hidden),
                    l.html(r);
                }
            };
            return this.bind("openPanel", d), this.bind("initPanels", function(e) {
                d.call(this);
            }), 0;
        }, e[t].configuration.classNames[i].panelTitle = "Title";
    }(e), function(e) {
        var t = "mmenu", i = "rtl";
        e[t].addons[i] = {
            setup: function() {
                var s = this.opts[i];
                this.conf[i], r = e[t].glbl, "object" != typeof s && (s = {
                    use: s
                }), s = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], s), "boolean" != typeof s.use && (s.use = "rtl" == (r.$html.attr("dir") || "").toLowerCase()),
                s.use && this.$menu.addClass(n.rtl);
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("rtl");
            },
            clickAnchor: function(e, t) {}
        }, e[t].defaults[i] = {
            use: "detect"
        };
        var n, s, o, r;
    }(e), function(e) {
        function t(e, t, i) {
            e.prop("aria-" + t, i)[i ? "attr" : "removeAttr"]("aria-" + t, "true");
        }
        function i(e) {
            return '<span class="' + o.sronly + '">' + e + "</span>";
        }
        var n = "mmenu", s = "screenReader";
        e[n].addons[s] = {
            setup: function() {
                var r = this.opts[s], a = this.conf[s];
                if (l = e[n].glbl, "boolean" == typeof r && (r = {
                    aria: r,
                    text: r
                }), "object" != typeof r && (r = {}), r = this.opts[s] = e.extend(!0, {}, e[n].defaults[s], r),
                r.aria) {
                    if (this.opts.offCanvas) {
                        var d = function() {
                            t(this.$menu, "hidden", !1);
                        }, c = function() {
                            t(this.$menu, "hidden", !0);
                        };
                        this.bind("open", d), this.bind("close", c), c.call(this);
                    }
                    var u = function() {
                        t(this.$menu.find("." + o.hidden), "hidden", !0), t(this.$menu.find('[aria-hidden="true"]').not("." + o.hidden), "hidden", !1);
                    }, p = function(e) {
                        t(this.$pnls.children("." + o.panel).not(e).not("." + o.hidden), "hidden", !0),
                        t(e, "hidden", !1);
                    };
                    this.bind("update", u), this.bind("openPanel", u), this.bind("openPanel", p);
                    var h = function(e) {
                        t(e.find("." + o.prev + ", ." + o.next), "haspopup", !0);
                    };
                    this.bind("initPanels", h), h.call(this, this.$menu.children("." + o.navbar));
                }
                if (r.text) {
                    var f = function(t) {
                        t.children("." + o.navbar).children("." + o.prev).html(i(a.text.closeSubmenu)).end().children("." + o.next).html(i(a.text.openSubmenu)).end().children("." + o.close).html(i(a.text.closeMenu)),
                        t.is("." + o.panel) && t.find("." + o.listview).find("." + o.next).each(function() {
                            e(this).html(i(a.text[e(this).parent().is("." + o.vertical) ? "toggleSubmenu" : "openSubmenu"]));
                        });
                    };
                    this.bind("initPanels", f), f.call(this, this.$menu);
                }
            },
            add: function() {
                o = e[n]._c, r = e[n]._d, a = e[n]._e, o.add("sronly");
            },
            clickAnchor: function(e, t) {}
        }, e[n].defaults[s] = {
            aria: !1,
            text: !1
        }, e[n].configuration[s] = {
            text: {
                closeMenu: "Close menu",
                closeSubmenu: "Close submenu",
                openSubmenu: "Open submenu",
                toggleSubmenu: "Toggle submenu"
            }
        };
        var o, r, a, l;
    }(e), function(e) {
        function t(e) {
            switch (e) {
              case 9:
              case 16:
              case 17:
              case 18:
              case 37:
              case 38:
              case 39:
              case 40:
                return !0;
            }
            return !1;
        }
        var i = "mmenu", n = "searchfield";
        e[i].addons[n] = {
            setup: function() {
                var l = this, d = this.opts[n], c = this.conf[n];
                a = e[i].glbl, "boolean" == typeof d && (d = {
                    add: d
                }), "object" != typeof d && (d = {}), "boolean" == typeof d.resultsPanel && (d.resultsPanel = {
                    add: d.resultsPanel
                }), d = this.opts[n] = e.extend(!0, {}, e[i].defaults[n], d), c = this.conf[n] = e.extend(!0, {}, e[i].configuration[n], c),
                this.bind("close", function() {
                    this.$menu.find("." + s.search).find("input").blur();
                }), this.bind("initPanels", function(i) {
                    if (d.add) {
                        var a;
                        switch (d.addTo) {
                          case "panels":
                            a = i;
                            break;

                          default:
                            a = this.$menu.find(d.addTo);
                        }
                        if (a.each(function() {
                            var t = e(this);
                            if (!t.is("." + s.panel) || !t.is("." + s.vertical)) {
                                if (!t.children("." + s.search).length) {
                                    var i = l.__valueOrFn(c.clear, t), n = l.__valueOrFn(c.form, t), o = l.__valueOrFn(c.input, t), a = l.__valueOrFn(c.submit, t), u = e("<" + (n ? "form" : "div") + ' class="' + s.search + '" />'), p = e('<input placeholder="' + d.placeholder + '" type="text" autocomplete="off" />');
                                    u.append(p);
                                    var h;
                                    if (o) for (h in o) p.attr(h, o[h]);
                                    if (i && e('<a class="' + s.btn + " " + s.clear + '" href="#" />').appendTo(u).on(r.click + "-searchfield", function(e) {
                                        e.preventDefault(), p.val("").trigger(r.keyup + "-searchfield");
                                    }), n) {
                                        for (h in n) u.attr(h, n[h]);
                                        a && !i && e('<a class="' + s.btn + " " + s.next + '" href="#" />').appendTo(u).on(r.click + "-searchfield", function(e) {
                                            e.preventDefault(), u.submit();
                                        });
                                    }
                                    t.hasClass(s.search) ? t.replaceWith(u) : t.prepend(u).addClass(s.hassearch);
                                }
                                if (d.noResults) {
                                    var f = t.closest("." + s.panel).length;
                                    if (f || (t = l.$pnls.children("." + s.panel).first()), !t.children("." + s.noresultsmsg).length) {
                                        var v = t.children("." + s.listview).first();
                                        e('<div class="' + s.noresultsmsg + " " + s.hidden + '" />').append(d.noResults)[v.length ? "insertAfter" : "prependTo"](v.length ? v : t);
                                    }
                                }
                            }
                        }), d.search) {
                            if (d.resultsPanel.add) {
                                d.showSubPanels = !1;
                                var u = this.$pnls.children("." + s.resultspanel);
                                u.length || (u = e('<div class="' + s.panel + " " + s.resultspanel + " " + s.hidden + '" />').appendTo(this.$pnls).append('<div class="' + s.navbar + " " + s.hidden + '"><a class="' + s.title + '">' + d.resultsPanel.title + "</a></div>").append('<ul class="' + s.listview + '" />').append(this.$pnls.find("." + s.noresultsmsg).first().clone()),
                                this.initPanels(u));
                            }
                            this.$menu.find("." + s.search).each(function() {
                                var i, a, c = e(this), p = c.closest("." + s.panel).length;
                                p ? (i = c.closest("." + s.panel), a = i) : (i = e("." + s.panel, l.$menu), a = l.$menu),
                                d.resultsPanel.add && (i = i.not(u));
                                var h = c.children("input"), f = l.__findAddBack(i, "." + s.listview).children("li"), v = f.filter("." + s.divider), g = l.__filterListItems(f), m = "a", y = m + ", span", b = "", w = function() {
                                    var t = h.val().toLowerCase();
                                    if (t != b) {
                                        if (b = t, d.resultsPanel.add && u.children("." + s.listview).empty(), i.scrollTop(0),
                                        g.add(v).addClass(s.hidden).find("." + s.fullsubopensearch).removeClass(s.fullsubopen + " " + s.fullsubopensearch),
                                        g.each(function() {
                                            var t = e(this), i = m;
                                            (d.showTextItems || d.showSubPanels && t.find("." + s.next)) && (i = y);
                                            var n = t.data(o.searchtext) || t.children(i).text();
                                            n.toLowerCase().indexOf(b) > -1 && t.add(t.prevAll("." + s.divider).first()).removeClass(s.hidden);
                                        }), d.showSubPanels && i.each(function(t) {
                                            var i = e(this);
                                            l.__filterListItems(i.find("." + s.listview).children()).each(function() {
                                                var t = e(this), i = t.data(o.child);
                                                t.removeClass(s.nosubresults), i && i.find("." + s.listview).children().removeClass(s.hidden);
                                            });
                                        }), d.resultsPanel.add) if ("" === b) this.closeAllPanels(), this.openPanel(this.$pnls.children("." + s.subopened).last()); else {
                                            var n = e();
                                            i.each(function() {
                                                var t = l.__filterListItems(e(this).find("." + s.listview).children()).not("." + s.hidden).clone(!0);
                                                t.length && (d.resultsPanel.dividers && (n = n.add('<li class="' + s.divider + '">' + e(this).children("." + s.navbar).text() + "</li>")),
                                                n = n.add(t));
                                            }), n.find("." + s.next).remove(), u.children("." + s.listview).append(n), this.openPanel(u);
                                        } else e(i.get().reverse()).each(function(t) {
                                            var i = e(this), n = i.data(o.parent);
                                            n && (l.__filterListItems(i.find("." + s.listview).children()).length ? (n.hasClass(s.hidden) && n.children("." + s.next).not("." + s.fullsubopen).addClass(s.fullsubopen).addClass(s.fullsubopensearch),
                                            n.removeClass(s.hidden).removeClass(s.nosubresults).prevAll("." + s.divider).first().removeClass(s.hidden)) : p || (i.hasClass(s.opened) && setTimeout(function() {
                                                l.openPanel(n.closest("." + s.panel));
                                            }, (t + 1) * (1.5 * l.conf.openingInterval)), n.addClass(s.nosubresults)));
                                        });
                                        a.find("." + s.noresultsmsg)[g.not("." + s.hidden).length ? "addClass" : "removeClass"](s.hidden),
                                        this.update();
                                    }
                                };
                                h.off(r.keyup + "-" + n + " " + r.change + "-" + n).on(r.keyup + "-" + n, function(e) {
                                    t(e.keyCode) || w.call(l);
                                }).on(r.change + "-" + n, function(e) {
                                    w.call(l);
                                });
                                var $ = c.children("." + s.btn);
                                $.length && h.on(r.keyup + "-" + n, function(e) {
                                    $[h.val().length ? "removeClass" : "addClass"](s.hidden);
                                }), h.trigger(r.keyup + "-" + n);
                            });
                        }
                    }
                });
            },
            add: function() {
                s = e[i]._c, o = e[i]._d, r = e[i]._e, s.add("clear search hassearch resultspanel noresultsmsg noresults nosubresults fullsubopensearch"),
                o.add("searchtext"), r.add("change keyup");
            },
            clickAnchor: function(e, t) {}
        }, e[i].defaults[n] = {
            add: !1,
            addTo: "panels",
            placeholder: "Search",
            noResults: "No results found.",
            resultsPanel: {
                add: !1,
                dividers: !0,
                title: "Search results"
            },
            search: !0,
            showTextItems: !1,
            showSubPanels: !0
        }, e[i].configuration[n] = {
            clear: !1,
            form: !1,
            input: !1,
            submit: !1
        };
        var s, o, r, a;
    }(e), function(e) {
        var t = "mmenu", i = "sectionIndexer";
        e[t].addons[i] = {
            setup: function() {
                var s = this, a = this.opts[i];
                this.conf[i], r = e[t].glbl, "boolean" == typeof a && (a = {
                    add: a
                }), "object" != typeof a && (a = {}), a = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], a),
                this.bind("initPanels", function(t) {
                    if (a.add) {
                        var i;
                        switch (a.addTo) {
                          case "panels":
                            i = t;
                            break;

                          default:
                            i = e(a.addTo, this.$menu).filter("." + n.panel);
                        }
                        i.find("." + n.divider).closest("." + n.panel).addClass(n.hasindexer);
                    }
                    if (!this.$indexer && this.$pnls.children("." + n.hasindexer).length) {
                        this.$indexer = e('<div class="' + n.indexer + '" />').prependTo(this.$pnls).append('<a href="#a">a</a><a href="#b">b</a><a href="#c">c</a><a href="#d">d</a><a href="#e">e</a><a href="#f">f</a><a href="#g">g</a><a href="#h">h</a><a href="#i">i</a><a href="#j">j</a><a href="#k">k</a><a href="#l">l</a><a href="#m">m</a><a href="#n">n</a><a href="#o">o</a><a href="#p">p</a><a href="#q">q</a><a href="#r">r</a><a href="#s">s</a><a href="#t">t</a><a href="#u">u</a><a href="#v">v</a><a href="#w">w</a><a href="#x">x</a><a href="#y">y</a><a href="#z">z</a>'),
                        this.$indexer.children().on(o.mouseover + "-sectionindexer " + n.touchstart + "-sectionindexer", function(t) {
                            var i = e(this).attr("href").slice(1), o = s.$pnls.children("." + n.current), r = o.find("." + n.listview), a = !1, l = o.scrollTop();
                            o.scrollTop(0), r.children("." + n.divider).not("." + n.hidden).each(function() {
                                a === !1 && i == e(this).text().slice(0, 1).toLowerCase() && (a = e(this).position().top);
                            }), o.scrollTop(a !== !1 ? a : l);
                        });
                        var r = function(e) {
                            s.$menu[(e.hasClass(n.hasindexer) ? "add" : "remove") + "Class"](n.hasindexer);
                        };
                        this.bind("openPanel", r), r.call(this, this.$pnls.children("." + n.current));
                    }
                });
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("indexer hasindexer"), o.add("mouseover touchstart");
            },
            clickAnchor: function(e, t) {
                return !!e.parent().is("." + n.indexer) || void 0;
            }
        }, e[t].defaults[i] = {
            add: !1,
            addTo: "panels"
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "setSelected";
        e[t].addons[i] = {
            setup: function() {
                var o = this, a = this.opts[i];
                if (this.conf[i], r = e[t].glbl, "boolean" == typeof a && (a = {
                    hover: a,
                    parent: a
                }), "object" != typeof a && (a = {}), a = this.opts[i] = e.extend(!0, {}, e[t].defaults[i], a),
                "detect" == a.current) {
                    var l = function(e) {
                        e = e.split("?")[0].split("#")[0];
                        var t = o.$menu.find('a[href="' + e + '"], a[href="' + e + '/"]');
                        t.length ? o.setSelected(t.parent(), !0) : (e = e.split("/").slice(0, -1), e.length && l(e.join("/")));
                    };
                    l(window.location.href);
                } else a.current || this.bind("initPanels", function(e) {
                    e.find("." + n.listview).children("." + n.selected).removeClass(n.selected);
                });
                if (a.hover && this.$menu.addClass(n.hoverselected), a.parent) {
                    this.$menu.addClass(n.parentselected);
                    var d = function(e) {
                        this.$pnls.find("." + n.listview).find("." + n.next).removeClass(n.selected);
                        for (var t = e.data(s.parent); t && t.length; ) t = t.children("." + n.next).addClass(n.selected).closest("." + n.panel).data(s.parent);
                    };
                    this.bind("openedPanel", d), this.bind("initPanels", function(e) {
                        d.call(this, this.$pnls.children("." + n.current));
                    });
                }
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("hoverselected parentselected");
            },
            clickAnchor: function(e, t) {}
        }, e[t].defaults[i] = {
            current: !0,
            hover: !1,
            parent: !1
        };
        var n, s, o, r;
    }(e), function(e) {
        var t = "mmenu", i = "toggles";
        e[t].addons[i] = {
            setup: function() {
                var s = this;
                this.opts[i], this.conf[i], r = e[t].glbl, this.bind("initPanels", function(t) {
                    this.__refactorClass(e("input", t), this.conf.classNames[i].toggle, "toggle"), this.__refactorClass(e("input", t), this.conf.classNames[i].check, "check"),
                    e("input." + n.toggle + ", input." + n.check, t).each(function() {
                        var t = e(this), i = t.closest("li"), o = t.hasClass(n.toggle) ? "toggle" : "check", r = t.attr("id") || s.__getUniqueId();
                        i.children('label[for="' + r + '"]').length || (t.attr("id", r), i.prepend(t), e('<label for="' + r + '" class="' + n[o] + '"></label>').insertBefore(i.children("a, span").last()));
                    });
                });
            },
            add: function() {
                n = e[t]._c, s = e[t]._d, o = e[t]._e, n.add("toggle check");
            },
            clickAnchor: function(e, t) {}
        }, e[t].configuration.classNames[i] = {
            toggle: "Toggle",
            check: "Check"
        };
        var n, s, o, r;
    }(e), !0;
}), !function(e) {
    e.fn.equalHeights = function() {
        var t = 0, i = e(this);
        return i.each(function() {
            var i = e(this).innerHeight();
            i > t && (t = i);
        }), i.css("height", t);
    }, e("[data-equal]").each(function() {
        var t = e(this), i = t.data("equal");
        t.find(i).equalHeights();
    });
}(jQuery), function() {
    function e(t, i, n) {
        var s = e.resolve(t);
        if (null == s) {
            n = n || t, i = i || "root";
            var o = Error('Failed to require "' + n + '" from "' + i + '"');
            throw o.path = n, o.parent = i, o.require = !0, o;
        }
        var r = e.modules[s];
        if (!r._resolving && !r.exports) {
            var a = {};
            a.exports = {}, a.client = a.component = !0, r._resolving = !0, r.call(this, a.exports, e.relative(s), a),
            delete r._resolving, r.exports = a.exports;
        }
        return r.exports;
    }
    function t(e, t, i) {
        return i = i || "0", e += "", e.length >= t ? e : Array(t - e.length + 1).join(i) + e;
    }
    e.modules = {}, e.aliases = {}, e.resolve = function(t) {
        "/" === t.charAt(0) && (t = t.slice(1));
        for (var i = [ t, t + ".js", t + ".json", t + "/index.js", t + "/index.json" ], n = 0; n < i.length; n++) {
            var t = i[n];
            if (e.modules.hasOwnProperty(t)) return t;
            if (e.aliases.hasOwnProperty(t)) return e.aliases[t];
        }
    }, e.normalize = function(e, t) {
        var i = [];
        if ("." != t.charAt(0)) return t;
        e = e.split("/"), t = t.split("/");
        for (var n = 0; n < t.length; ++n) ".." == t[n] ? e.pop() : "." != t[n] && "" != t[n] && i.push(t[n]);
        return e.concat(i).join("/");
    }, e.register = function(t, i) {
        e.modules[t] = i;
    }, e.alias = function(t, i) {
        if (!e.modules.hasOwnProperty(t)) throw Error('Failed to alias "' + t + '", it does not exist');
        e.aliases[i] = t;
    }, e.relative = function(t) {
        function i(e, t) {
            for (var i = e.length; i--; ) if (e[i] === t) return i;
            return -1;
        }
        function n(i) {
            var s = n.resolve(i);
            return e(s, t, i);
        }
        var s = e.normalize(t, "..");
        return n.resolve = function(n) {
            var o = n.charAt(0);
            if ("/" == o) return n.slice(1);
            if ("." == o) return e.normalize(s, n);
            var r = t.split("/"), a = i(r, "deps") + 1;
            return a || (a = 0), n = r.slice(0, a + 1).join("/") + "/deps/" + n;
        }, n.exists = function(t) {
            return e.modules.hasOwnProperty(n.resolve(t));
        }, n;
    }, e.register("component-raf/index.js", function(e, t, i) {
        function n(e) {
            var t = new Date().getTime(), i = Math.max(0, 16 - (t - s)), n = setTimeout(e, i);
            return s = t, n;
        }
        e = i.exports = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || n;
        var s = new Date().getTime(), o = window.cancelAnimationFrame || window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || window.oCancelAnimationFrame || window.msCancelAnimationFrame || window.clearTimeout;
        e.cancel = function(e) {
            o.call(window, e);
        };
    }), e.register("segmentio-extend/index.js", function(e, t, i) {
        i.exports = function(e) {
            for (var t, i = Array.prototype.slice.call(arguments, 1), n = 0; t = i[n]; n++) if (t) for (var s in t) e[s] = t[s];
            return e;
        };
    }), e.register("techjacker-isfunction/lib/main.js", function(e, t, i) {
        var n = function(e) {
            var t = {};
            return e && "[object Function]" === t.toString.call(e);
        };
        void 0 !== i && i.exports && (i.exports = n);
    }), e.register("component-bind/index.js", function(e, t, i) {
        var n = [].slice;
        i.exports = function(e, t) {
            if ("string" == typeof t && (t = e[t]), "function" != typeof t) throw Error("bind() requires a function");
            var i = n.call(arguments, 2);
            return function() {
                return t.apply(e, i.concat(n.call(arguments)));
            };
        };
    }), e.register("component-emitter/index.js", function(e, t, i) {
        function n(e) {
            return e ? s(e) : void 0;
        }
        function s(e) {
            for (var t in n.prototype) e[t] = n.prototype[t];
            return e;
        }
        i.exports = n, n.prototype.on = function(e, t) {
            return this._callbacks = this._callbacks || {}, (this._callbacks[e] = this._callbacks[e] || []).push(t),
            this;
        }, n.prototype.once = function(e, t) {
            function i() {
                n.off(e, i), t.apply(this, arguments);
            }
            var n = this;
            return this._callbacks = this._callbacks || {}, t._off = i, this.on(e, i), this;
        }, n.prototype.off = n.prototype.removeListener = n.prototype.removeAllListeners = function(e, t) {
            if (this._callbacks = this._callbacks || {}, 0 == arguments.length) return this._callbacks = {},
            this;
            var i = this._callbacks[e];
            if (!i) return this;
            if (1 == arguments.length) return delete this._callbacks[e], this;
            var n = i.indexOf(t._off || t);
            return ~n && i.splice(n, 1), this;
        }, n.prototype.emit = function(e) {
            this._callbacks = this._callbacks || {};
            var t = [].slice.call(arguments, 1), i = this._callbacks[e];
            if (i) {
                i = i.slice(0);
                for (var n = 0, s = i.length; s > n; ++n) i[n].apply(this, t);
            }
            return this;
        }, n.prototype.listeners = function(e) {
            return this._callbacks = this._callbacks || {}, this._callbacks[e] || [];
        }, n.prototype.hasListeners = function(e) {
            return !!this.listeners(e).length;
        };
    }), e.register("component-tween/index.js", function(e, t, i) {
        function n(e) {
            return this instanceof n ? (this._from = e, this.ease("linear"), void this.duration(500)) : new n(e);
        }
        function s(e) {
            if (Array.isArray(e)) return e.slice();
            var t = {};
            for (var i in e) t[i] = e[i];
            return t;
        }
        var o = t("emitter"), r = t("ease");
        i.exports = n, o(n.prototype), n.prototype.reset = function() {
            return this.isArray = Array.isArray(this._from), this._curr = s(this._from), this._done = !1,
            this._start = Date.now(), this;
        }, n.prototype.to = function(e) {
            return this.reset(), this._to = e, this;
        }, n.prototype.duration = function(e) {
            return this._duration = e, this;
        }, n.prototype.ease = function(e) {
            if (e = "function" == typeof e ? e : r[e], !e) throw new TypeError("invalid easing function");
            return this._ease = e, this;
        }, n.prototype.stop = function() {
            return this.stopped = !0, this._done = !0, this.emit("stop"), this.emit("end"),
            this;
        }, n.prototype.step = function() {
            if (!this._done) {
                var e = this._duration, t = Date.now(), i = t - this._start, n = i >= e;
                if (n) return this._from = this._to, this._update(this._to), this._done = !0, this.emit("end"),
                this;
                var s = this._from, o = this._to, r = this._curr, a = this._ease, l = (t - this._start) / e, d = a(l);
                if (this.isArray) {
                    for (var c = 0; c < s.length; ++c) r[c] = s[c] + (o[c] - s[c]) * d;
                    return this._update(r), this;
                }
                for (var u in s) r[u] = s[u] + (o[u] - s[u]) * d;
                return this._update(r), this;
            }
        }, n.prototype.update = function(e) {
            return 0 == arguments.length ? this.step() : (this._update = e, this);
        };
    }), e.register("component-ease/index.js", function(e, t, i) {
        e.linear = function(e) {
            return e;
        }, e.inQuad = function(e) {
            return e * e;
        }, e.outQuad = function(e) {
            return e * (2 - e);
        }, e.inOutQuad = function(e) {
            return e *= 2, 1 > e ? .5 * e * e : -.5 * (--e * (e - 2) - 1);
        }, e.inCube = function(e) {
            return e * e * e;
        }, e.outCube = function(e) {
            return --e * e * e + 1;
        }, e.inOutCube = function(e) {
            return e *= 2, 1 > e ? .5 * e * e * e : .5 * ((e -= 2) * e * e + 2);
        }, e.inQuart = function(e) {
            return e * e * e * e;
        }, e.outQuart = function(e) {
            return 1 - --e * e * e * e;
        }, e.inOutQuart = function(e) {
            return e *= 2, 1 > e ? .5 * e * e * e * e : -.5 * ((e -= 2) * e * e * e - 2);
        }, e.inQuint = function(e) {
            return e * e * e * e * e;
        }, e.outQuint = function(e) {
            return --e * e * e * e * e + 1;
        }, e.inOutQuint = function(e) {
            return e *= 2, 1 > e ? .5 * e * e * e * e * e : .5 * ((e -= 2) * e * e * e * e + 2);
        }, e.inSine = function(e) {
            return 1 - Math.cos(e * Math.PI / 2);
        }, e.outSine = function(e) {
            return Math.sin(e * Math.PI / 2);
        }, e.inOutSine = function(e) {
            return .5 * (1 - Math.cos(Math.PI * e));
        }, e.inExpo = function(e) {
            return 0 == e ? 0 : Math.pow(1024, e - 1);
        }, e.outExpo = function(e) {
            return 1 == e ? e : 1 - Math.pow(2, -10 * e);
        }, e.inOutExpo = function(e) {
            return 0 == e ? 0 : 1 == e ? 1 : (e *= 2) < 1 ? .5 * Math.pow(1024, e - 1) : .5 * (-Math.pow(2, -10 * (e - 1)) + 2);
        }, e.inCirc = function(e) {
            return 1 - Math.sqrt(1 - e * e);
        }, e.outCirc = function(e) {
            return Math.sqrt(1 - --e * e);
        }, e.inOutCirc = function(e) {
            return e *= 2, 1 > e ? -.5 * (Math.sqrt(1 - e * e) - 1) : .5 * (Math.sqrt(1 - (e -= 2) * e) + 1);
        }, e.inBack = function(e) {
            var t = 1.70158;
            return e * e * ((t + 1) * e - t);
        }, e.outBack = function(e) {
            var t = 1.70158;
            return --e * e * ((t + 1) * e + t) + 1;
        }, e.inOutBack = function(e) {
            var t = 2.5949095;
            return (e *= 2) < 1 ? .5 * (e * e * ((t + 1) * e - t)) : .5 * ((e -= 2) * e * ((t + 1) * e + t) + 2);
        }, e.inBounce = function(t) {
            return 1 - e.outBounce(1 - t);
        }, e.outBounce = function(e) {
            return 1 / 2.75 > e ? 7.5625 * e * e : 2 / 2.75 > e ? 7.5625 * (e -= 1.5 / 2.75) * e + .75 : 2.5 / 2.75 > e ? 7.5625 * (e -= 2.25 / 2.75) * e + .9375 : 7.5625 * (e -= 2.625 / 2.75) * e + .984375;
        }, e.inOutBounce = function(t) {
            return .5 > t ? .5 * e.inBounce(2 * t) : .5 * e.outBounce(2 * t - 1) + .5;
        }, e["in-quad"] = e.inQuad, e["out-quad"] = e.outQuad, e["in-out-quad"] = e.inOutQuad,
        e["in-cube"] = e.inCube, e["out-cube"] = e.outCube, e["in-out-cube"] = e.inOutCube,
        e["in-quart"] = e.inQuart, e["out-quart"] = e.outQuart, e["in-out-quart"] = e.inOutQuart,
        e["in-quint"] = e.inQuint, e["out-quint"] = e.outQuint, e["in-out-quint"] = e.inOutQuint,
        e["in-sine"] = e.inSine, e["out-sine"] = e.outSine, e["in-out-sine"] = e.inOutSine,
        e["in-expo"] = e.inExpo, e["out-expo"] = e.outExpo, e["in-out-expo"] = e.inOutExpo,
        e["in-circ"] = e.inCirc, e["out-circ"] = e.outCirc, e["in-out-circ"] = e.inOutCirc,
        e["in-back"] = e.inBack, e["out-back"] = e.outBack, e["in-out-back"] = e.inOutBack,
        e["in-bounce"] = e.inBounce, e["out-bounce"] = e.outBounce, e["in-out-bounce"] = e.inOutBounce;
    }), e.register("component-domify/index.js", function(e, t, i) {
        function n(e) {
            if ("string" != typeof e) throw new TypeError("String expected");
            var t = /<([\w:]+)/.exec(e);
            if (!t) return document.createTextNode(e);
            e = e.replace(/^\s+|\s+$/g, "");
            var i = t[1];
            if ("body" == i) {
                var n = document.createElement("html");
                return n.innerHTML = e, n.removeChild(n.lastChild);
            }
            var o = s[i] || s._default, r = o[0], a = o[1], l = o[2], n = document.createElement("div");
            for (n.innerHTML = a + e + l; r--; ) n = n.lastChild;
            if (n.firstChild == n.lastChild) return n.removeChild(n.firstChild);
            for (var d = document.createDocumentFragment(); n.firstChild; ) d.appendChild(n.removeChild(n.firstChild));
            return d;
        }
        i.exports = n;
        var s = {
            legend: [ 1, "<fieldset>", "</fieldset>" ],
            tr: [ 2, "<table><tbody>", "</tbody></table>" ],
            col: [ 2, "<table><tbody></tbody><colgroup>", "</colgroup></table>" ],
            _default: [ 0, "", "" ]
        };
        s.td = s.th = [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ], s.option = s.optgroup = [ 1, '<select multiple="multiple">', "</select>" ],
        s.thead = s.tbody = s.colgroup = s.caption = s.tfoot = [ 1, "<table>", "</table>" ],
        s.text = s.circle = s.ellipse = s.line = s.path = s.polygon = s.polyline = s.rect = [ 1, '<svg xmlns="http://www.w3.org/2000/svg" version="1.1">', "</svg>" ];
    }), e.register("tweening-counter/index.js", function(e, i, n) {
        "use strict";
        function s() {
            this.el = c(d), this.interpolate = a(this, "interpolate"), this.update = a(this, "update"),
            this.cancelAnimationFrame = a(this, "cancelAnimationFrame"), this.tween = r({
                val: 0
            }).ease("out-cube").to({
                val: 0
            }).update(this.update).duration(1e3).on("end", this.cancelAnimationFrame), this.onUpdate = !1;
        }
        var o = i("raf"), r = i("tween"), a = i("bind"), l = i("isfunction"), d = i("./template.html"), c = i("domify");
        n.exports = s, s.prototype.to = function(e) {
            return this.tween.to({
                val: e
            }), this;
        }, s.prototype.ease = function(e) {
            return this.tween.ease(e), this;
        }, s.prototype.duration = function(e) {
            return this.tween.duration(e), this;
        }, s.prototype.onEnd = function(e) {
            return l(e) && this.tween.on("end", a(this, e)), this;
        }, s.prototype.start = function() {
            this.interpolate();
        }, s.prototype.interpolate = function() {
            this.id = o(this.interpolate), this.tween.update();
        }, s.prototype.onUpdateCallback = function(e) {
            this.onUpdate = e;
        }, s.prototype.update = function(e) {
            var i = Math.round(e.val), n = t(i, 5, "0");
            "function" == typeof this.onUpdate && this.onUpdate(i, n), this.el.textContent = n;
        }, s.prototype.cancelAnimationFrame = function() {
            o.cancel(this.rafId);
        };
    }), e.register("tweening-counter/template.html", function(e, t, i) {
        i.exports = '<div class="tweening-counter"></div>';
    }), e.alias("component-raf/index.js", "tweening-counter/deps/raf/index.js"), e.alias("component-raf/index.js", "raf/index.js"),
    e.alias("segmentio-extend/index.js", "tweening-counter/deps/extend/index.js"), e.alias("segmentio-extend/index.js", "extend/index.js"),
    e.alias("techjacker-isfunction/lib/main.js", "tweening-counter/deps/isfunction/lib/main.js"),
    e.alias("techjacker-isfunction/lib/main.js", "tweening-counter/deps/isfunction/index.js"),
    e.alias("techjacker-isfunction/lib/main.js", "isfunction/index.js"), e.alias("techjacker-isfunction/lib/main.js", "techjacker-isfunction/index.js"),
    e.alias("component-bind/index.js", "tweening-counter/deps/bind/index.js"), e.alias("component-bind/index.js", "bind/index.js"),
    e.alias("component-tween/index.js", "tweening-counter/deps/tween/index.js"), e.alias("component-tween/index.js", "tween/index.js"),
    e.alias("component-emitter/index.js", "component-tween/deps/emitter/index.js"),
    e.alias("component-ease/index.js", "component-tween/deps/ease/index.js"), e.alias("component-ease/index.js", "tweening-counter/deps/ease/index.js"),
    e.alias("component-ease/index.js", "ease/index.js"), e.alias("component-domify/index.js", "tweening-counter/deps/domify/index.js"),
    e.alias("component-domify/index.js", "domify/index.js"), e.alias("tweening-counter/index.js", "tweening-counter/index.js"),
    "object" == typeof exports ? module.exports = e("tweening-counter") : "function" == typeof define && define.amd ? define([], function() {
        return e("tweening-counter");
    }) : this.TweeningCounter = e("tweening-counter");
}(), function(e) {
    "use strict";
    "function" == typeof define && define.amd ? define([ "jquery" ], e) : "undefined" != typeof exports ? module.exports = e(require("jquery")) : e(jQuery);
}(function(e) {
    "use strict";
    var t = window.Slick || {};
    t = function() {
        function t(t, n) {
            var s, o = this;
            o.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: e(t),
                appendDots: e(t),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function(t, i) {
                    return e('<button type="button" data-role="none" role="button" tabindex="0" />').text(i + 1);
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnFocus: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            }, o.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            }, e.extend(o, o.initials), o.activeBreakpoint = null, o.animType = null, o.animProp = null,
            o.breakpoints = [], o.breakpointSettings = [], o.cssTransitions = !1, o.focussed = !1,
            o.interrupted = !1, o.hidden = "hidden", o.paused = !0, o.positionProp = null, o.respondTo = null,
            o.rowCount = 1, o.shouldClick = !0, o.$slider = e(t), o.$slidesCache = null, o.transformType = null,
            o.transitionType = null, o.visibilityChange = "visibilitychange", o.windowWidth = 0,
            o.windowTimer = null, s = e(t).data("slick") || {}, o.options = e.extend({}, o.defaults, n, s),
            o.currentSlide = o.options.initialSlide, o.originalSettings = o.options, void 0 !== document.mozHidden ? (o.hidden = "mozHidden",
            o.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (o.hidden = "webkitHidden",
            o.visibilityChange = "webkitvisibilitychange"), o.autoPlay = e.proxy(o.autoPlay, o),
            o.autoPlayClear = e.proxy(o.autoPlayClear, o), o.autoPlayIterator = e.proxy(o.autoPlayIterator, o),
            o.changeSlide = e.proxy(o.changeSlide, o), o.clickHandler = e.proxy(o.clickHandler, o),
            o.selectHandler = e.proxy(o.selectHandler, o), o.setPosition = e.proxy(o.setPosition, o),
            o.swipeHandler = e.proxy(o.swipeHandler, o), o.dragHandler = e.proxy(o.dragHandler, o),
            o.keyHandler = e.proxy(o.keyHandler, o), o.instanceUid = i++, o.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/,
            o.registerBreakpoints(), o.init(!0);
        }
        var i = 0;
        return t;
    }(), t.prototype.activateADA = function() {
        var e = this;
        e.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        });
    }, t.prototype.addSlide = t.prototype.slickAdd = function(t, i, n) {
        var s = this;
        if ("boolean" == typeof i) n = i, i = null; else if (0 > i || i >= s.slideCount) return !1;
        s.unload(), "number" == typeof i ? 0 === i && 0 === s.$slides.length ? e(t).appendTo(s.$slideTrack) : n ? e(t).insertBefore(s.$slides.eq(i)) : e(t).insertAfter(s.$slides.eq(i)) : n === !0 ? e(t).prependTo(s.$slideTrack) : e(t).appendTo(s.$slideTrack),
        s.$slides = s.$slideTrack.children(this.options.slide), s.$slideTrack.children(this.options.slide).detach(),
        s.$slideTrack.append(s.$slides), s.$slides.each(function(t, i) {
            e(i).attr("data-slick-index", t);
        }), s.$slidesCache = s.$slides, s.reinit();
    }, t.prototype.animateHeight = function() {
        var e = this;
        if (1 === e.options.slidesToShow && e.options.adaptiveHeight === !0 && e.options.vertical === !1) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.animate({
                height: t
            }, e.options.speed);
        }
    }, t.prototype.animateSlide = function(t, i) {
        var n = {}, s = this;
        s.animateHeight(), s.options.rtl === !0 && s.options.vertical === !1 && (t = -t),
        s.transformsEnabled === !1 ? s.options.vertical === !1 ? s.$slideTrack.animate({
            left: t
        }, s.options.speed, s.options.easing, i) : s.$slideTrack.animate({
            top: t
        }, s.options.speed, s.options.easing, i) : s.cssTransitions === !1 ? (s.options.rtl === !0 && (s.currentLeft = -s.currentLeft),
        e({
            animStart: s.currentLeft
        }).animate({
            animStart: t
        }, {
            duration: s.options.speed,
            easing: s.options.easing,
            step: function(e) {
                e = Math.ceil(e), s.options.vertical === !1 ? (n[s.animType] = "translate(" + e + "px, 0px)",
                s.$slideTrack.css(n)) : (n[s.animType] = "translate(0px," + e + "px)", s.$slideTrack.css(n));
            },
            complete: function() {
                i && i.call();
            }
        })) : (s.applyTransition(), t = Math.ceil(t), s.options.vertical === !1 ? n[s.animType] = "translate3d(" + t + "px, 0px, 0px)" : n[s.animType] = "translate3d(0px," + t + "px, 0px)",
        s.$slideTrack.css(n), i && setTimeout(function() {
            s.disableTransition(), i.call();
        }, s.options.speed));
    }, t.prototype.getNavTarget = function() {
        var t = this, i = t.options.asNavFor;
        return i && null !== i && (i = e(i).not(t.$slider)), i;
    }, t.prototype.asNavFor = function(t) {
        var i = this, n = i.getNavTarget();
        null !== n && "object" == typeof n && n.each(function() {
            var i = e(this).slick("getSlick");
            i.unslicked || i.slideHandler(t, !0);
        });
    }, t.prototype.applyTransition = function(e) {
        var t = this, i = {};
        t.options.fade === !1 ? i[t.transitionType] = t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : i[t.transitionType] = "opacity " + t.options.speed + "ms " + t.options.cssEase,
        t.options.fade === !1 ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i);
    }, t.prototype.autoPlay = function() {
        var e = this;
        e.autoPlayClear(), e.slideCount > e.options.slidesToShow && (e.autoPlayTimer = setInterval(e.autoPlayIterator, e.options.autoplaySpeed));
    }, t.prototype.autoPlayClear = function() {
        var e = this;
        e.autoPlayTimer && clearInterval(e.autoPlayTimer);
    }, t.prototype.autoPlayIterator = function() {
        var e = this, t = e.currentSlide + e.options.slidesToScroll;
        e.paused || e.interrupted || e.focussed || (e.options.infinite === !1 && (1 === e.direction && e.currentSlide + 1 === e.slideCount - 1 ? e.direction = 0 : 0 === e.direction && (t = e.currentSlide - e.options.slidesToScroll,
        e.currentSlide - 1 === 0 && (e.direction = 1))), e.slideHandler(t));
    }, t.prototype.buildArrows = function() {
        var t = this;
        t.options.arrows === !0 && (t.$prevArrow = e(t.options.prevArrow).addClass("slick-arrow"),
        t.$nextArrow = e(t.options.nextArrow).addClass("slick-arrow"), t.slideCount > t.options.slidesToShow ? (t.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
        t.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.prependTo(t.options.appendArrows),
        t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.appendTo(t.options.appendArrows),
        t.options.infinite !== !0 && t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : t.$prevArrow.add(t.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }));
    }, t.prototype.buildDots = function() {
        var t, i, n = this;
        if (n.options.dots === !0 && n.slideCount > n.options.slidesToShow) {
            for (n.$slider.addClass("slick-dotted"), i = e("<ul />").addClass(n.options.dotsClass),
            t = 0; t <= n.getDotCount(); t += 1) i.append(e("<li />").append(n.options.customPaging.call(this, n, t)));
            n.$dots = i.appendTo(n.options.appendDots), n.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false");
        }
    }, t.prototype.buildOut = function() {
        var t = this;
        t.$slides = t.$slider.children(t.options.slide + ":not(.slick-cloned)").addClass("slick-slide"),
        t.slideCount = t.$slides.length, t.$slides.each(function(t, i) {
            e(i).attr("data-slick-index", t).data("originalStyling", e(i).attr("style") || "");
        }), t.$slider.addClass("slick-slider"), t.$slideTrack = 0 === t.slideCount ? e('<div class="slick-track"/>').appendTo(t.$slider) : t.$slides.wrapAll('<div class="slick-track"/>').parent(),
        t.$list = t.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),
        t.$slideTrack.css("opacity", 0), t.options.centerMode !== !0 && t.options.swipeToSlide !== !0 || (t.options.slidesToScroll = 1),
        e("img[data-lazy]", t.$slider).not("[src]").addClass("slick-loading"), t.setupInfinite(),
        t.buildArrows(), t.buildDots(), t.updateDots(), t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0),
        t.options.draggable === !0 && t.$list.addClass("draggable");
    }, t.prototype.buildRows = function() {
        var e, t, i, n, s, o, r, a = this;
        if (n = document.createDocumentFragment(), o = a.$slider.children(), a.options.rows > 1) {
            for (r = a.options.slidesPerRow * a.options.rows, s = Math.ceil(o.length / r), e = 0; s > e; e++) {
                var l = document.createElement("div");
                for (t = 0; t < a.options.rows; t++) {
                    var d = document.createElement("div");
                    for (i = 0; i < a.options.slidesPerRow; i++) {
                        var c = e * r + (t * a.options.slidesPerRow + i);
                        o.get(c) && d.appendChild(o.get(c));
                    }
                    l.appendChild(d);
                }
                n.appendChild(l);
            }
            a.$slider.empty().append(n), a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            });
        }
    }, t.prototype.checkResponsive = function(t, i) {
        var n, s, o, r = this, a = !1, l = r.$slider.width(), d = window.innerWidth || e(window).width();
        if ("window" === r.respondTo ? o = d : "slider" === r.respondTo ? o = l : "min" === r.respondTo && (o = Math.min(d, l)),
        r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
            s = null;
            for (n in r.breakpoints) r.breakpoints.hasOwnProperty(n) && (r.originalSettings.mobileFirst === !1 ? o < r.breakpoints[n] && (s = r.breakpoints[n]) : o > r.breakpoints[n] && (s = r.breakpoints[n]));
            null !== s ? null !== r.activeBreakpoint ? (s !== r.activeBreakpoint || i) && (r.activeBreakpoint = s,
            "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = e.extend({}, r.originalSettings, r.breakpointSettings[s]),
            t === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(t)), a = s) : (r.activeBreakpoint = s,
            "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = e.extend({}, r.originalSettings, r.breakpointSettings[s]),
            t === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(t)), a = s) : null !== r.activeBreakpoint && (r.activeBreakpoint = null,
            r.options = r.originalSettings, t === !0 && (r.currentSlide = r.options.initialSlide),
            r.refresh(t), a = s), t || a === !1 || r.$slider.trigger("breakpoint", [ r, a ]);
        }
    }, t.prototype.changeSlide = function(t, i) {
        var n, s, o, r = this, a = e(t.currentTarget);
        switch (a.is("a") && t.preventDefault(), a.is("li") || (a = a.closest("li")), o = r.slideCount % r.options.slidesToScroll !== 0,
        n = o ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll, t.data.message) {
          case "previous":
            s = 0 === n ? r.options.slidesToScroll : r.options.slidesToShow - n, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - s, !1, i);
            break;

          case "next":
            s = 0 === n ? r.options.slidesToScroll : n, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + s, !1, i);
            break;

          case "index":
            var l = 0 === t.data.index ? 0 : t.data.index || a.index() * r.options.slidesToScroll;
            r.slideHandler(r.checkNavigable(l), !1, i), a.children().trigger("focus");
            break;

          default:
            return;
        }
    }, t.prototype.checkNavigable = function(e) {
        var t, i, n = this;
        if (t = n.getNavigableIndexes(), i = 0, e > t[t.length - 1]) e = t[t.length - 1]; else for (var s in t) {
            if (e < t[s]) {
                e = i;
                break;
            }
            i = t[s];
        }
        return e;
    }, t.prototype.cleanUpEvents = function() {
        var t = this;
        t.options.dots && null !== t.$dots && e("li", t.$dots).off("click.slick", t.changeSlide).off("mouseenter.slick", e.proxy(t.interrupt, t, !0)).off("mouseleave.slick", e.proxy(t.interrupt, t, !1)),
        t.$slider.off("focus.slick blur.slick"), t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow && t.$prevArrow.off("click.slick", t.changeSlide),
        t.$nextArrow && t.$nextArrow.off("click.slick", t.changeSlide)), t.$list.off("touchstart.slick mousedown.slick", t.swipeHandler),
        t.$list.off("touchmove.slick mousemove.slick", t.swipeHandler), t.$list.off("touchend.slick mouseup.slick", t.swipeHandler),
        t.$list.off("touchcancel.slick mouseleave.slick", t.swipeHandler), t.$list.off("click.slick", t.clickHandler),
        e(document).off(t.visibilityChange, t.visibility), t.cleanUpSlideEvents(), t.options.accessibility === !0 && t.$list.off("keydown.slick", t.keyHandler),
        t.options.focusOnSelect === !0 && e(t.$slideTrack).children().off("click.slick", t.selectHandler),
        e(window).off("orientationchange.slick.slick-" + t.instanceUid, t.orientationChange),
        e(window).off("resize.slick.slick-" + t.instanceUid, t.resize), e("[draggable!=true]", t.$slideTrack).off("dragstart", t.preventDefault),
        e(window).off("load.slick.slick-" + t.instanceUid, t.setPosition), e(document).off("ready.slick.slick-" + t.instanceUid, t.setPosition);
    }, t.prototype.cleanUpSlideEvents = function() {
        var t = this;
        t.$list.off("mouseenter.slick", e.proxy(t.interrupt, t, !0)), t.$list.off("mouseleave.slick", e.proxy(t.interrupt, t, !1));
    }, t.prototype.cleanUpRows = function() {
        var e, t = this;
        t.options.rows > 1 && (e = t.$slides.children().children(), e.removeAttr("style"),
        t.$slider.empty().append(e));
    }, t.prototype.clickHandler = function(e) {
        var t = this;
        t.shouldClick === !1 && (e.stopImmediatePropagation(), e.stopPropagation(), e.preventDefault());
    }, t.prototype.destroy = function(t) {
        var i = this;
        i.autoPlayClear(), i.touchObject = {}, i.cleanUpEvents(), e(".slick-cloned", i.$slider).detach(),
        i.$dots && i.$dots.remove(), i.$prevArrow && i.$prevArrow.length && (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""),
        i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove()), i.$nextArrow && i.$nextArrow.length && (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""),
        i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove()), i.$slides && (i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
            e(this).attr("style", e(this).data("originalStyling"));
        }), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.detach(),
        i.$list.detach(), i.$slider.append(i.$slides)), i.cleanUpRows(), i.$slider.removeClass("slick-slider"),
        i.$slider.removeClass("slick-initialized"), i.$slider.removeClass("slick-dotted"),
        i.unslicked = !0, t || i.$slider.trigger("destroy", [ i ]);
    }, t.prototype.disableTransition = function(e) {
        var t = this, i = {};
        i[t.transitionType] = "", t.options.fade === !1 ? t.$slideTrack.css(i) : t.$slides.eq(e).css(i);
    }, t.prototype.fadeSlide = function(e, t) {
        var i = this;
        i.cssTransitions === !1 ? (i.$slides.eq(e).css({
            zIndex: i.options.zIndex
        }), i.$slides.eq(e).animate({
            opacity: 1
        }, i.options.speed, i.options.easing, t)) : (i.applyTransition(e), i.$slides.eq(e).css({
            opacity: 1,
            zIndex: i.options.zIndex
        }), t && setTimeout(function() {
            i.disableTransition(e), t.call();
        }, i.options.speed));
    }, t.prototype.fadeSlideOut = function(e) {
        var t = this;
        t.cssTransitions === !1 ? t.$slides.eq(e).animate({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }, t.options.speed, t.options.easing) : (t.applyTransition(e), t.$slides.eq(e).css({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }));
    }, t.prototype.filterSlides = t.prototype.slickFilter = function(e) {
        var t = this;
        null !== e && (t.$slidesCache = t.$slides, t.unload(), t.$slideTrack.children(this.options.slide).detach(),
        t.$slidesCache.filter(e).appendTo(t.$slideTrack), t.reinit());
    }, t.prototype.focusHandler = function() {
        var t = this;
        t.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function(i) {
            i.stopImmediatePropagation();
            var n = e(this);
            setTimeout(function() {
                t.options.pauseOnFocus && (t.focussed = n.is(":focus"), t.autoPlay());
            }, 0);
        });
    }, t.prototype.getCurrent = t.prototype.slickCurrentSlide = function() {
        var e = this;
        return e.currentSlide;
    }, t.prototype.getDotCount = function() {
        var e = this, t = 0, i = 0, n = 0;
        if (e.options.infinite === !0) for (;t < e.slideCount; ) ++n, t = i + e.options.slidesToScroll,
        i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else if (e.options.centerMode === !0) n = e.slideCount; else if (e.options.asNavFor) for (;t < e.slideCount; ) ++n,
        t = i + e.options.slidesToScroll, i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow; else n = 1 + Math.ceil((e.slideCount - e.options.slidesToShow) / e.options.slidesToScroll);
        return n - 1;
    }, t.prototype.getLeft = function(e) {
        var t, i, n, s = this, o = 0;
        return s.slideOffset = 0, i = s.$slides.first().outerHeight(!0), s.options.infinite === !0 ? (s.slideCount > s.options.slidesToShow && (s.slideOffset = s.slideWidth * s.options.slidesToShow * -1,
        o = i * s.options.slidesToShow * -1), s.slideCount % s.options.slidesToScroll !== 0 && e + s.options.slidesToScroll > s.slideCount && s.slideCount > s.options.slidesToShow && (e > s.slideCount ? (s.slideOffset = (s.options.slidesToShow - (e - s.slideCount)) * s.slideWidth * -1,
        o = (s.options.slidesToShow - (e - s.slideCount)) * i * -1) : (s.slideOffset = s.slideCount % s.options.slidesToScroll * s.slideWidth * -1,
        o = s.slideCount % s.options.slidesToScroll * i * -1))) : e + s.options.slidesToShow > s.slideCount && (s.slideOffset = (e + s.options.slidesToShow - s.slideCount) * s.slideWidth,
        o = (e + s.options.slidesToShow - s.slideCount) * i), s.slideCount <= s.options.slidesToShow && (s.slideOffset = 0,
        o = 0), s.options.centerMode === !0 && s.options.infinite === !0 ? s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2) - s.slideWidth : s.options.centerMode === !0 && (s.slideOffset = 0,
        s.slideOffset += s.slideWidth * Math.floor(s.options.slidesToShow / 2)), t = s.options.vertical === !1 ? e * s.slideWidth * -1 + s.slideOffset : e * i * -1 + o,
        s.options.variableWidth === !0 && (n = s.slideCount <= s.options.slidesToShow || s.options.infinite === !1 ? s.$slideTrack.children(".slick-slide").eq(e) : s.$slideTrack.children(".slick-slide").eq(e + s.options.slidesToShow),
        t = s.options.rtl === !0 ? n[0] ? -1 * (s.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0,
        s.options.centerMode === !0 && (n = s.slideCount <= s.options.slidesToShow || s.options.infinite === !1 ? s.$slideTrack.children(".slick-slide").eq(e) : s.$slideTrack.children(".slick-slide").eq(e + s.options.slidesToShow + 1),
        t = s.options.rtl === !0 ? n[0] ? -1 * (s.$slideTrack.width() - n[0].offsetLeft - n.width()) : 0 : n[0] ? -1 * n[0].offsetLeft : 0,
        t += (s.$list.width() - n.outerWidth()) / 2)), t;
    }, t.prototype.getOption = t.prototype.slickGetOption = function(e) {
        var t = this;
        return t.options[e];
    }, t.prototype.getNavigableIndexes = function() {
        var e, t = this, i = 0, n = 0, s = [];
        for (t.options.infinite === !1 ? e = t.slideCount : (i = -1 * t.options.slidesToScroll,
        n = -1 * t.options.slidesToScroll, e = 2 * t.slideCount); e > i; ) s.push(i), i = n + t.options.slidesToScroll,
        n += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        return s;
    }, t.prototype.getSlick = function() {
        return this;
    }, t.prototype.getSlideCount = function() {
        var t, i, n, s = this;
        return n = s.options.centerMode === !0 ? s.slideWidth * Math.floor(s.options.slidesToShow / 2) : 0,
        s.options.swipeToSlide === !0 ? (s.$slideTrack.find(".slick-slide").each(function(t, o) {
            return o.offsetLeft - n + e(o).outerWidth() / 2 > -1 * s.swipeLeft ? (i = o, !1) : void 0;
        }), t = Math.abs(e(i).attr("data-slick-index") - s.currentSlide) || 1) : s.options.slidesToScroll;
    }, t.prototype.goTo = t.prototype.slickGoTo = function(e, t) {
        var i = this;
        i.changeSlide({
            data: {
                message: "index",
                index: parseInt(e)
            }
        }, t);
    }, t.prototype.init = function(t) {
        var i = this;
        e(i.$slider).hasClass("slick-initialized") || (e(i.$slider).addClass("slick-initialized"),
        i.buildRows(), i.buildOut(), i.setProps(), i.startLoad(), i.loadSlider(), i.initializeEvents(),
        i.updateArrows(), i.updateDots(), i.checkResponsive(!0), i.focusHandler()), t && i.$slider.trigger("init", [ i ]),
        i.options.accessibility === !0 && i.initADA(), i.options.autoplay && (i.paused = !1,
        i.autoPlay());
    }, t.prototype.initADA = function() {
        var t = this;
        t.$slides.add(t.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        }), t.$slideTrack.attr("role", "listbox"), t.$slides.not(t.$slideTrack.find(".slick-cloned")).each(function(i) {
            e(this).attr({
                role: "option",
                "aria-describedby": "slick-slide" + t.instanceUid + i
            });
        }), null !== t.$dots && t.$dots.attr("role", "tablist").find("li").each(function(i) {
            e(this).attr({
                role: "presentation",
                "aria-selected": "false",
                "aria-controls": "navigation" + t.instanceUid + i,
                id: "slick-slide" + t.instanceUid + i
            });
        }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"),
        t.activateADA();
    }, t.prototype.initArrowEvents = function() {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.off("click.slick").on("click.slick", {
            message: "previous"
        }, e.changeSlide), e.$nextArrow.off("click.slick").on("click.slick", {
            message: "next"
        }, e.changeSlide));
    }, t.prototype.initDotEvents = function() {
        var t = this;
        t.options.dots === !0 && t.slideCount > t.options.slidesToShow && e("li", t.$dots).on("click.slick", {
            message: "index"
        }, t.changeSlide), t.options.dots === !0 && t.options.pauseOnDotsHover === !0 && e("li", t.$dots).on("mouseenter.slick", e.proxy(t.interrupt, t, !0)).on("mouseleave.slick", e.proxy(t.interrupt, t, !1));
    }, t.prototype.initSlideEvents = function() {
        var t = this;
        t.options.pauseOnHover && (t.$list.on("mouseenter.slick", e.proxy(t.interrupt, t, !0)),
        t.$list.on("mouseleave.slick", e.proxy(t.interrupt, t, !1)));
    }, t.prototype.initializeEvents = function() {
        var t = this;
        t.initArrowEvents(), t.initDotEvents(), t.initSlideEvents(), t.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, t.swipeHandler), t.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, t.swipeHandler), t.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, t.swipeHandler), t.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, t.swipeHandler), t.$list.on("click.slick", t.clickHandler), e(document).on(t.visibilityChange, e.proxy(t.visibility, t)),
        t.options.accessibility === !0 && t.$list.on("keydown.slick", t.keyHandler), t.options.focusOnSelect === !0 && e(t.$slideTrack).children().on("click.slick", t.selectHandler),
        e(window).on("orientationchange.slick.slick-" + t.instanceUid, e.proxy(t.orientationChange, t)),
        e(window).on("resize.slick.slick-" + t.instanceUid, e.proxy(t.resize, t)), e("[draggable!=true]", t.$slideTrack).on("dragstart", t.preventDefault),
        e(window).on("load.slick.slick-" + t.instanceUid, t.setPosition), e(document).on("ready.slick.slick-" + t.instanceUid, t.setPosition);
    }, t.prototype.initUI = function() {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.show(),
        e.$nextArrow.show()), e.options.dots === !0 && e.slideCount > e.options.slidesToShow && e.$dots.show();
    }, t.prototype.keyHandler = function(e) {
        var t = this;
        e.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === e.keyCode && t.options.accessibility === !0 ? t.changeSlide({
            data: {
                message: t.options.rtl === !0 ? "next" : "previous"
            }
        }) : 39 === e.keyCode && t.options.accessibility === !0 && t.changeSlide({
            data: {
                message: t.options.rtl === !0 ? "previous" : "next"
            }
        }));
    }, t.prototype.lazyLoad = function() {
        function t(t) {
            e("img[data-lazy]", t).each(function() {
                var t = e(this), i = e(this).attr("data-lazy"), n = document.createElement("img");
                n.onload = function() {
                    t.animate({
                        opacity: 0
                    }, 100, function() {
                        t.attr("src", i).animate({
                            opacity: 1
                        }, 200, function() {
                            t.removeAttr("data-lazy").removeClass("slick-loading");
                        }), r.$slider.trigger("lazyLoaded", [ r, t, i ]);
                    });
                }, n.onerror = function() {
                    t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),
                    r.$slider.trigger("lazyLoadError", [ r, t, i ]);
                }, n.src = i;
            });
        }
        var i, n, s, o, r = this;
        r.options.centerMode === !0 ? r.options.infinite === !0 ? (s = r.currentSlide + (r.options.slidesToShow / 2 + 1),
        o = s + r.options.slidesToShow + 2) : (s = Math.max(0, r.currentSlide - (r.options.slidesToShow / 2 + 1)),
        o = 2 + (r.options.slidesToShow / 2 + 1) + r.currentSlide) : (s = r.options.infinite ? r.options.slidesToShow + r.currentSlide : r.currentSlide,
        o = Math.ceil(s + r.options.slidesToShow), r.options.fade === !0 && (s > 0 && s--,
        o <= r.slideCount && o++)), i = r.$slider.find(".slick-slide").slice(s, o), t(i),
        r.slideCount <= r.options.slidesToShow ? (n = r.$slider.find(".slick-slide"), t(n)) : r.currentSlide >= r.slideCount - r.options.slidesToShow ? (n = r.$slider.find(".slick-cloned").slice(0, r.options.slidesToShow),
        t(n)) : 0 === r.currentSlide && (n = r.$slider.find(".slick-cloned").slice(-1 * r.options.slidesToShow),
        t(n));
    }, t.prototype.loadSlider = function() {
        var e = this;
        e.setPosition(), e.$slideTrack.css({
            opacity: 1
        }), e.$slider.removeClass("slick-loading"), e.initUI(), "progressive" === e.options.lazyLoad && e.progressiveLazyLoad();
    }, t.prototype.next = t.prototype.slickNext = function() {
        var e = this;
        e.changeSlide({
            data: {
                message: "next"
            }
        });
    }, t.prototype.orientationChange = function() {
        var e = this;
        e.checkResponsive(), e.setPosition();
    }, t.prototype.pause = t.prototype.slickPause = function() {
        var e = this;
        e.autoPlayClear(), e.paused = !0;
    }, t.prototype.play = t.prototype.slickPlay = function() {
        var e = this;
        e.autoPlay(), e.options.autoplay = !0, e.paused = !1, e.focussed = !1, e.interrupted = !1;
    }, t.prototype.postSlide = function(e) {
        var t = this;
        t.unslicked || (t.$slider.trigger("afterChange", [ t, e ]), t.animating = !1, t.setPosition(),
        t.swipeLeft = null, t.options.autoplay && t.autoPlay(), t.options.accessibility === !0 && t.initADA());
    }, t.prototype.prev = t.prototype.slickPrev = function() {
        var e = this;
        e.changeSlide({
            data: {
                message: "previous"
            }
        });
    }, t.prototype.preventDefault = function(e) {
        e.preventDefault();
    }, t.prototype.progressiveLazyLoad = function(t) {
        t = t || 1;
        var i, n, s, o = this, r = e("img[data-lazy]", o.$slider);
        r.length ? (i = r.first(), n = i.attr("data-lazy"), s = document.createElement("img"),
        s.onload = function() {
            i.attr("src", n).removeAttr("data-lazy").removeClass("slick-loading"), o.options.adaptiveHeight === !0 && o.setPosition(),
            o.$slider.trigger("lazyLoaded", [ o, i, n ]), o.progressiveLazyLoad();
        }, s.onerror = function() {
            3 > t ? setTimeout(function() {
                o.progressiveLazyLoad(t + 1);
            }, 500) : (i.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),
            o.$slider.trigger("lazyLoadError", [ o, i, n ]), o.progressiveLazyLoad());
        }, s.src = n) : o.$slider.trigger("allImagesLoaded", [ o ]);
    }, t.prototype.refresh = function(t) {
        var i, n, s = this;
        n = s.slideCount - s.options.slidesToShow, !s.options.infinite && s.currentSlide > n && (s.currentSlide = n),
        s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0), i = s.currentSlide,
        s.destroy(!0), e.extend(s, s.initials, {
            currentSlide: i
        }), s.init(), t || s.changeSlide({
            data: {
                message: "index",
                index: i
            }
        }, !1);
    }, t.prototype.registerBreakpoints = function() {
        var t, i, n, s = this, o = s.options.responsive || null;
        if ("array" === e.type(o) && o.length) {
            s.respondTo = s.options.respondTo || "window";
            for (t in o) if (n = s.breakpoints.length - 1, i = o[t].breakpoint, o.hasOwnProperty(t)) {
                for (;n >= 0; ) s.breakpoints[n] && s.breakpoints[n] === i && s.breakpoints.splice(n, 1),
                n--;
                s.breakpoints.push(i), s.breakpointSettings[i] = o[t].settings;
            }
            s.breakpoints.sort(function(e, t) {
                return s.options.mobileFirst ? e - t : t - e;
            });
        }
    }, t.prototype.reinit = function() {
        var t = this;
        t.$slides = t.$slideTrack.children(t.options.slide).addClass("slick-slide"), t.slideCount = t.$slides.length,
        t.currentSlide >= t.slideCount && 0 !== t.currentSlide && (t.currentSlide = t.currentSlide - t.options.slidesToScroll),
        t.slideCount <= t.options.slidesToShow && (t.currentSlide = 0), t.registerBreakpoints(),
        t.setProps(), t.setupInfinite(), t.buildArrows(), t.updateArrows(), t.initArrowEvents(),
        t.buildDots(), t.updateDots(), t.initDotEvents(), t.cleanUpSlideEvents(), t.initSlideEvents(),
        t.checkResponsive(!1, !0), t.options.focusOnSelect === !0 && e(t.$slideTrack).children().on("click.slick", t.selectHandler),
        t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0), t.setPosition(),
        t.focusHandler(), t.paused = !t.options.autoplay, t.autoPlay(), t.$slider.trigger("reInit", [ t ]);
    }, t.prototype.resize = function() {
        var t = this;
        e(window).width() !== t.windowWidth && (clearTimeout(t.windowDelay), t.windowDelay = window.setTimeout(function() {
            t.windowWidth = e(window).width(), t.checkResponsive(), t.unslicked || t.setPosition();
        }, 50));
    }, t.prototype.removeSlide = t.prototype.slickRemove = function(e, t, i) {
        var n = this;
        return "boolean" == typeof e ? (t = e, e = t === !0 ? 0 : n.slideCount - 1) : e = t === !0 ? --e : e,
        !(n.slideCount < 1 || 0 > e || e > n.slideCount - 1) && (n.unload(), i === !0 ? n.$slideTrack.children().remove() : n.$slideTrack.children(this.options.slide).eq(e).remove(),
        n.$slides = n.$slideTrack.children(this.options.slide), n.$slideTrack.children(this.options.slide).detach(),
        n.$slideTrack.append(n.$slides), n.$slidesCache = n.$slides, void n.reinit());
    }, t.prototype.setCSS = function(e) {
        var t, i, n = this, s = {};
        n.options.rtl === !0 && (e = -e), t = "left" == n.positionProp ? Math.ceil(e) + "px" : "0px",
        i = "top" == n.positionProp ? Math.ceil(e) + "px" : "0px", s[n.positionProp] = e,
        n.transformsEnabled === !1 ? n.$slideTrack.css(s) : (s = {}, n.cssTransitions === !1 ? (s[n.animType] = "translate(" + t + ", " + i + ")",
        n.$slideTrack.css(s)) : (s[n.animType] = "translate3d(" + t + ", " + i + ", 0px)",
        n.$slideTrack.css(s)));
    }, t.prototype.setDimensions = function() {
        var e = this;
        e.options.vertical === !1 ? e.options.centerMode === !0 && e.$list.css({
            padding: "0px " + e.options.centerPadding
        }) : (e.$list.height(e.$slides.first().outerHeight(!0) * e.options.slidesToShow),
        e.options.centerMode === !0 && e.$list.css({
            padding: e.options.centerPadding + " 0px"
        })), e.listWidth = e.$list.width(), e.listHeight = e.$list.height(), e.options.vertical === !1 && e.options.variableWidth === !1 ? (e.slideWidth = Math.ceil(e.listWidth / e.options.slidesToShow),
        e.$slideTrack.width(Math.ceil(e.slideWidth * e.$slideTrack.children(".slick-slide").length))) : e.options.variableWidth === !0 ? e.$slideTrack.width(5e3 * e.slideCount) : (e.slideWidth = Math.ceil(e.listWidth),
        e.$slideTrack.height(Math.ceil(e.$slides.first().outerHeight(!0) * e.$slideTrack.children(".slick-slide").length)));
        var t = e.$slides.first().outerWidth(!0) - e.$slides.first().width();
        e.options.variableWidth === !1 && e.$slideTrack.children(".slick-slide").width(e.slideWidth - t);
    }, t.prototype.setFade = function() {
        var t, i = this;
        i.$slides.each(function(n, s) {
            t = i.slideWidth * n * -1, i.options.rtl === !0 ? e(s).css({
                position: "relative",
                right: t,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            }) : e(s).css({
                position: "relative",
                left: t,
                top: 0,
                zIndex: i.options.zIndex - 2,
                opacity: 0
            });
        }), i.$slides.eq(i.currentSlide).css({
            zIndex: i.options.zIndex - 1,
            opacity: 1
        });
    }, t.prototype.setHeight = function() {
        var e = this;
        if (1 === e.options.slidesToShow && e.options.adaptiveHeight === !0 && e.options.vertical === !1) {
            var t = e.$slides.eq(e.currentSlide).outerHeight(!0);
            e.$list.css("height", t);
        }
    }, t.prototype.setOption = t.prototype.slickSetOption = function() {
        var t, i, n, s, o, r = this, a = !1;
        if ("object" === e.type(arguments[0]) ? (n = arguments[0], a = arguments[1], o = "multiple") : "string" === e.type(arguments[0]) && (n = arguments[0],
        s = arguments[1], a = arguments[2], "responsive" === arguments[0] && "array" === e.type(arguments[1]) ? o = "responsive" : void 0 !== arguments[1] && (o = "single")),
        "single" === o) r.options[n] = s; else if ("multiple" === o) e.each(n, function(e, t) {
            r.options[e] = t;
        }); else if ("responsive" === o) for (i in s) if ("array" !== e.type(r.options.responsive)) r.options.responsive = [ s[i] ]; else {
            for (t = r.options.responsive.length - 1; t >= 0; ) r.options.responsive[t].breakpoint === s[i].breakpoint && r.options.responsive.splice(t, 1),
            t--;
            r.options.responsive.push(s[i]);
        }
        a && (r.unload(), r.reinit());
    }, t.prototype.setPosition = function() {
        var e = this;
        e.setDimensions(), e.setHeight(), e.options.fade === !1 ? e.setCSS(e.getLeft(e.currentSlide)) : e.setFade(),
        e.$slider.trigger("setPosition", [ e ]);
    }, t.prototype.setProps = function() {
        var e = this, t = document.body.style;
        e.positionProp = e.options.vertical === !0 ? "top" : "left", "top" === e.positionProp ? e.$slider.addClass("slick-vertical") : e.$slider.removeClass("slick-vertical"),
        void 0 === t.WebkitTransition && void 0 === t.MozTransition && void 0 === t.msTransition || e.options.useCSS === !0 && (e.cssTransitions = !0),
        e.options.fade && ("number" == typeof e.options.zIndex ? e.options.zIndex < 3 && (e.options.zIndex = 3) : e.options.zIndex = e.defaults.zIndex),
        void 0 !== t.OTransform && (e.animType = "OTransform", e.transformType = "-o-transform",
        e.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)),
        void 0 !== t.MozTransform && (e.animType = "MozTransform", e.transformType = "-moz-transform",
        e.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (e.animType = !1)),
        void 0 !== t.webkitTransform && (e.animType = "webkitTransform", e.transformType = "-webkit-transform",
        e.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (e.animType = !1)),
        void 0 !== t.msTransform && (e.animType = "msTransform", e.transformType = "-ms-transform",
        e.transitionType = "msTransition", void 0 === t.msTransform && (e.animType = !1)),
        void 0 !== t.transform && e.animType !== !1 && (e.animType = "transform", e.transformType = "transform",
        e.transitionType = "transition"), e.transformsEnabled = e.options.useTransform && null !== e.animType && e.animType !== !1;
    }, t.prototype.setSlideClasses = function(e) {
        var t, i, n, s, o = this;
        i = o.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"),
        o.$slides.eq(e).addClass("slick-current"), o.options.centerMode === !0 ? (t = Math.floor(o.options.slidesToShow / 2),
        o.options.infinite === !0 && (e >= t && e <= o.slideCount - 1 - t ? o.$slides.slice(e - t, e + t + 1).addClass("slick-active").attr("aria-hidden", "false") : (n = o.options.slidesToShow + e,
        i.slice(n - t + 1, n + t + 2).addClass("slick-active").attr("aria-hidden", "false")),
        0 === e ? i.eq(i.length - 1 - o.options.slidesToShow).addClass("slick-center") : e === o.slideCount - 1 && i.eq(o.options.slidesToShow).addClass("slick-center")),
        o.$slides.eq(e).addClass("slick-center")) : e >= 0 && e <= o.slideCount - o.options.slidesToShow ? o.$slides.slice(e, e + o.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= o.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (s = o.slideCount % o.options.slidesToShow,
        n = o.options.infinite === !0 ? o.options.slidesToShow + e : e, o.options.slidesToShow == o.options.slidesToScroll && o.slideCount - e < o.options.slidesToShow ? i.slice(n - (o.options.slidesToShow - s), n + s).addClass("slick-active").attr("aria-hidden", "false") : i.slice(n, n + o.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")),
        "ondemand" === o.options.lazyLoad && o.lazyLoad();
    }, t.prototype.setupInfinite = function() {
        var t, i, n, s = this;
        if (s.options.fade === !0 && (s.options.centerMode = !1), s.options.infinite === !0 && s.options.fade === !1 && (i = null,
        s.slideCount > s.options.slidesToShow)) {
            for (n = s.options.centerMode === !0 ? s.options.slidesToShow + 1 : s.options.slidesToShow,
            t = s.slideCount; t > s.slideCount - n; t -= 1) i = t - 1, e(s.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i - s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");
            for (t = 0; n > t; t += 1) i = t, e(s.$slides[i]).clone(!0).attr("id", "").attr("data-slick-index", i + s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");
            s.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                e(this).attr("id", "");
            });
        }
    }, t.prototype.interrupt = function(e) {
        var t = this;
        e || t.autoPlay(), t.interrupted = e;
    }, t.prototype.selectHandler = function(t) {
        var i = this, n = e(t.target).is(".slick-slide") ? e(t.target) : e(t.target).parents(".slick-slide"), s = parseInt(n.attr("data-slick-index"));
        return s || (s = 0), i.slideCount <= i.options.slidesToShow ? (i.setSlideClasses(s),
        void i.asNavFor(s)) : void i.slideHandler(s);
    }, t.prototype.slideHandler = function(e, t, i) {
        var n, s, o, r, a, l = null, d = this;
        return t = t || !1, d.animating === !0 && d.options.waitForAnimate === !0 || d.options.fade === !0 && d.currentSlide === e || d.slideCount <= d.options.slidesToShow ? void 0 : (t === !1 && d.asNavFor(e),
        n = e, l = d.getLeft(n), r = d.getLeft(d.currentSlide), d.currentLeft = null === d.swipeLeft ? r : d.swipeLeft,
        d.options.infinite === !1 && d.options.centerMode === !1 && (0 > e || e > d.getDotCount() * d.options.slidesToScroll) ? void (d.options.fade === !1 && (n = d.currentSlide,
        i !== !0 ? d.animateSlide(r, function() {
            d.postSlide(n);
        }) : d.postSlide(n))) : d.options.infinite === !1 && d.options.centerMode === !0 && (0 > e || e > d.slideCount - d.options.slidesToScroll) ? void (d.options.fade === !1 && (n = d.currentSlide,
        i !== !0 ? d.animateSlide(r, function() {
            d.postSlide(n);
        }) : d.postSlide(n))) : (d.options.autoplay && clearInterval(d.autoPlayTimer), s = 0 > n ? d.slideCount % d.options.slidesToScroll !== 0 ? d.slideCount - d.slideCount % d.options.slidesToScroll : d.slideCount + n : n >= d.slideCount ? d.slideCount % d.options.slidesToScroll !== 0 ? 0 : n - d.slideCount : n,
        d.animating = !0, d.$slider.trigger("beforeChange", [ d, d.currentSlide, s ]), o = d.currentSlide,
        d.currentSlide = s, d.setSlideClasses(d.currentSlide), d.options.asNavFor && (a = d.getNavTarget(),
        a = a.slick("getSlick"), a.slideCount <= a.options.slidesToShow && a.setSlideClasses(d.currentSlide)),
        d.updateDots(), d.updateArrows(), d.options.fade === !0 ? (i !== !0 ? (d.fadeSlideOut(o),
        d.fadeSlide(s, function() {
            d.postSlide(s);
        })) : d.postSlide(s), void d.animateHeight()) : void (i !== !0 ? d.animateSlide(l, function() {
            d.postSlide(s);
        }) : d.postSlide(s))));
    }, t.prototype.startLoad = function() {
        var e = this;
        e.options.arrows === !0 && e.slideCount > e.options.slidesToShow && (e.$prevArrow.hide(),
        e.$nextArrow.hide()), e.options.dots === !0 && e.slideCount > e.options.slidesToShow && e.$dots.hide(),
        e.$slider.addClass("slick-loading");
    }, t.prototype.swipeDirection = function() {
        var e, t, i, n, s = this;
        return e = s.touchObject.startX - s.touchObject.curX, t = s.touchObject.startY - s.touchObject.curY,
        i = Math.atan2(t, e), n = Math.round(180 * i / Math.PI), 0 > n && (n = 360 - Math.abs(n)),
        45 >= n && n >= 0 ? s.options.rtl === !1 ? "left" : "right" : 360 >= n && n >= 315 ? s.options.rtl === !1 ? "left" : "right" : n >= 135 && 225 >= n ? s.options.rtl === !1 ? "right" : "left" : s.options.verticalSwiping === !0 ? n >= 35 && 135 >= n ? "down" : "up" : "vertical";
    }, t.prototype.swipeEnd = function(e) {
        var t, i, n = this;
        if (n.dragging = !1, n.interrupted = !1, n.shouldClick = !(n.touchObject.swipeLength > 10),
        void 0 === n.touchObject.curX) return !1;
        if (n.touchObject.edgeHit === !0 && n.$slider.trigger("edge", [ n, n.swipeDirection() ]),
        n.touchObject.swipeLength >= n.touchObject.minSwipe) {
            switch (i = n.swipeDirection()) {
              case "left":
              case "down":
                t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide + n.getSlideCount()) : n.currentSlide + n.getSlideCount(),
                n.currentDirection = 0;
                break;

              case "right":
              case "up":
                t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide - n.getSlideCount()) : n.currentSlide - n.getSlideCount(),
                n.currentDirection = 1;
            }
            "vertical" != i && (n.slideHandler(t), n.touchObject = {}, n.$slider.trigger("swipe", [ n, i ]));
        } else n.touchObject.startX !== n.touchObject.curX && (n.slideHandler(n.currentSlide),
        n.touchObject = {});
    }, t.prototype.swipeHandler = function(e) {
        var t = this;
        if (!(t.options.swipe === !1 || "ontouchend" in document && t.options.swipe === !1 || t.options.draggable === !1 && -1 !== e.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = e.originalEvent && void 0 !== e.originalEvent.touches ? e.originalEvent.touches.length : 1,
        t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, t.options.verticalSwiping === !0 && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold),
        e.data.action) {
          case "start":
            t.swipeStart(e);
            break;

          case "move":
            t.swipeMove(e);
            break;

          case "end":
            t.swipeEnd(e);
        }
    }, t.prototype.swipeMove = function(e) {
        var t, i, n, s, o, r = this;
        return o = void 0 !== e.originalEvent ? e.originalEvent.touches : null, !(!r.dragging || o && 1 !== o.length) && (t = r.getLeft(r.currentSlide),
        r.touchObject.curX = void 0 !== o ? o[0].pageX : e.clientX, r.touchObject.curY = void 0 !== o ? o[0].pageY : e.clientY,
        r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curX - r.touchObject.startX, 2))),
        r.options.verticalSwiping === !0 && (r.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(r.touchObject.curY - r.touchObject.startY, 2)))),
        i = r.swipeDirection(), "vertical" !== i ? (void 0 !== e.originalEvent && r.touchObject.swipeLength > 4 && e.preventDefault(),
        s = (r.options.rtl === !1 ? 1 : -1) * (r.touchObject.curX > r.touchObject.startX ? 1 : -1),
        r.options.verticalSwiping === !0 && (s = r.touchObject.curY > r.touchObject.startY ? 1 : -1),
        n = r.touchObject.swipeLength, r.touchObject.edgeHit = !1, r.options.infinite === !1 && (0 === r.currentSlide && "right" === i || r.currentSlide >= r.getDotCount() && "left" === i) && (n = r.touchObject.swipeLength * r.options.edgeFriction,
        r.touchObject.edgeHit = !0), r.options.vertical === !1 ? r.swipeLeft = t + n * s : r.swipeLeft = t + n * (r.$list.height() / r.listWidth) * s,
        r.options.verticalSwiping === !0 && (r.swipeLeft = t + n * s), r.options.fade !== !0 && r.options.touchMove !== !1 && (r.animating === !0 ? (r.swipeLeft = null,
        !1) : void r.setCSS(r.swipeLeft))) : void 0);
    }, t.prototype.swipeStart = function(e) {
        var t, i = this;
        return i.interrupted = !0, 1 !== i.touchObject.fingerCount || i.slideCount <= i.options.slidesToShow ? (i.touchObject = {},
        !1) : (void 0 !== e.originalEvent && void 0 !== e.originalEvent.touches && (t = e.originalEvent.touches[0]),
        i.touchObject.startX = i.touchObject.curX = void 0 !== t ? t.pageX : e.clientX,
        i.touchObject.startY = i.touchObject.curY = void 0 !== t ? t.pageY : e.clientY,
        void (i.dragging = !0));
    }, t.prototype.unfilterSlides = t.prototype.slickUnfilter = function() {
        var e = this;
        null !== e.$slidesCache && (e.unload(), e.$slideTrack.children(this.options.slide).detach(),
        e.$slidesCache.appendTo(e.$slideTrack), e.reinit());
    }, t.prototype.unload = function() {
        var t = this;
        e(".slick-cloned", t.$slider).remove(), t.$dots && t.$dots.remove(), t.$prevArrow && t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove(),
        t.$nextArrow && t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove(), t.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "");
    }, t.prototype.unslick = function(e) {
        var t = this;
        t.$slider.trigger("unslick", [ t, e ]), t.destroy();
    }, t.prototype.updateArrows = function() {
        var e, t = this;
        e = Math.floor(t.options.slidesToShow / 2), t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && !t.options.infinite && (t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"),
        t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === t.currentSlide ? (t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - t.options.slidesToShow && t.options.centerMode === !1 ? (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - 1 && t.options.centerMode === !0 && (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")));
    }, t.prototype.updateDots = function() {
        var e = this;
        null !== e.$dots && (e.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"),
        e.$dots.find("li").eq(Math.floor(e.currentSlide / e.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"));
    }, t.prototype.visibility = function() {
        var e = this;
        e.options.autoplay && (document[e.hidden] ? e.interrupted = !0 : e.interrupted = !1);
    }, e.fn.slick = function() {
        var e, i, n = this, s = arguments[0], o = Array.prototype.slice.call(arguments, 1), r = n.length;
        for (e = 0; r > e; e++) if ("object" == typeof s || void 0 === s ? n[e].slick = new t(n[e], s) : i = n[e].slick[s].apply(n[e].slick, o),
        void 0 !== i) return i;
        return n;
    };
}), function(e) {
    "function" == typeof define && define.amd ? define([ "jquery" ], e) : e("object" == typeof exports ? require("jquery") : jQuery);
}(function(e) {
    var t = function() {
        if (e && e.fn && e.fn.select2 && e.fn.select2.amd) var t = e.fn.select2.amd;
        var t;
        return function() {
            if (!t || !t.requirejs) {
                t ? i = t : t = {};
                var e, i, n;
                !function(t) {
                    function s(e, t) {
                        return w.call(e, t);
                    }
                    function o(e, t) {
                        var i, n, s, o, r, a, l, d, c, u, p, h = t && t.split("/"), f = y.map, v = f && f["*"] || {};
                        if (e && "." === e.charAt(0)) if (t) {
                            for (e = e.split("/"), r = e.length - 1, y.nodeIdCompat && _.test(e[r]) && (e[r] = e[r].replace(_, "")),
                            e = h.slice(0, h.length - 1).concat(e), c = 0; c < e.length; c += 1) if (p = e[c],
                            "." === p) e.splice(c, 1), c -= 1; else if (".." === p) {
                                if (1 === c && (".." === e[2] || ".." === e[0])) break;
                                c > 0 && (e.splice(c - 1, 2), c -= 2);
                            }
                            e = e.join("/");
                        } else 0 === e.indexOf("./") && (e = e.substring(2));
                        if ((h || v) && f) {
                            for (i = e.split("/"), c = i.length; c > 0; c -= 1) {
                                if (n = i.slice(0, c).join("/"), h) for (u = h.length; u > 0; u -= 1) if (s = f[h.slice(0, u).join("/")],
                                s && (s = s[n])) {
                                    o = s, a = c;
                                    break;
                                }
                                if (o) break;
                                !l && v && v[n] && (l = v[n], d = c);
                            }
                            !o && l && (o = l, a = d), o && (i.splice(0, a, o), e = i.join("/"));
                        }
                        return e;
                    }
                    function r(e, i) {
                        return function() {
                            var n = $.call(arguments, 0);
                            return "string" != typeof n[0] && 1 === n.length && n.push(null), h.apply(t, n.concat([ e, i ]));
                        };
                    }
                    function a(e) {
                        return function(t) {
                            return o(t, e);
                        };
                    }
                    function l(e) {
                        return function(t) {
                            g[e] = t;
                        };
                    }
                    function d(e) {
                        if (s(m, e)) {
                            var i = m[e];
                            delete m[e], b[e] = !0, p.apply(t, i);
                        }
                        if (!s(g, e) && !s(b, e)) throw Error("No " + e);
                        return g[e];
                    }
                    function c(e) {
                        var t, i = e ? e.indexOf("!") : -1;
                        return i > -1 && (t = e.substring(0, i), e = e.substring(i + 1, e.length)), [ t, e ];
                    }
                    function u(e) {
                        return function() {
                            return y && y.config && y.config[e] || {};
                        };
                    }
                    var p, h, f, v, g = {}, m = {}, y = {}, b = {}, w = Object.prototype.hasOwnProperty, $ = [].slice, _ = /\.js$/;
                    f = function(e, t) {
                        var i, n = c(e), s = n[0];
                        return e = n[1], s && (s = o(s, t), i = d(s)), s ? e = i && i.normalize ? i.normalize(e, a(t)) : o(e, t) : (e = o(e, t),
                        n = c(e), s = n[0], e = n[1], s && (i = d(s))), {
                            f: s ? s + "!" + e : e,
                            n: e,
                            pr: s,
                            p: i
                        };
                    }, v = {
                        require: function(e) {
                            return r(e);
                        },
                        exports: function(e) {
                            var t = g[e];
                            return void 0 !== t ? t : g[e] = {};
                        },
                        module: function(e) {
                            return {
                                id: e,
                                uri: "",
                                exports: g[e],
                                config: u(e)
                            };
                        }
                    }, p = function(e, i, n, o) {
                        var a, c, u, p, h, y, w = [], $ = typeof n;
                        if (o = o || e, "undefined" === $ || "function" === $) {
                            for (i = !i.length && n.length ? [ "require", "exports", "module" ] : i, h = 0; h < i.length; h += 1) if (p = f(i[h], o),
                            c = p.f, "require" === c) w[h] = v.require(e); else if ("exports" === c) w[h] = v.exports(e),
                            y = !0; else if ("module" === c) a = w[h] = v.module(e); else if (s(g, c) || s(m, c) || s(b, c)) w[h] = d(c); else {
                                if (!p.p) throw Error(e + " missing " + c);
                                p.p.load(p.n, r(o, !0), l(c), {}), w[h] = g[c];
                            }
                            u = n ? n.apply(g[e], w) : void 0, e && (a && a.exports !== t && a.exports !== g[e] ? g[e] = a.exports : u === t && y || (g[e] = u));
                        } else e && (g[e] = n);
                    }, e = i = h = function(e, i, n, s, o) {
                        if ("string" == typeof e) return v[e] ? v[e](i) : d(f(e, i).f);
                        if (!e.splice) {
                            if (y = e, y.deps && h(y.deps, y.callback), !i) return;
                            i.splice ? (e = i, i = n, n = null) : e = t;
                        }
                        return i = i || function() {}, "function" == typeof n && (n = s, s = o), s ? p(t, e, i, n) : setTimeout(function() {
                            p(t, e, i, n);
                        }, 4), h;
                    }, h.config = function(e) {
                        return h(e);
                    }, e._defined = g, n = function(e, t, i) {
                        if ("string" != typeof e) throw Error("See almond README: incorrect module build, no module name");
                        t.splice || (i = t, t = []), s(g, e) || s(m, e) || (m[e] = [ e, t, i ]);
                    }, n.amd = {
                        jQuery: !0
                    };
                }(), t.requirejs = e, t.require = i, t.define = n;
            }
        }(), t.define("almond", function() {}), t.define("jquery", [], function() {
            var t = e || $;
            return null == t && console && console.error && console.error("Select2: An instance of jQuery or a jQuery-compatible library was not found. Make sure that you are including jQuery before Select2 on your web page."),
            t;
        }), t.define("select2/utils", [ "jquery" ], function(e) {
            function t(e) {
                var t = e.prototype, i = [];
                for (var n in t) {
                    var s = t[n];
                    "function" == typeof s && "constructor" !== n && i.push(n);
                }
                return i;
            }
            var i = {};
            i.Extend = function(e, t) {
                function i() {
                    this.constructor = e;
                }
                var n = {}.hasOwnProperty;
                for (var s in t) n.call(t, s) && (e[s] = t[s]);
                return i.prototype = t.prototype, e.prototype = new i(), e.__super__ = t.prototype,
                e;
            }, i.Decorate = function(e, i) {
                function n() {
                    var t = Array.prototype.unshift, n = i.prototype.constructor.length, s = e.prototype.constructor;
                    n > 0 && (t.call(arguments, e.prototype.constructor), s = i.prototype.constructor),
                    s.apply(this, arguments);
                }
                function s() {
                    this.constructor = n;
                }
                var o = t(i), r = t(e);
                i.displayName = e.displayName, n.prototype = new s();
                for (var a = 0; a < r.length; a++) {
                    var l = r[a];
                    n.prototype[l] = e.prototype[l];
                }
                for (var d = (function(e) {
                    var t = function() {};
                    e in n.prototype && (t = n.prototype[e]);
                    var s = i.prototype[e];
                    return function() {
                        var e = Array.prototype.unshift;
                        return e.call(arguments, t), s.apply(this, arguments);
                    };
                }), c = 0; c < o.length; c++) {
                    var u = o[c];
                    n.prototype[u] = d(u);
                }
                return n;
            };
            var n = function() {
                this.listeners = {};
            };
            return n.prototype.on = function(e, t) {
                this.listeners = this.listeners || {}, e in this.listeners ? this.listeners[e].push(t) : this.listeners[e] = [ t ];
            }, n.prototype.trigger = function(e) {
                var t = Array.prototype.slice, i = t.call(arguments, 1);
                this.listeners = this.listeners || {}, null == i && (i = []), 0 === i.length && i.push({}),
                i[0]._type = e, e in this.listeners && this.invoke(this.listeners[e], t.call(arguments, 1)),
                "*" in this.listeners && this.invoke(this.listeners["*"], arguments);
            }, n.prototype.invoke = function(e, t) {
                for (var i = 0, n = e.length; n > i; i++) e[i].apply(this, t);
            }, i.Observable = n, i.generateChars = function(e) {
                for (var t = "", i = 0; e > i; i++) {
                    var n = Math.floor(36 * Math.random());
                    t += n.toString(36);
                }
                return t;
            }, i.bind = function(e, t) {
                return function() {
                    e.apply(t, arguments);
                };
            }, i._convertData = function(e) {
                for (var t in e) {
                    var i = t.split("-"), n = e;
                    if (1 !== i.length) {
                        for (var s = 0; s < i.length; s++) {
                            var o = i[s];
                            o = o.substring(0, 1).toLowerCase() + o.substring(1), o in n || (n[o] = {}), s == i.length - 1 && (n[o] = e[t]),
                            n = n[o];
                        }
                        delete e[t];
                    }
                }
                return e;
            }, i.hasScroll = function(t, i) {
                var n = e(i), s = i.style.overflowX, o = i.style.overflowY;
                return (s !== o || "hidden" !== o && "visible" !== o) && ("scroll" === s || "scroll" === o || n.innerHeight() < i.scrollHeight || n.innerWidth() < i.scrollWidth);
            }, i.escapeMarkup = function(e) {
                var t = {
                    "\\": "&#92;",
                    "&": "&amp;",
                    "<": "&lt;",
                    ">": "&gt;",
                    '"': "&quot;",
                    "'": "&#39;",
                    "/": "&#47;"
                };
                return "string" != typeof e ? e : (e + "").replace(/[&<>"'\/\\]/g, function(e) {
                    return t[e];
                });
            }, i.appendMany = function(t, i) {
                if ("1.7" === e.fn.jquery.substr(0, 3)) {
                    var n = e();
                    e.map(i, function(e) {
                        n = n.add(e);
                    }), i = n;
                }
                t.append(i);
            }, i;
        }), t.define("select2/results", [ "jquery", "./utils" ], function(e, t) {
            function i(e, t, n) {
                this.$element = e, this.data = n, this.options = t, i.__super__.constructor.call(this);
            }
            return t.Extend(i, t.Observable), i.prototype.render = function() {
                var t = e('<ul class="select2-results__options" role="tree"></ul>');
                return this.options.get("multiple") && t.attr("aria-multiselectable", "true"), this.$results = t,
                t;
            }, i.prototype.clear = function() {
                this.$results.empty();
            }, i.prototype.displayMessage = function(t) {
                var i = this.options.get("escapeMarkup");
                this.clear(), this.hideLoading();
                var n = e('<li role="treeitem" aria-live="assertive" class="select2-results__option"></li>'), s = this.options.get("translations").get(t.message);
                n.append(i(s(t.args))), n[0].className += " select2-results__message", this.$results.append(n);
            }, i.prototype.hideMessages = function() {
                this.$results.find(".select2-results__message").remove();
            }, i.prototype.append = function(e) {
                this.hideLoading();
                var t = [];
                if (null == e.results || 0 === e.results.length) return void (0 === this.$results.children().length && this.trigger("results:message", {
                    message: "noResults"
                }));
                e.results = this.sort(e.results);
                for (var i = 0; i < e.results.length; i++) {
                    var n = e.results[i], s = this.option(n);
                    t.push(s);
                }
                this.$results.append(t);
            }, i.prototype.position = function(e, t) {
                var i = t.find(".select2-results");
                i.append(e);
            }, i.prototype.sort = function(e) {
                var t = this.options.get("sorter");
                return t(e);
            }, i.prototype.highlightFirstItem = function() {
                var e = this.$results.find(".select2-results__option[aria-selected]"), t = e.filter("[aria-selected=true]");
                t.length > 0 ? t.first().trigger("mouseenter") : e.first().trigger("mouseenter"),
                this.ensureHighlightVisible();
            }, i.prototype.setClasses = function() {
                var t = this;
                this.data.current(function(i) {
                    var n = e.map(i, function(e) {
                        return "" + e.id;
                    }), s = t.$results.find(".select2-results__option[aria-selected]");
                    s.each(function() {
                        var t = e(this), i = e.data(this, "data"), s = "" + i.id;
                        null != i.element && i.element.selected || null == i.element && e.inArray(s, n) > -1 ? t.attr("aria-selected", "true") : t.attr("aria-selected", "false");
                    });
                });
            }, i.prototype.showLoading = function(e) {
                this.hideLoading();
                var t = this.options.get("translations").get("searching"), i = {
                    disabled: !0,
                    loading: !0,
                    text: t(e)
                }, n = this.option(i);
                n.className += " loading-results", this.$results.prepend(n);
            }, i.prototype.hideLoading = function() {
                this.$results.find(".loading-results").remove();
            }, i.prototype.option = function(t) {
                var i = document.createElement("li");
                i.className = "select2-results__option";
                var n = {
                    role: "treeitem",
                    "aria-selected": "false"
                };
                t.disabled && (delete n["aria-selected"], n["aria-disabled"] = "true"), null == t.id && delete n["aria-selected"],
                null != t._resultId && (i.id = t._resultId), t.title && (i.title = t.title), t.children && (n.role = "group",
                n["aria-label"] = t.text, delete n["aria-selected"]);
                for (var s in n) {
                    var o = n[s];
                    i.setAttribute(s, o);
                }
                if (t.children) {
                    var r = e(i), a = document.createElement("strong");
                    a.className = "select2-results__group", e(a), this.template(t, a);
                    for (var l = [], d = 0; d < t.children.length; d++) {
                        var c = t.children[d], u = this.option(c);
                        l.push(u);
                    }
                    var p = e("<ul></ul>", {
                        "class": "select2-results__options select2-results__options--nested"
                    });
                    p.append(l), r.append(a), r.append(p);
                } else this.template(t, i);
                return e.data(i, "data", t), i;
            }, i.prototype.bind = function(t, i) {
                var n = this, s = t.id + "-results";
                this.$results.attr("id", s), t.on("results:all", function(e) {
                    n.clear(), n.append(e.data), t.isOpen() && (n.setClasses(), n.highlightFirstItem());
                }), t.on("results:append", function(e) {
                    n.append(e.data), t.isOpen() && n.setClasses();
                }), t.on("query", function(e) {
                    n.hideMessages(), n.showLoading(e);
                }), t.on("select", function() {
                    t.isOpen() && (n.setClasses(), n.highlightFirstItem());
                }), t.on("unselect", function() {
                    t.isOpen() && (n.setClasses(), n.highlightFirstItem());
                }), t.on("open", function() {
                    n.$results.attr("aria-expanded", "true"), n.$results.attr("aria-hidden", "false"),
                    n.setClasses(), n.ensureHighlightVisible();
                }), t.on("close", function() {
                    n.$results.attr("aria-expanded", "false"), n.$results.attr("aria-hidden", "true"),
                    n.$results.removeAttr("aria-activedescendant");
                }), t.on("results:toggle", function() {
                    var e = n.getHighlightedResults();
                    0 !== e.length && e.trigger("mouseup");
                }), t.on("results:select", function() {
                    var e = n.getHighlightedResults();
                    if (0 !== e.length) {
                        var t = e.data("data");
                        "true" == e.attr("aria-selected") ? n.trigger("close", {}) : n.trigger("select", {
                            data: t
                        });
                    }
                }), t.on("results:previous", function() {
                    var e = n.getHighlightedResults(), t = n.$results.find("[aria-selected]"), i = t.index(e);
                    if (0 !== i) {
                        var s = i - 1;
                        0 === e.length && (s = 0);
                        var o = t.eq(s);
                        o.trigger("mouseenter");
                        var r = n.$results.offset().top, a = o.offset().top, l = n.$results.scrollTop() + (a - r);
                        0 === s ? n.$results.scrollTop(0) : 0 > a - r && n.$results.scrollTop(l);
                    }
                }), t.on("results:next", function() {
                    var e = n.getHighlightedResults(), t = n.$results.find("[aria-selected]"), i = t.index(e), s = i + 1;
                    if (!(s >= t.length)) {
                        var o = t.eq(s);
                        o.trigger("mouseenter");
                        var r = n.$results.offset().top + n.$results.outerHeight(!1), a = o.offset().top + o.outerHeight(!1), l = n.$results.scrollTop() + a - r;
                        0 === s ? n.$results.scrollTop(0) : a > r && n.$results.scrollTop(l);
                    }
                }), t.on("results:focus", function(e) {
                    e.element.addClass("select2-results__option--highlighted");
                }), t.on("results:message", function(e) {
                    n.displayMessage(e);
                }), e.fn.mousewheel && this.$results.on("mousewheel", function(e) {
                    var t = n.$results.scrollTop(), i = n.$results.get(0).scrollHeight - t + e.deltaY, s = e.deltaY > 0 && t - e.deltaY <= 0, o = e.deltaY < 0 && i <= n.$results.height();
                    s ? (n.$results.scrollTop(0), e.preventDefault(), e.stopPropagation()) : o && (n.$results.scrollTop(n.$results.get(0).scrollHeight - n.$results.height()),
                    e.preventDefault(), e.stopPropagation());
                }), this.$results.on("mouseup", ".select2-results__option[aria-selected]", function(t) {
                    var i = e(this), s = i.data("data");
                    return "true" === i.attr("aria-selected") ? void (n.options.get("multiple") ? n.trigger("unselect", {
                        originalEvent: t,
                        data: s
                    }) : n.trigger("close", {})) : void n.trigger("select", {
                        originalEvent: t,
                        data: s
                    });
                }), this.$results.on("mouseenter", ".select2-results__option[aria-selected]", function(t) {
                    var i = e(this).data("data");
                    n.getHighlightedResults().removeClass("select2-results__option--highlighted"), n.trigger("results:focus", {
                        data: i,
                        element: e(this)
                    });
                });
            }, i.prototype.getHighlightedResults = function() {
                var e = this.$results.find(".select2-results__option--highlighted");
                return e;
            }, i.prototype.destroy = function() {
                this.$results.remove();
            }, i.prototype.ensureHighlightVisible = function() {
                var e = this.getHighlightedResults();
                if (0 !== e.length) {
                    var t = this.$results.find("[aria-selected]"), i = t.index(e), n = this.$results.offset().top, s = e.offset().top, o = this.$results.scrollTop() + (s - n), r = s - n;
                    o -= 2 * e.outerHeight(!1), 2 >= i ? this.$results.scrollTop(0) : (r > this.$results.outerHeight() || 0 > r) && this.$results.scrollTop(o);
                }
            }, i.prototype.template = function(t, i) {
                var n = this.options.get("templateResult"), s = this.options.get("escapeMarkup"), o = n(t, i);
                null == o ? i.style.display = "none" : "string" == typeof o ? i.innerHTML = s(o) : e(i).append(o);
            }, i;
        }), t.define("select2/keys", [], function() {
            var e = {
                BACKSPACE: 8,
                TAB: 9,
                ENTER: 13,
                SHIFT: 16,
                CTRL: 17,
                ALT: 18,
                ESC: 27,
                SPACE: 32,
                PAGE_UP: 33,
                PAGE_DOWN: 34,
                END: 35,
                HOME: 36,
                LEFT: 37,
                UP: 38,
                RIGHT: 39,
                DOWN: 40,
                DELETE: 46
            };
            return e;
        }), t.define("select2/selection/base", [ "jquery", "../utils", "../keys" ], function(e, t, i) {
            function n(e, t) {
                this.$element = e, this.options = t, n.__super__.constructor.call(this);
            }
            return t.Extend(n, t.Observable), n.prototype.render = function() {
                var t = e('<span class="select2-selection" role="combobox"  aria-haspopup="true" aria-expanded="false"></span>');
                return this._tabindex = 0, null != this.$element.data("old-tabindex") ? this._tabindex = this.$element.data("old-tabindex") : null != this.$element.attr("tabindex") && (this._tabindex = this.$element.attr("tabindex")),
                t.attr("title", this.$element.attr("title")), t.attr("tabindex", this._tabindex),
                this.$selection = t, t;
            }, n.prototype.bind = function(e, t) {
                var n = this, s = (e.id + "-container", e.id + "-results");
                this.container = e, this.$selection.on("focus", function(e) {
                    n.trigger("focus", e);
                }), this.$selection.on("blur", function(e) {
                    n._handleBlur(e);
                }), this.$selection.on("keydown", function(e) {
                    n.trigger("keypress", e), e.which === i.SPACE && e.preventDefault();
                }), e.on("results:focus", function(e) {
                    n.$selection.attr("aria-activedescendant", e.data._resultId);
                }), e.on("selection:update", function(e) {
                    n.update(e.data);
                }), e.on("open", function() {
                    n.$selection.attr("aria-expanded", "true"), n.$selection.attr("aria-owns", s), n._attachCloseHandler(e);
                }), e.on("close", function() {
                    n.$selection.attr("aria-expanded", "false"), n.$selection.removeAttr("aria-activedescendant"),
                    n.$selection.removeAttr("aria-owns"), n.$selection.focus(), n._detachCloseHandler(e);
                }), e.on("enable", function() {
                    n.$selection.attr("tabindex", n._tabindex);
                }), e.on("disable", function() {
                    n.$selection.attr("tabindex", "-1");
                });
            }, n.prototype._handleBlur = function(t) {
                var i = this;
                window.setTimeout(function() {
                    document.activeElement == i.$selection[0] || e.contains(i.$selection[0], document.activeElement) || i.trigger("blur", t);
                }, 1);
            }, n.prototype._attachCloseHandler = function(t) {
                e(document.body).on("mousedown.select2." + t.id, function(t) {
                    var i = e(t.target), n = i.closest(".select2"), s = e(".select2.select2-container--open");
                    s.each(function() {
                        var t = e(this);
                        if (this != n[0]) {
                            var i = t.data("element");
                            i.select2("close");
                        }
                    });
                });
            }, n.prototype._detachCloseHandler = function(t) {
                e(document.body).off("mousedown.select2." + t.id);
            }, n.prototype.position = function(e, t) {
                var i = t.find(".selection");
                i.append(e);
            }, n.prototype.destroy = function() {
                this._detachCloseHandler(this.container);
            }, n.prototype.update = function(e) {
                throw Error("The `update` method must be defined in child classes.");
            }, n;
        }), t.define("select2/selection/single", [ "jquery", "./base", "../utils", "../keys" ], function(e, t, i, n) {
            function s() {
                s.__super__.constructor.apply(this, arguments);
            }
            return i.Extend(s, t), s.prototype.render = function() {
                var e = s.__super__.render.call(this);
                return e.addClass("select2-selection--single"), e.html('<span class="select2-selection__rendered"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'),
                e;
            }, s.prototype.bind = function(e, t) {
                var i = this;
                s.__super__.bind.apply(this, arguments);
                var n = e.id + "-container";
                this.$selection.find(".select2-selection__rendered").attr("id", n), this.$selection.attr("aria-labelledby", n),
                this.$selection.on("mousedown", function(e) {
                    1 === e.which && i.trigger("toggle", {
                        originalEvent: e
                    });
                }), this.$selection.on("focus", function(e) {}), this.$selection.on("blur", function(e) {}),
                e.on("focus", function(t) {
                    e.isOpen() || i.$selection.focus();
                }), e.on("selection:update", function(e) {
                    i.update(e.data);
                });
            }, s.prototype.clear = function() {
                this.$selection.find(".select2-selection__rendered").empty();
            }, s.prototype.display = function(e, t) {
                var i = this.options.get("templateSelection"), n = this.options.get("escapeMarkup");
                return n(i(e, t));
            }, s.prototype.selectionContainer = function() {
                return e("<span></span>");
            }, s.prototype.update = function(e) {
                if (0 === e.length) return void this.clear();
                var t = e[0], i = this.$selection.find(".select2-selection__rendered"), n = this.display(t, i);
                i.empty().append(n), i.prop("title", t.title || t.text);
            }, s;
        }), t.define("select2/selection/multiple", [ "jquery", "./base", "../utils" ], function(e, t, i) {
            function n(e, t) {
                n.__super__.constructor.apply(this, arguments);
            }
            return i.Extend(n, t), n.prototype.render = function() {
                var e = n.__super__.render.call(this);
                return e.addClass("select2-selection--multiple"), e.html('<ul class="select2-selection__rendered"></ul>'),
                e;
            }, n.prototype.bind = function(t, i) {
                var s = this;
                n.__super__.bind.apply(this, arguments), this.$selection.on("click", function(e) {
                    s.trigger("toggle", {
                        originalEvent: e
                    });
                }), this.$selection.on("click", ".select2-selection__choice__remove", function(t) {
                    if (!s.options.get("disabled")) {
                        var i = e(this), n = i.parent(), o = n.data("data");
                        s.trigger("unselect", {
                            originalEvent: t,
                            data: o
                        });
                    }
                });
            }, n.prototype.clear = function() {
                this.$selection.find(".select2-selection__rendered").empty();
            }, n.prototype.display = function(e, t) {
                var i = this.options.get("templateSelection"), n = this.options.get("escapeMarkup");
                return n(i(e, t));
            }, n.prototype.selectionContainer = function() {
                var t = e('<li class="select2-selection__choice"><span class="select2-selection__choice__remove" role="presentation">&times;</span></li>');
                return t;
            }, n.prototype.update = function(e) {
                if (this.clear(), 0 !== e.length) {
                    for (var t = [], n = 0; n < e.length; n++) {
                        var s = e[n], o = this.selectionContainer(), r = this.display(s, o);
                        o.append(r), o.prop("title", s.title || s.text), o.data("data", s), t.push(o);
                    }
                    var a = this.$selection.find(".select2-selection__rendered");
                    i.appendMany(a, t);
                }
            }, n;
        }), t.define("select2/selection/placeholder", [ "../utils" ], function(e) {
            function t(e, t, i) {
                this.placeholder = this.normalizePlaceholder(i.get("placeholder")), e.call(this, t, i);
            }
            return t.prototype.normalizePlaceholder = function(e, t) {
                return "string" == typeof t && (t = {
                    id: "",
                    text: t
                }), t;
            }, t.prototype.createPlaceholder = function(e, t) {
                var i = this.selectionContainer();
                return i.html(this.display(t)), i.addClass("select2-selection__placeholder").removeClass("select2-selection__choice"),
                i;
            }, t.prototype.update = function(e, t) {
                var i = 1 == t.length && t[0].id != this.placeholder.id, n = t.length > 1;
                if (n || i) return e.call(this, t);
                this.clear();
                var s = this.createPlaceholder(this.placeholder);
                this.$selection.find(".select2-selection__rendered").append(s);
            }, t;
        }), t.define("select2/selection/allowClear", [ "jquery", "../keys" ], function(e, t) {
            function i() {}
            return i.prototype.bind = function(e, t, i) {
                var n = this;
                e.call(this, t, i), null == this.placeholder && this.options.get("debug") && window.console && console.error && console.error("Select2: The `allowClear` option should be used in combination with the `placeholder` option."),
                this.$selection.on("mousedown", ".select2-selection__clear", function(e) {
                    n._handleClear(e);
                }), t.on("keypress", function(e) {
                    n._handleKeyboardClear(e, t);
                });
            }, i.prototype._handleClear = function(e, t) {
                if (!this.options.get("disabled")) {
                    var i = this.$selection.find(".select2-selection__clear");
                    if (0 !== i.length) {
                        t.stopPropagation();
                        for (var n = i.data("data"), s = 0; s < n.length; s++) {
                            var o = {
                                data: n[s]
                            };
                            if (this.trigger("unselect", o), o.prevented) return;
                        }
                        this.$element.val(this.placeholder.id).trigger("change"), this.trigger("toggle", {});
                    }
                }
            }, i.prototype._handleKeyboardClear = function(e, i, n) {
                n.isOpen() || i.which != t.DELETE && i.which != t.BACKSPACE || this._handleClear(i);
            }, i.prototype.update = function(t, i) {
                if (t.call(this, i), !(this.$selection.find(".select2-selection__placeholder").length > 0 || 0 === i.length)) {
                    var n = e('<span class="select2-selection__clear">&times;</span>');
                    n.data("data", i), this.$selection.find(".select2-selection__rendered").prepend(n);
                }
            }, i;
        }), t.define("select2/selection/search", [ "jquery", "../utils", "../keys" ], function(e, t, i) {
            function n(e, t, i) {
                e.call(this, t, i);
            }
            return n.prototype.render = function(t) {
                var i = e('<li class="select2-search select2-search--inline"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" aria-autocomplete="list" /></li>');
                this.$searchContainer = i, this.$search = i.find("input");
                var n = t.call(this);
                return this._transferTabIndex(), n;
            }, n.prototype.bind = function(e, t, n) {
                var s = this;
                e.call(this, t, n), t.on("open", function() {
                    s.$search.trigger("focus");
                }), t.on("close", function() {
                    s.$search.val(""), s.$search.removeAttr("aria-activedescendant"), s.$search.trigger("focus");
                }), t.on("enable", function() {
                    s.$search.prop("disabled", !1), s._transferTabIndex();
                }), t.on("disable", function() {
                    s.$search.prop("disabled", !0);
                }), t.on("focus", function(e) {
                    s.$search.trigger("focus");
                }), t.on("results:focus", function(e) {
                    s.$search.attr("aria-activedescendant", e.id);
                }), this.$selection.on("focusin", ".select2-search--inline", function(e) {
                    s.trigger("focus", e);
                }), this.$selection.on("focusout", ".select2-search--inline", function(e) {
                    s._handleBlur(e);
                }), this.$selection.on("keydown", ".select2-search--inline", function(e) {
                    e.stopPropagation(), s.trigger("keypress", e), s._keyUpPrevented = e.isDefaultPrevented();
                    var t = e.which;
                    if (t === i.BACKSPACE && "" === s.$search.val()) {
                        var n = s.$searchContainer.prev(".select2-selection__choice");
                        if (n.length > 0) {
                            var o = n.data("data");
                            s.searchRemoveChoice(o), e.preventDefault();
                        }
                    }
                });
                var o = document.documentMode, r = o && 11 >= o;
                this.$selection.on("input.searchcheck", ".select2-search--inline", function(e) {
                    return r ? void s.$selection.off("input.search input.searchcheck") : void s.$selection.off("keyup.search");
                }), this.$selection.on("keyup.search input.search", ".select2-search--inline", function(e) {
                    if (r && "input" === e.type) return void s.$selection.off("input.search input.searchcheck");
                    var t = e.which;
                    t != i.SHIFT && t != i.CTRL && t != i.ALT && t != i.TAB && s.handleSearch(e);
                });
            }, n.prototype._transferTabIndex = function(e) {
                this.$search.attr("tabindex", this.$selection.attr("tabindex")), this.$selection.attr("tabindex", "-1");
            }, n.prototype.createPlaceholder = function(e, t) {
                this.$search.attr("placeholder", t.text);
            }, n.prototype.update = function(e, t) {
                var i = this.$search[0] == document.activeElement;
                this.$search.attr("placeholder", ""), e.call(this, t), this.$selection.find(".select2-selection__rendered").append(this.$searchContainer),
                this.resizeSearch(), i && this.$search.focus();
            }, n.prototype.handleSearch = function() {
                if (this.resizeSearch(), !this._keyUpPrevented) {
                    var e = this.$search.val();
                    this.trigger("query", {
                        term: e
                    });
                }
                this._keyUpPrevented = !1;
            }, n.prototype.searchRemoveChoice = function(e, t) {
                this.trigger("unselect", {
                    data: t
                }), this.$search.val(t.text), this.handleSearch();
            }, n.prototype.resizeSearch = function() {
                this.$search.css("width", "25px");
                var e = "";
                if ("" !== this.$search.attr("placeholder")) e = this.$selection.find(".select2-selection__rendered").innerWidth(); else {
                    var t = this.$search.val().length + 1;
                    e = .75 * t + "em";
                }
                this.$search.css("width", e);
            }, n;
        }), t.define("select2/selection/eventRelay", [ "jquery" ], function(e) {
            function t() {}
            return t.prototype.bind = function(t, i, n) {
                var s = this, o = [ "open", "opening", "close", "closing", "select", "selecting", "unselect", "unselecting" ], r = [ "opening", "closing", "selecting", "unselecting" ];
                t.call(this, i, n), i.on("*", function(t, i) {
                    if (-1 !== e.inArray(t, o)) {
                        i = i || {};
                        var n = e.Event("select2:" + t, {
                            params: i
                        });
                        s.$element.trigger(n), -1 !== e.inArray(t, r) && (i.prevented = n.isDefaultPrevented());
                    }
                });
            }, t;
        }), t.define("select2/translation", [ "jquery", "require" ], function(e, t) {
            function i(e) {
                this.dict = e || {};
            }
            return i.prototype.all = function() {
                return this.dict;
            }, i.prototype.get = function(e) {
                return this.dict[e];
            }, i.prototype.extend = function(t) {
                this.dict = e.extend({}, t.all(), this.dict);
            }, i._cache = {}, i.loadPath = function(e) {
                if (!(e in i._cache)) {
                    var n = t(e);
                    i._cache[e] = n;
                }
                return new i(i._cache[e]);
            }, i;
        }), t.define("select2/diacritics", [], function() {
            var e = {
                "Ⓐ": "A",
                "Ａ": "A",
                "À": "A",
                "Á": "A",
                "Â": "A",
                "Ầ": "A",
                "Ấ": "A",
                "Ẫ": "A",
                "Ẩ": "A",
                "Ã": "A",
                "Ā": "A",
                "Ă": "A",
                "Ằ": "A",
                "Ắ": "A",
                "Ẵ": "A",
                "Ẳ": "A",
                "Ȧ": "A",
                "Ǡ": "A",
                "Ä": "A",
                "Ǟ": "A",
                "Ả": "A",
                "Å": "A",
                "Ǻ": "A",
                "Ǎ": "A",
                "Ȁ": "A",
                "Ȃ": "A",
                "Ạ": "A",
                "Ậ": "A",
                "Ặ": "A",
                "Ḁ": "A",
                "Ą": "A",
                "Ⱥ": "A",
                "Ɐ": "A",
                "Ꜳ": "AA",
                "Æ": "AE",
                "Ǽ": "AE",
                "Ǣ": "AE",
                "Ꜵ": "AO",
                "Ꜷ": "AU",
                "Ꜹ": "AV",
                "Ꜻ": "AV",
                "Ꜽ": "AY",
                "Ⓑ": "B",
                "Ｂ": "B",
                "Ḃ": "B",
                "Ḅ": "B",
                "Ḇ": "B",
                "Ƀ": "B",
                "Ƃ": "B",
                "Ɓ": "B",
                "Ⓒ": "C",
                "Ｃ": "C",
                "Ć": "C",
                "Ĉ": "C",
                "Ċ": "C",
                "Č": "C",
                "Ç": "C",
                "Ḉ": "C",
                "Ƈ": "C",
                "Ȼ": "C",
                "Ꜿ": "C",
                "Ⓓ": "D",
                "Ｄ": "D",
                "Ḋ": "D",
                "Ď": "D",
                "Ḍ": "D",
                "Ḑ": "D",
                "Ḓ": "D",
                "Ḏ": "D",
                "Đ": "D",
                "Ƌ": "D",
                "Ɗ": "D",
                "Ɖ": "D",
                "Ꝺ": "D",
                "Ǳ": "DZ",
                "Ǆ": "DZ",
                "ǲ": "Dz",
                "ǅ": "Dz",
                "Ⓔ": "E",
                "Ｅ": "E",
                "È": "E",
                "É": "E",
                "Ê": "E",
                "Ề": "E",
                "Ế": "E",
                "Ễ": "E",
                "Ể": "E",
                "Ẽ": "E",
                "Ē": "E",
                "Ḕ": "E",
                "Ḗ": "E",
                "Ĕ": "E",
                "Ė": "E",
                "Ë": "E",
                "Ẻ": "E",
                "Ě": "E",
                "Ȅ": "E",
                "Ȇ": "E",
                "Ẹ": "E",
                "Ệ": "E",
                "Ȩ": "E",
                "Ḝ": "E",
                "Ę": "E",
                "Ḙ": "E",
                "Ḛ": "E",
                "Ɛ": "E",
                "Ǝ": "E",
                "Ⓕ": "F",
                "Ｆ": "F",
                "Ḟ": "F",
                "Ƒ": "F",
                "Ꝼ": "F",
                "Ⓖ": "G",
                "Ｇ": "G",
                "Ǵ": "G",
                "Ĝ": "G",
                "Ḡ": "G",
                "Ğ": "G",
                "Ġ": "G",
                "Ǧ": "G",
                "Ģ": "G",
                "Ǥ": "G",
                "Ɠ": "G",
                "Ꞡ": "G",
                "Ᵹ": "G",
                "Ꝿ": "G",
                "Ⓗ": "H",
                "Ｈ": "H",
                "Ĥ": "H",
                "Ḣ": "H",
                "Ḧ": "H",
                "Ȟ": "H",
                "Ḥ": "H",
                "Ḩ": "H",
                "Ḫ": "H",
                "Ħ": "H",
                "Ⱨ": "H",
                "Ⱶ": "H",
                "Ɥ": "H",
                "Ⓘ": "I",
                "Ｉ": "I",
                "Ì": "I",
                "Í": "I",
                "Î": "I",
                "Ĩ": "I",
                "Ī": "I",
                "Ĭ": "I",
                "İ": "I",
                "Ï": "I",
                "Ḯ": "I",
                "Ỉ": "I",
                "Ǐ": "I",
                "Ȉ": "I",
                "Ȋ": "I",
                "Ị": "I",
                "Į": "I",
                "Ḭ": "I",
                "Ɨ": "I",
                "Ⓙ": "J",
                "Ｊ": "J",
                "Ĵ": "J",
                "Ɉ": "J",
                "Ⓚ": "K",
                "Ｋ": "K",
                "Ḱ": "K",
                "Ǩ": "K",
                "Ḳ": "K",
                "Ķ": "K",
                "Ḵ": "K",
                "Ƙ": "K",
                "Ⱪ": "K",
                "Ꝁ": "K",
                "Ꝃ": "K",
                "Ꝅ": "K",
                "Ꞣ": "K",
                "Ⓛ": "L",
                "Ｌ": "L",
                "Ŀ": "L",
                "Ĺ": "L",
                "Ľ": "L",
                "Ḷ": "L",
                "Ḹ": "L",
                "Ļ": "L",
                "Ḽ": "L",
                "Ḻ": "L",
                "Ł": "L",
                "Ƚ": "L",
                "Ɫ": "L",
                "Ⱡ": "L",
                "Ꝉ": "L",
                "Ꝇ": "L",
                "Ꞁ": "L",
                "Ǉ": "LJ",
                "ǈ": "Lj",
                "Ⓜ": "M",
                "Ｍ": "M",
                "Ḿ": "M",
                "Ṁ": "M",
                "Ṃ": "M",
                "Ɱ": "M",
                "Ɯ": "M",
                "Ⓝ": "N",
                "Ｎ": "N",
                "Ǹ": "N",
                "Ń": "N",
                "Ñ": "N",
                "Ṅ": "N",
                "Ň": "N",
                "Ṇ": "N",
                "Ņ": "N",
                "Ṋ": "N",
                "Ṉ": "N",
                "Ƞ": "N",
                "Ɲ": "N",
                "Ꞑ": "N",
                "Ꞥ": "N",
                "Ǌ": "NJ",
                "ǋ": "Nj",
                "Ⓞ": "O",
                "Ｏ": "O",
                "Ò": "O",
                "Ó": "O",
                "Ô": "O",
                "Ồ": "O",
                "Ố": "O",
                "Ỗ": "O",
                "Ổ": "O",
                "Õ": "O",
                "Ṍ": "O",
                "Ȭ": "O",
                "Ṏ": "O",
                "Ō": "O",
                "Ṑ": "O",
                "Ṓ": "O",
                "Ŏ": "O",
                "Ȯ": "O",
                "Ȱ": "O",
                "Ö": "O",
                "Ȫ": "O",
                "Ỏ": "O",
                "Ő": "O",
                "Ǒ": "O",
                "Ȍ": "O",
                "Ȏ": "O",
                "Ơ": "O",
                "Ờ": "O",
                "Ớ": "O",
                "Ỡ": "O",
                "Ở": "O",
                "Ợ": "O",
                "Ọ": "O",
                "Ộ": "O",
                "Ǫ": "O",
                "Ǭ": "O",
                "Ø": "O",
                "Ǿ": "O",
                "Ɔ": "O",
                "Ɵ": "O",
                "Ꝋ": "O",
                "Ꝍ": "O",
                "Ƣ": "OI",
                "Ꝏ": "OO",
                "Ȣ": "OU",
                "Ⓟ": "P",
                "Ｐ": "P",
                "Ṕ": "P",
                "Ṗ": "P",
                "Ƥ": "P",
                "Ᵽ": "P",
                "Ꝑ": "P",
                "Ꝓ": "P",
                "Ꝕ": "P",
                "Ⓠ": "Q",
                "Ｑ": "Q",
                "Ꝗ": "Q",
                "Ꝙ": "Q",
                "Ɋ": "Q",
                "Ⓡ": "R",
                "Ｒ": "R",
                "Ŕ": "R",
                "Ṙ": "R",
                "Ř": "R",
                "Ȑ": "R",
                "Ȓ": "R",
                "Ṛ": "R",
                "Ṝ": "R",
                "Ŗ": "R",
                "Ṟ": "R",
                "Ɍ": "R",
                "Ɽ": "R",
                "Ꝛ": "R",
                "Ꞧ": "R",
                "Ꞃ": "R",
                "Ⓢ": "S",
                "Ｓ": "S",
                "ẞ": "S",
                "Ś": "S",
                "Ṥ": "S",
                "Ŝ": "S",
                "Ṡ": "S",
                "Š": "S",
                "Ṧ": "S",
                "Ṣ": "S",
                "Ṩ": "S",
                "Ș": "S",
                "Ş": "S",
                "Ȿ": "S",
                "Ꞩ": "S",
                "Ꞅ": "S",
                "Ⓣ": "T",
                "Ｔ": "T",
                "Ṫ": "T",
                "Ť": "T",
                "Ṭ": "T",
                "Ț": "T",
                "Ţ": "T",
                "Ṱ": "T",
                "Ṯ": "T",
                "Ŧ": "T",
                "Ƭ": "T",
                "Ʈ": "T",
                "Ⱦ": "T",
                "Ꞇ": "T",
                "Ꜩ": "TZ",
                "Ⓤ": "U",
                "Ｕ": "U",
                "Ù": "U",
                "Ú": "U",
                "Û": "U",
                "Ũ": "U",
                "Ṹ": "U",
                "Ū": "U",
                "Ṻ": "U",
                "Ŭ": "U",
                "Ü": "U",
                "Ǜ": "U",
                "Ǘ": "U",
                "Ǖ": "U",
                "Ǚ": "U",
                "Ủ": "U",
                "Ů": "U",
                "Ű": "U",
                "Ǔ": "U",
                "Ȕ": "U",
                "Ȗ": "U",
                "Ư": "U",
                "Ừ": "U",
                "Ứ": "U",
                "Ữ": "U",
                "Ử": "U",
                "Ự": "U",
                "Ụ": "U",
                "Ṳ": "U",
                "Ų": "U",
                "Ṷ": "U",
                "Ṵ": "U",
                "Ʉ": "U",
                "Ⓥ": "V",
                "Ｖ": "V",
                "Ṽ": "V",
                "Ṿ": "V",
                "Ʋ": "V",
                "Ꝟ": "V",
                "Ʌ": "V",
                "Ꝡ": "VY",
                "Ⓦ": "W",
                "Ｗ": "W",
                "Ẁ": "W",
                "Ẃ": "W",
                "Ŵ": "W",
                "Ẇ": "W",
                "Ẅ": "W",
                "Ẉ": "W",
                "Ⱳ": "W",
                "Ⓧ": "X",
                "Ｘ": "X",
                "Ẋ": "X",
                "Ẍ": "X",
                "Ⓨ": "Y",
                "Ｙ": "Y",
                "Ỳ": "Y",
                "Ý": "Y",
                "Ŷ": "Y",
                "Ỹ": "Y",
                "Ȳ": "Y",
                "Ẏ": "Y",
                "Ÿ": "Y",
                "Ỷ": "Y",
                "Ỵ": "Y",
                "Ƴ": "Y",
                "Ɏ": "Y",
                "Ỿ": "Y",
                "Ⓩ": "Z",
                "Ｚ": "Z",
                "Ź": "Z",
                "Ẑ": "Z",
                "Ż": "Z",
                "Ž": "Z",
                "Ẓ": "Z",
                "Ẕ": "Z",
                "Ƶ": "Z",
                "Ȥ": "Z",
                "Ɀ": "Z",
                "Ⱬ": "Z",
                "Ꝣ": "Z",
                "ⓐ": "a",
                "ａ": "a",
                "ẚ": "a",
                "à": "a",
                "á": "a",
                "â": "a",
                "ầ": "a",
                "ấ": "a",
                "ẫ": "a",
                "ẩ": "a",
                "ã": "a",
                "ā": "a",
                "ă": "a",
                "ằ": "a",
                "ắ": "a",
                "ẵ": "a",
                "ẳ": "a",
                "ȧ": "a",
                "ǡ": "a",
                "ä": "a",
                "ǟ": "a",
                "ả": "a",
                "å": "a",
                "ǻ": "a",
                "ǎ": "a",
                "ȁ": "a",
                "ȃ": "a",
                "ạ": "a",
                "ậ": "a",
                "ặ": "a",
                "ḁ": "a",
                "ą": "a",
                "ⱥ": "a",
                "ɐ": "a",
                "ꜳ": "aa",
                "æ": "ae",
                "ǽ": "ae",
                "ǣ": "ae",
                "ꜵ": "ao",
                "ꜷ": "au",
                "ꜹ": "av",
                "ꜻ": "av",
                "ꜽ": "ay",
                "ⓑ": "b",
                "ｂ": "b",
                "ḃ": "b",
                "ḅ": "b",
                "ḇ": "b",
                "ƀ": "b",
                "ƃ": "b",
                "ɓ": "b",
                "ⓒ": "c",
                "ｃ": "c",
                "ć": "c",
                "ĉ": "c",
                "ċ": "c",
                "č": "c",
                "ç": "c",
                "ḉ": "c",
                "ƈ": "c",
                "ȼ": "c",
                "ꜿ": "c",
                "ↄ": "c",
                "ⓓ": "d",
                "ｄ": "d",
                "ḋ": "d",
                "ď": "d",
                "ḍ": "d",
                "ḑ": "d",
                "ḓ": "d",
                "ḏ": "d",
                "đ": "d",
                "ƌ": "d",
                "ɖ": "d",
                "ɗ": "d",
                "ꝺ": "d",
                "ǳ": "dz",
                "ǆ": "dz",
                "ⓔ": "e",
                "ｅ": "e",
                "è": "e",
                "é": "e",
                "ê": "e",
                "ề": "e",
                "ế": "e",
                "ễ": "e",
                "ể": "e",
                "ẽ": "e",
                "ē": "e",
                "ḕ": "e",
                "ḗ": "e",
                "ĕ": "e",
                "ė": "e",
                "ë": "e",
                "ẻ": "e",
                "ě": "e",
                "ȅ": "e",
                "ȇ": "e",
                "ẹ": "e",
                "ệ": "e",
                "ȩ": "e",
                "ḝ": "e",
                "ę": "e",
                "ḙ": "e",
                "ḛ": "e",
                "ɇ": "e",
                "ɛ": "e",
                "ǝ": "e",
                "ⓕ": "f",
                "ｆ": "f",
                "ḟ": "f",
                "ƒ": "f",
                "ꝼ": "f",
                "ⓖ": "g",
                "ｇ": "g",
                "ǵ": "g",
                "ĝ": "g",
                "ḡ": "g",
                "ğ": "g",
                "ġ": "g",
                "ǧ": "g",
                "ģ": "g",
                "ǥ": "g",
                "ɠ": "g",
                "ꞡ": "g",
                "ᵹ": "g",
                "ꝿ": "g",
                "ⓗ": "h",
                "ｈ": "h",
                "ĥ": "h",
                "ḣ": "h",
                "ḧ": "h",
                "ȟ": "h",
                "ḥ": "h",
                "ḩ": "h",
                "ḫ": "h",
                "ẖ": "h",
                "ħ": "h",
                "ⱨ": "h",
                "ⱶ": "h",
                "ɥ": "h",
                "ƕ": "hv",
                "ⓘ": "i",
                "ｉ": "i",
                "ì": "i",
                "í": "i",
                "î": "i",
                "ĩ": "i",
                "ī": "i",
                "ĭ": "i",
                "ï": "i",
                "ḯ": "i",
                "ỉ": "i",
                "ǐ": "i",
                "ȉ": "i",
                "ȋ": "i",
                "ị": "i",
                "į": "i",
                "ḭ": "i",
                "ɨ": "i",
                "ı": "i",
                "ⓙ": "j",
                "ｊ": "j",
                "ĵ": "j",
                "ǰ": "j",
                "ɉ": "j",
                "ⓚ": "k",
                "ｋ": "k",
                "ḱ": "k",
                "ǩ": "k",
                "ḳ": "k",
                "ķ": "k",
                "ḵ": "k",
                "ƙ": "k",
                "ⱪ": "k",
                "ꝁ": "k",
                "ꝃ": "k",
                "ꝅ": "k",
                "ꞣ": "k",
                "ⓛ": "l",
                "ｌ": "l",
                "ŀ": "l",
                "ĺ": "l",
                "ľ": "l",
                "ḷ": "l",
                "ḹ": "l",
                "ļ": "l",
                "ḽ": "l",
                "ḻ": "l",
                "ſ": "l",
                "ł": "l",
                "ƚ": "l",
                "ɫ": "l",
                "ⱡ": "l",
                "ꝉ": "l",
                "ꞁ": "l",
                "ꝇ": "l",
                "ǉ": "lj",
                "ⓜ": "m",
                "ｍ": "m",
                "ḿ": "m",
                "ṁ": "m",
                "ṃ": "m",
                "ɱ": "m",
                "ɯ": "m",
                "ⓝ": "n",
                "ｎ": "n",
                "ǹ": "n",
                "ń": "n",
                "ñ": "n",
                "ṅ": "n",
                "ň": "n",
                "ṇ": "n",
                "ņ": "n",
                "ṋ": "n",
                "ṉ": "n",
                "ƞ": "n",
                "ɲ": "n",
                "ŉ": "n",
                "ꞑ": "n",
                "ꞥ": "n",
                "ǌ": "nj",
                "ⓞ": "o",
                "ｏ": "o",
                "ò": "o",
                "ó": "o",
                "ô": "o",
                "ồ": "o",
                "ố": "o",
                "ỗ": "o",
                "ổ": "o",
                "õ": "o",
                "ṍ": "o",
                "ȭ": "o",
                "ṏ": "o",
                "ō": "o",
                "ṑ": "o",
                "ṓ": "o",
                "ŏ": "o",
                "ȯ": "o",
                "ȱ": "o",
                "ö": "o",
                "ȫ": "o",
                "ỏ": "o",
                "ő": "o",
                "ǒ": "o",
                "ȍ": "o",
                "ȏ": "o",
                "ơ": "o",
                "ờ": "o",
                "ớ": "o",
                "ỡ": "o",
                "ở": "o",
                "ợ": "o",
                "ọ": "o",
                "ộ": "o",
                "ǫ": "o",
                "ǭ": "o",
                "ø": "o",
                "ǿ": "o",
                "ɔ": "o",
                "ꝋ": "o",
                "ꝍ": "o",
                "ɵ": "o",
                "ƣ": "oi",
                "ȣ": "ou",
                "ꝏ": "oo",
                "ⓟ": "p",
                "ｐ": "p",
                "ṕ": "p",
                "ṗ": "p",
                "ƥ": "p",
                "ᵽ": "p",
                "ꝑ": "p",
                "ꝓ": "p",
                "ꝕ": "p",
                "ⓠ": "q",
                "ｑ": "q",
                "ɋ": "q",
                "ꝗ": "q",
                "ꝙ": "q",
                "ⓡ": "r",
                "ｒ": "r",
                "ŕ": "r",
                "ṙ": "r",
                "ř": "r",
                "ȑ": "r",
                "ȓ": "r",
                "ṛ": "r",
                "ṝ": "r",
                "ŗ": "r",
                "ṟ": "r",
                "ɍ": "r",
                "ɽ": "r",
                "ꝛ": "r",
                "ꞧ": "r",
                "ꞃ": "r",
                "ⓢ": "s",
                "ｓ": "s",
                "ß": "s",
                "ś": "s",
                "ṥ": "s",
                "ŝ": "s",
                "ṡ": "s",
                "š": "s",
                "ṧ": "s",
                "ṣ": "s",
                "ṩ": "s",
                "ș": "s",
                "ş": "s",
                "ȿ": "s",
                "ꞩ": "s",
                "ꞅ": "s",
                "ẛ": "s",
                "ⓣ": "t",
                "ｔ": "t",
                "ṫ": "t",
                "ẗ": "t",
                "ť": "t",
                "ṭ": "t",
                "ț": "t",
                "ţ": "t",
                "ṱ": "t",
                "ṯ": "t",
                "ŧ": "t",
                "ƭ": "t",
                "ʈ": "t",
                "ⱦ": "t",
                "ꞇ": "t",
                "ꜩ": "tz",
                "ⓤ": "u",
                "ｕ": "u",
                "ù": "u",
                "ú": "u",
                "û": "u",
                "ũ": "u",
                "ṹ": "u",
                "ū": "u",
                "ṻ": "u",
                "ŭ": "u",
                "ü": "u",
                "ǜ": "u",
                "ǘ": "u",
                "ǖ": "u",
                "ǚ": "u",
                "ủ": "u",
                "ů": "u",
                "ű": "u",
                "ǔ": "u",
                "ȕ": "u",
                "ȗ": "u",
                "ư": "u",
                "ừ": "u",
                "ứ": "u",
                "ữ": "u",
                "ử": "u",
                "ự": "u",
                "ụ": "u",
                "ṳ": "u",
                "ų": "u",
                "ṷ": "u",
                "ṵ": "u",
                "ʉ": "u",
                "ⓥ": "v",
                "ｖ": "v",
                "ṽ": "v",
                "ṿ": "v",
                "ʋ": "v",
                "ꝟ": "v",
                "ʌ": "v",
                "ꝡ": "vy",
                "ⓦ": "w",
                "ｗ": "w",
                "ẁ": "w",
                "ẃ": "w",
                "ŵ": "w",
                "ẇ": "w",
                "ẅ": "w",
                "ẘ": "w",
                "ẉ": "w",
                "ⱳ": "w",
                "ⓧ": "x",
                "ｘ": "x",
                "ẋ": "x",
                "ẍ": "x",
                "ⓨ": "y",
                "ｙ": "y",
                "ỳ": "y",
                "ý": "y",
                "ŷ": "y",
                "ỹ": "y",
                "ȳ": "y",
                "ẏ": "y",
                "ÿ": "y",
                "ỷ": "y",
                "ẙ": "y",
                "ỵ": "y",
                "ƴ": "y",
                "ɏ": "y",
                "ỿ": "y",
                "ⓩ": "z",
                "ｚ": "z",
                "ź": "z",
                "ẑ": "z",
                "ż": "z",
                "ž": "z",
                "ẓ": "z",
                "ẕ": "z",
                "ƶ": "z",
                "ȥ": "z",
                "ɀ": "z",
                "ⱬ": "z",
                "ꝣ": "z",
                "Ά": "Α",
                "Έ": "Ε",
                "Ή": "Η",
                "Ί": "Ι",
                "Ϊ": "Ι",
                "Ό": "Ο",
                "Ύ": "Υ",
                "Ϋ": "Υ",
                "Ώ": "Ω",
                "ά": "α",
                "έ": "ε",
                "ή": "η",
                "ί": "ι",
                "ϊ": "ι",
                "ΐ": "ι",
                "ό": "ο",
                "ύ": "υ",
                "ϋ": "υ",
                "ΰ": "υ",
                "ω": "ω",
                "ς": "σ"
            };
            return e;
        }), t.define("select2/data/base", [ "../utils" ], function(e) {
            function t(e, i) {
                t.__super__.constructor.call(this);
            }
            return e.Extend(t, e.Observable), t.prototype.current = function(e) {
                throw Error("The `current` method must be defined in child classes.");
            }, t.prototype.query = function(e, t) {
                throw Error("The `query` method must be defined in child classes.");
            }, t.prototype.bind = function(e, t) {}, t.prototype.destroy = function() {}, t.prototype.generateResultId = function(t, i) {
                var n = t.id + "-result-";
                return n += e.generateChars(4), n += null != i.id ? "-" + i.id : "-" + e.generateChars(4);
            }, t;
        }), t.define("select2/data/select", [ "./base", "../utils", "jquery" ], function(e, t, i) {
            function n(e, t) {
                this.$element = e, this.options = t, n.__super__.constructor.call(this);
            }
            return t.Extend(n, e), n.prototype.current = function(e) {
                var t = [], n = this;
                this.$element.find(":selected").each(function() {
                    var e = i(this), s = n.item(e);
                    t.push(s);
                }), e(t);
            }, n.prototype.select = function(e) {
                var t = this;
                if (e.selected = !0, i(e.element).is("option")) return e.element.selected = !0,
                void this.$element.trigger("change");
                if (this.$element.prop("multiple")) this.current(function(n) {
                    var s = [];
                    e = [ e ], e.push.apply(e, n);
                    for (var o = 0; o < e.length; o++) {
                        var r = e[o].id;
                        -1 === i.inArray(r, s) && s.push(r);
                    }
                    t.$element.val(s), t.$element.trigger("change");
                }); else {
                    var n = e.id;
                    this.$element.val(n), this.$element.trigger("change");
                }
            }, n.prototype.unselect = function(e) {
                var t = this;
                return this.$element.prop("multiple") ? (e.selected = !1, i(e.element).is("option") ? (e.element.selected = !1,
                void this.$element.trigger("change")) : void this.current(function(n) {
                    for (var s = [], o = 0; o < n.length; o++) {
                        var r = n[o].id;
                        r !== e.id && -1 === i.inArray(r, s) && s.push(r);
                    }
                    t.$element.val(s), t.$element.trigger("change");
                })) : void 0;
            }, n.prototype.bind = function(e, t) {
                var i = this;
                this.container = e, e.on("select", function(e) {
                    i.select(e.data);
                }), e.on("unselect", function(e) {
                    i.unselect(e.data);
                });
            }, n.prototype.destroy = function() {
                this.$element.find("*").each(function() {
                    i.removeData(this, "data");
                });
            }, n.prototype.query = function(e, t) {
                var n = [], s = this, o = this.$element.children();
                o.each(function() {
                    var t = i(this);
                    if (t.is("option") || t.is("optgroup")) {
                        var o = s.item(t), r = s.matches(e, o);
                        null !== r && n.push(r);
                    }
                }), t({
                    results: n
                });
            }, n.prototype.addOptions = function(e) {
                t.appendMany(this.$element, e);
            }, n.prototype.option = function(e) {
                var t;
                e.children ? (t = document.createElement("optgroup"), t.label = e.text) : (t = document.createElement("option"),
                void 0 !== t.textContent ? t.textContent = e.text : t.innerText = e.text), e.id && (t.value = e.id),
                e.disabled && (t.disabled = !0), e.selected && (t.selected = !0), e.title && (t.title = e.title);
                var n = i(t), s = this._normalizeItem(e);
                return s.element = t, i.data(t, "data", s), n;
            }, n.prototype.item = function(e) {
                var t = {};
                if (t = i.data(e[0], "data"), null != t) return t;
                if (e.is("option")) t = {
                    id: e.val(),
                    text: e.text(),
                    disabled: e.prop("disabled"),
                    selected: e.prop("selected"),
                    title: e.prop("title")
                }; else if (e.is("optgroup")) {
                    t = {
                        text: e.prop("label"),
                        children: [],
                        title: e.prop("title")
                    };
                    for (var n = e.children("option"), s = [], o = 0; o < n.length; o++) {
                        var r = i(n[o]), a = this.item(r);
                        s.push(a);
                    }
                    t.children = s;
                }
                return t = this._normalizeItem(t), t.element = e[0], i.data(e[0], "data", t), t;
            }, n.prototype._normalizeItem = function(e) {
                i.isPlainObject(e) || (e = {
                    id: e,
                    text: e
                }), e = i.extend({}, {
                    text: ""
                }, e);
                var t = {
                    selected: !1,
                    disabled: !1
                };
                return null != e.id && (e.id = "" + e.id), null != e.text && (e.text = "" + e.text),
                null == e._resultId && e.id && null != this.container && (e._resultId = this.generateResultId(this.container, e)),
                i.extend({}, t, e);
            }, n.prototype.matches = function(e, t) {
                var i = this.options.get("matcher");
                return i(e, t);
            }, n;
        }), t.define("select2/data/array", [ "./select", "../utils", "jquery" ], function(e, t, i) {
            function n(e, t) {
                var i = t.get("data") || [];
                n.__super__.constructor.call(this, e, t), this.addOptions(this.convertToOptions(i));
            }
            return t.Extend(n, e), n.prototype.select = function(e) {
                var t = this.$element.find("option").filter(function(t, i) {
                    return i.value == "" + e.id;
                });
                0 === t.length && (t = this.option(e), this.addOptions(t)), n.__super__.select.call(this, e);
            }, n.prototype.convertToOptions = function(e) {
                function n(e) {
                    return function() {
                        return i(this).val() == e.id;
                    };
                }
                for (var s = this, o = this.$element.find("option"), r = o.map(function() {
                    return s.item(i(this)).id;
                }).get(), a = [], l = 0; l < e.length; l++) {
                    var d = this._normalizeItem(e[l]);
                    if (i.inArray(d.id, r) >= 0) {
                        var c = o.filter(n(d)), u = this.item(c), p = i.extend(!0, {}, d, u), h = this.option(p);
                        c.replaceWith(h);
                    } else {
                        var f = this.option(d);
                        if (d.children) {
                            var v = this.convertToOptions(d.children);
                            t.appendMany(f, v);
                        }
                        a.push(f);
                    }
                }
                return a;
            }, n;
        }), t.define("select2/data/ajax", [ "./array", "../utils", "jquery" ], function(e, t, i) {
            function n(e, t) {
                this.ajaxOptions = this._applyDefaults(t.get("ajax")), null != this.ajaxOptions.processResults && (this.processResults = this.ajaxOptions.processResults),
                n.__super__.constructor.call(this, e, t);
            }
            return t.Extend(n, e), n.prototype._applyDefaults = function(e) {
                var t = {
                    data: function(e) {
                        return i.extend({}, e, {
                            q: e.term
                        });
                    },
                    transport: function(e, t, n) {
                        var s = i.ajax(e);
                        return s.then(t), s.fail(n), s;
                    }
                };
                return i.extend({}, t, e, !0);
            }, n.prototype.processResults = function(e) {
                return e;
            }, n.prototype.query = function(e, t) {
                function n() {
                    var n = o.transport(o, function(n) {
                        var o = s.processResults(n, e);
                        s.options.get("debug") && window.console && console.error && (o && o.results && i.isArray(o.results) || console.error("Select2: The AJAX results did not return an array in the `results` key of the response.")),
                        t(o);
                    }, function() {
                        n.status && "0" === n.status || s.trigger("results:message", {
                            message: "errorLoading"
                        });
                    });
                    s._request = n;
                }
                var s = this;
                null != this._request && (i.isFunction(this._request.abort) && this._request.abort(),
                this._request = null);
                var o = i.extend({
                    type: "GET"
                }, this.ajaxOptions);
                "function" == typeof o.url && (o.url = o.url.call(this.$element, e)), "function" == typeof o.data && (o.data = o.data.call(this.$element, e)),
                this.ajaxOptions.delay && null != e.term ? (this._queryTimeout && window.clearTimeout(this._queryTimeout),
                this._queryTimeout = window.setTimeout(n, this.ajaxOptions.delay)) : n();
            }, n;
        }), t.define("select2/data/tags", [ "jquery" ], function(e) {
            function t(t, i, n) {
                var s = n.get("tags"), o = n.get("createTag");
                void 0 !== o && (this.createTag = o);
                var r = n.get("insertTag");
                if (void 0 !== r && (this.insertTag = r), t.call(this, i, n), e.isArray(s)) for (var a = 0; a < s.length; a++) {
                    var l = s[a], d = this._normalizeItem(l), c = this.option(d);
                    this.$element.append(c);
                }
            }
            return t.prototype.query = function(e, t, i) {
                function n(e, o) {
                    for (var r = e.results, a = 0; a < r.length; a++) {
                        var l = r[a], d = null != l.children && !n({
                            results: l.children
                        }, !0), c = l.text === t.term;
                        if (c || d) return !o && (e.data = r, void i(e));
                    }
                    if (o) return !0;
                    var u = s.createTag(t);
                    if (null != u) {
                        var p = s.option(u);
                        p.attr("data-select2-tag", !0), s.addOptions([ p ]), s.insertTag(r, u);
                    }
                    e.results = r, i(e);
                }
                var s = this;
                return this._removeOldTags(), null == t.term || null != t.page ? void e.call(this, t, i) : void e.call(this, t, n);
            }, t.prototype.createTag = function(t, i) {
                var n = e.trim(i.term);
                return "" === n ? null : {
                    id: n,
                    text: n
                };
            }, t.prototype.insertTag = function(e, t, i) {
                t.unshift(i);
            }, t.prototype._removeOldTags = function(t) {
                var i = (this._lastTag, this.$element.find("option[data-select2-tag]"));
                i.each(function() {
                    this.selected || e(this).remove();
                });
            }, t;
        }), t.define("select2/data/tokenizer", [ "jquery" ], function(e) {
            function t(e, t, i) {
                var n = i.get("tokenizer");
                void 0 !== n && (this.tokenizer = n), e.call(this, t, i);
            }
            return t.prototype.bind = function(e, t, i) {
                e.call(this, t, i), this.$search = t.dropdown.$search || t.selection.$search || i.find(".select2-search__field");
            }, t.prototype.query = function(t, i, n) {
                function s(t) {
                    var i = r._normalizeItem(t), n = r.$element.find("option").filter(function() {
                        return e(this).val() === i.id;
                    });
                    if (!n.length) {
                        var s = r.option(i);
                        s.attr("data-select2-tag", !0), r._removeOldTags(), r.addOptions([ s ]);
                    }
                    o(i);
                }
                function o(e) {
                    r.trigger("select", {
                        data: e
                    });
                }
                var r = this;
                i.term = i.term || "";
                var a = this.tokenizer(i, this.options, s);
                a.term !== i.term && (this.$search.length && (this.$search.val(a.term), this.$search.focus()),
                i.term = a.term), t.call(this, i, n);
            }, t.prototype.tokenizer = function(t, i, n, s) {
                for (var o = n.get("tokenSeparators") || [], r = i.term, a = 0, l = this.createTag || function(e) {
                    return {
                        id: e.term,
                        text: e.term
                    };
                }; a < r.length; ) {
                    var d = r[a];
                    if (-1 !== e.inArray(d, o)) {
                        var c = r.substr(0, a), u = e.extend({}, i, {
                            term: c
                        }), p = l(u);
                        null != p ? (s(p), r = r.substr(a + 1) || "", a = 0) : a++;
                    } else a++;
                }
                return {
                    term: r
                };
            }, t;
        }), t.define("select2/data/minimumInputLength", [], function() {
            function e(e, t, i) {
                this.minimumInputLength = i.get("minimumInputLength"), e.call(this, t, i);
            }
            return e.prototype.query = function(e, t, i) {
                return t.term = t.term || "", t.term.length < this.minimumInputLength ? void this.trigger("results:message", {
                    message: "inputTooShort",
                    args: {
                        minimum: this.minimumInputLength,
                        input: t.term,
                        params: t
                    }
                }) : void e.call(this, t, i);
            }, e;
        }), t.define("select2/data/maximumInputLength", [], function() {
            function e(e, t, i) {
                this.maximumInputLength = i.get("maximumInputLength"), e.call(this, t, i);
            }
            return e.prototype.query = function(e, t, i) {
                return t.term = t.term || "", this.maximumInputLength > 0 && t.term.length > this.maximumInputLength ? void this.trigger("results:message", {
                    message: "inputTooLong",
                    args: {
                        maximum: this.maximumInputLength,
                        input: t.term,
                        params: t
                    }
                }) : void e.call(this, t, i);
            }, e;
        }), t.define("select2/data/maximumSelectionLength", [], function() {
            function e(e, t, i) {
                this.maximumSelectionLength = i.get("maximumSelectionLength"), e.call(this, t, i);
            }
            return e.prototype.query = function(e, t, i) {
                var n = this;
                this.current(function(s) {
                    var o = null != s ? s.length : 0;
                    return n.maximumSelectionLength > 0 && o >= n.maximumSelectionLength ? void n.trigger("results:message", {
                        message: "maximumSelected",
                        args: {
                            maximum: n.maximumSelectionLength
                        }
                    }) : void e.call(n, t, i);
                });
            }, e;
        }), t.define("select2/dropdown", [ "jquery", "./utils" ], function(e, t) {
            function i(e, t) {
                this.$element = e, this.options = t, i.__super__.constructor.call(this);
            }
            return t.Extend(i, t.Observable), i.prototype.render = function() {
                var t = e('<span class="select2-dropdown"><span class="select2-results"></span></span>');
                return t.attr("dir", this.options.get("dir")), this.$dropdown = t, t;
            }, i.prototype.bind = function() {}, i.prototype.position = function(e, t) {}, i.prototype.destroy = function() {
                this.$dropdown.remove();
            }, i;
        }), t.define("select2/dropdown/search", [ "jquery", "../utils" ], function(e, t) {
            function i() {}
            return i.prototype.render = function(t) {
                var i = t.call(this), n = e('<span class="select2-search select2-search--dropdown"><input class="select2-search__field" type="search" tabindex="-1" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="textbox" /></span>');
                return this.$searchContainer = n, this.$search = n.find("input"), i.prepend(n),
                i;
            }, i.prototype.bind = function(t, i, n) {
                var s = this;
                t.call(this, i, n), this.$search.on("keydown", function(e) {
                    s.trigger("keypress", e), s._keyUpPrevented = e.isDefaultPrevented();
                }), this.$search.on("input", function(t) {
                    e(this).off("keyup");
                }), this.$search.on("keyup input", function(e) {
                    s.handleSearch(e);
                }), i.on("open", function() {
                    s.$search.attr("tabindex", 0), s.$search.focus(), window.setTimeout(function() {
                        s.$search.focus();
                    }, 0);
                }), i.on("close", function() {
                    s.$search.attr("tabindex", -1), s.$search.val("");
                }), i.on("focus", function() {
                    i.isOpen() && s.$search.focus();
                }), i.on("results:all", function(e) {
                    if (null == e.query.term || "" === e.query.term) {
                        var t = s.showSearch(e);
                        t ? s.$searchContainer.removeClass("select2-search--hide") : s.$searchContainer.addClass("select2-search--hide");
                    }
                });
            }, i.prototype.handleSearch = function(e) {
                if (!this._keyUpPrevented) {
                    var t = this.$search.val();
                    this.trigger("query", {
                        term: t
                    });
                }
                this._keyUpPrevented = !1;
            }, i.prototype.showSearch = function(e, t) {
                return !0;
            }, i;
        }), t.define("select2/dropdown/hidePlaceholder", [], function() {
            function e(e, t, i, n) {
                this.placeholder = this.normalizePlaceholder(i.get("placeholder")), e.call(this, t, i, n);
            }
            return e.prototype.append = function(e, t) {
                t.results = this.removePlaceholder(t.results), e.call(this, t);
            }, e.prototype.normalizePlaceholder = function(e, t) {
                return "string" == typeof t && (t = {
                    id: "",
                    text: t
                }), t;
            }, e.prototype.removePlaceholder = function(e, t) {
                for (var i = t.slice(0), n = t.length - 1; n >= 0; n--) {
                    var s = t[n];
                    this.placeholder.id === s.id && i.splice(n, 1);
                }
                return i;
            }, e;
        }), t.define("select2/dropdown/infiniteScroll", [ "jquery" ], function(e) {
            function t(e, t, i, n) {
                this.lastParams = {}, e.call(this, t, i, n), this.$loadingMore = this.createLoadingMore(),
                this.loading = !1;
            }
            return t.prototype.append = function(e, t) {
                this.$loadingMore.remove(), this.loading = !1, e.call(this, t), this.showLoadingMore(t) && this.$results.append(this.$loadingMore);
            }, t.prototype.bind = function(t, i, n) {
                var s = this;
                t.call(this, i, n), i.on("query", function(e) {
                    s.lastParams = e, s.loading = !0;
                }), i.on("query:append", function(e) {
                    s.lastParams = e, s.loading = !0;
                }), this.$results.on("scroll", function() {
                    var t = e.contains(document.documentElement, s.$loadingMore[0]);
                    if (!s.loading && t) {
                        var i = s.$results.offset().top + s.$results.outerHeight(!1), n = s.$loadingMore.offset().top + s.$loadingMore.outerHeight(!1);
                        i + 50 >= n && s.loadMore();
                    }
                });
            }, t.prototype.loadMore = function() {
                this.loading = !0;
                var t = e.extend({}, {
                    page: 1
                }, this.lastParams);
                t.page++, this.trigger("query:append", t);
            }, t.prototype.showLoadingMore = function(e, t) {
                return t.pagination && t.pagination.more;
            }, t.prototype.createLoadingMore = function() {
                var t = e('<li class="select2-results__option select2-results__option--load-more"role="treeitem" aria-disabled="true"></li>'), i = this.options.get("translations").get("loadingMore");
                return t.html(i(this.lastParams)), t;
            }, t;
        }), t.define("select2/dropdown/attachBody", [ "jquery", "../utils" ], function(e, t) {
            function i(t, i, n) {
                this.$dropdownParent = n.get("dropdownParent") || e(document.body), t.call(this, i, n);
            }
            return i.prototype.bind = function(e, t, i) {
                var n = this, s = !1;
                e.call(this, t, i), t.on("open", function() {
                    n._showDropdown(), n._attachPositioningHandler(t), s || (s = !0, t.on("results:all", function() {
                        n._positionDropdown(), n._resizeDropdown();
                    }), t.on("results:append", function() {
                        n._positionDropdown(), n._resizeDropdown();
                    }));
                }), t.on("close", function() {
                    n._hideDropdown(), n._detachPositioningHandler(t);
                }), this.$dropdownContainer.on("mousedown", function(e) {
                    e.stopPropagation();
                });
            }, i.prototype.destroy = function(e) {
                e.call(this), this.$dropdownContainer.remove();
            }, i.prototype.position = function(e, t, i) {
                t.attr("class", i.attr("class")), t.removeClass("select2"), t.addClass("select2-container--open"),
                t.css({
                    position: "absolute",
                    top: -999999
                }), this.$container = i;
            }, i.prototype.render = function(t) {
                var i = e("<span></span>"), n = t.call(this);
                return i.append(n), this.$dropdownContainer = i, i;
            }, i.prototype._hideDropdown = function(e) {
                this.$dropdownContainer.detach();
            }, i.prototype._attachPositioningHandler = function(i, n) {
                var s = this, o = "scroll.select2." + n.id, r = "resize.select2." + n.id, a = "orientationchange.select2." + n.id, l = this.$container.parents().filter(t.hasScroll);
                l.each(function() {
                    e(this).data("select2-scroll-position", {
                        x: e(this).scrollLeft(),
                        y: e(this).scrollTop()
                    });
                }), l.on(o, function(t) {
                    var i = e(this).data("select2-scroll-position");
                    e(this).scrollTop(i.y);
                }), e(window).on(o + " " + r + " " + a, function(e) {
                    s._positionDropdown(), s._resizeDropdown();
                });
            }, i.prototype._detachPositioningHandler = function(i, n) {
                var s = "scroll.select2." + n.id, o = "resize.select2." + n.id, r = "orientationchange.select2." + n.id, a = this.$container.parents().filter(t.hasScroll);
                a.off(s), e(window).off(s + " " + o + " " + r);
            }, i.prototype._positionDropdown = function() {
                var t = e(window), i = this.$dropdown.hasClass("select2-dropdown--above"), n = this.$dropdown.hasClass("select2-dropdown--below"), s = null, o = this.$container.offset();
                o.bottom = o.top + this.$container.outerHeight(!1);
                var r = {
                    height: this.$container.outerHeight(!1)
                };
                r.top = o.top, r.bottom = o.top + r.height;
                var a = {
                    height: this.$dropdown.outerHeight(!1)
                }, l = {
                    top: t.scrollTop(),
                    bottom: t.scrollTop() + t.height()
                }, d = l.top < o.top - a.height, c = l.bottom > o.bottom + a.height, u = {
                    left: o.left,
                    top: r.bottom
                }, p = this.$dropdownParent;
                "static" === p.css("position") && (p = p.offsetParent());
                var h = p.offset();
                u.top -= h.top, u.left -= h.left, i || n || (s = "below"), c || !d || i ? !d && c && i && (s = "below") : s = "above",
                ("above" == s || i && "below" !== s) && (u.top = r.top - h.top - a.height), null != s && (this.$dropdown.removeClass("select2-dropdown--below select2-dropdown--above").addClass("select2-dropdown--" + s),
                this.$container.removeClass("select2-container--below select2-container--above").addClass("select2-container--" + s)),
                this.$dropdownContainer.css(u);
            }, i.prototype._resizeDropdown = function() {
                var e = {
                    width: this.$container.outerWidth(!1) + "px"
                };
                this.options.get("dropdownAutoWidth") && (e.minWidth = e.width, e.position = "relative",
                e.width = "auto"), this.$dropdown.css(e);
            }, i.prototype._showDropdown = function(e) {
                this.$dropdownContainer.appendTo(this.$dropdownParent), this._positionDropdown(),
                this._resizeDropdown();
            }, i;
        }), t.define("select2/dropdown/minimumResultsForSearch", [], function() {
            function e(t) {
                for (var i = 0, n = 0; n < t.length; n++) {
                    var s = t[n];
                    s.children ? i += e(s.children) : i++;
                }
                return i;
            }
            function t(e, t, i, n) {
                this.minimumResultsForSearch = i.get("minimumResultsForSearch"), this.minimumResultsForSearch < 0 && (this.minimumResultsForSearch = 1 / 0),
                e.call(this, t, i, n);
            }
            return t.prototype.showSearch = function(t, i) {
                return !(e(i.data.results) < this.minimumResultsForSearch) && t.call(this, i);
            }, t;
        }), t.define("select2/dropdown/selectOnClose", [], function() {
            function e() {}
            return e.prototype.bind = function(e, t, i) {
                var n = this;
                e.call(this, t, i), t.on("close", function(e) {
                    n._handleSelectOnClose(e);
                });
            }, e.prototype._handleSelectOnClose = function(e, t) {
                if (t && null != t.originalSelect2Event) {
                    var i = t.originalSelect2Event;
                    if ("select" === i._type || "unselect" === i._type) return;
                }
                var n = this.getHighlightedResults();
                if (!(n.length < 1)) {
                    var s = n.data("data");
                    null != s.element && s.element.selected || null == s.element && s.selected || this.trigger("select", {
                        data: s
                    });
                }
            }, e;
        }), t.define("select2/dropdown/closeOnSelect", [], function() {
            function e() {}
            return e.prototype.bind = function(e, t, i) {
                var n = this;
                e.call(this, t, i), t.on("select", function(e) {
                    n._selectTriggered(e);
                }), t.on("unselect", function(e) {
                    n._selectTriggered(e);
                });
            }, e.prototype._selectTriggered = function(e, t) {
                var i = t.originalEvent;
                i && i.ctrlKey || this.trigger("close", {
                    originalEvent: i,
                    originalSelect2Event: t
                });
            }, e;
        }), t.define("select2/i18n/en", [], function() {
            return {
                errorLoading: function() {
                    return "The results could not be loaded.";
                },
                inputTooLong: function(e) {
                    var t = e.input.length - e.maximum, i = "Please delete " + t + " character";
                    return 1 != t && (i += "s"), i;
                },
                inputTooShort: function(e) {
                    var t = e.minimum - e.input.length, i = "Please enter " + t + " or more characters";
                    return i;
                },
                loadingMore: function() {
                    return "Loading more results…";
                },
                maximumSelected: function(e) {
                    var t = "You can only select " + e.maximum + " item";
                    return 1 != e.maximum && (t += "s"), t;
                },
                noResults: function() {
                    return "No results found";
                },
                searching: function() {
                    return "Searching…";
                }
            };
        }), t.define("select2/defaults", [ "jquery", "require", "./results", "./selection/single", "./selection/multiple", "./selection/placeholder", "./selection/allowClear", "./selection/search", "./selection/eventRelay", "./utils", "./translation", "./diacritics", "./data/select", "./data/array", "./data/ajax", "./data/tags", "./data/tokenizer", "./data/minimumInputLength", "./data/maximumInputLength", "./data/maximumSelectionLength", "./dropdown", "./dropdown/search", "./dropdown/hidePlaceholder", "./dropdown/infiniteScroll", "./dropdown/attachBody", "./dropdown/minimumResultsForSearch", "./dropdown/selectOnClose", "./dropdown/closeOnSelect", "./i18n/en" ], function(e, t, i, n, s, o, r, a, l, d, c, u, p, h, f, v, g, m, y, b, w, $, _, k, C, x, T, S, A) {
            function P() {
                this.reset();
            }
            P.prototype.apply = function(u) {
                if (u = e.extend(!0, {}, this.defaults, u), null == u.dataAdapter) {
                    if (null != u.ajax ? u.dataAdapter = f : null != u.data ? u.dataAdapter = h : u.dataAdapter = p,
                    u.minimumInputLength > 0 && (u.dataAdapter = d.Decorate(u.dataAdapter, m)), u.maximumInputLength > 0 && (u.dataAdapter = d.Decorate(u.dataAdapter, y)),
                    u.maximumSelectionLength > 0 && (u.dataAdapter = d.Decorate(u.dataAdapter, b)),
                    u.tags && (u.dataAdapter = d.Decorate(u.dataAdapter, v)), null == u.tokenSeparators && null == u.tokenizer || (u.dataAdapter = d.Decorate(u.dataAdapter, g)),
                    null != u.query) {
                        var A = t(u.amdBase + "compat/query");
                        u.dataAdapter = d.Decorate(u.dataAdapter, A);
                    }
                    if (null != u.initSelection) {
                        var P = t(u.amdBase + "compat/initSelection");
                        u.dataAdapter = d.Decorate(u.dataAdapter, P);
                    }
                }
                if (null == u.resultsAdapter && (u.resultsAdapter = i, null != u.ajax && (u.resultsAdapter = d.Decorate(u.resultsAdapter, k)),
                null != u.placeholder && (u.resultsAdapter = d.Decorate(u.resultsAdapter, _)), u.selectOnClose && (u.resultsAdapter = d.Decorate(u.resultsAdapter, T))),
                null == u.dropdownAdapter) {
                    if (u.multiple) u.dropdownAdapter = w; else {
                        var O = d.Decorate(w, $);
                        u.dropdownAdapter = O;
                    }
                    if (0 !== u.minimumResultsForSearch && (u.dropdownAdapter = d.Decorate(u.dropdownAdapter, x)),
                    u.closeOnSelect && (u.dropdownAdapter = d.Decorate(u.dropdownAdapter, S)), null != u.dropdownCssClass || null != u.dropdownCss || null != u.adaptDropdownCssClass) {
                        var j = t(u.amdBase + "compat/dropdownCss");
                        u.dropdownAdapter = d.Decorate(u.dropdownAdapter, j);
                    }
                    u.dropdownAdapter = d.Decorate(u.dropdownAdapter, C);
                }
                if (null == u.selectionAdapter) {
                    if (u.multiple ? u.selectionAdapter = s : u.selectionAdapter = n, null != u.placeholder && (u.selectionAdapter = d.Decorate(u.selectionAdapter, o)),
                    u.allowClear && (u.selectionAdapter = d.Decorate(u.selectionAdapter, r)), u.multiple && (u.selectionAdapter = d.Decorate(u.selectionAdapter, a)),
                    null != u.containerCssClass || null != u.containerCss || null != u.adaptContainerCssClass) {
                        var E = t(u.amdBase + "compat/containerCss");
                        u.selectionAdapter = d.Decorate(u.selectionAdapter, E);
                    }
                    u.selectionAdapter = d.Decorate(u.selectionAdapter, l);
                }
                if ("string" == typeof u.language) if (u.language.indexOf("-") > 0) {
                    var M = u.language.split("-"), D = M[0];
                    u.language = [ u.language, D ];
                } else u.language = [ u.language ];
                if (e.isArray(u.language)) {
                    var q = new c();
                    u.language.push("en");
                    for (var L = u.language, I = 0; I < L.length; I++) {
                        var z = L[I], H = {};
                        try {
                            H = c.loadPath(z);
                        } catch (N) {
                            try {
                                z = this.defaults.amdLanguageBase + z, H = c.loadPath(z);
                            } catch (U) {
                                u.debug && window.console && console.warn && console.warn('Select2: The language file for "' + z + '" could not be automatically loaded. A fallback will be used instead.');
                                continue;
                            }
                        }
                        q.extend(H);
                    }
                    u.translations = q;
                } else {
                    var R = c.loadPath(this.defaults.amdLanguageBase + "en"), F = new c(u.language);
                    F.extend(R), u.translations = F;
                }
                return u;
            }, P.prototype.reset = function() {
                function t(e) {
                    function t(e) {
                        return u[e] || e;
                    }
                    return e.replace(/[^\u0000-\u007E]/g, t);
                }
                function i(n, s) {
                    if ("" === e.trim(n.term)) return s;
                    if (s.children && s.children.length > 0) {
                        for (var o = e.extend(!0, {}, s), r = s.children.length - 1; r >= 0; r--) {
                            var a = s.children[r], l = i(n, a);
                            null == l && o.children.splice(r, 1);
                        }
                        return o.children.length > 0 ? o : i(n, o);
                    }
                    var d = t(s.text).toUpperCase(), c = t(n.term).toUpperCase();
                    return d.indexOf(c) > -1 ? s : null;
                }
                this.defaults = {
                    amdBase: "./",
                    amdLanguageBase: "./i18n/",
                    closeOnSelect: !0,
                    debug: !1,
                    dropdownAutoWidth: !1,
                    escapeMarkup: d.escapeMarkup,
                    language: A,
                    matcher: i,
                    minimumInputLength: 0,
                    maximumInputLength: 0,
                    maximumSelectionLength: 0,
                    minimumResultsForSearch: 0,
                    selectOnClose: !1,
                    sorter: function(e) {
                        return e;
                    },
                    templateResult: function(e) {
                        return e.text;
                    },
                    templateSelection: function(e) {
                        return e.text;
                    },
                    theme: "default",
                    width: "resolve"
                };
            }, P.prototype.set = function(t, i) {
                var n = e.camelCase(t), s = {};
                s[n] = i;
                var o = d._convertData(s);
                e.extend(this.defaults, o);
            };
            var O = new P();
            return O;
        }), t.define("select2/options", [ "require", "jquery", "./defaults", "./utils" ], function(e, t, i, n) {
            function s(t, s) {
                if (this.options = t, null != s && this.fromElement(s), this.options = i.apply(this.options),
                s && s.is("input")) {
                    var o = e(this.get("amdBase") + "compat/inputData");
                    this.options.dataAdapter = n.Decorate(this.options.dataAdapter, o);
                }
            }
            return s.prototype.fromElement = function(e) {
                var i = [ "select2" ];
                null == this.options.multiple && (this.options.multiple = e.prop("multiple")), null == this.options.disabled && (this.options.disabled = e.prop("disabled")),
                null == this.options.language && (e.prop("lang") ? this.options.language = e.prop("lang").toLowerCase() : e.closest("[lang]").prop("lang") && (this.options.language = e.closest("[lang]").prop("lang"))),
                null == this.options.dir && (e.prop("dir") ? this.options.dir = e.prop("dir") : e.closest("[dir]").prop("dir") ? this.options.dir = e.closest("[dir]").prop("dir") : this.options.dir = "ltr"),
                e.prop("disabled", this.options.disabled), e.prop("multiple", this.options.multiple),
                e.data("select2Tags") && (this.options.debug && window.console && console.warn && console.warn('Select2: The `data-select2-tags` attribute has been changed to use the `data-data` and `data-tags="true"` attributes and will be removed in future versions of Select2.'),
                e.data("data", e.data("select2Tags")), e.data("tags", !0)), e.data("ajaxUrl") && (this.options.debug && window.console && console.warn && console.warn("Select2: The `data-ajax-url` attribute has been changed to `data-ajax--url` and support for the old attribute will be removed in future versions of Select2."),
                e.attr("ajax--url", e.data("ajaxUrl")), e.data("ajax--url", e.data("ajaxUrl")));
                var s = {};
                s = t.fn.jquery && "1." == t.fn.jquery.substr(0, 2) && e[0].dataset ? t.extend(!0, {}, e[0].dataset, e.data()) : e.data();
                var o = t.extend(!0, {}, s);
                o = n._convertData(o);
                for (var r in o) t.inArray(r, i) > -1 || (t.isPlainObject(this.options[r]) ? t.extend(this.options[r], o[r]) : this.options[r] = o[r]);
                return this;
            }, s.prototype.get = function(e) {
                return this.options[e];
            }, s.prototype.set = function(e, t) {
                this.options[e] = t;
            }, s;
        }), t.define("select2/core", [ "jquery", "./options", "./utils", "./keys" ], function(e, t, i, n) {
            var s = function(e, i) {
                null != e.data("select2") && e.data("select2").destroy(), this.$element = e, this.id = this._generateId(e),
                i = i || {}, this.options = new t(i, e), s.__super__.constructor.call(this);
                var n = e.attr("tabindex") || 0;
                e.data("old-tabindex", n), e.attr("tabindex", "-1");
                var o = this.options.get("dataAdapter");
                this.dataAdapter = new o(e, this.options);
                var r = this.render();
                this._placeContainer(r);
                var a = this.options.get("selectionAdapter");
                this.selection = new a(e, this.options), this.$selection = this.selection.render(),
                this.selection.position(this.$selection, r);
                var l = this.options.get("dropdownAdapter");
                this.dropdown = new l(e, this.options), this.$dropdown = this.dropdown.render(),
                this.dropdown.position(this.$dropdown, r);
                var d = this.options.get("resultsAdapter");
                this.results = new d(e, this.options, this.dataAdapter), this.$results = this.results.render(),
                this.results.position(this.$results, this.$dropdown);
                var c = this;
                this._bindAdapters(), this._registerDomEvents(), this._registerDataEvents(), this._registerSelectionEvents(),
                this._registerDropdownEvents(), this._registerResultsEvents(), this._registerEvents(),
                this.dataAdapter.current(function(e) {
                    c.trigger("selection:update", {
                        data: e
                    });
                }), e.addClass("select2-hidden-accessible"), e.attr("aria-hidden", "true"), this._syncAttributes(),
                e.data("select2", this);
            };
            return i.Extend(s, i.Observable), s.prototype._generateId = function(e) {
                var t = "";
                return t = null != e.attr("id") ? e.attr("id") : null != e.attr("name") ? e.attr("name") + "-" + i.generateChars(2) : i.generateChars(4),
                t = t.replace(/(:|\.|\[|\]|,)/g, ""), t = "select2-" + t;
            }, s.prototype._placeContainer = function(e) {
                e.insertAfter(this.$element);
                var t = this._resolveWidth(this.$element, this.options.get("width"));
                null != t && e.css("width", t);
            }, s.prototype._resolveWidth = function(e, t) {
                var i = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;
                if ("resolve" == t) {
                    var n = this._resolveWidth(e, "style");
                    return null != n ? n : this._resolveWidth(e, "element");
                }
                if ("element" == t) {
                    var s = e.outerWidth(!1);
                    return 0 >= s ? "auto" : s + "px";
                }
                if ("style" == t) {
                    var o = e.attr("style");
                    if ("string" != typeof o) return null;
                    for (var r = o.split(";"), a = 0, l = r.length; l > a; a += 1) {
                        var d = r[a].replace(/\s/g, ""), c = d.match(i);
                        if (null !== c && c.length >= 1) return c[1];
                    }
                    return null;
                }
                return t;
            }, s.prototype._bindAdapters = function() {
                this.dataAdapter.bind(this, this.$container), this.selection.bind(this, this.$container),
                this.dropdown.bind(this, this.$container), this.results.bind(this, this.$container);
            }, s.prototype._registerDomEvents = function() {
                var t = this;
                this.$element.on("change.select2", function() {
                    t.dataAdapter.current(function(e) {
                        t.trigger("selection:update", {
                            data: e
                        });
                    });
                }), this.$element.on("focus.select2", function(e) {
                    t.trigger("focus", e);
                }), this._syncA = i.bind(this._syncAttributes, this), this._syncS = i.bind(this._syncSubtree, this),
                this.$element[0].attachEvent && this.$element[0].attachEvent("onpropertychange", this._syncA);
                var n = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
                null != n ? (this._observer = new n(function(i) {
                    e.each(i, t._syncA), e.each(i, t._syncS);
                }), this._observer.observe(this.$element[0], {
                    attributes: !0,
                    childList: !0,
                    subtree: !1
                })) : this.$element[0].addEventListener && (this.$element[0].addEventListener("DOMAttrModified", t._syncA, !1),
                this.$element[0].addEventListener("DOMNodeInserted", t._syncS, !1), this.$element[0].addEventListener("DOMNodeRemoved", t._syncS, !1));
            }, s.prototype._registerDataEvents = function() {
                var e = this;
                this.dataAdapter.on("*", function(t, i) {
                    e.trigger(t, i);
                });
            }, s.prototype._registerSelectionEvents = function() {
                var t = this, i = [ "toggle", "focus" ];
                this.selection.on("toggle", function() {
                    t.toggleDropdown();
                }), this.selection.on("focus", function(e) {
                    t.focus(e);
                }), this.selection.on("*", function(n, s) {
                    -1 === e.inArray(n, i) && t.trigger(n, s);
                });
            }, s.prototype._registerDropdownEvents = function() {
                var e = this;
                this.dropdown.on("*", function(t, i) {
                    e.trigger(t, i);
                });
            }, s.prototype._registerResultsEvents = function() {
                var e = this;
                this.results.on("*", function(t, i) {
                    e.trigger(t, i);
                });
            }, s.prototype._registerEvents = function() {
                var e = this;
                this.on("open", function() {
                    e.$container.addClass("select2-container--open");
                }), this.on("close", function() {
                    e.$container.removeClass("select2-container--open");
                }), this.on("enable", function() {
                    e.$container.removeClass("select2-container--disabled");
                }), this.on("disable", function() {
                    e.$container.addClass("select2-container--disabled");
                }), this.on("blur", function() {
                    e.$container.removeClass("select2-container--focus");
                }), this.on("query", function(t) {
                    e.isOpen() || e.trigger("open", {}), this.dataAdapter.query(t, function(i) {
                        e.trigger("results:all", {
                            data: i,
                            query: t
                        });
                    });
                }), this.on("query:append", function(t) {
                    this.dataAdapter.query(t, function(i) {
                        e.trigger("results:append", {
                            data: i,
                            query: t
                        });
                    });
                }), this.on("keypress", function(t) {
                    var i = t.which;
                    e.isOpen() ? i === n.ESC || i === n.TAB || i === n.UP && t.altKey ? (e.close(),
                    t.preventDefault()) : i === n.ENTER ? (e.trigger("results:select", {}), t.preventDefault()) : i === n.SPACE && t.ctrlKey ? (e.trigger("results:toggle", {}),
                    t.preventDefault()) : i === n.UP ? (e.trigger("results:previous", {}), t.preventDefault()) : i === n.DOWN && (e.trigger("results:next", {}),
                    t.preventDefault()) : (i === n.ENTER || i === n.SPACE || i === n.DOWN && t.altKey) && (e.open(),
                    t.preventDefault());
                });
            }, s.prototype._syncAttributes = function() {
                this.options.set("disabled", this.$element.prop("disabled")), this.options.get("disabled") ? (this.isOpen() && this.close(),
                this.trigger("disable", {})) : this.trigger("enable", {});
            }, s.prototype._syncSubtree = function(e, t) {
                var i = !1, n = this;
                if (!e || !e.target || "OPTION" === e.target.nodeName || "OPTGROUP" === e.target.nodeName) {
                    if (t) if (t.addedNodes && t.addedNodes.length > 0) for (var s = 0; s < t.addedNodes.length; s++) {
                        var o = t.addedNodes[s];
                        o.selected && (i = !0);
                    } else t.removedNodes && t.removedNodes.length > 0 && (i = !0); else i = !0;
                    i && this.dataAdapter.current(function(e) {
                        n.trigger("selection:update", {
                            data: e
                        });
                    });
                }
            }, s.prototype.trigger = function(e, t) {
                var i = s.__super__.trigger, n = {
                    open: "opening",
                    close: "closing",
                    select: "selecting",
                    unselect: "unselecting"
                };
                if (void 0 === t && (t = {}), e in n) {
                    var o = n[e], r = {
                        prevented: !1,
                        name: e,
                        args: t
                    };
                    if (i.call(this, o, r), r.prevented) return void (t.prevented = !0);
                }
                i.call(this, e, t);
            }, s.prototype.toggleDropdown = function() {
                this.options.get("disabled") || (this.isOpen() ? this.close() : this.open());
            }, s.prototype.open = function() {
                this.isOpen() || this.trigger("query", {});
            }, s.prototype.close = function() {
                this.isOpen() && this.trigger("close", {});
            }, s.prototype.isOpen = function() {
                return this.$container.hasClass("select2-container--open");
            }, s.prototype.hasFocus = function() {
                return this.$container.hasClass("select2-container--focus");
            }, s.prototype.focus = function(e) {
                this.hasFocus() || (this.$container.addClass("select2-container--focus"), this.trigger("focus", {}));
            }, s.prototype.enable = function(e) {
                this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("enable")` method has been deprecated and will be removed in later Select2 versions. Use $element.prop("disabled") instead.'),
                null != e && 0 !== e.length || (e = [ !0 ]);
                var t = !e[0];
                this.$element.prop("disabled", t);
            }, s.prototype.data = function() {
                this.options.get("debug") && arguments.length > 0 && window.console && console.warn && console.warn('Select2: Data can no longer be set using `select2("data")`. You should consider setting the value instead using `$element.val()`.');
                var e = [];
                return this.dataAdapter.current(function(t) {
                    e = t;
                }), e;
            }, s.prototype.val = function(t) {
                if (this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("val")` method has been deprecated and will be removed in later Select2 versions. Use $element.val() instead.'),
                null == t || 0 === t.length) return this.$element.val();
                var i = t[0];
                e.isArray(i) && (i = e.map(i, function(e) {
                    return "" + e;
                })), this.$element.val(i).trigger("change");
            }, s.prototype.destroy = function() {
                this.$container.remove(), this.$element[0].detachEvent && this.$element[0].detachEvent("onpropertychange", this._syncA),
                null != this._observer ? (this._observer.disconnect(), this._observer = null) : this.$element[0].removeEventListener && (this.$element[0].removeEventListener("DOMAttrModified", this._syncA, !1),
                this.$element[0].removeEventListener("DOMNodeInserted", this._syncS, !1), this.$element[0].removeEventListener("DOMNodeRemoved", this._syncS, !1)),
                this._syncA = null, this._syncS = null, this.$element.off(".select2"), this.$element.attr("tabindex", this.$element.data("old-tabindex")),
                this.$element.removeClass("select2-hidden-accessible"), this.$element.attr("aria-hidden", "false"),
                this.$element.removeData("select2"), this.dataAdapter.destroy(), this.selection.destroy(),
                this.dropdown.destroy(), this.results.destroy(), this.dataAdapter = null, this.selection = null,
                this.dropdown = null, this.results = null;
            }, s.prototype.render = function() {
                var t = e('<span class="select2 select2-container"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>');
                return t.attr("dir", this.options.get("dir")), this.$container = t, this.$container.addClass("select2-container--" + this.options.get("theme")),
                t.data("element", this.$element), t;
            }, s;
        }), t.define("jquery-mousewheel", [ "jquery" ], function(e) {
            return e;
        }), t.define("jquery.select2", [ "jquery", "jquery-mousewheel", "./select2/core", "./select2/defaults" ], function(e, t, i, n) {
            if (null == e.fn.select2) {
                var s = [ "open", "close", "destroy" ];
                e.fn.select2 = function(t) {
                    if (t = t || {}, "object" == typeof t) return this.each(function() {
                        var n = e.extend(!0, {}, t);
                        new i(e(this), n);
                    }), this;
                    if ("string" == typeof t) {
                        var n, o = Array.prototype.slice.call(arguments, 1);
                        return this.each(function() {
                            var i = e(this).data("select2");
                            null == i && window.console && console.error && console.error("The select2('" + t + "') method was called on an element that is not using Select2."),
                            n = i[t].apply(i, o);
                        }), e.inArray(t, s) > -1 ? this : n;
                    }
                    throw Error("Invalid arguments for Select2: " + t);
                };
            }
            return null == e.fn.select2.defaults && (e.fn.select2.defaults = n), i;
        }), {
            define: t.define,
            require: t.require
        };
    }(), i = t.require("jquery.select2");
    return e.fn.select2.amd = t, i;
}), function(e) {
    e(document).ready(function() {
        function t() {
            var t = [], i = [], t = "";
            "-1" != e(".select_refine.location").val() && (i.push(".filter_location_" + e(".select_refine.location").val()),
            t = ".filter_location_" + e(".select_refine.location").val(), e(".select_refine.seating option[value='none']").remove(),
            r && (e(".select_refine.seating").prepend('<option value="reserved">Reserved Seat</option>'),
            r = !1), ".filter_location_no-home-games" == t || ".filter_location_interstateregional-games" == t ? (e(".select_refine.seating").prop("disabled", "disabled"),
            e(".select_refine.seating").prepend('<option value="none"></option>'), e(".select_refine.seating option[value='none']").prop("selected", !0),
            o = !0) : ".filter_location_6-home-games" == t ? (e(".select_refine.seating option[value='reserved']").remove(),
            e(".select_refine.seating option[value='general-admission']").prop("selected", !0),
            r = !0) : 1 == o && (e(".select_refine.seating").prop("disabled", !1), o = !1)),
            1 != o && "-1" != e(".select_refine.seating").val() && (i.push(".filter_seating_" + e(".select_refine.seating").val()),
            t = t + ".filter_seating_" + e(".select_refine.seating").val()), "" == t ? e(".filterable").show() : (e(".filterable").hide(),
            e(".filterable" + t).show());
        }
        e(".section__home-hero--down").on("click", function(t) {
            if ("" !== this.hash) {
                t.preventDefault();
                var i = this.hash;
                e("html, body").animate({
                    scrollTop: e(i).offset().top
                }, 800, function() {
                    window.location.hash = i;
                });
            }
        }), e(".banner__sub-nav>li.current_page_parent a").trigger("click"), e.when(e("#mobile-menu").mmenu({
            slidingSubmenus: !1,
            extensions: [ "pagedim-black" ],
            offCanvas: {
                position: "right"
            },
            iconPanels: !0,
            navbars: [ {
                position: "bottom",
                content: [ "<a href='https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login' target='_blank'>RENEW</a>", "<a href='https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login' target='_blank'>MY ACCOUNT</a>" ]
            } ]
        })).done(function() {
            e(".mm-panel.mm-vertical.mm-opened.mm-current").height(e(window).height() - 160);
        }), e(window).resize(function() {
            e(".mm-panel.mm-vertical.mm-opened.mm-current").height(e(window).height() - 160);
        });
        var i = e(".member-tally .counter");
        if (i.hasClass("animate")) {
            var n = new TweeningCounter().duration(2e3), s = i.data("membership-count");
            n.ease("out-expo"), i.html(""), n.onUpdateCallback(function(t, n) {
                var s = n.split("");
                if (i.html(""), e('<div class="digit digit-number"></div>'), t > 0) {
                    var o = '<div class="digit digit-number">' + s[0] + '</div><div class="digit digit-number">' + s[1] + '</div><div class="digit digit-comma">,</div><div class="digit digit-number">' + s[2] + '</div><div class="digit digit-number">' + s[3] + '</div><div class="digit digit-number">' + s[4] + "</div>";
                    i.html(o);
                }
            }), n.to(s).start();
        }
        if (e(".faq-item__answer").hide(), e(".faq-item__question").click(function() {
            e(this).toggleClass("active"), e(this).next().slideToggle("normal");
        }), e(".action-row__form").hide(), e("a.js-request-a-call").click(function(t) {
            t.preventDefault(), e(".action-row__form").slideToggle("normal");
        }), e(".package-item").equalHeights(), e(".js-live-chat").click(function(t) {
            t.preventDefault(), e("#hfc-badge").trigger("click");
        }), e(".row-slider__slider").slick({
            dots: !1,
            infinite: !0,
            speed: 500,
            autoplay: !0,
            fade: !0,
            centerMode: !0,
            cssEase: "linear",
            prevArrow: ".slick-prev",
            nextArrow: ".slick-next"
        }), e(".package_select").click(function() {
            var t = e(this).data("id");
            e(".package_select").removeClass("selected"), e(this).addClass("selected"), window.location.hash = "#" + e(this).data("slug");
            var i = 0;
            e(".package_details_out").each(function() {
                e(this).data("id") == t ? (e(this).show(), i = e(this).height() + 65) : e(this).hide();
            }), e(".package_details_in").each(function() {
                e(this).data("id") == t ? e(this).slideDown() : e(this).slideUp();
            }), e(window).width() < 800 || e(".packages-row__packages").css("min-height", i);
        }), e(".select_refine").length > 0) {
            e.when(e(".select_refine").select2({
                minimumResultsForSearch: 1 / 0,
                placeholder: {
                    id: "-1",
                    placeholder: "Please select"
                }
            }).on("change", function(e) {
                t();
            })).done(function() {
                e(".select2-selection__placeholder").html("Please select"), e(".select2-selection__arrow b").remove(),
                e(".select2-selection__arrow").append('<img src="/wp-content/themes/sharks-membership/assets/images/select-down.png" class="select-arrow">'),
                e(".select_refine.location").on("select2:open", function() {
                    e(".select2-selection__arrow img").first().remove(), e(".select2-selection__arrow").first().append('<img src="/wp-content/themes/sharks-membership/assets/images/select-up.png" class="select-arrow">');
                }).on("select2:close", function() {
                    e(".select2-selection__arrow img").first().remove(), e(".select2-selection__arrow").first().append('<img src="/wp-content/themes/sharks-membership/assets/images/select-down.png" class="select-arrow">');
                }), e(".select_refine.seating").on("select2:open", function() {
                    e(".select2-selection__arrow img").last().remove(), e(".select2-selection__arrow").last().append('<img src="/wp-content/themes/sharks-membership/assets/images/select-up.png" class="select-arrow">');
                }).on("select2:close", function() {
                    e(".select2-selection__arrow img").last().remove(), e(".select2-selection__arrow").last().append('<img src="/wp-content/themes/sharks-membership/assets/images/select-down.png" class="select-arrow">');
                });
            });
            var o = !1, r = !1;
        }
        if (e(".benefit").length > 0) {
            var a = 0;
            e(".benefit").each(function() {
                a = e(this).height() > a ? e(this).height() : a;
            }), e(".benefit").height(a);
        }
        if (e(".pack").length > 0) {
            var a = 0;
            e(".pack").each(function() {
                a = e(this).height() > a ? e(this).height() : a;
            }), e(".pack").height(a);
        }
        if (e(".package_select").length > 0 && (e(".package_nav").on("click", function() {
            var t = e(this).data("id");
            e(".package_select").each(function() {
                e(this).data("id") == t && e(this).trigger("click");
            });
        }), window.location.hash ? e(".package_select").each(function() {
            "#" + e(this).data("slug") == window.location.hash && e(this).trigger("click");
        }) : window.location.hash = "#" + e(".package_select.selected").data("slug"), e(window).width() > 800)) {
            var l = e(".packages-row__out_package_details").height() + 65;
            e(".packages-row__packages").css("min-height", l);
        }
    }), e(window).load(function() {
        e(".package-item").equalHeights();
    });

    //Filter Categories
    var $filterCheckboxes = $('.checkbox');

    $filterCheckboxes.on('change', function() {

    var selectedFilters = {};

    $filterCheckboxes.filter(':checked').each(function() {

    	if (!selectedFilters.hasOwnProperty(this.name)) {
    		selectedFilters[this.name] = [];
    	}

    	selectedFilters[this.name].push(this.value);

    });

    // create a collection containing all of the filterable elements
    var $filteredResults = $('.item');

    // loop over the selected filter name -> (array) values pairs
    $.each(selectedFilters, function(name, filterValues) {

    	// filter each .flower element
    	$filteredResults = $filteredResults.filter(function() {

    		var matched = false,
    			currentFilterValues = $(this).data('category').split(' ');

    		// loop over each category value in the current .flower's data-category
    		$.each(currentFilterValues, function(_, currentFilterValue) {

    			// if the current category exists in the selected filters array
    			// set matched to true, and stop looping. as we're ORing in each
    			// set of filters, we only need to match once

    			if ($.inArray(currentFilterValue, filterValues) != -1) {
    				matched = true;
    				return false;
    			}
    		});

    		// if matched is true the current .flower element is returned
    		return matched;

    	});
    });

    $('.item').fadeOut().filter($filteredResults).fadeIn();

    });

}(jQuery);
