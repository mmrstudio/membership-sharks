(function($) {
	// document ready
	$(document).ready(function() {

		// Add smooth scrolling to all links
		$(".section__home-hero--down").on('click', function(event) {

		// Make sure this.hash has a value before overriding default behavior
		if (this.hash !== "") {
			// Prevent default anchor click behavior
			event.preventDefault();

			// Store hash
			var hash = this.hash;

			// Using jQuery's animate() method to add smooth page scroll
			// The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
			$('html, body').animate({
				scrollTop: $(hash).offset().top
			}, 800, function(){

			// Add hash (#) to URL when done scrolling (default click behavior)
			window.location.hash = hash;
			});
		} // End if
	});



	// create a collection containing all of the filterable elements
	var $filteredResults = $('.item');

	// loop over the selected filter name -> (array) values pairs
	$.each(selectedFilters, function(name, filterValues) {

		// filter each .flower element
		$filteredResults = $filteredResults.filter(function() {

			var matched = false,
				currentFilterValues = $(this).data('category').split(' ');

			// loop over each category value in the current .flower's data-category
			$.each(currentFilterValues, function(_, currentFilterValue) {

				// if the current category exists in the selected filters array
				// set matched to true, and stop looping. as we're ORing in each
				// set of filters, we only need to match once

				if ($.inArray(currentFilterValue, filterValues) != -1) {
					matched = true;
					return false;
				}
			});

			// if matched is true the current .flower element is returned
			return matched;

		});
	});

	$('.item').fadeOut().filter($filteredResults).fadeIn();

	});



		// // JS for scroll to top
		// $("a[href='#package-info']").click(function() {
		// 	$('html,body').animate({
		// 	  scrollTop: target.offset().top
		// 	}, 1000);
		// 	return false;
		// });

		// mega menu, show when page (ID:20) is clicked
		// $('.main-nav--large li.page-item-10>a').click(function() {
		// 	event.preventDefault();
		// 	$('body').toggleClass("mega-menu--active");
		// });

		// // mega menu, hide when blocker is clicked
		// $('section.page-blocker').click(function() {
		// 	$('body').toggleClass("mega-menu--active");
		// });

		// // mega menu column, hover
		// $('.mega-menu__info').hide();
		// $('.mega-menu__nav li').hover(function() {
		// 	// on hover-in grab the id to create the selctor
		// 	var pageid = $(this).data('pageid');
		// 	var collselector = '.mega-menu__info--';
		// 	var infocol = collselector+pageid;
		// 	console.log('hover-in:'+infocol);
		// 	$(infocol).show();
		// 	$('.mega-menu__info--default').hide();
		// 	$(this).children().next().css( "display", "block" );
		// }, function() {
		// 	// on hover-out show the default
		// 	var pageid = $(this).data('pageid');
		// 	console.log('hover-out'+pageid);
		// 	$('.mega-menu__info').hide();
		// 	$('.mega-menu__info--default').show();
		// 	$(this).children().next().css( "display", "none" );
  // 		});

  // 		// mega menu packages navigation
  // 		//$('.banner__sub-nav li.page_item_has_children ul').hide();
  // 		$('.banner__sub-nav>li.page_item_has_children>a').click(function() {
		// 	event.preventDefault();
		// 	$(this).parent().toggleClass("is-active");
		// 	$(this).next().slideToggle('normal');
		// 	$(this).parent().siblings().children().next().slideUp();
		// 	$(this).parent().siblings().removeClass("is-active");
		// });

  		// trigger a click of current page item on load
		$('.banner__sub-nav>li.current_page_parent a').trigger("click");

  		// M (mobile) menu
		$.when(
			$("#mobile-menu").mmenu({

				"slidingSubmenus": false,
				"extensions": [
					"pagedim-black"
				],
				"offCanvas": {
					"position": "right"
				},
				"iconPanels": true,
				"navbars": [
					{
						"position": "bottom",
						"content": [
							"<a href='https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login' target='_blank'>RENEW</a>",
                            "<a href='https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login' target='_blank'>MY ACCOUNT</a>",
						]
					}
				]
			})

		).done(function(){
			$('.mm-panel.mm-vertical.mm-opened.mm-current').height($(window).height()-160);
		});

		// fix for mmenu bottom thinggy covering menu items
		$( window ).resize(function() {
			$('.mm-panel.mm-vertical.mm-opened.mm-current').height($(window).height()-160);
		});

		// JS for Member Tally
		var memberTally = $('.member-tally .counter');
		if( memberTally.hasClass('animate') ) {

		    var memberTallyCounter = new TweeningCounter().duration(2000),
		        membershipCount = memberTally.data('membership-count');

		    memberTallyCounter.ease('out-expo');
		    memberTally.html('');
		    memberTallyCounter.onUpdateCallback(function(val, num) {

		        var numArr = num.split('');
		        memberTally.html('');
		        var digit = $('<div class="digit digit-number"></div>');
		        if(val > 0) {
		            var counterStr = '<div class="digit digit-number">' + numArr[0] + '</div><div class="digit digit-number">' + numArr[1] + '</div><div class="digit digit-comma">,</div><div class="digit digit-number">' + numArr[2] + '</div><div class="digit digit-number">' + numArr[3] + '</div><div class="digit digit-number">' + numArr[4] + '</div>';
		            memberTally.html(counterStr);
		        }
		    });

		    memberTallyCounter.to(membershipCount).start();
		}

		// faq accordion
		$('.faq-item__answer').hide();
		$('.faq-item__question').click(function() {
			$(this).toggleClass("active");
			$(this).next().slideToggle('normal');
		});


		// action row - request a call form
		$('.action-row__form').hide();
		$('a.js-request-a-call').click(function(event) {
			event.preventDefault();
			//event.preventDefault ? event.preventDefault() : event.returnValue = false;
			//$('.action-row__form').toggleClass("active");
			$('.action-row__form').slideToggle('normal');
		});

		// match height - doc ready
		$('.package-item').equalHeights();

		// live chat click to toggle button
		$('.js-live-chat').click(function(event) {
			event.preventDefault();
			//event.preventDefault ? event.preventDefault() : event.returnValue = false;
			$('#hfc-badge').trigger( "click");
		});


		$('.row-slider__slider').slick({
			dots: false,
			infinite: true,
			speed: 500,
			autoplay: true,
			fade: true,
			centerMode: true,
			cssEase: 'linear',
			prevArrow:'.slick-prev',
			nextArrow:'.slick-next'
		});


		$('.package_select').click(function(){
			var id = $(this).data('id');
			$('.package_select').removeClass('selected');
			$(this).addClass('selected');
			window.location.hash = '#' +  $(this).data('slug');
			// $('.package_details').hide();
			var max_h = 0;

			$('.package_details_out').each(function(){
				if ($(this).data('id') == id)
				{
					$(this).show();
					max_h = $(this).height()+65;
				}
				else
				{
					$(this).hide();
				}
			});

			$('.package_details_in').each(function(){
				if ($(this).data('id') == id)
				{
					$(this).slideDown();

				}
				else
				{
					$(this).slideUp();
				}
			});

			// $('.package_benefits').each(function(){
			// 	if ($(this).data('id') == id)
			// 	{
			// 		// $(this).slideDown();
			// 		$(this).show();
			// 	}
			// 	else
			// 	{
			// 		// $(this).slideUp();
			// 		$(this).hide();
			// 	}
			// });
			// if on mobile, scroll to the pricing table
			if ($(window).width() < 800)
			{
				// var aTag = $("a[name='details_"+ id +"']");
				// var aTag = $(".packages-row__details");
				// $('html,body').animate({scrollTop: aTag.offset().top},'slow');
				// $('.packages-row__packages').css('min-height',0);
			}
			else
			{
				$('.packages-row__packages').css('min-height',max_h);
			}

		});

		/**
		 * Filter on home page, bug with mixIn animation so just hide() for now.
         */

		if ($('.select_refine').length > 0){


			$.when(
				$('.select_refine').select2({
					minimumResultsForSearch: Infinity,

					placeholder: {
						id: "-1",
						placeholder: "Please select"
					}
				}).on('change',function(e){
					help_me_update();
				})



			).done(function(){
				$('.select2-selection__placeholder').html('Please select'); // select2 palceholder isnt working?
				$('.select2-selection__arrow b').remove();

				$('.select2-selection__arrow').append('<img src="/wp-content/themes/sharks-membership/assets/images/select-down.png" class="select-arrow">');

				$('.select_refine.location').on("select2:open", function(){
					$('.select2-selection__arrow img').first().remove();
					$('.select2-selection__arrow').first().append('<img src="/wp-content/themes/sharks-membership/assets/images/select-up.png" class="select-arrow">');
				}).on("select2:close", function(){
					$('.select2-selection__arrow img').first().remove();
					$('.select2-selection__arrow').first().append('<img src="/wp-content/themes/sharks-membership/assets/images/select-down.png" class="select-arrow">');
				});

				$('.select_refine.seating').on("select2:open", function(){
					$('.select2-selection__arrow img').last().remove();
					$('.select2-selection__arrow').last().append('<img src="/wp-content/themes/sharks-membership/assets/images/select-up.png" class="select-arrow">');
				}).on("select2:close", function(){
					$('.select2-selection__arrow img').last().remove();
					$('.select2-selection__arrow').last().append('<img src="/wp-content/themes/sharks-membership/assets/images/select-down.png" class="select-arrow">');
				});

			});




			// $('.select_refine').selectric({
			// 	arrowButtonMarkup: '<b class="button">aaa&#x25be;</b>',
			// }).on('change', function() {
			// 		help_me_update();
			// 	});
			// strip_please_select();
			// function strip_please_select(){
			// 	//scrappy way of deleting the please select options
			// 	$('.selectric-scroll ul li').each(function(){
			// 		if ($(this).html() == 'Please select')
			// 		{
			// 			$(this).remove();
			// 		}
			// 	});
			// }

			var seating_disabled = false;
			var reserved_disabled = false;
			function help_me_update() {
				var refine = false;
				var filter = [];
				var filter_str = '';


				var filterArr = [];
				var filter = '';

				if ($('.select_refine.location').val() != '-1') {
					filterArr.push('.filter_location_' + $('.select_refine.location').val());
					filter = '.filter_location_' + $('.select_refine.location').val();
					$(".select_refine.seating option[value='none']").remove();
					if (reserved_disabled){
						$(".select_refine.seating").prepend('<option value="reserved">Reserved Seat</option>');
						reserved_disabled=false;
					}


					if (filter == '.filter_location_no-home-games' || filter == '.filter_location_interstateregional-games')
					{
						$('.select_refine.seating').prop('disabled', 'disabled'); //.selectric('destroy').selectric('init');
						$(".select_refine.seating").prepend('<option value="none"></option>');
						$(".select_refine.seating option[value='none']").prop('selected',true);
						seating_disabled = true;
					}
					else if (filter == '.filter_location_6-home-games'){

						$(".select_refine.seating option[value='reserved']").remove();
						$(".select_refine.seating option[value='general-admission']").prop('selected',true);

						reserved_disabled = true;
					}
					else if (seating_disabled == true){

						$('.select_refine.seating').prop('disabled', false);
						// $('.select_refine.seating').selectric('refresh');
						// strip_please_select();
						seating_disabled = false;
					}

				}

				if (seating_disabled != true)
				{
					if ($('.select_refine.seating').val() != '-1') {
						filterArr.push('.filter_seating_' + $('.select_refine.seating').val());
						filter = filter + '.filter_seating_' + $('.select_refine.seating').val();
					}
				}

				if (filter == '') {
					$('.filterable').show();
				}
				else {
					$('.filterable').hide();
					$('.filterable' + filter).show();
				}
			}
		}




		// make sure all the benefits are the same size
		if ($('.benefit').length > 0){
			var max_v = 0;
			$('.benefit').each(function(){
				max_v = $(this).height() > max_v ? $(this).height() : max_v;
			});
			$('.benefit').height(max_v);
		}

		if ($('.pack').length > 0){
			var max_v = 0;
			$('.pack').each(function(){
				max_v = $(this).height() > max_v ? $(this).height() : max_v;
			});
			$('.pack').height(max_v);
		}

		// Packages Page
		if ($('.package_select').length >0)
		{

			$('.package_nav').on('click',function(){
				var nav_to = $(this).data('id');
				$('.package_select').each(function() {
					if ($(this).data('id') == nav_to) {
						$(this).trigger("click");
					}
				});
			});
			if(window.location.hash) {
				$('.package_select').each(function(){
					if ('#' + $(this).data('slug') == window.location.hash){
						$(this).trigger("click");
					}
				});
			} else {
				window.location.hash = '#' +  $('.package_select.selected').data('slug');
			}


			if ($(window).width() > 800)
			{
				var max_h = $('.packages-row__out_package_details').height()+65;
				$('.packages-row__packages').css('min-height',max_h);
			}




		}
		// Resize package boxes to same height..
		// if ($('.packages-row__boxes').length > 0) {
        //
		// 		var resize_boxes = function(){
		// 			if ($(window).width() >= 800) // format boxes for larger screens
		// 			{
		// 				var max_h = 0;
		// 				var max_w = 0;
		// 				$('.packages-row__boxes ul li a').each(function () {
		// 					max_h = max_h < $(this).height() ? $(this).height() : max_h;
		// 					max_w = max_w < $(this).width() ? $(this).width() : max_w;
		// 				});
		// 				if (max_h > max_w) { max_w = max_h; }
		// 				else { max_h = max_w; }
		// 				if (max_h > 0)
		// 				{
		// 					$('.packages-row__boxes ul li a').height(max_h).width(max_w);
		// 					$('.packages-row__boxes ul li a').css('line-height', (max_h-10) + 'px'); // hack job to get vertical align
		// 				}
		// 			}
		// 			else //ignore all of the above for mobile devices
		// 			{
		// 				$('.packages-row__boxes ul li a').css('height','').css('width','').css('line-height','');
		// 			}
        //
        //
		// 		};
		// 		resize_boxes();
		// 	$( window ).resize(resize_boxes); // update box heights on window resize
        //
		// };
	});

	// document loaded
	$(window).load(function() {
		$('.package-item').equalHeights();
	});

} )( jQuery );
