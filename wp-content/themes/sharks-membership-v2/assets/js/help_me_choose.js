
    (function ($) {

        $.fn.helpMeDecide = function(questions) {

            var wrap = $(this)
                questionsArray = {},
                firstQ = false,
                answeredQuestions = [],
                currentQ = false,
                suggestedPackages = $('#suggestedPackages'),
                suggestedPackagesWrap = $('.suggested-packages-wrap');

            // clear wrap contents
            wrap.html('');

            var questionBlock = $($('#questionBlock').html());
            $('#questionBlock').remove();

            var setAnswer = function(q, answerText) {
                questionsArray[q].addClass('answered');
                questionsArray[q].find('.response-answer').html(answerText);
            }

            var removeQuestion = function(q) {
                questionsArray[q].detach();
                questionsArray[q].appended = false;
                questionsArray[q].answered = false;
                questionsArray[q].answer = false;
            }

            var setCurrentQuestion = function(q) {
                currentQ = q;
            }

            var openQuestion = function(q) {
                closeAllQuestions();
                if(questionsArray[q].appended === false) {
                    wrap.append(questionsArray[q]);
                    questionsArray[q].appended = true;
                }
                questionsArray[q].addClass('open');
            }

            var closeAllQuestions = function(){
                $.each(questionsArray, function(i,q) {
                    $(q).removeClass('open');
                });
            };

            var answerQuestion = function(q, a) {

                if( ! questionsArray[q].answered) {
                    answeredQuestions.push(q);
                    questionsArray[q].answered = true;
                    questionsArray[q].answer = a.text;
                } else {

                    // change answer
                    suggestedPackagesWrap.removeClass('loaded');
                    if( currentQ !== q ) removeQuestion(currentQ);
                    var currentQIndex = answeredQuestions.indexOf(q);
                    var updateAnswersedQuestions = [];

                    $.each(answeredQuestions, function(i, aq) {

                        if(i <= currentQIndex || i == q) {
                            updateAnswersedQuestions.push(aq);
                        } else {
                            console.log('remove ' + aq);
                            removeQuestion(aq);
                        }

                    });

                    answeredQuestions = updateAnswersedQuestions;

                }

                setAnswer(q, a.text);
                a.response.call(responseActions);

            }

            var responseActions = {

                showPackages : function(packages) {
                    closeAllQuestions();

                    suggestedPackagesWrap.removeClass('loaded');
                    suggestedPackages.html('');

                    $.each(packages, function(i, url) {

                        $.get(url, false, function(content) {
                             suggestedPackages.append(content);
                             suggestedPackagesWrap.addClass('loaded');

							 loadPackageinit();
							 //console.log('packagesloaded');

                        });

                    });

                    //alert('showPackages: ' + packages);
                },

                gotoQuestion : function(q) {
                    closeAllQuestions();
                    openQuestion(q);
                    setCurrentQuestion(q);
                },

                showContent : function(url, packages) {
                    closeAllQuestions();

                    suggestedPackagesWrap.removeClass('loaded');

                    $.get(url, false, function(content) {

                        suggestedPackages.html('');

                        var contentWrap = $('<div class="suggested-packages-content"></div>');
                        contentWrap.html(content);

                        suggestedPackages.append(contentWrap);
                        suggestedPackagesWrap.addClass('loaded');

                        $.each(packages, function(i, url) {

                            $.get(url, false, function(content) {
                                 suggestedPackages.append(content);

                            });

                        });

                    });
                }

            };

            $.each(questions, function(i, q) {

                // set first question
                firstQ = (firstQ === false) ? i : firstQ;

                var questionHTML = questionBlock.clone();

                questionHTML.attr('id', i);

                // heading & response
                var questionHeading = questionHTML.find('.heading'),
                    questionHeadingTitle = questionHeading.find('.heading-title'),
                    questionResponse = questionHeading.find('.response-answer');


                questionHeading.on('click', function() {

                    if( ! questionHTML.hasClass('open') && questionHTML.hasClass('answered') ) {
                        closeAllQuestions();
                        openQuestion(i);
                    }

                });

                // question body
                var questionText = questionHTML.find('.question-text'),
                    questionAnswers = questionHTML.find('.question-answers');

                // answers
                $.each(q.answers, function(ai, a) {

                    var answerButton = $('<button class="btn btn--answer answer-button"></button>').html(a.text);

                    answerButton.on('click', function() {
                        answerQuestion(i, a);
                    });

                    questionAnswers.append(answerButton);

                });

                // set content
                questionHeadingTitle.html(q.heading);
                questionText.html(q.text);

                questionHTML.appended = false;
                questionHTML.answered = false;
                questionHTML.answer = '';

                questionsArray[i] = questionHTML;

            });

            openQuestion(firstQ);
            setCurrentQuestion(firstQ);

        }

    })(jQuery);


    jQuery(document).ready(function($) {
        $('#questions').helpMeDecide(questions);
    });
