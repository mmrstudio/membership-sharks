<?php
/**
 * Template Name: Rewards Page
 */

get_header();

?>

<section class="banner banner--sub">
	<?php  $banner_image = wp_get_attachment_image_src(get_field('banner_image'),'full');  ?>
	<div class="container" style="background: url(<?php echo $banner_image[0] ?>) no-repeat scroll center top transparent;">
		<div class="text">
			<span class="text"><h1 class="banner__headline"><?php the_field('banner_line_1'); ?> <span class="highlight"><?php the_field('banner_line_2'); ?></span></h1></span>
		</div>
		<a class="btn btn--buy-now" href="http://rewards.sharks.com.au/" target="_blank">Account Login</a>
	</div>
</section>




<section class="main main--sub-page" role="main">

	<section class="content-row content-row--specials">
		<div class="container">
			<div class="content-row__copy">

				<?php the_field('description'); ?>

					<div class="return__table">
							 <p>Tip: Scroll left and right to browse the table</p>
							 <table>
										<thead>
												 <tr>
															<th>Category </th>
															<th>Average Discount</th>
															<th>Average Spend</th>
															<th>Average Savings P/A</th>
												 </tr>
										</thead>
										<tbody>
												 <?php if(get_field('table_rows')): ?>
															<?php while(the_repeater_field('table_rows')): ?>
												 <tr>
															<td>
																	 <?php the_sub_field('col_1'); ?>
															</td>
															<td>
																	 <?php the_sub_field('col_2'); ?>
															</td>
															<td>
																	 <?php the_sub_field('col_3'); ?>
															</td>
															<td>
																	 <?php the_sub_field('col_4'); ?>
															</td>
												 </tr>
															<?php endwhile; ?>
												 <?php endif; ?>
										</tbody>
							 </table>
					</div>


			</div>
			<div class="content-row__image">
				<?php if (get_sub_field('image') != '') { ?>
					<?php   $thumb = wp_get_attachment_image_src(get_sub_field('image'), 'thumb-640');  ?>
					<img src="<?php echo $thumb[0]; ?>" />
				<?php } ?>
			</div>
		</div>
	</section>

</section><!-- main -->

<?php get_footer(); ?>
