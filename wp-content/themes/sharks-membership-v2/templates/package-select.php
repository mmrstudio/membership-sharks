<?php
/**
 * Template Name: Package Select Page
 *
 * The template for displaying a home page
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');
?>


<?php get_template_part('parts/row_packages'); ?>
<?php // Get Rows by looping over the ACF flexible fields on this page
?>
<?php get_footer(); ?>
