<?php
/**
 * Template Name: Contact Page
 *
 * The template for displaying a home page
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main main--sub-page" role="main">
		<div class="container">

			<div class="content-row__contact">

				<div class="contact-block">
					<h3>Membership Phone</h3>
					<p><?php the_field('phone'); ?></p>

					<h3>Membership Email</h3>
					<p><?php the_field('email'); ?></p>

				</div>

				<div class="contact-block">

					<!--<h3>Fan Relationship Management Centre</h3>
					<p><?php the_field('management_centre'); ?></p>-->

					<h3>Street Address:</h3>
					<p><?php the_field('street_address'); ?></p>

					<h3>Postal Address</h3>
					<p><?php the_field('postal_address'); ?></p>
					<p><a href="http://www.sharks.com.au">www.sharks.com.au</a></p>

				</div>

				<ul class="social social--contact">
					<li><a class="social__item social__icon--facebook" href="<?php the_field('facebook_link','options'); ?>" target="_blank"></a></li>
					<li><a class="social__item social__icon--instagram" href="<?php the_field('instagram_link','options'); ?>" target="_blank"></a></li>
					<li><a class="social__item social__icon--twitter" href="<?php the_field('twitter_link','options'); ?>" target="_blank"></a></li>
				</ul>

			</div>
			<div class="content-row__contact-map">
				<div class="embed-container">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13133.496018478572!2d151.14206636264814!3d-34.03844086163944!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5b1443890b3ed30b!2sSharkies+Leagues+Club!5e0!3m2!1sen!2sau!4v1441171466061&zoom=1" width="780" height="470" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div>

</section><!-- main -->

<?php get_template_part('parts/action-row'); ?>


<?php get_footer(); ?>
