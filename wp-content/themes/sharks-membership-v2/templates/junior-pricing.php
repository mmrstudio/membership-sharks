<?php
/**
 * Template Name: Junior Pricing Page
 *
 * The template for displaying the Family Pricing page
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main main--sub-page" role="main">

<?php // Get Rows by looping over the ACF flexible fields on this page
// Get template part for each of the content rows based on layout name
while (the_flexible_field('content_rows')) {
    get_template_part('parts/'. get_row_layout());
} ?>

<section class="membership-table--junior">
<div class="container">
	<h2>Junior Memberships</h2>

		<ul class="junior-member-table">
		<li class="table-head">
			<span class="table-col table-col--title">Package </span>
      <span class="table-col table-col--month">&nbsp;</span>
			<span class="table-col table-col--season">Annual Fee</span>
			<?php /* <span class="table-col table-col--month">Per Month*</span> */ ?>
		</li>

		<?php
		// wp query
		// https://gist.github.com/AaronRutley/5654937
		// $page_id = $post->ID;
		// $parent_page_id = $post->post_parent;

		//args
		$args = array(
		    'post_type' => 'packages',
			//'post_parent' => 1,
		    'posts_per_page' => '-1',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		);

		// new query
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) : $the_query->the_post();?>

		<?php if (get_field('price_junior') != '') {
				$package_id = $post->ID;
				$junior_yearly_price =  get_field('price_junior');
				$junior_monthly_price = number_format(round($junior_yearly_price / 10, 2), 2) ; ?>

				<li class="table-row tabel-row-id-<?php echo $package_id; ?>">
					<span class="table-col table-col--title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></span>
          <span class="table-col table-col--month">&nbsp;</span>
					<span class="table-col table-col--season">$<?php echo $junior_yearly_price;?></span>
					<?php /*<span class="table-col table-col--month">
					<?php if($package_id != '162') { ?> $<?php echo $junior_monthly_price; ?> <?php } ?></span> */ ?>
				</li>
		<?php } ?>

		<?php  endwhile; endif;wp_reset_postdata();?>

		</ul>

</div>
</section><!-- .membership-table membership-table-junior -->

</section><!-- main -->

<?php get_footer(); ?>
