<?php
/**
 * Template Name: Package Summary Page
 *
 * The template for displaying a home page
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main main--package-summary-page" role="main">
	<div class="container filter-wrap">

		<?php get_template_part('parts/filter-controls'); ?>

		<div id="filter-container" class="filter-container">
			<div class="fail-message"><span>No items were found matching the selected filters</span></div>

			<?php
			// wp query
			// https://gist.github.com/AaronRutley/5654937
			// $page_id = $post->ID;
			// $parent_page_id = $post->post_parent;

			//args
			$args = array(
			'post_type' => 'packages',
			'posts_per_page' => '-1',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			//'post__not_in' => array(59),
			);

			// new query

			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();

				get_template_part('parts/package-item');

			?>

			<?php  endwhile; endif;wp_reset_postdata();?>

		</div><!-- filter-container-->

	</div><!-- container -->
</section>



<div class="cd-panel from-right">
	<div class="cd-panel-container">
		<div class="cd-panel-content">
			<a href="#0" class="cd-panel-close">Close Tab</a>
			<?php // placeholder div, content is sent here via AJAX ?>
			<div id="package-results">
			</div>
		</div> <!-- cd-panel-content -->
	</div> <!-- cd-panel-container -->
</div> <!-- cd-panel -->



<?php get_footer(); ?>
