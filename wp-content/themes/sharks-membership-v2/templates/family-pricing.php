<?php
/**
 * Template Name: Family Pricing Page
 *
 * The template for displaying the Family Pricing page
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main main--sub-page" role="main">

<?php // Get Rows by looping over the ACF flexible fields on this page
// Get template part for each of the content rows based on layout name
while (the_flexible_field('content_rows')) {
    get_template_part('parts/'. get_row_layout());
} ?>




<section class="membership-table--family">
<div class="container">
	<h2>Family Memberships</h2>
		<p class="table-helper">Tip: Scroll left and right to browse the table</p>
		<ul class="junior-member-table">
		<li class="table-new">
			<span class="table-col--season-new">New in 2016</span>
		</li>
		<li class="table-head">
			<span class="table-col table-col--title">Package </span>
			<span class="table-col table-col--season">2 Adults + </br>2 Juniors</span>
			<span class="table-col table-col--season">1 Adult + </br>2 Juniors</span>
			<span class="table-col table-col--season">1 Adult + </br>3 Juniors</span>
			<span class="table-col table-col--season">1 Adult + </br>4 Juniors</span>
			<span class="table-col table-col--season">2 Adults + </br>1 Junior</span>
			<span class="table-col table-col--season">2 Adults + </br>3 Juniors</span>
			<span class="table-col table-col--season">2 Adults + </br>4 Juniors</span>
		</li>

		<?php
		// wp query
		// https://gist.github.com/AaronRutley/5654937
		// $page_id = $post->ID;
		// $parent_page_id = $post->post_parent;

		//args
		$args = array(
		    'post_type' => 'packages',
			//'post_parent' => 1,
		    'posts_per_page' => '-1',
			'orderby' => 'menu_order',
			'order' => 'ASC'
		);

		// new query
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) :
		while ( $the_query->have_posts() ) : $the_query->the_post();?>

		<?php if (get_field('family_pricing') != '') {
				$package_id = $post->ID; ?>

				<li class="table-row tabel-row-id-<?php echo $package_id; ?>">
					<span class="table-col table-col--title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></span>
					<span class="table-col table-col--season"><?php $field = get_field('2A_2J'); if ($field != '') { echo '$'.$field; } else { echo '&nbsp'; } ?></span>
					<span class="table-col table-col--season"><?php $field = get_field('1A_2J'); if ($field != '') { echo '$'.$field; } else { echo '&nbsp'; } ?></span>
					<span class="table-col table-col--season"><?php $field = get_field('1A_3J'); if ($field != '') { echo '$'.$field; } else { echo '&nbsp'; } ?></span>
					<span class="table-col table-col--season"><?php $field = get_field('1A_4J'); if ($field != '') { echo '$'.$field; } else { echo '&nbsp'; } ?></span>
					<span class="table-col table-col--season"><?php $field = get_field('2A_1J'); if ($field != '') { echo '$'.$field; } else { echo '&nbsp'; } ?></span>
					<span class="table-col table-col--season"><?php $field = get_field('2A_3J'); if ($field != '') { echo '$'.$field; } else { echo '&nbsp'; } ?></span>
					<span class="table-col table-col--season"><?php $field = get_field('2A_4J'); if ($field != '') { echo '$'.$field; } else { echo '&nbsp'; } ?></span>
				</li>
		<?php } ?>

		<?php  endwhile; endif;wp_reset_postdata();?>

		</ul>

		<p><a href="<?php bloginfo('url'); ?>/sharks-info/part-payment/">Part payment available</a></p>

</div>
</section><!-- .membership-table membership-table-junior -->

</section><!-- main -->

<?php get_footer(); ?>
