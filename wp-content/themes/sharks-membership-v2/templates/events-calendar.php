<?php
/**
 * Template Name: Events Calendar
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main main--sub-page" role="main">

	<div class="events-page">

		<?php
			$wp_query = new WP_Query( array('posts_per_page'=>-1,
															'post_type'=>'events',
															'paged' => get_query_var('paged') ? get_query_var('paged') : 1,
															'meta_key'			=> 'date',
															'orderby'			=> 'meta_value',
															'order'				=> 'ASC'
															)
												 );
			?>
			<?php if ($wp_query->have_posts() ) : while ($wp_query -> have_posts()) : $wp_query -> the_post(); ?>

		<div class="event-block">

			<div class="event-block__inner">
			<?php

			$image = get_field('image');
			$size = 'crop-320';

			if( $image ) {

				echo wp_get_attachment_image( $image, $size, "", array( "class" => "event-block__image" ) );

			}

			?>

			<div class="event-block__info">
				<div class="event-info__title"><?php the_title(); ?></div>
				<div class="event-info__date"><label>date</label><span><?php the_field('date'); ?></span></div>
				<div class="event-info__time"><label>time</label><span><?php the_field('time'); ?></span></div>
				<div class="event-info__description">
					<?php the_excerpt();  ?>
				</div>
				<div class="event-info__action">
					<a href="<?php the_field('custom_url_link'); ?>" class="btn"><span>more info & rsvp</span></a>
				</div>
			</div>
		</div>
		</div>

		<?php

			endwhile;
				echo '<div class="pagination">';
				turbo_wp_pagination();
				wp_reset_postdata();
				echo '</div>';
			else:
				echo '<p>No content found</p>';
			endif;

		?>

	</div>

</section><!-- main -->

<?php get_footer(); ?>
