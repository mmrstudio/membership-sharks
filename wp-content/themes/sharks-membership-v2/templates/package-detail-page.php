<?php
/**
 * Template Name: Package Detail Page
 *
 * The template for displaying a home page
 *
 * @package Turbo Starter Theme
 */
?>

<section class="banner--sub">
	<div class="container">
		<h1 class="banner__headline">
            2016 <?php the_title(); ?>
            <?php if(get_field('sold_out')) : ?>
            (SOLD OUT)
            <?php endif; ?>
            <span class="highlight">Membership</span>
        </h1>



        <?php // if button is not hidden, show buy now, if it is hidden, show register

        if(get_field('sold_out')) {

        ?>
			<a class="btn btn--buy-now" href="#">Sold Out</a>
		<?php

        } else {

        if (get_field('hide_buy_now_buttons') != '1') { ?>

			<?php if (get_field('buy_now_link') != '') { ?>
				<a class="btn btn--buy-now" target="_blank" href="<?php the_field('buy_now_link'); ?>">Buy Now</a>
			<?php } else { ?>
				<a class="btn btn--buy-now" target="_blank" href="<?php the_field('buy_now_link','options'); ?>">Buy Now</a>
			<?php } ?>

		<?php } else { ?>
			<a class="btn btn--buy-now" href="<?php the_field('register_link','options'); ?>">Register</a>
		<?php } } ?>
	</div>
</section>

<section class="main main--pacakge-page" role="main">
		<div class="container">
				<div class="package-detail">

					<ul class="social">
						<li><span class="social__icon--arrow"></span></li>
						<li><a class="social__icon--facebook" href="<?php echo social_share_link("facebook"); ?>" target="_blank"></a></li>
						<li><a class="social__icon--twitter" href="<?php echo social_share_link("twitter"); ?>" target="_blank"></a></li>
						<li><a class="social__icon--email" href="<?php echo social_share_link("email"); ?>"></a></li>
					</ul>

					<div class="package-detail__content">

						<div class="package-detail__header">

							<h1 class="banner__headline">2016 <?php the_title(); ?> <span class="highlight">Membership</span></h1>
							<?php // if button is not hidden, show buy now, if it is hidden, show register
							if (get_field('hide_buy_now_buttons') != '1') { ?>
								<?php if (get_field('buy_now_link') != '') { ?>
									<a class="btn btn--buy-now" target="_blank" href="<?php the_field('buy_now_link'); ?>">Buy Now</a>
								<?php } else { ?>
									<a class="btn btn--buy-now" target="_blank" href="<?php the_field('buy_now_link','options'); ?>">Buy Now</a>
								<?php } ?>
							<?php } else { ?>
								<a class="btn btn--buy-now" href="<?php the_field('register_link','options'); ?>">Register</a>
							<?php } ?>
						</div>

						<div class="package-detail__info">

							<h2>Membership Benefits:</h2>

							<?php if (get_field('membership_package_introduction') != '') { ?>
								<blockquote><?php the_field('membership_package_introduction'); ?></blockquote>
							<?php } ?>


							<?php the_field('membership_benifits'); ?>

							<p><a href="<?php the_field('member_benefits_link','options'); ?>" class="btn">View additional Member Benefits</a></p>


							<?php if (get_field('the_view') != '') {
								$thumb = wp_get_attachment_image_src(get_field('the_view'), 'thumb-1140'); ?>
								<h2 class="helper-padding-top">The View </h2>
									<img src="<?php echo $thumb[0]; ?>" />
								</a>
							<?php } ?>


							<?php if (get_field('stadium_map') != '') {
								$map_thumb = wp_get_attachment_image_src(get_field('stadium_map'), 'thumb-640');
								$map_large = wp_get_attachment_image_src(get_field('stadium_map'), 'thumb-1920');?>
								<h2 class="helper-padding-top">Stadium Map</h2>
								<a href="<?php echo $map_large[0]; ?>" target="_blank">
									<img src="<?php echo $map_thumb[0]; ?>" />
								</a>
								<a class="btn" href="<?php echo $map_large[0]; ?>" target="_blank">
									<p>View full size</p>
								</a>
								<?php } ?>
						</div>

						<div class="package-detail__pricing">

							<h2 class="helper-padding-top">Pricing</h2>

							<ul>

								<li class="table-head">
									<span class="table-col table-col--title"><?php the_title(); ?></span>
									<span class="table-col table-col--season">Season</span>
									<?php /*if (get_field('hide_monthly_pricing') != '1') { ?>
										<span class="table-col table-col--month">Month*</span>
									<?php }*/ ?>
                                    <?php if (get_field('savings') != '') { ?>
										<span class="table-col table-col--month">Savings</span>
									<?php } ?>
									<?php if (get_field('hide_buy_now_buttons') != '1') { ?>
										<span class="table-col table-col--action"></span>
									<?php } ?>
								</li>

								<?php // setup an array for each of the pricing fields
								$packages_array = array(
									array(
										'label' => 'Adult',
										'field' => 'price_adult'
									),
									array(
										'label' => 'Concession',
										'field' => 'price_concession'
									),
									array(
										'label' => 'Junior',
										'field' => 'price_junior'
									),
									array(
										'label' => '2 Adults + 2 Juniors',
										'field' => '2A_2J'
									),
									array(
										'label' => '1 Adult + 2 Juniors',
										'field' => '1A_2J'
									),
									array(
										'label' => '1 Adult + 3 Juniors',
										'field' => '1A_3J'
									),
									array(
										'label' => '1 Adult + 4 Juniors',
										'field' => '1A_4J'
									),
									array(
										'label' => '2 Adults + 1 Junior',
										'field' => '2A_1J'
									),
									array(
										'label' => '2 Adults + 3 Juniors',
										'field' => '2A_3J'
									),
									array(
										'label' => '2 Adults + 4 Juniors',
										'field' => '2A_4J'
									)
								);

								// loop over our array to create the faux table rows
								foreach ($packages_array as $package) {

								$package_label = $package['label'];
								$package_field = $package['field'];
								$yearly_price = get_field($package_field);
								$monthly_price = number_format(round($yearly_price / 10, 2), 2) ;
								$buy_now_link = get_field('buy_now_link','options');

								if (get_field($package_field) != '') { ?>
								<li class="table-row">
									<?php  ?>
									<span class="table-col table-col--title"><?php echo $package_label; ?></span>
									<span class="table-col table-col--season">$<?php echo $yearly_price;?></span>
									<?php /*if (get_field('hide_monthly_pricing') != '1') { ?>
										<span class="table-col table-col--month">$<?php echo $monthly_price; ?></span>
									<?php }*/ ?>
                                    <?php if (get_field('savings') != '') { ?>
										<span class="table-col table-col--month">$<?php echo number_format(get_field('savings'), 2); ?></span>
									<?php } ?>
									<?php if (get_field('hide_buy_now_buttons') != '1') { ?>

										<?php if (get_field('buy_now_link') != '') { ?>
											<span class="table-col table-col--action"><a href="<?php the_field('buy_now_link'); ?>">Buy <span>Now</span></a></span>
										<?php } else { ?>
											<span class="table-col table-col--action"><a href="<?php echo $buy_now_link;  ?>">Buy <span>Now</span></a></span>
										<?php } ?>

									<?php } ?>
								</li>
								<?php } // if
								}  // each ?>

							</ul>

							<p>*Based on a full 10 month payment plan</p>

						</div><!-- pricing -->



							<?php // custom relationship query
							$posts = get_field('you_may_also_like');
							if( $posts ): ?>
							<div class="package-detail__related">
								<h2>You may also like</h2>
								<?php // loop over the package items
								foreach( $posts as $post):
									setup_postdata($post);
									get_template_part('parts/package-item');
								endforeach;
								wp_reset_postdata(); ?>
								</div>
							<?php endif;?>

					</div><!-- package detail content-->

			</div><!-- package detail -->
		</div><!-- container -->
</section><!-- main -->
