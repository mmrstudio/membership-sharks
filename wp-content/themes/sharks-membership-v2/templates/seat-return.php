<?php
/**
 * Template Name: Seat-Return
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

     <div class="seat-return-page">

          <div class="box-container">

               <?php if(get_field('step_box')): ?>
     			<?php while(the_repeater_field('step_box')): ?>
               <div class="step-box" style="background: url('<?php the_sub_field('steps_image'); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center;">
                    <span><?php the_sub_field('steps_title'); ?></span>
                    <p><?php the_sub_field('steps_statement'); ?></p>
                    <span><?php the_sub_field('steps_subtext'); ?></span>
                    <div class="overlay"></div>
               </div>
                    <?php endwhile; ?>
          	<?php endif; ?>
          </div>

          <?php if(have_rows('guide_upper')): ?>
               <?php while(have_rows('guide_upper')): the_row(); ?>
                    <?php if(get_row_layout() == 'guide_upper_content'): ?>
          <div class="guide--upper">
               <h2><?php the_sub_field('guide_upper_title'); ?></h2>
               <p><?php the_sub_field('guide_upper_paragraph'); ?></p>
          </div>
                    <?php endif; ?>
               <?php endwhile; ?>
          <?php endif; ?>

          <div class="container--lower">
               <?php if(have_rows('guide_lower')): ?>
                    <?php while(have_rows('guide_lower')): the_row(); ?>
                         <?php if(get_row_layout() == 'guide_lower_content'): ?>
               <div class="guide--lower">
                    <h2><?php the_sub_field('guide_lower_title'); ?></h2>
                    <article>
                         <p><?php the_sub_field('guide_lower_paragraph'); ?></p>
                    </article>
                          <?php if(get_sub_field('guide_lower_button')): ?>
                                <a class="btn" href="<?php the_sub_field('guide_lower_button_link'); ?>" target="_blank"><span><?php the_sub_field('guide_lower_button'); ?></span></a>
                           <?php endif; ?>
                         <?php endif; ?>
                    <?php endwhile; ?>
               <?php endif; ?>

               </div>

               <div class="return__table">
                    <p class="table-title">rebate prices</p>
                    <table>
                         <thead>
                              <tr>
                                   <th>seat category</th>
                                   <th>membership type</th>
                                   <th>rebate if sold</th>
                              </tr>
                         </thead>
                         <tbody>
                              <?php if(get_field('rebate_prices')): ?>
                                   <?php while(the_repeater_field('rebate_prices')): ?>
                              <tr>
                                   <td>
                                        <?php the_sub_field('seat_category'); ?>
                                   </td>
                                   <td>
                                        <?php the_sub_field('membership_type'); ?>
                                   </td>
                                   <td>
                                        <?php the_sub_field('rebate_if_sold'); ?>
                                   </td>
                              </tr>
                                   <?php endwhile; ?>
                              <?php endif; ?>
                         </tbody>
                    </table>
               </div>
          </div><!-- END container--lower -->

     </div>

<?php get_footer(); ?>
