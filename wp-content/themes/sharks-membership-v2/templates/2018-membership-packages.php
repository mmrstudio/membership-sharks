<?php
/**
 * Template Name: 2018 Membership Packs page
 *
 * Membership Packs page template
 *

 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>




	<section class="year-membership-packs-page">
		<!-- <div class="container">
			<div class="membership-packs-page__list"> -->


		<div class="all-membership__sidebar">

			<div class="all-membership__sidebar__category">
				<h3>location</h3>
				<?php
				function get_terms_chekboxes_location($taxonomies, $args) {
					$terms = get_terms($taxonomies, $args);
					foreach($terms as $term){
						$output .= '<input class="hidden checkbox" type="checkbox" id="'.$term->slug.'" name="'.$term->taxonomy.'" value="'.$term->slug.'"><label for="'.$term->slug.'" class="checkbox-button"><span> '.$term->name.'</span></label>';
					}
					return $output;
				}

				echo get_terms_chekboxes_location('location', $args = array('hide_empty'=>false));
				?>
			</div>

			<div class="all-membership__sidebar__category">
				<h3>access</h3>
				<?php
				function get_terms_chekboxes_membership($taxonomies, $args) {
					$terms = get_terms($taxonomies, $args);
					foreach($terms as $term){
						$output .= '<input class="hidden checkbox" type="checkbox" id="'.$term->slug.'" name="'.$term->taxonomy.'" value="'.$term->slug.'"><label for="'.$term->slug.'" class="checkbox-button"><span> '.$term->name.'</span></label>';
					}
					return $output;
				}

				echo get_terms_chekboxes_membership('membership', $args = array('hide_empty'=>false));
				?>
			</div>
			<div class="all-membership__sidebar__category">
				<h3>seating</h3>
				<?php
				function get_terms_chekboxes_seat($taxonomies, $args) {
					$terms = get_terms($taxonomies, $args);
					foreach($terms as $term){
						$output .= '<input class="hidden checkbox" type="checkbox" id="'.$term->slug.'" name="'.$term->taxonomy.'" value="'.$term->slug.'"><label for="'.$term->slug.'" class="checkbox-button"><span> '.$term->name.'</span></label>';
					}
					return $output;
				}

				echo get_terms_chekboxes_seat('seat', $args = array('hide_empty'=>false));
				?>

			</div>
		</div>



		<div class="all-membership__main">
			<?php
				$orderby = get_field('order_package_by');
				$order = get_field('order');
				$wp_query = new WP_Query( array('posts_per_page'=>100,
																'post_type'=>'packages',
																'orderby' => $orderby,
																'order' => $order,
																'paged' => get_query_var('paged') ? get_query_var('paged') : 1)

													 );
				?>
				<?php if ($wp_query->have_posts() ) : while ($wp_query -> have_posts()) : $wp_query -> the_post(); ?>

			<div class="help_me-row__packages--group_details item" data-category="<?php

				$terms = get_the_terms( $post->ID, array( 'seat', 'membership', 'location' ) );
				if ($terms) {
				    foreach($terms as $term) {
				      echo ' '.$term->slug;
				    }
				}

			?>">

				<div class="bg">
					<div class="title">
						<h3><?php echo get_the_title($group->ID) ?></h3>
					</div>
					<div class="details">
						<div class="descriptor"><?php echo get_field('price_descriptor',$group->ID); ?></div>
						<div class="price">$ <?php echo get_field('price_start',$group->ID); ?></div>
						<div class="subtext"><?php echo get_field('price_subtext',$group->ID); ?></div>
					</div>
					<div class="action">
						<a href="<?php echo get_field('custom_page_link',$group->ID); ?>" class="btn">MORE INFO</a>
					</div>
					<div class="image">
						<?php $image = wp_get_attachment_image_src(get_field('panel_image',$group->ID),'panel');  ?>
						<img src="<?php echo $image[0]; ?>" />
						<?php if(get_field('sold_out')) : ?>
								<div class="sold_out"><img src="<?php bloginfo('template_url'); ?>/assets/images/sold-out.png" /></div>
						<?php endif; ?>
					</div>
				</div>
			</div>

									<?php

										endwhile;
											// echo '<div class="pagination">';
											// turbo_wp_pagination();
											// wp_reset_postdata();
											// echo '</div>';
										else:
											echo '<p>No content found</p>';
										endif;

									?>
		</div>


			<!-- </div><!-- -END membership-packs-page__list -->

		<!-- </div> -->
	</section><!-- .member-benefits -->


<?php get_footer();
