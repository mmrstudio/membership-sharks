<?php
/**
 * Template Name: Membership Packs page
 *
 * Membership Packs page template
 *

 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main main--sub-page" role="main">

<div class="fluid-img">
	<?php if(get_field('image_packs')): ?>
	<?php while(the_repeater_field('image_packs')): ?>
		<p><img src="<?php the_sub_field('image'); ?>"></p>
	<?php endwhile; ?>
	<?php endif; ?>
</div>



	<?php // Get Rows by looping over the ACF flexible fields on this page
	// Get template part for each of the content rows based on layout name
	while (the_flexible_field('content_rows')) {
		get_template_part('parts/'. get_row_layout());
	} ?>


	<section class="membership-packs-page">
		<div class="container">
			<div class="membership-packs-page__list">

				<?php
				if( have_rows('packs') ):
					// loop through the rows of data
					while ( have_rows('packs') ) : the_row();
						$thumb = wp_get_attachment_image_src(get_sub_field('image'),"full");  ?>

						<div class="pack_container">

							<div class="pack">
								<div class="title"><?php echo strtoupper(get_sub_field('title')); ?></div>
								<div class="image">
									<img src="<?php echo $thumb[0]; ?>" >
								</div>

								<?php if( have_rows('items') ): ?>
									<div class="description">
										<ul>
											<?php
											// loop through rows (sub repeater)
											while( have_rows('items') ): the_row();

												// display each item as a list - with a class of completed ( if completed )
												?>
												<li><span><?php the_sub_field('Item'); ?></span></li>
											<?php endwhile; ?>
										</ul>
									</div>
								<?php endif; //if( get_sub_field('items') ): ?>
							</div>
						</div>
						<?php
					endwhile;
				endif;
				?>

			</div>

		</div>
	</section><!-- .member-benefits -->
</section>

<?php get_footer();
