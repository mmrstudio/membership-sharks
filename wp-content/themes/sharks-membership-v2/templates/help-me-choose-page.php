<?php
/**
 * Template Name: Help Me Choose Page
 *
 * The template for displaying a home page
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>


<section class="main main--help-me-choose-page" role="main">

		<div class="container help-me-decide">

			<?php if (get_field('banner_help_text') != '') { ?>
				<p class="help-me-decide__help-text"><?php the_field('banner_help_text'); ?></p>
			<?php } ?>

			<div id="questionBlock">
				<div class="question">

					<div class="heading">
						<h6 class="heading-title"></h6>
						<div class="response"><div class="response-answer"></div></div>
					</div>

					<div class="question-wrap">
						<h2 class="question-text"></h2>
						<div class="question-answers"></div>
					</div>
				</div>
			</div>

			<div class="questions" id="questions">
			</div>

			<div class="suggested-packages-wrap">

				<div class="heading">
					<h6 class="heading-title">Suggested Package/s</h6>
				</div>

				<div class="load-area">
					<div class="suggested-packages" id="suggestedPackages"></div>
				</div>
			</div>

<script>

var questions = {

<?php

    $questions = array();

    if( have_rows('questions') ) : while( have_rows('questions') ): the_row();

        $answers = array();
        $q_id = get_sub_field('id');
        $a_num = 0;

        if( have_rows('answers') ) : while( have_rows('answers') ): the_row();

            switch(get_sub_field('action')) :
                case 'Go To Question' : $action = "function() { this.gotoQuestion('". get_sub_field('goto_question') ."'); }"; break;
                case 'Show Package' :
                    $package_urls = array();
                    foreach(get_sub_field('packages') as $pkg) :
                        $package_urls[] = "'" . get_permalink($pkg->ID) . "/?summary=true'";
                    endforeach;
                    $action = "function() { this.showPackages([". implode(',', $package_urls) ."]); }";
                break;
                case 'Show Content' :

                    $page = get_sub_field('content');

                    $package_urls = array();
                    foreach(get_sub_field('packages') as $pkg) :
                        $package_urls[] = "'" . get_permalink($pkg->ID) . "/?summary=true'";
                    endforeach;
                    //$action = "function() { this.showPackages([". implode(',', $package_urls) ."]); }";

                    $action = "function() { this.showContent('".get_permalink($page->ID)."/?summary=true', [". implode(',', $package_urls) ."]); }";

                break;
            endswitch;

            $answers[] = $q_id . "_a" . $a_num . " : { text: '".addslashes(get_sub_field('answer'))."', response: ".$action." }";

            $a_num++;

        endwhile; endif;

        $questions[] = $q_id . " : { heading: '".get_sub_field('heading')."', text: '".get_sub_field('question')."', answers: {".implode(',', $answers)."} }";

    endwhile; endif;

    echo implode(",\n", $questions);
    //print_r($questions);

?>

};



</script>





<div class="cd-panel from-right">
	<div class="cd-panel-container">
		<div class="cd-panel-content">
			<a href="#0" class="cd-panel-close">Close Tab</a>
			<?php // placeholder div, content is sent here via AJAX ?>
			<div id="package-results">
			</div>
		</div> <!-- cd-panel-content -->
	</div> <!-- cd-panel-container -->
</div> <!-- cd-panel -->



		</div>
</section><!-- main -->



<?php get_footer(); ?>
