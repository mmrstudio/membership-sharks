<?php
/**
 * Template Name: Package Summary Page (Renew)
 *
 * The template for displaying a home page
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<?php if(have_rows('content_rows')) : ?>
<section class="main main--sub-page">

<?php // Get Rows by looping over the ACF flexible fields on this page
// Get template part for each of the content rows based on layout name

while (the_flexible_field('content_rows')) {
    get_template_part('parts/'. get_row_layout());
} ?>

</section><!-- main -->
<?php endif; ?>

<section class="main main--package-summary-page" role="main">
	<div class="container filter-wrap">

		<?php get_template_part('parts/filter-controls'); ?>

		<div id="filter-container" class="filter-container">
			<div class="fail-message"><span>No items were found matching the selected filters</span></div>

			<?php
			// wp query
			// https://gist.github.com/AaronRutley/5654937
			// $page_id = $post->ID;
			// $parent_page_id = $post->post_parent;

			//args
			$args = array(
			'post_type' => 'packages',
			'posts_per_page' => '-1',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			);

			// new query

			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();

				get_template_part('parts/package-item-renew');

			?>

			<?php  endwhile; endif;wp_reset_postdata();?>

		</div><!-- filter-container-->

	</div><!-- container -->
</section>



<div class="cd-panel from-right">
	<div class="cd-panel-container">
		<div class="cd-panel-content">
			<a href="#0" class="cd-panel-close">Close Tab</a>
			<?php // placeholder div, content is sent here via AJAX ?>
			<div id="package-results">
			</div>
		</div> <!-- cd-panel-content -->
	</div> <!-- cd-panel-container -->
</div> <!-- cd-panel -->



<?php get_footer(); ?>
