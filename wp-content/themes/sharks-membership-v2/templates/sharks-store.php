<?php
/**
 * Template Name: Sharks Store Discount
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main" role="main">

	<div class="sharks-store">

		<div>
			<?php if(have_rows('store_intro')): ?>
	               <?php while(have_rows('store_intro')): the_row(); ?>
	                    <?php if(get_row_layout() == 'store_intro_content'): ?>
			<div class="sharks-store__intro">
				<p><?php the_sub_field('store_intro_content_paragraph'); ?></p>
				<!--<a class="btn" href="https://sharksstore.com.au/" target="_blank">login & shop now</a>--><!-- HIDDEN -->
			</div>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>

			<?php if(have_rows('store_range')): ?>
	               <?php while(have_rows('store_range')): the_row(); ?>
	                    <?php if(get_row_layout() == 'store_range_content'): ?>
			<div class="sharks-store__range">
				<p><?php the_sub_field('store_range_content_paragraph'); ?></p>

				<?php if(get_sub_field('button')): ?>
							<a class="btn" href="<?php the_sub_field('button_link'); ?>" target="_blank"><span><?php the_sub_field('button'); ?></span></a>
				 <?php endif; ?>
				
			</div>
					<?php endif; ?>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>

		<div class="sharks-store__steps-side">
		<?php if(get_field('store_steps')): ?>
			<?php while(the_repeater_field('store_steps')): ?>
			<div class="steps-side__line">
				<span class="steps-icon icon">
					<span class="steps-icon__steps">step</span>
					<span class="steps-icon__number"><?php the_sub_field('store_steps_order'); ?></span>
				</span>
				<h4><?php the_sub_field('store_steps_text'); ?></h4>
			</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>

</section><!-- main -->

<?php get_footer(); ?>
