<?php
/**
 * Template Name: Home Page
 *
 * The template for displaying a home page
 *
 * @package Turbo Starter Theme
 */

get_header(); ?>



<?php // Get Rows by looping over the ACF flexible fields on this page
// Get template part for each of the content rows based on layout name
//get_template_part('parts/row_help_me');
while (the_flexible_field('content_rows')) {
    get_template_part('parts/'. get_row_layout());
} ?>


<?php get_footer(); ?>
