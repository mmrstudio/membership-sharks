<?php
/**
 * Template Name: Member Benefits page
 *
 * The template for displaying the Member Benefits page
 *

 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

	<?php // Get Rows by looping over the ACF flexible fields on this page
	// Get template part for each of the content rows based on layout name
	while (the_flexible_field('content_rows')) {
		get_template_part('parts/'. get_row_layout());
	} ?>

	<script>
		$(function(){


		});
	</script>

	<section class="member-benefits-page">
		<div class="container">
			<h2>NEW IN 2018</h2>
			<div class="member-benefits-page__list">
				<?php
				if( have_rows('new') ):
					// loop through the rows of data
					while ( have_rows('new') ) : the_row();
						$thumb = wp_get_attachment_image_src(get_sub_field('image'));  ?>
				<div class="benefit dark">

					<?php if (get_sub_field('link') != ''): ?>
					<a href="<?php the_sub_field('link'); ?>">
					<?php endif; ?>
						<div class="image">
							<img src="<?php echo $thumb[0]; ?>" >
						</div>
						<div class="title"><?php the_sub_field('title'); ?></div>
						<?php  if (get_sub_field('description') != ''): ?>
						<div class="description"><?php the_sub_field('description'); ?></div>
						<?php endif; ?>
					</a>

				</div>
						<?php
					endwhile;
				endif;
				?>
			</div>

			<hr>

			<h2>ADDITIONAL MEMBER BENEFITS</h2>
			<div class="member-benefits-page__list">
				<?php
				if( have_rows('additional') ):
					// loop through the rows of data
					while ( have_rows('additional') ) : the_row();
						$thumb = wp_get_attachment_image_src(get_sub_field('image'));  ?>
				<div class="benefit">

					<?php if (get_sub_field('link') != ''): ?>
					<a href="<?php the_sub_field('link'); ?>">
					<?php endif; ?>
						<div class="image">
							<img src="<?php echo $thumb[0]; ?>" >
						</div>
						<div class="title"><?php the_sub_field('title'); ?></div>
						<?php  if (get_sub_field('description') != ''): ?>
						<div class="description"><?php the_sub_field('description'); ?></div>
						<?php endif; ?>
					</a>

				</div>
						<?php
					endwhile;
				endif;
				?>
			</div>
		</div>
	</section><!-- .member-benefits -->

<?php get_footer();