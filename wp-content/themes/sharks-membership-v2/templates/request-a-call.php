<?php
/**
 * Template Name: Request A Call
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--sub');

?>

<section class="main" role="main">

	<div class="sharks-request">

		<?php the_field('description'); ?>

		<div>
			<div class="sharks-store__intro">
				<?php the_field('form_code'); ?>
			</div>
		</div>


</section><!-- main -->

<?php get_footer(); ?>
