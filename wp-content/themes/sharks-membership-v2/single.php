<?php
/**
 * The template for displaying a default single post / news item
 *
 * @package Turbo Starter Theme
 */

get_header();

// get banner for sub page
get_template_part('parts/banner--news');

?>

<section class="main main--news-single" role="main">
		<div class="container">

			<article class="news-single">
				<span class="post-border"></span>
				<div class="post-date">
					<span class="post-date__item post-date__item--month"><?php echo get_the_time('M') ?></span>
					<span class="post-date__item post-date__item--day"><?php echo get_the_time('j') ?></span>
					<span class="post-date__item post-date__item--yaer"><?php echo get_the_time('Y') ?></span>
				</div>


				<div class="news-single__content">
					<h1><?php the_title(); ?></h1>
					<?php if (get_field('news_thumbnail') != '') { ?>
						<?php   $thumb = wp_get_attachment_image_src(get_field('news_thumbnail'), 'thumb-1140');  ?>
						<img src="<?php echo $thumb[0]; ?>" />
					<?php } ?>

					<div class="news-single__content-full">
						<?php the_field('full_content'); ?>
					</div>
				</div>

			</article>

			<aside class="news-sidebar">
				<h3>Other News</h3>

				<?php
				// wp query
				// https://gist.github.com/AaronRutley/5654937
				$post_id = $post->ID;
				// $parent_page_id = $post->post_parent;

				//args
				$args = array(
				    'post_type' => 'post',
				    'posts_per_page' => '2',
				    'post__not_in' => array($post_id),
					//'orderby' => 'menu_order',
					//'order' => 'DESC'
				);

				// new query
				$the_query = new WP_Query( $args );
				if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) : $the_query->the_post();?>
					<div class="news-sidebar__post">
						<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
						<p><?php the_field('news_intro'); ?></p>
						<a href="<?php the_permalink() ?>" class="btn btn--read-more">Read More</a>
					</div>

				<?php  endwhile; endif;wp_reset_postdata();?>

			</aside>

		</div><!-- container -->
</section><!-- main -->

<?php get_footer(); ?>
