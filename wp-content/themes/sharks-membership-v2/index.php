<?php
/**
 * The template for displaying posts / news / 'the loop'
 *
 * @package Turbo Starter Theme
 */
get_header();

// get banner for news page
get_template_part('parts/banner--news');

?>

<section class="main main--news-summary-page" role="main">
		<div class="container">

		<?php // The WordPress 'loop'
		if ( have_posts() ) :
		while ( have_posts() ) : the_post();
			// get template part for news summary
			get_template_part( 'parts/news-summary');
		endwhile;
			echo '<div class="pagination">';
			turbo_wp_pagination();
			echo '</div>';
		else:
			echo '<p>No content found</p>';
		endif;?>

		</div><!-- container -->
</section><!-- main -->

<?php get_footer(); ?>
