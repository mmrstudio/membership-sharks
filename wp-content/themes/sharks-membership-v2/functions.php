<?php
// Project specific functions belong in this file or partials

// Require function partials
require get_template_directory() . '/inc/custom_header.php';
require get_template_directory() . '/inc/custom_scripts.php';
require get_template_directory() . '/inc/custom_core.php';
require get_template_directory() . '/inc/custom_menus.php';
require get_template_directory() . '/inc/custom_fields.php';
require get_template_directory() . '/inc/custom_post_types.php';

// Add Theme Thumbnails and custom image sizes
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support('post-thumbnails');
	add_image_size( 'thumb-320', 320, '', true );
	add_image_size( 'crop-320', 320, 200, true );
	add_image_size( 'square-400', 400, 400, true );
	add_image_size( 'thumb-640', 640, '', true );
	add_image_size( 'crop-640', 640, 400, true );
	add_image_size( 'thumb-1140', 1140, '', true );
	add_image_size( 'thumb-1920', 1920, '', true );
	add_image_size( 'slider', '' , 580, true );
	add_image_size( 'slider_mobile', '' , 360, true );

	add_image_size( 'panel', 540, 400, true );
	add_image_size( 'packs', '', 280, true );
}

// Theme Editor Styles
function turbo_theme_editor_styles() {
	add_editor_style( 'assets/css/editor_style.css' );
}
add_action('admin_init', 'turbo_theme_editor_styles' );

// Social Share
function social_share_link($service) {
	global $post;
	$site_title = get_bloginfo('title');
	$post_url = get_the_permalink($post->ID);
	$post_url_encoded = urlencode($post_url);
	$post_package_name = get_the_title($post->ID);
	$post_title = $site_title.' 2016 '.$post_package_name.' Membership';
	if($service == "twitter") {
		return "https://twitter.com/intent/tweet/?text=".$post_title."&url=".$post_url_encoded;
	}
	if($service == "facebook") {
		return "https://www.facebook.com/sharer/sharer.php?u=".$post_url_encoded;
	}
	if($service == "linkedin") {
		return "https://www.linkedin.com/shareArticle?mini=true&url=".$post_url_encoded."&title=".$post_title."&source=".$post_url_encoded."&summary=".$post_title;
	}
	if($service = "email") {
		return "mailto:?subject=".$post_title."&body=".$post_title.' </br> '.$post_url_encoded;
	}
}

// First time session (used for counter)
function is_first_time() {
    if(session_id() == '') session_start();
    if (isset($_SESSION['_wp_first_time'])) return false;
    $_SESSION['_wp_first_time'] = true;
    return true;
}
define('FIRST_VISIT', is_first_time());

// Helper - is ajax
// function is_ajax() {
//     return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
// }

// gravity forms swap spinner

function tgm_io_custom_gforms_spinner( $src ) {
    return get_stylesheet_directory_uri() . '/assets/images/gform-loading.gif';
}
add_filter( 'gform_ajax_spinner_url', 'tgm_io_custom_gforms_spinner' );

function mmr_disable_scripts() {

	if (!is_admin()) {
		wp_deregister_script('wp-embed');

		// Load jQuery from CDN
		$jquery_version = '2.1.3';
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/' . $jquery_version . '/jquery.min.js' , array() , $jquery_version , FALSE );
		wp_enqueue_script( 'jquery' );
	}

}
add_action('init', 'mmr_disable_scripts');
