<?php // single column text ?>

	<section class="row-banner">


		<?php $banner = get_sub_field('background_image');  ?>
		<?php
			// get membership count from options panel

			$membership_count = false;
			$membership_total = 0;
			$membership_total = get_field('membership_total', 'option');

			if( $membership_total ) :
				$membership_total = 15000 - (int)$membership_total;
				$membership_count = str_pad($membership_total, 3, "0", STR_PAD_LEFT);
				$membership_count = str_split($membership_count);
				//var_dump($membership_count);
			endif;

			// Apply animate if First visit and total is greater than 0
			$animate_class = false;
			if(FIRST_VISIT && $membership_total > 0) :
				$animate_class = 'animate';
			endif;
		?>


		<?php
			// calc days left to round 15
			$date1 = new DateTime("now", new DateTimeZone('Australia/Melbourne'));
			$finish = "2017-06-17 23:59:59";
			$finish = new DateTime($finish, new DateTimeZone('Australia/Melbourne'));
		//var_dump($date1);
		//var_dump($finish);
			//difference
			$diff = date_diff($date1, $finish);
			//var_dump($diff);
			$diff = ($diff -> days) ;

			$day_count = str_pad($diff, 2, "0", STR_PAD_LEFT);

			$day_count= str_split($day_count);
			//var_dump($day_count);

		?>

		<div class="row-banner__banner" style="background: url(<?php echo $banner['url'] ?>) no-repeat scroll center top transparent;background-size:cover;">
			<div class="row-banner__headline">
				<h1 class="row-banner__headline"><?php the_sub_field('heading_1');  ?></h1>
				<div class="subtext"><?php the_sub_field('heading_2');  ?></div>

				<div class="counter_wrapper">

					<!--

					<div class="counter members animate" data-membership-count="<?php echo ($membership_total > 0) ? $membership_total: '0'; ?>">
						<div><span><?php echo ($membership_count) ? $membership_count[0] : '0'; ?></span></div>

						<div><span><?php echo ($membership_count) ? $membership_count[1] : '0'; ?></span></div>
						<div><span><?php echo ($membership_count) ? $membership_count[2] : '0'; ?></span></div>


						<div class="text_members">Members to recruit</div>
					</div>
					<div class="counter arrow"></div>
					<div class="counter days">
						<div><span><?php echo ($day_count) ? $day_count[0] : '0'; ?></span></div><div><span><?php echo ($day_count) ? $day_count[1] : '0'; ?></span></div>
						<div class="text_days">Days left</div>
					</div>
					<div class="counter arrow last"></div>-->
					<div class="counter button"><a href="/package/general-admission/#flexi-3-copy" class="btn" tabindex="-1">JOIN NOW</a></div>
				</div>
			</div>
		</div>
		<div class="row-banner__actions">
			<div>
				<div>
					<a href="https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login" target="_blank" class="btn">RENEW</a>
				</div>
				<div>
					<a href="https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login " target="_blank" class="btn">My Account</a>
				</div>
			</div>
		</div>
	</section>
