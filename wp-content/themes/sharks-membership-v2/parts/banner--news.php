<section class="banner banner--sub">
	<?php  $banner_image = wp_get_attachment_image_src(get_field('banner_image',9),'full');  ?>

	<div class="container" style="background: url(<?php echo $banner_image[0] ?>) no-repeat scroll center top transparent;">
		<div class="text">
			<span class="text"><h1 class="banner__headline"><?php the_field('banner_line_1', 9); ?> <span class="highlight"><?php the_field('banner_line_2',9); ?></span></h1></span>
		</div>
	</div>
</section>
