<section class="filter-controls">
<h3>Refine your options</h3>
<form id="filter-form" class="filter-form" >
	  <!-- We can add an unlimited number of "filter groups" using the following format: -->
		<fieldset>
		  <h4>Seat Type</h4>
		  <?php // query the terms for the seat taxonomy and create a check box for each
			$terms = get_terms( 'seat', array( 'parent' => 0 ));
				//var_dump($terms);
				foreach ( $terms as $term ) {
					// The $term is an object, so we don't need to specify the $taxonomy.
					$term_link = get_term_link( $term );
					$term_name = $term->name;
					$term_slug = '.package-item--'.$term->slug;
					// If there was an error, continue to the next term.
					if ( is_wp_error( $term_link ) ) {
						continue;
					}
					?>
					<div class="checkbox">
					  <input type="checkbox" value="<?php echo $term_slug; ?>" class="js-<?php echo $term->slug; ?>"/>
					  <label><?php echo $term_name; ?></label>
					</div>
			<?php } // end for each  ?>
		</fieldset>

		<fieldset>
		  <h4>Membership Type</h4>
		  <?php // query the terms for the membership taxonomy and create a check box for each
			$terms = get_terms( 'membership', array( 'parent' => 0 ));
				//var_dump($terms);

				foreach ( $terms as $term ) {
					// The $term is an object, so we don't need to specify the $taxonomy.
					$term_link = get_term_link( $term );
					$term_name = $term->name;
					$term_slug = '.package-item--'.$term->slug;
					// If there was an error, continue to the next term.
					if ( is_wp_error( $term_link ) ) {
						continue;
					}
					?>
					<div class="checkbox">
					  <input type="checkbox" value="<?php echo $term_slug; ?>"/>
					  <label><?php echo $term_name; ?></label>
					</div>
			<?php } // end for each  ?>
		</fieldset>
	  <button id="filter-reset">Clear Filters</button>
</form>

</section><!-- .filter-controls -->
