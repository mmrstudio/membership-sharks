<section class="content-row content-row--specials">
	<div class="container">
		<div class="content-row__copy">

			<?php the_field('description'); ?>

				<div class="speicals-table">
					<p class="table-helper">Tip: Scroll left and right to browse the table</p>
				<ul>
					<li class="table-head">
					<span class="table-col">Category </span>
					<span class="table-col">Average Discount</span>
					<span class="table-col">Average Spend</span>
					<span class="table-col">Average Savings P/A</span>
					</li>

						<?php if(get_sub_field('table_rows')): ?>
						<?php while(the_repeater_field('table_rows')): ?>

						<li class="table-row">
							<span class="table-col"><?php the_sub_field('col_1'); ?></span>
							<span class="table-col"><?php the_sub_field('col_2'); ?></span>
							<span class="table-col"><?php the_sub_field('col_3'); ?></span>
							<span class="table-col"><?php the_sub_field('col_4'); ?></span>
						</li>

					<?php endwhile; ?>
					<?php endif; ?>
				</ul>
				</div>
		</div>
		<div class="content-row__image">
			<?php if (get_sub_field('image') != '') { ?>
				<?php   $thumb = wp_get_attachment_image_src(get_sub_field('image'), 'thumb-640');  ?>
				<img src="<?php echo $thumb[0]; ?>" />
			<?php } ?>
		</div>
	</div>
</section>
