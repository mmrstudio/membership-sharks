<?php // setup a container class
$container_class = '';
// get the layout from the flexible content field
$layout = get_sub_field('layout');
// if layout is highlight, create a modifier class
if($layout == 'highlight') {
	$container_class = "content-row--highlight";
}
?>
<script src="https://rawgit.com/wenzhixin/multiple-select/master/multiple-select.js"></script>

<section class="help_me-row__refine-container <?php echo $container_class; ?>">
	<div class="container">
		<div class="help_me-row__refine">
			<h1>FIND THE RIGHT PACKAGE FOR YOU</h1>
			Select one or more options from the filters below to find the membership package that suits your needs.

			<div class="selection">
				<div class="option_empty">&nbsp;</div>
				<?php foreach (array('location' => 'Game Access','seat' => 'Seating') as $tax => $title): ?>
					<div class="option">
						<h3><?php echo $title ?></h3>
						<select class="help_me_refine" multiple="multiple" palceholder="Please select" data-type="<?php echo $tax ?>">
							<?php

							// your taxonomy name
							//						$tax = 'seat';

							// get the terms of taxonomy
							$terms = get_terms( $tax, [
								'hide_empty' => false, // do not hide empty terms
							]);
							foreach( $terms as $term ): ?>
								<?php if($term->count > 0 ):?>
									<option value="<?php echo $term->slug ?>"><?php echo  $term->name  ?></option>
								<?php endif; ?>
							<?php endforeach; ?>


						</select>

					</div>
				<?php endforeach; ?>
			</div>

			<script>
				$('.help_me_refine').multipleSelect({
					width: '100%',
					placeholder: "Please select",
					onClick: function(view) {
						help_me_update();
					}
				});

				function help_me_update()
				{
					var refine = false;
					var filter = [];
					var filter_str = '';
					$("select.help_me_refine").each(function(){
						var types = $(this).multipleSelect("getSelects");
						var tax = $(this).data('type');
						if (types.length > 0)
						{
							refine = true;
							types.forEach(function(value, key, myArray) {
								filter.push('.' + tax + '_' + value);
//									filter_str += '.' + tax + '_' + value;
								filter_str += '.package-item--' + value;
							});
						}
					});
					if (refine)
					{
						$('.package-item').hide();
						$(filter_str).show();
						$('#packages_selects').hide();
						$('#packages_no_matches').hide();
						$('#packages_all').show();

						if ($('.package-item:visible').length == 0)
						{
							$('#packages_no_matches').show();
							$('#packages_all').hide();
						}
					}
					else
					{
						$('#packages_selects').show();
						$('#packages_all').hide();
						$('#packages_no_matches').hide();
					}
				}


			</script>


		</div>
	</div>
</section>
<section class="help_me-row__results-container <?php echo $container_class; ?>">
	<div class="container">
		<div class="help_me-row__packages">
			<div id="packages_selects" class="results"><?php get_template_part('parts/row_help_me-groups'); ?></div>
			<div id="packages_no_matches" class="results" style="display:none" ><?php get_template_part('parts/row_help_me-no_matches'); ?></div>
		</div>
		<div class="player player_right">
			<img src="/wp-content/themes/sharks-membership/assets/images/Player_Right.png">
		</div>
		<div class="player player_left">
			<img src="/wp-content/themes/sharks-membership/assets/images/Player_Left.png">

		</div>
	</div>


</section>
