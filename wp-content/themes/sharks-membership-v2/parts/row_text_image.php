<?php // setup a container class
$container_class = '';
// get the layout from the flexible content field
$layout = get_sub_field('layout');
// if layout is highlight, create a modifier class
if($layout == 'highlight') {
	$container_class = "content-row--highlight";
}
?>

<section class="content-row_text_image <?php echo $container_class; ?>">
	<div class="container">
		<div class="content-row_text_image__title">
			<h2><?php the_sub_field('sub_heading'); ?></h2>
		</div>
		<div class="content-row_text_image__copy">

			<?php the_sub_field('text'); ?>
			<?php if (get_sub_field('button_text') != '') { ?>
				<p><a href="<?php the_sub_field('button_link'); ?>" class="btn"><?php the_sub_field('button_text'); ?></a></p>
			<?php } ?>

		</div>
		<div class="content-row_text_image__image">
            <?php if (get_sub_field('content')) { ?>
                <?php echo get_sub_field('content'); ?>
            <?php } else { ?>
			<?php if (get_sub_field('image') != '') { ?>
				<?php   $thumb = wp_get_attachment_image_src(get_sub_field('image'), 'thumb-640');  ?>
					<?php if (get_sub_field('image_link') != '') { ?>
                    	<a href="<?php the_sub_field('image_link'); ?>" ><img src="<?php echo $thumb[0]; ?>" /></a>
                    <?php }else{ ?>
                    		<img src="<?php echo $thumb[0]; ?>" />
                    <?php } ?>
			<?php } ?>
            <?php } ?>
			<?php if (get_sub_field('image_description')) { ?>
				<div class="content-row_text_image__image_description">
					<?php echo get_sub_field('image_description'); ?>
				</div>
			<?php } ?>
</div>
	</div>
</section>
