<section class="mega-menu">

	<div class="mega-menu__col mega-menu__col--info">
		<div class="mega-menu__info--default">
			<?php   $thumb = wp_get_attachment_image_src(get_field('mega_menu_image','options'), 'thumb-320');  ?>
			<img class="mega-menu__info--default-image" src="<?php echo $thumb[0]; ?>">
		</div>

		<?php // loop over the repeater items and display the image + text for the info blocks
		if(get_field('mega_menu','options')):
			$item_count = 0;
			while(the_repeater_field('mega_menu','options')):
				$item_count++; ?>
				<div class="mega-menu__info mega-menu__info--<?php echo $item_count; ?>">
					<?php   $thumb = wp_get_attachment_image_src(get_sub_field('info_block_image'), 'thumb-320');  ?>
					<img src="<?php echo $thumb[0]; ?>" />
				</div>

			<?php endwhile; ?>
		<?php endif; ?>
	</div>


	<div class="mega-menu__nav">
		<ul>
		<?php // powered by the ACF options panel
		// loop over each of the items to build a menu col (max of 4)
		// the data attribute is used in the JS to swap out the 'info' block on the RHS
		if(get_field('mega_menu','options')):
			$item_count = 0;
			while(the_repeater_field('mega_menu','options')):
					$item_count++;
					$type = get_sub_field('type');
					if($type == 'category') {
						$category_object = get_sub_field('category');
						$category_name = $category_object[0]->name;
						$category_slug = $category_object[0]->slug;
						$category_id = $category_object[0]->term_id;

						//echo $category_id;
						//$quantityTermObject = get_term_by( 'id', absint( $category_id ), 'seat' );
						//$quantityTermName = $quantityTermObject->name;
						 //= get_term( $category_id, 'seat' );
						//$category_name = $term->name;
						//var_dump($category_id);

						//$term = get_term_by( 'id', get_query_var( $category_id ), get_query_var( 'seat' ) );
						//echo $term->name;
						?>


						<li class="" data-pageid="<?php echo $item_count; ?>">

							<a href="<?php bloginfo('url'); ?>/2016-packages/?membership=<?php echo $category_slug; ?>" class="title"><?php 	echo $category_name; ?> Memberships</a>
							<ul class="children" data-pageid="<?php echo $item_count; ?>">
								<?php
								// wp query
								// https://gist.github.com/AaronRutley/5654937
								// $page_id = $post->ID;
								// $parent_page_id = $post->post_parent;

								//args
								$args = array(
								    'post_type' => 'packages',
								    'posts_per_page' => '-1',
									'orderby' => 'menu_order',
									'order' => 'DESC',
									'tax_query' => array(                     //(array) - use taxonomy parameters (available with Version 3.1).
									    'relation' => 'AND',                      //(string) - Possible values are 'AND' or 'OR' and is the equivalent of ruuning a JOIN for each taxonomy
									      array(
									        'taxonomy' => 'seat',                //(string) - Taxonomy.
									        'field' => 'id',                    //(string) - Select taxonomy term by ('id' or 'slug')
									        'terms' => array($category_id),    //(int/string/array) - Taxonomy term(s).
									        'include_children' => false,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
									        'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
									      )
									    ),
								);

								// new query
								$the_query = new WP_Query( $args );
								if ( $the_query->have_posts() ) :
								while ( $the_query->have_posts() ) : $the_query->the_post();?>
									<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
								<?php  endwhile; endif;wp_reset_postdata();?>
							</ul>
						</li>

					<?php } elseif($type == 'link') { ?>
						<li class="" data-pageid="<?php echo $item_count; ?>">
							<a class="title" href="<?php the_sub_field('page_link'); ?>"><?php the_sub_field('link_text'); ?></a>
							<div class="children" data-pageid="<?php echo $item_count; ?>">
						</li>
					<?php } ?>

			<?php endwhile; ?>
		<?php endif; ?>
		</ul>
	</div>

</section>

