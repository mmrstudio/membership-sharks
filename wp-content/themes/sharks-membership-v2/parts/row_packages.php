<span class="arrow" style="top:400px; z-index:999;"></span>
<section class="packages-row <?php echo $container_class; ?>">
	<div class="container" >
		<div class="packages-row__packages">
			<h2>Packages</h2>
<!--			<div class=" packages-row__--><?php //the_field('display_type'); ?><!--">-->
			<div class="packages-row__rows">
					<ul>
					<?php
					$i = 1;
					$max_i = 0;
					foreach (get_field('packages') as $package):
						$id = $package->ID;
						$max_i = $i;
						$class = get_field('renew_pricing',$id) ? '_renew' : '';
						?>
						<li class="selection">
							<div class="btn">

								<a class="package_select <?php echo $i == 1 ? "selected" : "" ?>"  data-id="<?php echo $i; ?>" data-slug="<?php echo $package->post_name ?>">
								<span class="arrow"></span>
								<span class="title"><?php echo $package->post_title; ?>
								<?php if (get_sub_field('description') != ''): ?>
									<br><?php the_sub_field('description'); ?>
								<?php endif; ?>
								</span>
								</a>
							</div>
							<?php if (get_field('note',$id)): ?>
								<div class="packages-row__note">
									<?php the_field('note',$id); ?>
									<?php if (get_field('note_button_url',$id) !== ''): ?>
										<br>
										<a href="<?php the_field('note_button_url',$id); ?>" target="_blank" class="packages-row__buy-now"><?php the_field('note_button_text',$id); ?></a>
									<?php endif; ?>
								</div>
							<?php endif; ?>

							<div class="packages-row__inline_package_details_container">
<!--								<a name="details_--><?php //echo $i; ?><!--"></a>-->
<!--								<a name="--><?php //echo $package->post_name; ?><!--"></a>-->
								<div class="packages-row__inline_package_details package_details packages-row__details package_details_in"  style="<?php echo $i != 1 ? "display:none" : "" ?>" data-id="<?php echo $i ?>"  >
									<div class="price-table ">
										<h3>Pricing</h3>
										<ul>
											<li class="table-head">
												<span class="table-col title<?php echo $class ?>">MEMBERSHIP TYPE</span>
												<?php if (get_field('renew_pricing',$id)): ?>
													<span class="table-col price<?php echo $class ?>"><?php the_field('price_header_renew',$id) ?></span>
												<?php endif; ?>
												<span class="table-col price<?php echo $class ?>"><?php the_field('price_header',$id) ?></span>
											</li>
											<?php

											$price_fields = [
												'price_platinum' => 'Platinum',
												'price_adult' => 'Adult',
												'price_concession' => 'Concession',
												'price_junior'	=> 'Junior'
											];
											foreach ($price_fields as $v => $title): ?>
												<?php if (is_numeric(get_field($v,$id)) || is_numeric(get_field($v . '_renew',$id))) : ?>
													<li class="table-row">
														<span class="table-col title<?php echo $class ?>"><?php echo $title ?></span>
														<?php if (get_field('renew_pricing',$id)): ?>
															<span class="table-col price<?php echo $class ?>"><?php echo get_field($v . '_renew',$id) !== '' ? '$' . number_format(get_field($v . '_renew',$id),2,'.',',') : '' ?></span>
														<?php endif; ?>
														<span class="table-col price<?php echo $class ?>"><?php echo get_field($v,$id) !== '' ? '$' . number_format(get_field($v,$id),2,'.',',') : '' ?></span>
													</li>
												<?php endif; ?>
											<?php endforeach; ?>
											<?php if (get_field('family_pricing',$id)): ?>
												<?php
												$price_fields = [
													'2A_2J' => 'Family (2 Adults + 2 Juniors)',
													'1A_2J' => 'Family (1 Adult + 2 Juniors)',
													'1A_3J'	=> 'Family (1 Adult + 3 Juniors)',
													'1A_4J'	=> 'Family (1 Adult + 4 Juniors)',
													'2A_1J'	=> 'Family (2 Adults + 1 Junior)',
													'2A_3J'	=> 'Family (2 Adults + 3 Juniors)',
													'2A_4J'	=> 'Family (2 Adults + 4 Juniors)'
												];
												foreach ($price_fields as $v => $title): ?>
													<?php if (is_numeric(get_field($v,$id)) || is_numeric(get_field($v . '_renew',$id))) : ?>
														<li class="table-row">
															<span class="table-col title<?php echo $class ?>"><?php echo $title ?></span>
															<?php if (get_field('renew_pricing',$id)): ?>
																<span class="table-col price<?php echo $class ?>"><?php echo get_field($v . '_renew',$id) !== '' ? '$' . number_format (get_field($v . '_renew',$id),2,'.',',') : '' ?></span>
															<?php endif; ?>
															<span class="table-col price<?php echo $class ?>"><?php echo get_field($v,$id) !== '' ? '$' . number_format (get_field($v,$id),2,'.',',') : '' ?></span>
														</li>
													<?php endif; ?>
												<?php endforeach; ?>
											<?php endif; ?>
										</ul>

									</div>

									<!-- Social Buttons -->
									<div class="packages-row__social">
										<ul>
											<li><a href="mailto:?subject=<?php echo $package->post_title; ?>&body=Hi, check this out <?php echo urlencode($_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] . '#' . $package->post_name ) ?>" ><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
											<li><a href="https://twitter.com/share?text=<?php echo urlencode($package->post_title); ?>&url=<?php echo urlencode('http://' . $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] . '#' . $package->post_name ) ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] . '#' . $package->post_name ) ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li class="share"><a>Share It<img src="/wp-content/themes/sharks-membership/assets/images/share_it.png"></a></li>
										</ul>
									</div>


									<!-- Buy Button-->


									<div class="packages-row__buy-now-container">
										<a class="packages-row__buy-now" target="_blank" href="<?php the_field('buy_now_link',$id); ?>"><?php the_field('buy_now_text',$id) ?></a>
									</div>





									<!-- Benefits -->
									<?php if (get_field('membership_benifits',$id) !== ''): ?>
										<div  class="packages-row__benefits package_benefits" data-id="<?php echo $i; ?>" >
											<h3 class="packages-row__benefits-title">Benefits</h3>
											<div class="details">
												<?php echo get_field('membership_benifits',$id); ?>
											</div>
										</div>
									<?php endif; ?>


									<!-- Seating Map -->
									<?php if (get_field('stadium_map',$id) != ''): ?>
										<div class="packages-row__seating-map">
											<h3>SEATING MAP</h3>
											<h3 class="black">SOUTHERN CROSS GROUP STADIUM</h3>
											<div class="packages-row__seating-map--title"  style="background-color: <?php echo get_field('stadium_title_color',$id); ?>
											<?php echo  (get_field('stadium_title_color',$id) == '#a4d5f3') ? ' color: #168bcc;' : ''; ?>
												">
												<?php echo get_field('stadium_title',$id); ?>
											</div>
											<div class="img">
												<a href="<?php echo get_field('stadium_link',$id)['url']; ?>" target="_blank">
													<div class="packages-row__seating-map--zoom"></div>
													<?php $thumb = wp_get_attachment_image_src(get_field('stadium_map',$id),'large');  ?>
													<img src="<?php echo $thumb[0]; ?>" />
												</a>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
<!--							--><?php //if (get_field('display_type') == 'rows' && get_field('membership_benifits',$id) !== ''): ?>
<!--								<div  class="packages-row__benefits package_benefits" data-id="--><?php //echo $i; ?><!--"  style="--><?php //echo $i != 1 ? "display:none" : "" ?><!--">-->
<!--									<h3 class="packages-row__benefits-title">Benefits</h3>-->
<!--									--><?php //echo get_field('membership_benifits',$id); ?>
<!--								</div>-->
<!--							--><?php //endif; ?>
						</li>
					<?php
					$i++;
					endforeach; ?>

				</ul>
			</div>
<!--			--><?php //if ( get_field('benefits') !== ''): ?>
<!--				<div class="packages-row__benefits">-->
<!--					<h3 class="packages-row__benefits-title">Benefits</h3>-->
<!--					--><?php //the_field('benefits'); ?>
<!--				</div>-->
<!--			--><?php //endif; ?>
		</div>
		<div class="packages-row__details packages-row__out_package_details">

			<?php
			$i = 1;
			foreach (get_field('packages') as $package):
				$id = $package->ID;
				// add _renew to the classes if we're showing three columns.
				$class = get_field('renew_pricing',$id) ? '_renew' : '';
				?>
				<div class="package_details package_details_out" data-id="<?php echo $i ?>"  style="<?php echo $i != 1 ? "display:none" : "" ?>">

					<div class="packages-row__next_prev">
						<ul>
							<?php if ($i < $max_i): ?>
								<li><a  class="package_nav next" data-id="<?php echo $i+1 ?>">
										Next
										<i class="fa  fa-angle-right" aria-hidden="true"></i>
									</a>

								</li>
							<?php endif; ?>
							<?php if ($i > 1): ?>
								<li><a  class="package_nav prev" data-id="<?php echo $i-1 ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Previous</a></li>
							<?php endif; ?>
						</ul>
					</div>


					<div class="packages-row__next_prev_bottom">

							<ul>
								<?php if ($i < $max_i): ?>
									<li><a  class="package_nav next" data-id="<?php echo $i+1 ?>">
											Next
											<i class="fa  fa-angle-right" aria-hidden="true"></i>
										</a>

									</li>
								<?php endif; ?>
								<?php if ($i > 1): ?>
									<li><a  class="package_nav prev" data-id="<?php echo $i-1 ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Previous</a></li>
								<?php endif; ?>
							</ul>

					</div>

					<div class="title-row">

						<h2><?php echo $package->post_title; ?></h2>
					</div>

					<div class="price-table  <?php if (get_field('renew_pricing',$id)): ?> three-cols <?php endif; ?>">
						<h3>Pricing</h3>
						<ul>
							<li class="table-head">
								<span class="table-col title<?php echo $class ?>">MEMBERSHIP TYPE</span>
								<?php if (get_field('renew_pricing',$id)): ?>
									<span class="table-col price<?php echo $class ?>"><?php the_field('price_header_renew',$id) ?></span>
								<?php endif; ?>
								<span class="table-col price<?php echo $class ?>"><?php the_field('price_header',$id) ?></span>
							</li>
						<?php

							$price_fields = [
								'price_platinum' => 'Platinum',
								'price_adult' => 'Adult',
								'price_concession' => 'Concession',
								'price_junior'	=> 'Junior'
							];
							foreach ($price_fields as $v => $title): ?>
								<?php if (is_numeric(get_field($v,$id)) || is_numeric(get_field($v . '_renew',$id))) : ?>
									<li class="table-row">
										<span class="table-col title<?php echo $class ?>"><?php echo $title ?></span>
										<?php if (get_field('renew_pricing',$id)): ?>
											<span class="table-col price<?php echo $class ?>"><?php echo get_field($v . '_renew',$id) !== '' ? '$' . number_format(get_field($v . '_renew',$id),2,'.',',') : '' ?></span>
										<?php endif; ?>
										<span class="table-col price<?php echo $class ?>"><?php echo get_field($v,$id) !== '' ? '$' . number_format(get_field($v,$id),2,'.',',') : '' ?></span>
									</li>
								<?php endif; ?>
							<?php endforeach; ?>
							<?php if (get_field('family_pricing',$id)): ?>
								<?php
								$price_fields = [
									'2A_2J' => 'Family (2 Adults + 2 Juniors)',
									'1A_2J' => 'Family (1 Adult + 2 Juniors)',
									'1A_3J'	=> 'Family (1 Adult + 3 Juniors)',
									'1A_4J'	=> 'Family (1 Adult + 4 Juniors)',
									'2A_1J'	=> 'Family (2 Adults + 1 Junior)',
									'2A_3J'	=> 'Family (2 Adults + 3 Juniors)',
									'2A_4J'	=> 'Family (2 Adults + 4 Juniors)'
								];
								foreach ($price_fields as $v => $title): ?>
									<?php if (is_numeric(get_field($v,$id)) || is_numeric(get_field($v . '_renew',$id))) : ?>
										<li class="table-row">
											<span class="table-col title<?php echo $class ?>"><?php echo $title ?></span>
											<?php if (get_field('renew_pricing',$id)): ?>
												<span class="table-col price<?php echo $class ?>"><?php echo get_field($v . '_renew',$id) !== '' ? '$' . number_format(get_field($v . '_renew',$id),2,'.',',') : '' ?></span>
											<?php endif; ?>
											<span class="table-col price<?php echo $class ?>"><?php echo get_field($v,$id) !== '' ? '$' . number_format (get_field($v,$id),2,'.',',') : '' ?></span>
										</li>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>

					</div>

					<!-- Social Buttons -->
					<div class="packages-row__social">
						<ul>
							<li><a href="mailto:?subject=<?php echo $package->post_title; ?>&body=Hi, check this out <?php echo urlencode($_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] . '#' . $package->post_name ) ?>" ><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
							<li><a href="https://twitter.com/share?text=<?php echo urlencode($package->post_title); ?>&url=<?php echo urlencode('http://' . $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] . '#' . $package->post_name ) ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI] . '#' . $package->post_name ) ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li class="share"><a>Share It<img src="/wp-content/themes/sharks-membership/assets/images/share_it.png"></a></li>
						</ul>
					</div>


					<!-- Buy Button-->


				<div class="packages-row__buy-now-container">
					<a class="packages-row__buy-now" target="_blank" href="<?php the_field('buy_now_link',$id); ?>"><?php the_field('buy_now_text',$id) ?></a>
				</div>





					<!-- Benefits -->
					<?php if (get_field('membership_benifits',$id) !== ''): ?>
						<div  class="packages-row__benefits package_benefits" data-id="<?php echo $i; ?>">
							<h3 class="packages-row__benefits-title">Benefits</h3>
							<div class="details">
								<?php echo get_field('membership_benifits',$id); ?>
							</div>
						</div>
					<?php endif; ?>

					<!-- Seating Map -->
					<?php if (get_field('stadium_map',$id) != ''): ?>
						<div class="packages-row__seating-map">
							<h3>SEATING MAP</h3>
							<h3 class="black">SOUTHERN CROSS GROUP STADIUM</h3>
							<div class="packages-row__seating-map--title"  style="background-color: <?php echo get_field('stadium_title_color',$id); ?>
								<?php echo  (get_field('stadium_title_color',$id) == '#a4d5f3') ? ' color: #168bcc;' : ''; ?>
								">
								<?php echo get_field('stadium_title',$id); ?>
							</div>
							<div class="img">
								<a href="<?php echo get_field('stadium_link',$id)['url']; ?>" target="_blank">
									<div class="packages-row__seating-map--zoom"></div>
									<?php $thumb = wp_get_attachment_image_src(get_field('stadium_map',$id),'large');  ?>
									<img src="<?php echo $thumb[0]; ?>" />
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
				<?php
				$i++;
				endforeach; ?>

		</div>
	</div>

</section>
