<div class="help_me-row__packages--group_details">
    <div class="bg">
        <div class="title">
            <h3><?php echo get_the_title($group->ID) ?></h3>
        </div>
        <div class="details">
            <div class="descriptor"><?php echo get_field('price_descriptor',$group->ID); ?></div>
            <div class="price"><?php echo get_field('price',$group->ID); ?></div>
            <div class="subtext"><?php echo get_field('price_subtext',$group->ID); ?></div>
        </div>
        <div class="action">
            <a href="<?php echo get_permalink($group->ID) ?>" class="btn">MORE INFO</a>
        </div>
        <div class="image">
            <?php $image = wp_get_attachment_image_src(get_field('panel_image',$group->ID),'panel');  ?>
            <img src="<?php echo $image[0]; ?>" />
        </div>
    </div>
</div>
