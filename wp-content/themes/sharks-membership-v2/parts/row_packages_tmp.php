<section class="packages-row <?php echo $container_class; ?>">
	<div class="container">
		<div class="packages-row__packages">
			<div class=" packages-row__<?php the_field('display_type'); ?>">
				<ul>
					<?php
					$id = 1;
					while ( have_rows('memberships') ) : the_row(); ?>
						<li>
							<div>
								<a class="package_select <?php echo $id == 1 ? "selected" : "" ?>"  data-id="<?php echo $id; ?>">
								<span><?php the_sub_field('title'); ?>
								<?php if (get_sub_field('description') != ''): ?>
									<br><?php the_sub_field('description'); ?>
								<?php endif; ?>
								</span>
								</a>
							</div>
							<?php if (get_field('display_type') == 'rows' && get_sub_field('benefits') !== ''): ?>
								<div  class="packages-row__benefits package_benefits" data-id="<?php echo $id; ?>"  style="<?php echo $id != 1 ? "display:none" : "" ?>">
									<h2>Benefits</h2>
									<?php the_sub_field('benefits'); ?>
								</div>
							<?php endif; ?>
						</li>
					<?php
					$id++;
					endwhile; ?>

				</ul>
			</div>
			<?php if ( get_field('benefits') !== ''): ?>
				<div class="packages-row__benefits">
					<h2>Benefits</h2>
					<?php the_field('benefits'); ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="packages-row__details">
			<?php
			$id = 1;
			while ( have_rows('memberships') ) : the_row(); ?>
				<div class="package_details" data-id="<?php echo $id ?>"  style="<?php echo $id != 1 ? "display:none" : "" ?>">
					<div class="title">
						<?php the_sub_field('title'); ?>
					</div>
					<?php if( have_rows('prices') ): ?>
						<div class="price-table">
							<ul>
								<?php while( have_rows('prices') ): the_row(); ?>
									<li class="table-row">
										<span class="table-col title"><?php the_sub_field('title') ?></span>
										<span class="table-col price"><?php echo get_sub_field('price_1') !== '' ? '$' . get_sub_field('price_1') : '' ?></span>
									</li>
								<?php endwhile; ?>

							</ul>
						</div>
					<?php endif; ?>
					<?php if (get_sub_field('button_title') !== ''): ?>
						<div>
							<a href="<?php echo the_field('button_url') ?>"><?php the_sub_field('button_title') ?></a>
						</div>
					<?php endif; ?>
					<div class="img">
						<?php $thumb = wp_get_attachment_image_src(get_sub_field('image'),'large');  ?>
						<img src="<?php echo $thumb[0]; ?>" />
					</div>
				</div>
				<?php
				$id++;
			endwhile; ?>

		</div>
	</div>
	<pre>
	<?php var_dump(get_field('packages')); ?>
</pre>
</section>
