<?php // single column text ?>

	<section class="section section--home-hero" id="homeHero">

		<video id="homeHeroVideo" autoplay loop muted controls>
			<source src="<?php echo str_replace('http://', '//', get_field('banner_video')); ?>" type="video/mp4">
		</video>

        <script>
            setTimeout(function() {
                document.getElementById('homeHeroVideo').play();
            }, 2500);
        </script>

		<div class="section--home-hero__content">

			<div class="content--upper">
				<h1 class="section--home-hero__content__title"><span>welcome</span><br>to season 2018</h1>
				<div class="logo">
					<a href="#"><img src="<?php bloginfo('template_url'); ?>/assets/images/sharks_logo.svg"></a>
				</div>
			</div>

			<div class="content--lower">
				<a href="https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login" onclick="window.open(this.href, 'newwindow', 'width=900, height=750'); return false;" class="btn"><span>Join</span></a>
				<a href="https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login " onclick="window.open(this.href, 'newwindow', 'width=900, height=750'); return false;" class="btn"><span>Renew</span></a>
			</div>

		</div>

		<div class="container__hero--down">
			<a class="section__home-hero--down" href="#help_me-row__results-container">
				<span class="steps-icon"></span>
			</a>
		</div>

	</section>
