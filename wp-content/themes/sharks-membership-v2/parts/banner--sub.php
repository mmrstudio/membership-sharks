<section class="banner banner--sub">
	<?php
	$full_image = wp_get_attachment_image_src(get_field('banner_image'), 'full');
	$mobile_image = wp_get_attachment_image_src(get_field('mobile_image'), 'slider_mobile');

	?>
	<style>
		.current_banner {
			background: url(<?php echo $full_image[0] ?>) no-repeat scroll center top transparent;
		}
		<?php if ($mobile_image) : ?>
			@media only screen and  (max-width: 800px) {
				.current_banner {
					background: url(<?php echo $mobile_image[0] ?>) no-repeat scroll center top transparent;
				}
			}
		<?php endif; ?>

	</style>
	<div class="container current_banner">
		<div class="text">
			<span class="text"><h1 class="banner__headline"><?php the_field('banner_line_1'); ?> <span class="highlight"><?php the_field('banner_line_2'); ?></span></h1></span>
		</div>
	</div>
</section>
