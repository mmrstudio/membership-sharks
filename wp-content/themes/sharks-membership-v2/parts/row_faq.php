<section class="faq-row">
	<div class="container">

		<div class="faq-item">
			<div class="faq-item__question"><h3><?php the_sub_field('question'); ?></h3></div>
			<div class="faq-item__answer"><?php the_sub_field('answer'); ?></div>
		</div>
	</div>
</section>
