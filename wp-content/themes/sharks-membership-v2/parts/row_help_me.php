<?php // setup a container class
$container_class = '';
// get the layout from the flexible content field
$layout = get_sub_field('layout');
// if layout is highlight, create a modifier class
if($layout == 'highlight') {
	$container_class = "content-row--highlight";
}
?>

<section class="help_me-row__results-container <?php echo $container_class; ?>" id="help_me-row__results-container">
	<div class="container">
		<div class="help_me-row__packages">
			<h2>2018 packages</h2>
			<div id="packages_selects" class="results"><?php get_template_part('parts/row_help_me-groups'); ?></div>
		</div>
		<div class="buttons">
			<div class="all-memberships">
				<a href="/2018membershippackages/" class="btn"><span>all memberships</span></a>
			</div>
			<div class="request-a-call">
				<a href="/request-a-call/" class="btn"><span>request a call</span></a>
			</div>
	</div>


</section>
