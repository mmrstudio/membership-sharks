<section class="download-row">
	<div class="container">


			<?php if(get_sub_field('downloads')): ?>
				<?php while(the_repeater_field('downloads')): ?>

					<div class="download-item">
						<a href="<?php the_sub_field('file'); ?>" target="_blank">
							<img src="<?php the_sub_field('thumbnail'); ?>" />
						</a>
							<h3><?php the_sub_field('title'); ?></h3>
							<p><?php the_sub_field('description'); ?></p>
							<a href="<?php the_sub_field('file'); ?>" target="_blank" class="btn">Download</a>
						</a>
					</div>

				<?php endwhile; ?>
			<?php endif; ?>


	</div>
</section>
