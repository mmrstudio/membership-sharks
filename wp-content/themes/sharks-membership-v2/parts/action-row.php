<section class="action-row">

	<div class="container">

		<div class="action-row__headline">
			<h2 class="">Live chat <span class="highlight">or</span> request a call</h2>
		</div>

		<div class="action-row__copy">
			<p class="highlight">Live chat is available from Monday to Friday 9am to 5pm ADST</p>
			<p>For any enquiries outside these hours please request a call from our Membership Team or send us an email at <a href="mailto:members@sharks.com.au">members@sharks.com.au</a></p>
			<ul>
				<li><a href="#" class="btn btn--on-black js-live-chat">Live Chat</a></li>
				<li><a href="" class="btn btn--on-black js-request-a-call">Request a call</a></li>
			</ul>
		</div>

		<div class="action-row__form">
			<?php echo do_shortcode('[gravityform id=1 title=false description=true ajax=true]'); ?>
		</div>

	</div>
</section><!-- action-row -->
