<?php // default class for each item
$package_class = 'package-item';
// base class that will be extended upon - this is added for filtering reasons
$package_class_extended = 'package-item';
// the terms for the seat taxonomy
$seat_terms = get_the_terms($post->ID,'seat');
// the terms for the membership taxonomy
$membershiip_terms = get_the_terms($post->ID,'membership');

$today = date('U');
$price_divider = false;

if($today >= date('U', strtotime('2016-05-14'))) :
    $price_divider = false;
elseif($today >= date('U', strtotime('2016-05-14'))) :
    $price_divider = 2;
elseif($today >= date('U', strtotime('2016-04-14'))) :
    $price_divider = 3;
elseif($today >= date('U', strtotime('2016-03-14'))) :
    $price_divider = 4;
else :
    $price_divider = false;
endif;

$price_adult = get_field('price_adult');
$price_adult_monthly = $price_divider !== false ? number_format(round($price_adult / $price_divider, 2), 2) : false ;

$price_junior = get_field('price_junior');
$price_junior_monthly = $price_divider !== false ? number_format(round($price_junior / 10, 2), 2) : false ;

// if we have terms, add them to the extended class
if ( !empty( $seat_terms ) ) {
	foreach ( $seat_terms as $term ) {
		$package_class_extended = $package_class_extended.' '.$package_class.'--'.$term->slug.' ';
	}
}

// if we have terms, add them to the extended class
if ( !empty( $membershiip_terms ) ) {
	foreach ( $membershiip_terms as $term ) {
		$package_class_extended = $package_class_extended.' '.$package_class.'--'.$term->slug.' ';
	}
}

?>

<div class="<?php echo $package_class_extended; ?>">

	<a href="<?php the_permalink() ?>" class="js-trigger-slideout">
		<div class="package-item__top-heading">Official 2016 Membership</div>
		<h2 class="package-item__title">
            <?php the_title(); ?>
            <?php if(get_field('sold_out')) : ?>
            (SOLD OUT)
            <?php endif; ?>
        </h2>
	</a>

	<?php
	/* sub heading was in design, but not in the content, commented out for now
	if (get_field('sub_heading') != '') { ?>
		<h3><?php the_sub_field('sub_heading'); ?></h3>
	<?php }
	*/ ?>

	<div class="package-item__intro"><p><?php the_field('membership_package_introduction'); ?></p></div>

	<div class="package-item__footer">
		<?php if ($price_adult != '') { ?>

			<p class="package-item__pirce">Adult $<?php echo $price_adult; ?></p>

			<?php /* if (get_field('hide_monthly_pricing') != '1' && $price_adult_monthly !== false) { ?>
				<p class="package-item__monthly">or $<?php echo $price_adult_monthly; ?> per month</p>
			<?php } */ ?>
		<?php } ?>

		<?php /* if ($price_adult == '') { ?>
			<p class="package-item__pirce">Junior $<?php echo $price_junior; ?></p>
			<?php if (get_field('hide_monthly_pricing') != '1' && $price_junior_monthly !== false) { ?>
				<p class="package-item__monthly">or $<?php echo $price_junior_monthly; ?> per month</p>
			<?php } ?>
		<?php } */ ?>
	</div>

	<?php if (get_field('hide_buy_now_buttons') == '1') { ?>
		<a class="package-item__btn package-item__btn--buy-now" target="_blank" href="<?php the_field('register_link','options'); ?>">Register</a>
	<?php } else { ?>
		<?php if (get_field('buy_now_link') != '') { ?>
			<a class="package-item__btn package-item__btn--buy-now" target="_blank" href="<?php the_field('buy_now_link'); ?>">Buy Now</a>
		<?php } else { ?>
			<a class="package-item__btn package-item__btn--buy-now" target="_blank" href="<?php the_field('buy_now_link','options'); ?>">Buy Now</a>
		<?php } ?>
	<?php } ?>
	<a class="package-item__btn package-item__btn--more-info js-trigger-slideout" href="<?php the_permalink() ?>">More info</a>

</div>
