<?php // single column text ?>

<section class="advertisement-row ">
	<div class="container">
		<?php   $thumb = wp_get_attachment_image_src(get_sub_field('image'),'full');  ?>
		<?php if (get_sub_field('image_link') != '') { ?>
			<a href="<?php the_sub_field('image_link'); ?>" ><img src="<?php echo $thumb[0]; ?>" /></a>
		<?php }else{ ?>
			<a><img src="<?php echo $thumb[0]; ?>" /></a>
		<?php } ?>
	</div>
</section>
