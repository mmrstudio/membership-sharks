<?php // single column text ?>

<section class="content-row ">
	<div class="container">
		<?php the_sub_field('text'); ?>
        <?php if (get_sub_field('html') != '') { the_sub_field('html'); } ?>
	</div>
</section>
