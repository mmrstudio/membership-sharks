

<?php /*
<div class="marketo">
	<h3>KEEP UP TO DATE WITH ALL YOUR SHARKS NEWS</h3>
	<script src="//app-sn02.marketo.com/js/forms2/js/forms2.min.js"></script>
		<form id="mktoForm_1241"></form>
	<script>MktoForms2.loadForm("//app-sn02.marketo.com", "860-GKO-248", 1241);</script>
</div>
*/ ?>


<section class="sponsors">
	<div class="container">
	<div class="sponsors__intro">
		<p>PROUD PARTNERS OF THE CRONULLA SHARKS</p>
	</div>
	<ul>

		<?php if(get_field('footer_sponsor_logos','options')): ?>
			<?php while(the_repeater_field('footer_sponsor_logos','options')): ?>
				<li><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('logo'); ?>" alt="" /></a></li>
			<?php endwhile; ?>
		<?php endif; ?>
	</ul>
	</div>
</section><!-- .sponsors -->

<section class="footer">
	<div class="container">

		<div class="footer__logo">
			<img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.svg">
		</div>

		<div class="footer__address-1">
			<p class="highlight">Cronulla Sharks</p>
			<p>461 Captain Cook Drive Woolooware 2230</p>
		</div>

		<div class="footer__address-2">
			<p class="highlight">Post</p>
			<p>PO Box 2219 <br> Taren Point NSW 2229</p>
		</div>

		<div class="footer__address-3">
			<p><span class="highlight">Phone: </span>1300 742 757 (SHARKS)</p>
			<p><span class="highlight">Email: </span><a href="mailto:members@sharks.com.au">members@sharks.com.au</a></p>
			<p><a href="http://www.sharks.com.au" class="highlight">www.sharks.com.au</a></p>
		</div>

		<ul class="social social--footer">
			<li><a class="social__item social__icon--facebook" href="<?php the_field('facebook_link','options'); ?>" target="_blank"></a></li>
			<li><a class="social__item social__icon--instagram" href="<?php the_field('instagram_link','options'); ?>" target="_blank"></a></li>
			<li><a class="social__item social__icon--twitter" href="<?php the_field('twitter_link','options'); ?>" target="_blank"></a></li>
		</ul>

		<ul class="footer__nav">
			<li>&copy;   Cronulla Sharks Rugby League Club <?php echo date("Y"); ?></li>
			<?php if(get_field('footer_menu','options')): ?>
				<?php while(the_repeater_field('footer_menu','options')): ?>
					<li><a href="<?php the_sub_field('link_url'); ?>"><?php the_sub_field('link_label'); ?></a></li>
				<?php endwhile; ?>
			<?php endif; ?>
			<li><a href="http://www.mmr.com.au" target="_blank">Site by MMR Studio</a></li>
		</ul>

	</div>
</section><!-- .footer -->

<?php wp_footer(); ?>

<?php // page blocker used as an overlay when the menu is open ?>
<section class="page-blocker"></section><!-- .page-blocker -->

</body>
</html>
