<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sharks-mb-wd
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

<!--
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
-->

	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-128.png" sizes="128x128" />
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/mstile-310x310.png" />


	<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">-->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/css/bootstrap.css'; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/assets/css/style.css'; ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="header-extend">
		<section class="header">

			<nav class="navbar navbar-expand-lg">
				<!--<a class="navbar-brand" href="<?php bloginfo('url'); ?>">
					<img src="www/wp-content/themes/sharks-mb-wd/assets/images/sharks_logo.svg" width="275" height="200" class="d-inline-block align-top" alt="Sharks">
				</a>-->

				<!-- SVG https://kinsta.com/blog/wordpress-svg/ -->
				<?php the_custom_logo(); ?>

				<!--<div class="logo logo--header">
					<a href="<?php bloginfo('url'); ?>"></a>
				</div>-->

<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#shark_menu" aria-controls="shark_menu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

				<div id="shark_menu" class="collapse navbar-collapse">
				<?php //wp_list_pages("depth=2&title_li="); ?>
				<?php // wp nav menu function defaults
				$nav_menu_large = array(
					'theme_location'  => 'large-screen-menu',
					'menu'            => '',
					'container'       => false,
					'container_class' => 'main-nav--large',
					'container_id'    => '',
					'menu_class'      => '',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => false,
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul class="main-nav--large navbar-nav" id="%1$s">%3$s</ul>',//main-nav--large
					'depth'           => 3,
					'walker'          => '',
				);

				wp_nav_menu( $nav_menu_large);
				?>

				<!--<a href="#mobile-menu" class="menu-toggle"><span></span><p class="menubtn">Menu</p><p class="closebtn">Close</p></a>-->
				<nav id="mobile-menu">

				<?php // wp nav menu function defaults
				$nav_menu_small = array(
					'theme_location'  => 'small-screen-menu',
					'menu'            => '',
					'container'       => false,
					'container_class' => 'small-screen-menu',
					'container_id'    => '',
					'menu_class'      => '',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => false,
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
					'depth'           => 3,
					'walker'          => '',
				);

				wp_nav_menu( $nav_menu_small );
				?>
				</nav>
				</div>

				<div class="member-tally member-tally--header">
					<!--<p class="member-tally__title">Member</br><span class="highlight">Tally</span></p>-->
					<?php
						// get membership count from options panel
						$membership_count = false;
						$membership_total = 0;
						$membership_total = get_field('membership_total', 'option');

						if( $membership_total ) :
							$membership_count = str_pad($membership_total, 5, STR_PAD_LEFT);
							$membership_count = str_split($membership_count);
						endif;

						// Apply animate if First visit and total is greater than 0
						$animate_class = false;
						if(FIRST_VISIT && $membership_total > 0) :
							$animate_class = 'animate';
						endif;
					?>
					<div class="member-tally__total counter <?php echo $animate_class; ?>" data-membership-count="<?php echo ($membership_total > 0) ? $membership_total : '0'; ?>">
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[0] : '0'; ?></div>
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[1] : '0'; ?></div>
						<div class="digit digit-comma">,</div>
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[2] : '0'; ?></div>
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[3] : '0'; ?></div>
						<div class="digit digit-number"><?php echo ($membership_count) ? $membership_count[4] : '0'; ?></div>
					</div>
					<p class="navbar-text">Members</p>
				</div>

			</nav>

<!--
				<ul class="social social--header">
					<li><a class="social__item social__icon--facebook" href="<?php the_field('facebook_link','options'); ?>" target="_blank"></a></li>
					<li><a class="social__item social__icon--instagram" href="<?php the_field('instagram_link','options'); ?>" target="_blank"></a></li>
					<li><a class="social__item social__icon--twitter" href="<?php the_field('twitter_link','options'); ?>" target="_blank"></a></li>
				</ul>


                    <div class="member-login">
                      <a href="https://oss.ticketmaster.com/aps/cronullasharks/EN/account/login" onclick="window.open(this.href, 'newwindow', 'width=900, height=750'); return false;" class="btn">Member Login</a>
                    </div>
-->

		</section><!-- header -->
	</div>