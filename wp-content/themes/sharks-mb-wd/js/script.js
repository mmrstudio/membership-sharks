(function($) {

	$(document).ready(function() {
		// JS for Member Tally
		var memberTally = $('.member-tally .counter');
		if( memberTally.hasClass('animate') ) {

		    var memberTallyCounter = new TweeningCounter().duration(2000),
		        membershipCount = memberTally.data('membership-count');

		    memberTallyCounter.ease('out-expo');
		    memberTally.html('');
		    memberTallyCounter.onUpdateCallback(function(val, num) {

		        var numArr = num.split('');
		        memberTally.html('');
		        var digit = $('<div class="digit digit-number"></div>');
		        if(val > 0) {
		            var counterStr = '<div class="digit digit-number">' + numArr[0] + '</div><div class="digit digit-number">' + numArr[1] + '</div><div class="digit digit-comma">,</div><div class="digit digit-number">' + numArr[2] + '</div><div class="digit digit-number">' + numArr[3] + '</div><div class="digit digit-number">' + numArr[4] + '</div>';
		            memberTally.html(counterStr);
		        }
		    });

		    memberTallyCounter.to(membershipCount).start();
		}

		$("#mktoform_join_now").click(function() {
			$(".mktoForm button[type=submit]").trigger("click");
		});

		mobileMenu();

		// member benefits page
		$(".benefits-info-box .mega-info-footer .mega-info-btn").each(function() {
			$(this).hover(
				function() {
					$(this).parent().find(".mega-info-title").show();
				}, function() {
					$(this).parent().find(".mega-info-title").hide();
				}
			);
		});
	});

	function mobileMenu() {
		if($(window).width() <= 991) {
			$(".main-nav--large li").on("click touch", function(e) {
				//e.preventDefault();

				if(!$(this).find("> .sub-menu").hasClass("mobile-opened")) {
					$(this).find(".sub-menu").addClass("mobile-opened").show();
				} else {
					$(this).find(".sub-menu").removeClass("mobile-opened").hide();
				}
			});
		}
	}

} )( jQuery );
