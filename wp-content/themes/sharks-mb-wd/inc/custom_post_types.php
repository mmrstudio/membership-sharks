<?php // Function Partial :  Custom Post Types

add_action('init', 'turbo_custom_post_types');

// Register Custom Post Type Packages
function turbo_custom_post_types() {
   $types = array(
		array(
			'the_type' => 'Packages',
			'single' => 'Package',
			'plural' => 'Packages'
		)
   );

	foreach ($types as $type) {

	   $the_type = $type['the_type'];
	   $single = $type['single'];
	   $plural = $type['plural'];

	   $labels = array(
		   'name' => _x($plural, 'post type general name'),
		   'singulturbo_name' => _x($single, 'post type singular name'),
		   'add_new' => _x('Add New', $single),
		   'add_new_item' => __('Add New '. $single),
		   'edit_item' => __('Edit '.$single),
		   'new_item' => __('New '.$single),
		   'view_item' => __('View '.$single),
		   'search_items' => __('Search '.$plural),
		   'not_found' =>  __('No '.$plural.' found'),
		   'not_found_in_trash' => __('No '.$plural.' found in Trash'),
		   'parent_item_colon' => ''
		 );

		$args = array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'menu_position' => 5,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'supports' => array('title','editor', 'author', 'custom-fields')
		);
		register_post_type($the_type, $args);
	}
}


add_action('init', 'turbo_custom_post_types_events');

// Register Custom Post Type Events
function turbo_custom_post_types_events() {
   $types = array(
		array(
			'the_type' => 'Events',
			'single' => 'Event',
			'plural' => 'Events'
		)
   );

	foreach ($types as $type) {

	   $the_type = $type['the_type'];
	   $single = $type['single'];
	   $plural = $type['plural'];

	   $labels = array(
		   'name' => _x($plural, 'post type general name'),
		   'singulturbo_name' => _x($single, 'post type singular name'),
		   'add_new' => _x('Add New', $single),
		   'add_new_item' => __('Add New '. $single),
		   'edit_item' => __('Edit '.$single),
		   'new_item' => __('New '.$single),
		   'view_item' => __('View '.$single),
		   'search_items' => __('Search '.$plural),
		   'not_found' =>  __('No '.$plural.' found'),
		   'not_found_in_trash' => __('No '.$plural.' found in Trash'),
		   'parent_item_colon' => ''
		 );

		$args = array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => true,
			'menu_position' => 5,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'supports' => array('title','editor', 'author', 'custom-fields','excerpt')
		);
		register_post_type($the_type, $args);
	}
}




// Register Custom Taxonomies

add_action( 'init', 'create_custom_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "book"
function create_custom_taxonomies() {

	   $customtaxs = array(
			array(
				'tag_slug' => 'seat',
				'tag_single' => 'Seat Type',
				'tag_plural' => 'Seat Types'
			),
			array(
				'tag_slug' => 'membership',
				'tag_single' => 'Membership Type',
				'tag_plural' => 'Membership Types'
			),
		   	array(
				'tag_slug' => 'location',
				'tag_single' => 'Location',
				'tag_plural' => 'Locations'
			)
	   );

		foreach ($customtaxs as $customtax) {
			$tag_slug = $customtax['tag_slug'];
			$tag_single = $customtax['tag_single'];
			$tag_plural = $customtax['tag_plural'];



		   $labels = array(
		   	'name'                       => _x( $tag_slug, 'taxonomy general name' ),
		   	'singular_name'              => _x( $tag_single, 'taxonomy singular name' ),
		   	'search_items'               => __( 'Search '.$tag_plural ),
		   	'popular_items'              => __( 'Popular '.$tag_plural ),
		   	'all_items'                  => __( 'All '.$tag_plural ),
		   	'parent_item'                => null,
		   	'parent_item_colon'          => null,
		   	'edit_item'                  => __( 'Edit '.$tag_single  ),
		   	'update_item'                => __( 'Update '.$tag_single ),
		   	'add_new_item'               => __( 'Add New '.$tag_single ),
		   	'new_item_name'              => __( 'New '.$tag_single ),
		   	'separate_items_with_commas' => __( 'Separate writers with commas' ),
		   	'add_or_remove_items'        => __( 'Add or remove '.$tag_plural ),
		   	'choose_from_most_used'      => __( 'Choose from the most used '.$tag_plural ),
		   	'not_found'                  => __( 'None found.' ),
		   	'menu_name'                  => __( $tag_plural),
		   );

		   $args = array(
		   	'hierarchical'          => true,
		   	'labels'                => $labels,
		   	'show_ui'               => true,
		   	'show_admin_column'     => false,
		   	'update_count_callback' => '_update_post_term_count',
		   	'query_var'             => true,
		   	'rewrite'               => array( 'slug' => $tag_slug ),
		   );

		   register_taxonomy( $tag_slug, 'packages', $args );

		}
}

?>
