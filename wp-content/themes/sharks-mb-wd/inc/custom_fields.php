<?php // Function Partial :  Advanced Custom Fields

// Add options page
if( function_exists('acf_add_options_page') ) {
	$page = acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title' 	=> 'Options',
		'menu_slug' 	=> 'site_options',
		'capability' 	=> 'edit_posts',
		'icon_url' => 'dashicons-dashboard',
		'redirect' 	=> false
	));
}
?>
