<?php
add_theme_support('menus');
register_nav_menus(
	array(
	  'small-screen-menu' => 'Small Screen Menu',
	  'large-screen-menu' => 'Large Screen Menu'
	)
);
?>