<?php
/**
 * The template part for news summary
 *
 * @package Turbo Starter Theme
 */
?>

<article id="news-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="container">
	<span class="post-border"></span>
	<div class="post-date">
		<span class="post-date__item post-date__item--month"><?php echo get_the_time('M') ?></span>
		<span class="post-date__item post-date__item--day"><?php echo get_the_time('j') ?></span>
		<span class="post-date__item post-date__item--yaer"><?php echo get_the_time('Y') ?></span>
	</div>

    <div class="container news-container">
      <div class="row">
	<div class="news__thumb col-md-3 col-xs-12">
		<a href="<?php the_permalink() ?>">
			<?php if (get_field('news_thumbnail') !== false) { ?>
				<?php   $thumb = wp_get_attachment_image_src(get_field('news_thumbnail'), 'medium');  ?>
				<img src="<?php echo $thumb[0]; ?>" />
			<?php } else { ?>
				<img src="<?php bloginfo('template_directory'); ?>/assets/images/news-temp.jpg">
			<?php } ?>
		</a>
	</div>
	<div class="news__intro col-md-9 col-xs-12">
		<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
		<p><?php the_field('news_intro'); ?></p>
		<a href="<?php the_permalink() ?>" class="btn btn--read-more">Read More</a>
	</div>
      </div>
    </div>
</div>
</article>
