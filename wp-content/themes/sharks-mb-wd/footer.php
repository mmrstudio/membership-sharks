<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sharks-mb-wd
 */

?>

<section class="footer-top">
	<div style="background-image: url('<?php the_field('footer_top_image','options'); ?>');"></div>
</section>

<section class="sponsors">
	<div class="container">
	<div class="sponsors__intro">
		<p class="text-center">PROUD PARTNERS OF THE CRONULLA SHARKS</p>
	</div>
	<ul class="d-flex p-2 align-items-center flex-wrap justify-content-between">

		<?php if(get_field('footer_sponsor_logos','options')): ?>
			<?php while(the_repeater_field('footer_sponsor_logos','options')): ?>
				<li class="list-inline-item"><a href="<?php the_sub_field('link'); ?>" target="_blank"><img class="img-fluid" src="<?php the_sub_field('logo'); ?>" alt="" /></a></li>
			<?php endwhile; ?>
		<?php endif; ?>
	</ul>
	</div>
</section><!-- .sponsors -->

<!--<section class="footer">-->
	<footer>
	<div class="container">

	<div class="row">

		<div class="footer__logo col-md-3 col-xs-12">
			<img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" class="img-fluid footer__sharks_logo">
			<img src="<?php bloginfo('template_directory'); ?>/assets/images/footer.png" class="img-fluid footer__img">
		</div>

		<div class="footer__address-1 col-md-2 col-xs-12">
			<p class="footer-address-header">Cronulla Sharks</p>
			<p>461 Captain Cook Drive Woolooware 2230</p>
		</div>

		<div class="footer__address-2 col-md-2 col-xs-12">
			<p class="footer-address-header">Post</p>
			<p>PO Box 2219 <br> Taren Point NSW 2229</p>
		</div>

		<div class="footer__address-3 col-md-3 col-xs-12">
			<p><span class="footer-address-header">Phone: </span>1300 742 757 (SHARKS)</p>
			<p><span class="footer-address-header">Email: </span><a href="mailto:members@sharks.com.au">members@sharks.com.au</a></p>
			<p class="footer-address-header text-uppercase"><a href="http://www.sharks.com.au">sharks.com.au</a></p>
			<p class="footer-address-header text-uppercase"><a href="http://store.sharks.com.au">store.sharks.com.au</a></p>
		</div>

		<ul class="social social--footer col-md-2 col-xs-12 text-lg-right">
			<li class="list-inline-item"><a class="social__item social__icon--facebook" href="<?php the_field('facebook_link','options'); ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
			<li class="list-inline-item"><a class="social__item social__icon--instagram" href="<?php the_field('instagram_link','options'); ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
			<li class="list-inline-item"><a class="social__item social__icon--twitter" href="<?php the_field('twitter_link','options'); ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
		</ul>

	</div>

	<div class="row links-container">

		<ul class="footer__nav col-md-12">
			<li class="list-inline-item">&copy;   Cronulla Sharks Rugby League Club <?php echo date("Y"); ?></li>
			<?php if(get_field('footer_menu','options')): ?>
				<?php while(the_repeater_field('footer_menu','options')): ?>
					<li class="list-inline-item"><a href="<?php the_sub_field('link_url'); ?>"><?php the_sub_field('link_label'); ?></a></li>
				<?php endwhile; ?>
			<?php endif; ?>
			<li class="list-inline-item float-md-right"><a href="http://www.mmr.com.au" target="_blank">Site by MMR</a></li>
		</ul>

	</div>

	</div>
	</footer>
<!--</section>--><!-- .footer -->

<?php wp_footer(); ?>

<?php // page blocker used as an overlay when the menu is open ?>
<section class="page-blocker"></section><!-- .page-blocker -->

</body>
</html>
