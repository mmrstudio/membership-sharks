<?php

if (!defined('HFC_ASSET_LOGO_URL'))
  define('HFC_ASSET_LOGO_URL', '/happyfox-chat/images/happyfox.png');

if (!defined('HFC_APP_MANAGE_PAGE'))
  define('HFC_APP_MANAGE_PAGE', 'https://happyfoxchat.com/a/apps/wordpress/manage/');

if (!defined('HFC_APP_REF_PAGE'))
  define('HFC_APP_REF_PAGE', 'https://happyfoxchat.com/?ref=wordpress-plugin');

if (!defined('HFC_APP_SIGNUP_PAGE'))
  define('HFC_APP_SIGNUP_PAGE', 'https://happyfoxchat.com/product-hunt-signup?ref=wordpress-plugin');

if (!defined('HFC_HOST_URL'))
  define('HFC_HOST_URL','https://happyfoxchat.com');

if (!defined('HFC_ASSETS_URL'))
  define('HFC_ASSETS_URL','https://widget.happyfoxchat.com/visitor');

if (!defined('HFC_APP_INTEGRATION_URL'))
  define('HFC_APP_INTEGRATION_URL','https://happyfoxchat.com/api/v1/integrations/wordpress/widget-info?apiKey=');

?>
