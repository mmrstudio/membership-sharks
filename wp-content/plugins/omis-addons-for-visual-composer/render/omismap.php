<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omismap extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'info_style' =>	'omis_info_box',
			'map_style' =>	'standard',
			'map_zoom' =>	'14',
			'map_lat' =>	'35.014685',
			'map_lng' =>	'-85.211300',
			'map_marker_text' =>	'',
			'omis_marker_icon' =>	'',
			'map_marker_lat' =>	'35.014685',
			'map_marker_lng' =>	'-85.211300',
			'image_id' 				=> 		'',
			'image_size' 			=> 		'',
			'image_radius' 			=> 		'0px',
			'font_icon' 			=> 		'',
			'icon_size' 			=> 		'25px',
			'icon_color' 			=> 		'',
			'icon_height'			=>		'80px',
			'icon_bg'				=>		'',
			'icon_radius'			=>		'0px',
			'border_width'			=>		'0px',
			'border_style'			=>		'solid',
			'border_clr'			=>		'',
			'css' 		 			=> 		'',
			'info_title' 			=> 		'',
			'title_size' 			=> 		'',
			'info_desc' 			=> 		'',
			'line_height' 			=> 		'',
			'info_size' 			=> 		'',
			'btn_visibility' 		=> 		'none',
			'btn_text' 				=> 		'',
			'btn_url' 				=> 		'',
			'btn_bg' 				=> 		'',
		), $atts ) );

		if ($omis_marker_icon != '') {
			$image_url = wp_get_attachment_url( $omis_marker_icon );		
		}

		wp_enqueue_script( 'omis-map', plugin_dir_url( dirname(__FILE__) ).'/js/omis-map.js', array('jquery') );

		ob_start(); ?>
		<div id="headquarters-omis-map" style="height: 509px;" class="gmap d-none d-md-block" data-map-style="<?php echo $map_style; ?>" data-zoom="<?php echo $map_zoom; ?>" data-lat="<?php echo $map_lat; ?>" data-lng="<?php echo $map_lng; ?>" data-map-marker-text="<?php echo $map_marker_text; ?>" data-omis-marker-icon="<?php echo $image_url; ?>" data-map-marker-lat="<?php echo $map_marker_lat; ?>" data-map-marker-lng="<?php echo $map_marker_lng; ?>">
		</div>
		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name" => __( 'Omis Map', 'omismap' ),
	"base" => "mvc_omismap",
	"category" => __('Omis Addons'),
	"description" => __('Add Google Map', 'omismap'),
	"icon" => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Select Style', 'omismap' ),
			"param_name" 	=> "map_style",
			"description" 	=> __( 'Choose map style', 'omismap' ),
			"group" 		=> 'General',
			"value" 		=> array(
				'Standard'	=>	'standard',
				'Silver'	=>	'silver',
				'Retro'		=>	'retro',
				'Dark'		=>	'dark',
				'Night'		=>	'night',
				'Aubergine'	=>	'aubergine',
			)
		),
		array(
			"type" => "textfield",
			"heading" => __( 'Zoom', 'omismap' ),
			"param_name" => "map_zoom",
			"description" => __( 'Set the zoom e.g. 14', 'omismap' ),
			"group" => 'General',
		),
		array(
			"type" => "textfield",
			"heading" => __( 'Latitude', 'omismap' ),
			"param_name" => "map_lat",
			"description" => __( 'Set the latitude', 'omismap' ),
			"group" => 'General',
		),
		array(
			"type" => "textfield",
			"heading" => __( 'Longitude', 'omismap' ),
			"param_name" => "map_lng",
			"description" => __( 'Set the longitude', 'omismap' ),
			"group" => 'General',
		),
		array(
			"type" => "textfield",
			"heading" => __( 'Marker Text', 'omismap' ),
			"param_name" => "map_marker_text",
			"description" => __( 'Set the marker text', 'omismap' ),
			"group" => 'Markers',
		),
		array(
			"type" => "attach_image",
			"heading" => __( 'Marker icon', 'omismap' ),
			"param_name" => "omis_marker_icon",
			"description" => __( 'Select the icon for marker', 'omismap' ),
			"group" => 'Markers',
		),
		array(
			"type" => "textfield",
			"heading" => __( 'Latitude', 'omismap' ),
			"param_name" => "map_marker_lat",
			"description" => __( 'Set the marker latitude', 'omismap' ),
			"group" => 'Markers',
		),
		array(
			"type" => "textfield",
			"heading" => __( 'Longitude', 'omismap' ),
			"param_name" => "map_marker_lng",
			"description" => __( 'Set the marker longitude', 'omismap' ),
			"group" => 'Markers',
		),
	),
) );