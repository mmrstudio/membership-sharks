<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_vc_omis_package_son extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'item_title_1' => '',
			'item_title_2' => '',
			'item_image_id' => '',
			'item_url' => '',
			'css' => '',
		), $atts ) );

		// Unique ID of item
		$item_unique_id = uniqid('product_item', true);
		$item_unique_id = str_replace('.', '_', $item_unique_id);

		if ($item_image_id != '') {
			$item_image_url = wp_get_attachment_url( $item_image_id );
		}

		$item_url_obj = vc_build_link( $item_url );

		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>
                <div id="<?php echo $item_unique_id; ?>" class="omis-package-item <?php echo (isset($css) ? $css : ""); ?>">
                    <a <?php echo (isset($item_url_obj["url"]) ? "href='".$item_url_obj["url"]."'" : ""); ?>>
                      <div class="omis-package-details">
                        <h1 class="omis-package-title-1"><?php echo isset($item_title_1) ? $item_title_1 : ''; ?></h1>
                        <h2 class="omis-package-title-2"><?php echo isset($item_title_2) ? $item_title_2 : ''; ?></h2>
                        <img class="omis-package-image" src="<?php echo $item_image_url; ?>" />
                      </div>
                    </a>
                </div>
		<?php

		return ob_get_clean();
	}
}


vc_map( array(
	"base"            => "vc_omis_package_son",
	"name"            => __( 'Omis Package Item', 'omispackage' ),
	"as_child"        => array('only' => 'vc_omis_package_father'),
	"content_element" => true,
	"category"        => __('Omis Package'),
	"description"     => __('package', ''),
	"icon"            => plugin_dir_url( __FILE__ ).'../icons/carousal-slider.png',
	'params' => array(
		array(
			"type"                  => "attach_image",
			"heading"               => __( 'Select Background Image', 'omispackage' ),
			"param_name"            => "item_image_id",
			"group"                 => 'General',
		),
		array(
			"type"                  => "textfield",
			"heading"               => __( 'Title 1', 'omispackage' ),
			"param_name"            => "item_title_1",
			"group"                 => 'General',
		),
		array(
			"type"                  => "textfield",
			"heading"               => __( 'Title 2', 'omispackage' ),
			"param_name"            => "item_title_2",
			"group"                 => 'General',
		),
		array(
			"type"                  => "vc_link",
			"heading"               => __( 'Package URL', 'omispackage' ),
			"param_name"            => "item_url",
			"group"                 => 'General',
		),
		array(
			"type"                  => "textfield",
			"heading"               => __( 'CSS classes', 'omispackage' ),
			"param_name"            => "css",
			"group"                 => 'CSS',
		),
	),
) );
