<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisreservedseating2 extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'css' => '',
		), $atts ) );

		wp_enqueue_script( 'prettyphoto' );
		wp_enqueue_style( 'prettyphoto' );

		$omis_js_ver  = date("ymd-Gis", filemtime( plugin_dir_path( __FILE__ ) . 'js/omis-reservedseating2.js' ));

		//wp_enqueue_script( 'omis-reservedseating2', plugin_dir_url( dirname(__FILE__) ).'js/omis-reservedseating2.js', array('jquery') );
		wp_enqueue_script( 'omis-reservedseating2', plugin_dir_url( dirname(__FILE__) ).'js/omis-reservedseating2.js', array(), $omis_js_ver );

		$membershiptype_array = [
						"Platinum"   => "price_platinum", 
						"Adult"      => "price_adult", 
						"Concession" => "price_concession",
						"Junior"     => "price_junior",
						"Family (1 Adult + 2 Juniors)" => "1A_2J",
						"Family (1 Adult + 3 Juniors)" => "1A_3J",
						"Family (1 Adult + 4 Juniors)" => "1A_4J",
						"Family (2 Adults + 1 Junior)" => "2A_1J",
						"Family (2 Adults + 2 Juniors)" => "2A_2J",
						"Family (2 Adults + 3 Juniors)" => "2A_3J",
						"Family (2 Adults + 4 Juniors)" => "2A_4J",
						"Toddler" => "toddler",
						"Baby" => "baby",
					];

		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>
                  <input type="hidden" id="omis_current_page_id" value="<?php echo get_the_ID(); ?>" />
                  <div class="seating-area">
                    <div class="sharks-dropdown-arrow-style seating-area-dropdown">
                      <span class="header">SEATING AREA:</span>
                      <select id="omis_reservedseating_package" class="selectpicker" data-style="omis-dropdown">
                        <?php
			foreach (get_field('packages') as $package) {
				$id = $package->ID;
				echo '<option value="'.$id.'" data-name="'.$package->post_name.'">'.$package->post_title.'</option>';
			}
                        ?>
                      </select>
                    </div>
                  </div>
                  <div class="table-sharks-text-container">
                    <table id="membership_prices_table" width="100%">
                      <!--
                      <thead>
                        <tr class="membership-price-table-heading">
                          <th>MEMBERSHIP</th>
                          <th>RENEWING MEMBER</th>
                          <th>NEW MEMBER</th>
                        </tr>
                      </thead>
                      -->
                      <tbody>
                      <?php foreach (get_field('packages') as $package): ?>
                        <?php $title_counter = 0; ?>
                        <?php foreach($membershiptype_array as $membershiptype_index => $membershiptype_value): ?>
                          <?php
				$mst_renewing_member_price = get_field($membershiptype_value . '_renew', $package->ID) !== '' ? '$' . number_format(get_field($membershiptype_value . '_renew', $package->ID),0,'.',',') : '';
				$mst_new_member_price = get_field($membershiptype_value, $package->ID) !== '' ? '$' . number_format(get_field($membershiptype_value, $package->ID),0,'.',',') : '';
                          ?>
                          <?php if($title_counter == 0): ?>
                          <tr class="membership-price-table-heading" data-package="<?php echo $package->ID; ?>" style="display: none;">
                            <th>MEMBERSHIP</th>
                            <?php if(get_field("renew_pricing", $package->ID) == true){ ?>
                            <th>RENEWING MEMBER</th>
                            <th>NEW MEMBER</th>
                            <?php } else { ?>
                            <th style="display: none;"></th>
                            <th>PRICE</th>
                            <?php } ?>
                          </tr>
                          <?php endif; ?>
                          <?php if(get_field($membershiptype_value . '_renew', $package->ID) != '' || get_field($membershiptype_value, $package->ID) != '' ): ?>
                            <tr data-package="<?php echo $package->ID; ?>" style="display: none;">
                              <td width="50%"><?php echo $membershiptype_index; ?></td>
                              <td width="25%" <?php if(get_field("renew_pricing", $package->ID) == false) { echo "style='display: none;'"; } ?> ><?php echo $mst_renewing_member_price; ?></td>
                              <td width="25%"><?php echo $mst_new_member_price; ?></td>
                            </tr>
                          <?php endif; ?>
                          <?php $title_counter++; ?>
                        <?php endforeach; ?>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                  <?php foreach (get_field('packages') as $package): ?>
                    <?php if(get_field('membership_benifits', $package->ID) !== false): ?>
                      <div class="omis-rs-benefits-container omis-reservedseating-container" data-package="<?php echo $package->ID; ?>" style="display: none;">
                        <h3 class="header-benefits">BENEFITS</h3>
                        <div class="omis-rs-benefits ul-sharks-style"><?php echo get_field('membership_benifits', $package->ID); ?></div>
                      </div>
                    <?php endif; ?>
                    <?php if(get_field('stadium_map', $package->ID) !== false): ?>
                  <div class="omis-rs-seating-map-container" data-package="<?php echo $package->ID; ?>" style="display: none;">
                    <h3>SEATING MAP</h3>
                    <div class="omis-rs-seating-map seating-map-container">
                      <div class="reserved-seating-map-container">
                        <div id="mega-line-container">
                          <h2><span class="omis-rs-seating-map-title1"></span> MEMBERSHIP AREA</h2>
                        </div>
                        <div class="omis-rs-seating-map-image-container">
                          <?php
				$stadium_map = get_field('stadium_map', $package->ID);
				$stadium_map_src = wp_get_attachment_image_src($stadium_map, 'large');
				$stadium_map_src = $stadium_map_src[0];
                          ?>
                          <a id="omis-rs-seating-map-image-link" data-rel="prettyPhoto" href="<?php echo $stadium_map_src; ?>" class="vc_single_image-wrapper vc_box_border_grey prettyphoto">
                            <img src="<?php echo $stadium_map_src; ?>" id="omis-rs-seating-map-image" />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                    <?php endif; ?>
                    <?php if(get_field('buy_now_link', $package->ID) !== false): ?>
                        <input type="hidden" class="buy_now_link" data-package="<?php echo $package->ID; ?>" value="<?php echo get_field('buy_now_link', $package->ID); ?>" />
                    <?php endif; ?>
                  <?php endforeach; ?>
		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name"                  => __( 'Omis Reserved Seating 2', 'omisreservedseating2' ),
	"base"                  => "mvc_omisreservedseating2",
	"category"              => __('Sharks Omis Addons'),
	"description"           => __('Reserved Seating Page with 1 dropdownlist', 'omisreservedseating2'),
	"icon"                  => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type"                  => "textfield",
			"heading"               => __( 'CSS classes', 'omisreservedseating2' ),
			"param_name"            => "css",
			"group"                 => 'CSS',
		),
	),
) );

