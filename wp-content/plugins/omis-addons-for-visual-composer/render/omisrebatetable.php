<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisrebatetable extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'years' => '',
			'css' => '',
		), $atts ) );

		$membershiptype_array = [
						"price_adult"      => "Adult", 
						"price_concession" => "Concession",
						"price_junior"     => "Junior",
					];

		$year_array = [
						"2018"   => "2018",
						"2019"   => "2019",
					];


		$year_array = explode(",", $years);



		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>
                  <input type="hidden" id="omis_current_page_id" value="<?php echo get_the_ID(); ?>" />
                  <div class="omis-rebate-container <?php echo (isset($css) ? $css : ""); ?>">
                    <div class="row">
                      <div class="col-lg-12 col-xs-12">
                        <div class="omis-rebate-cost">

                          <table id="rebate_prices_table" width="100%">
                            <thead>
                              <tr class="membership-price-table-heading">
                                <th>SEAT CATEGORY</th>
                                <th>MEMBERSHIP TYPE</th>
                                <?php foreach($year_array as $year_array_item): ?>
                                <th>REBATE IF SOLD <?php echo $year_array_item; ?></th>
                                <?php endforeach; ?>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
				$orderby = 'title';
				$order = 'ASC';
				$wp_query = new WP_Query( array('posts_per_page'=>100,
								'post_type'=>'packages',
								'orderby' => $orderby,
								'order' => $order,
								'paged' => get_query_var('paged') ? get_query_var('paged') : 1)
				);

				$package_counter = 0;
				$package_id_prev = 0;
				$package_row_class = "";

				if ($wp_query->have_posts() ) : while ($wp_query -> have_posts()) : $wp_query -> the_post();
					if(!is_null(get_field("rebate_prices", $group->ID)) && get_field("rebate_prices", $group->ID) !== false):
						$rebate_prices = get_field('rebate_prices', $group->ID);

						foreach($rebate_prices as $rebate_price):
							if(get_the_ID($group->ID) !== $package_id_prev) {
								if($package_row_class == "odd") {
									$package_row_class = "";
								} else {
									$package_row_class = "odd";
								}

								$package_id_prev = get_the_ID($group->ID);
							}
                            ?>
                              <tr class="<?php echo $package_row_class; ?>">
                                <?php if($package_counter == 0): ?>
                                <td width="34%" rowspan="<?php echo count($rebate_prices); ?>"><?php echo get_the_title($group->ID); ?></td>
                                <?php endif; ?>
                                <td width="22%"><?php echo $membershiptype_array[$rebate_price["membership_type"]]; ?></td>
                                <?php foreach($year_array as $year_array_item): ?>
                                <td width="<?php echo 44/count($year_array); ?>%">$<?php echo $rebate_price[trim($year_array_item)]; ?></td>
                                <?php endforeach; ?>
                              </tr>
                            <?php
							$package_counter++;
						endforeach;

						$package_counter = 0;
					endif;
					endwhile;
				endif;
                            ?>
                            </tbody>
                          </table>

                        </div>
                      </div>
                    </div>
                  </div>
		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name"                  => __( 'Omis Rebate Table', 'omisrebatetable' ),
	"base"                  => "mvc_omisrebatetable",
	"category"              => __('Sharks Omis Addons'),
	"description"           => __('Rebate Table View', 'omisrebatetable'),
	"icon"                  => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Years', 'omisrebatetable' ),
			"param_name" 		=> "years",
			"description" 		=> __( 'Put here years of rebate prices, comma separated. E.g. "2018, 2019"', 'omisrebatetable' ),
			"group" 		=> 'General',
		),
		array(
			"type"                  => "textfield",
			"heading"               => __( 'CSS classes', 'omisrebatetable' ),
			"param_name"            => "css",
			"group"                 => 'CSS',
		),
	),
) );

