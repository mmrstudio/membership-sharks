<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisreservedseating extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'css' => '',
		), $atts ) );

		wp_enqueue_script( 'prettyphoto' );
		wp_enqueue_style( 'prettyphoto' );

		wp_enqueue_script( 'omis-reservedseating', plugin_dir_url( dirname(__FILE__) ).'/js/omis-reservedseating.js', array('jquery') );

		$membershiptype_array = [
						"Platinum"   => "price_platinum", 
						"Adult"      => "price_adult", 
						"Concession" => "price_concession",
						"Junior"     => "price_junior",
						"Family (1 Adult + 2 Juniors)" => "1A_2J",
						"Family (1 Adult + 3 Juniors)" => "1A_3J",
						"Family (1 Adult + 4 Juniors)" => "1A_4J",
						"Family (2 Adults + 1 Junior)" => "2A_1J",
						"Family (2 Adults + 2 Juniors)" => "2A_2J",
						"Family (2 Adults + 3 Juniors)" => "2A_3J",
						"Family (2 Adults + 4 Juniors)" => "2A_4J",
					];

		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>
                  <input type="hidden" id="omis_current_page_id" value="<?php echo get_the_ID(); ?>" />
                  <div class="omis-reservedseating-filter-container">
                  <div class="container d-flex align-items-center">
                    <div class="omis-reservedseating-filter-item justify-content-lg-start justify-content-xl-center input-group">
                      <div class="input-group-prepend">
                        <label class="input-group-text text-uppercase" for="omis_reservedseating_package">Package:</label>
                      </div>
                      <select id="omis_reservedseating_package">
                      <?php
			foreach (get_field('packages') as $package) {
				$id = $package->ID;
				echo '<option value="'.$id.'" data-name="'.$package->post_name.'">'.$package->post_title.'</option>';
			}
                      ?>
                      </select>
                    </div>
                    <div class="omis-reservedseating-filter-item justify-content-lg-start justify-content-xl-center input-group">
                      <div class="input-group-prepend">
                        <label class="input-group-text text-uppercase" for="omis_reservedseating_membershiptype">Membership Type:</label>
                      </div>
                      <select id="omis_reservedseating_membershiptype">
                      <?php
			foreach($membershiptype_array as $membershiptype_index => $membershiptype_item) {
				echo '<option value="'.$membershiptype_item.'">'.$membershiptype_index.'</option>';
			}
                      ?>
                      </select>
                    </div>
                  </div>
                  </div>
                  <div class="omis-reservedseating-container <?php echo (isset($css) ? $css : ""); ?> container">
                    <div class="row">
                      <div class="col-md-12 col-lg-12 col-xl-6">
                        <div class="omis-rs-membershiptype-title"></div>
                        <div class="omis-rs-package-title text-uppercase"></div>
                      </div>
                      <div class="col-md-12 col-lg-12 col-xl-6">
                        <div class="omis-rs-renewing-member text-uppercase d-flex flex-column float-left">Renewing member <div class="omis-rs-renewing-member-price"></div></div>
                        <div class="omis-rs-new-member text-uppercase d-flex flex-column">New member <div class="omis-rs-new-member-price"></div></div>
                      </div>
                    </div>
                    <a href="" id="omis-rs-join-now" class="sharks-btn black sharks-link"></a>
                    <h3 class="header-benefits">BENEFITS</h3>
                    <div class="omis-rs-benefits ul-sharks-style"></div>
                    <h3>SEATING MAP</h3>
                    <div class="omis-rs-seating-map seating-map-container">
                      <div class="reserved-seating-map-container">
                        <div id="mega-line-container">
                          <h2><span class="omis-rs-seating-map-title1"></span> MEMBERSHIP AREA</h2>
                        </div>
                        <div class="omis-rs-seating-map-image-container">
                          <a id="omis-rs-seating-map-image-link" data-rel="prettyPhoto" href="http://sharks/wp-content/uploads/2018/08/stadium-map.jpg" class="vc_single_image-wrapper vc_box_border_grey prettyphoto">
                            <img src="" id="omis-rs-seating-map-image" />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name"                  => __( 'Omis Reserved Seating', 'omisreservedseating' ),
	"base"                  => "mvc_omisreservedseating",
	"category"              => __('Sharks Omis Addons'),
	"description"           => __('Reserved Seating Page', 'omisreservedseating'),
	"icon"                  => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type"                  => "textfield",
			"heading"               => __( 'CSS classes', 'omisreservedseating' ),
			"param_name"            => "css",
			"group"                 => 'CSS',
		),
	),
) );

