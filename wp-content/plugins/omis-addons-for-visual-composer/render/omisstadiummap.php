<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisstadiummap extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'item_image_id' => '',
			'css' => '',
		), $atts ) );

		if ($item_image_id != '') {
			$item_image_url = wp_get_attachment_url( $item_image_id );
		}

		$orderby = 'title';//get_field('order_package_by');
		$order = 'ASC';//get_field('order');
		$wp_query = new WP_Query( array('posts_per_page'=>100,
				'post_type'=>'packages',
				'orderby' => $orderby,
				'order' => $order,
				'paged' => get_query_var('paged') ? get_query_var('paged') : 1)
		);

		$stadium_map_css = '';
		$packages = array();
		$membershiptype_array = [
						"Platinum"   => "price_platinum", 
						"Adult"      => "price_adult", 
						"Concession" => "price_concession",
						"Junior"     => "price_junior",
						"Family (1 Adult + 2 Juniors)" => "1A_2J",
						"Family (1 Adult + 3 Juniors)" => "1A_3J",
						"Family (1 Adult + 4 Juniors)" => "1A_4J",
						"Family (2 Adults + 1 Junior)" => "2A_1J",
						"Family (2 Adults + 2 Juniors)" => "2A_2J",
						"Family (2 Adults + 3 Juniors)" => "2A_3J",
						"Family (2 Adults + 4 Juniors)" => "2A_4J",
					];
		$membership_prices = array();
		$package_stadium_seats = array();
		$package_links = array();

		if ($wp_query->have_posts() ) : while ($wp_query -> have_posts()) : $wp_query -> the_post();
			if(!is_null(get_field("stadium_seats", $group->ID)) && get_field("stadium_seats", $group->ID) !== false) {
				$stadium_seats = get_field("stadium_seats", $group->ID);
				$seat_color    = get_field("seat_color", $group->ID);

				if($stadium_seats !== false) {
					foreach($stadium_seats as $stadium_seat) {
						$stadium_map_css .= '.seat-'.$stadium_seat["seat_number"].' { fill: '.$seat_color.'; } .seat-'.$stadium_seat["seat_number"].':hover { fill: '.$seat_color.'!important; }';
						$package_stadium_seats[$stadium_seat["seat_number"]] = array("package_id" => get_the_ID($group->ID), "seat_image" => $stadium_seat["seat_image"]);
					}
				}

				$packages[get_the_ID($group->ID)] = get_the_title($group->ID);
				$package_links[get_the_ID($group->ID)] = get_field("custom_page_link");

				foreach($membershiptype_array as $membershiptype_index => $membershiptype_value) {
					$membership_prices[get_the_ID($group->ID)][$membershiptype_index] = array(
															"renewing_member" => get_field($membershiptype_value . '_renew', $group->ID) !== '' ? '$' . number_format(get_field($membershiptype_value . '_renew', $group->ID),0,'.',',') : '',
															"new_member" => get_field($membershiptype_value, $group->ID) !== '' ? '$' . number_format(get_field($membershiptype_value, $group->ID),0,'.',',') : ''
														);
				}
			}
		endwhile;
		endif;


		wp_enqueue_style('sharks-mb-wd-stadium-map', plugins_url( '../css/stadiummap.css' , __FILE__ ));
		wp_add_inline_style( 'sharks-mb-wd-stadium-map', $stadium_map_css );

		wp_enqueue_script( 'omis-omisstadiummap', plugin_dir_url( dirname(__FILE__) ).'/js/omis-stadiummap.js', array('jquery') );

		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>
                  <input type="hidden" id="omis_current_page_id" value="<?php echo get_the_ID(); ?>" />
                  <input type='hidden' id='omis_stadium_map_seats' value='<?php echo json_encode($package_stadium_seats); ?>' />
                  <div class="seat-image-container">
                    <img id="seat_image" />
                    <div id="back_to_seat"><a id="back_to_seat_link" href="#">BACK TO SEATING MAP</a></div>
                  </div>
                  <div id="stadium_map_container">
                    <?php echo file_get_contents($item_image_url); ?>
                  </div>
                  <div class="seating-area">
                    <div class="sharks-dropdown-arrow-style seating-area-dropdown">
                      <span class="header">SEATING AREA:</span>
                      <select id="stadium_map_package" class="selectpicker" data-style="omis-dropdown">
                        <option value="all">All</option>
                        <?php foreach($packages as $package_index => $package_value) : ?>
                        <option value="<?php echo $package_index; ?>"><?php echo $package_value; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="table-sharks-text-container">
                    <table id="membership_prices_table" width="100%">
                      <thead>
                        <tr class="membership-price-table-heading">
                          <th>MEMBERSHIP</th>
                          <th>RENEWING MEMBER</th>
                          <th>NEW MEMBER</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($membership_prices as $membership_price_index => $membership_price_value): ?>
                        <?php foreach($membership_price_value as $membership_price_value_index => $membership_price_value_value): ?>
                          <?php if($membership_price_value_value["renewing_member"] !== '' || $membership_price_value_value["new_member"] !== ''): ?>
                            <tr data-package="<?php echo $membership_price_index; ?>" style="display: none;">
                              <td width="50%"><?php echo $membership_price_value_index; ?></td>
                              <td width="25%"><?php echo $membership_price_value_value["renewing_member"]; ?></td>
                              <td width="25%"><?php echo $membership_price_value_value["new_member"]; ?></td>
                            </tr>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                  <?php foreach($package_links as $package_link_index => $package_link_value): ?>
                    <input type="hidden" id="package_link_<?php echo $package_link_index; ?>" value="<?php echo $package_link_value; ?>" />                     
                  <?php endforeach; ?>
		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name"                  => __( 'Omis Stadium Map', 'omisstadiummap' ),
	"base"                  => "mvc_omisstadiummap",
	"category"              => __('Sharks Omis Addons'),
	"description"           => __('Stadium Map', 'omisstadiummap'),
	"icon"                  => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type"                  => "attach_image",
			"heading"               => __( 'Select Stadium Image', 'omisstadiummap' ),
			"param_name"            => "item_image_id",
			"group"                 => 'General',
		),
		array(
			"type"                  => "textfield",
			"heading"               => __( 'CSS classes', 'omisstadiummap' ),
			"param_name"            => "css",
			"group"                 => 'CSS',
		),
	),
) );

