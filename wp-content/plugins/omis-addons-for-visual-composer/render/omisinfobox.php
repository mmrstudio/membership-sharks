<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisinfobox extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'info_style' 			=> 		'omis_info_box',
			'info_opt' 			=> 		'show_image',
			'image_id' 			=> 		'',
			'image_size' 			=> 		'',
			'image_radius' 			=> 		'0px',
			'font_icon' 			=> 		'',
			'icon_size' 			=> 		'25px',
			'icon_color' 			=> 		'',
			'icon_height'			=>		'80px',
			'icon_bg'			=>		'',
			'icon_radius'			=>		'0px',
			'border_width'			=>		'0px',
			'border_style'			=>		'solid',
			'border_clr'			=>		'',
			'css' 				=> 		'',
			'info_title' 			=> 		'',
			'title_size' 			=> 		'',
			'info_desc' 			=> 		'',
			'line_height' 			=> 		'',
			'info_size' 			=> 		'',
			'btn_visibility' 		=> 		'none',
			'btn_text' 				=> 		'',
			'btn_url' 				=> 		'',
			'btn_bg' 				=> 		'',
			'link_visibility' 		=> 		'none',
			'link_url' 			=> 		'',
		), $atts ) );
		$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
		if ($image_id != '') {
			$image_url = wp_get_attachment_url( $image_id );		
		}
		wp_enqueue_style( 'omis-info-box-css', plugins_url( '../css/omisinfobox.css' , __FILE__ ));
		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>

<div class="col-lg-4 col-md-6 col-padding">		
		<div class="<?php echo $info_style; ?>">
			<div class="omis-info-header">
				<?php if($link_visibility !== 'none') : ?>
                                <a href="<?php echo $link_url; ?>" target="_blank" class="omis-infobox-link">
				<?php endif; ?>

				<?php if ($info_opt == 'show_image') { ?>
					<img class="omis-info-img" src="<?php echo $image_url; ?>" style="width: <?php echo $image_size; ?>; border-radius: <?php echo $image_radius; ?>;">			
				<?php } ?>
				<?php if ($info_opt == 'show_icon') { ?>
					<i class="<?php echo $font_icon; ?>" aria-hidden="true" style="border: <?php echo $border_width; ?> <?php echo $border_style; ?> <?php echo $border_clr; ?>; border-radius: <?php echo $icon_radius; ?>; background: <?php echo $icon_bg; ?>; width: <?php echo $icon_height; ?>; height: <?php echo $icon_height; ?>; line-height: <?php echo $icon_height-$border_width*2; ?>px; font-size: <?php echo $icon_size; ?>; color: <?php echo $icon_color; ?>;"></i>
				<?php } ?>

				<?php if($link_visibility !== 'none') : ?>
				</a>
				<?php endif; ?>
			</div>
			<div class="omis-info-footer">
				<h3 class="omis-info-title" style="font-size: <?php echo $title_size; ?>; line-height: <?php echo $line_height; ?>;">
					<?php echo $info_title; ?>
				</h3>
				<p class="omis-info-desc" style="font-size: <?php echo $desc_size; ?>;">
					<?php echo $info_desc; ?>
				</p>
				<a class="omis-info-btn" href="<?php echo $btn_url; ?>" style="background: <?php echo $btn_bg; ?>; display: <?php echo $btn_visibility; ?>;">
					<?php echo $btn_text; ?>
				</a>
			</div>
		</div>
</div>

		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name" 			=> __( 'Omis Info Box', 'omisinfobox' ),
	"base" 			=> "mvc_omisinfobox",
	"category" 		=> __('Omis Addons'),
	"description" 	=> __('Add icon box with custom font icon', 'omisinfobox'),
	"icon" => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Select Style', 'omisinfobox' ),
			"param_name" 	=> "info_style",
			"description" 	=> __( 'Choose info style', 'omisinfobox' ),
			"group" 		=> 'General',
			"value" 		=> array(
				'Vertical'	=>	'omis_info_box',
				'Horizental'	=>	'omis_info_box_2',
			)
		),
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Select Image or Font icon', 'omisinfobox' ),
			"param_name" 	=> "info_opt",
			"description" 	=> __( 'Select Image or Font icon', 'omisinfobox' ),
			"group" 		=> 'General',
			"value" 		=> array(
				'Image'	=>	'show_image',
				'Font Icon'	=>	'show_icon',
			)
		),
		array(
            "type" 			=> 	"attach_image",
			"heading" 		=> 	__( 'Image', 'omisinfobox' ),
			"param_name" 	=> 	"image_id",
			"description" 	=> 	__( 'Select the image', 'omisinfobox' ),
			"group" 		=> 	'General',
			"dependency" => array('element' => "info_opt", 'value' => 'show_image'),
        ),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Width', 'omisinfobox' ),
			"param_name" 	=> "image_size",
			"description" 	=> __( 'Set the width in pixel e.g 80px', 'omisinfobox' ),
			"group" 		=> 'General',
			"dependency" => array('element' => "info_opt", 'value' => 'show_image'),
		),
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Image Radius', 'omisinfobox' ),
			"param_name" 	=> "image_radius",
			"description" 	=> __( 'set the image border radius', 'omisinfobox' ),
			"group" 		=> 'General',
			"dependency" => array('element' => "info_opt", 'value' => 'show_image'),
			"value"			=>	array(
					"None"		=>		"0px",
					"5px"		=>		"5px",
					"10px"		=>		"10px",
					"15px"		=>		"15px",
					"20px"		=>		"20px",
					"25px"		=>		"25px",
					"50%"		=>		"50%",
				)
		),
		array(
			"type" 			=> "iconpicker",
			"heading" 		=> __( 'Font icon', 'omisinfobox' ),
			"param_name" 	=> "font_icon",
			"description" 	=> __( 'Select the font icon', 'omisinfobox' ),
			"group" 		=> 'General',
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Font size', 'omisinfobox' ),
			"param_name" 	=> "icon_size",
			"description" 	=> __( 'Set icon font size in pixel e.g 30px', 'omisinfobox' ),
			"group" 		=> 'General',
			"value"			=>	"25px",
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Font Height/Width', 'omisinfobox' ),
			"param_name" 	=> "icon_height",
			"description" 	=> __( 'height & width for icon, set in pixel', 'omisinfobox' ),
			"group" 		=> 'General',
			"value"			=>	"80px",
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
		),
		array(
			"type" 			=> "colorpicker",
			"heading" 		=> __( 'Font Color', 'omisinfobox' ),
			"param_name" 	=> "icon_color",
			"description" 	=> __( 'Set icon color', 'omisinfobox' ),
			"group" 		=> 'General',
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
		),
		array(
			"type" 			=> "colorpicker",
			"heading" 		=> __( 'Backgroud', 'omisinfobox' ),
			"param_name" 	=> "icon_bg",
			"group" 		=> 'General',
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
		),
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Border Radius', 'omisinfobox' ),
			"param_name" 	=> "icon_radius",
			"description" 	=> __( 'set the border radius around icon', 'omisinfobox' ),
			"group" 		=> 'Border',
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
			"value"			=>	array(
					"None"		=>		"0px",
					"5px"		=>		"5px",
					"10px"		=>		"10px",
					"15px"		=>		"15px",
					"20px"		=>		"20px",
					"25px"		=>		"25px",
					"50%"		=>		"50%",
				)
		),
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Border Width', 'omisinfobox' ),
			"param_name" 	=> "border_width",
			"description" 	=> __( 'select the border width', 'omisinfobox' ),
			"group" 		=> 'Border',
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
			"value"			=>	array(
					"0px"		=>		"0px",
					"1px"		=>		"1px",
					"2px"		=>		"2px",
					"3px"		=>		"3px",
					"5px"		=>		"5px",
					"7px"		=>		"7px",
					"10px"		=>		"10px",
					"15px"		=>		"15px",
				)
		),
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Border Style', 'omisinfobox' ),
			"param_name" 	=> "border_style",
			"group" 		=> 'Border',
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
			"value"			=>	array(
					"Solid"		=>		"solid",
					"Dotted"	=>		"dotted",
					"Rige"		=>		"rige",
					"Dashed"	=>		"dashed",
					"Double"	=>		"double",
					"Groove"	=>		"groove",
					"Inset"		=>		"inset",
				)
		),
		array(
			"type" 			=> "colorpicker",
			"heading" 		=> __( 'Border Color', 'omisinfobox' ),
			"param_name" 	=> "border_clr",
			"description" 	=> __( 'set the border color', 'omisinfobox' ),
			"group" 		=> 'Border',
			"dependency" => array('element' => "info_opt", 'value' => 'show_icon'),
		),

		/* Detail */

		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Line Height', 'omisinfobox' ),
			"param_name" 	=> "line_height",
			"description" 	=> __( 'Set line height for text', 'omisinfobox' ),
			"value"			=>	"0",
			"group" 		=> 'Detail',
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Title', 'omisinfobox' ),
			"param_name" 	=> "info_title",
			"description" 	=> __( 'Write title for heading', 'omisinfobox' ),
			"group" 		=> 'Detail',
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Title font size', 'omisinfobox' ),
			"param_name" 	=> "title_size",
			"description" 	=> __( 'Set font size for title e.g 16px', 'omisinfobox' ),
			"group" 		=> 'Detail',
		),
		array(
			"type" 			=> "textarea",
			"heading" 		=> __( 'Description', 'omisinfobox' ),
			"param_name" 	=> "info_desc",
			"description" 	=> __( 'Write description for detail', 'omisinfobox' ),
			"group" 		=> 'Detail',
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Description font size', 'omisinfobox' ),
			"param_name" 	=> "desc_size",
			"description" 	=> __( 'Set font size for description e.g 14px', 'omisinfobox' ),
			"group" 		=> 'Detail',
		),

		/* Button */

        array(
            "type" 			=> 	"dropdown",
			"heading" 		=> 	__( 'Show/Hide button', 'omisinfobox' ),
			"param_name" 	=> 	"btn_visibility",
			"description" 	=> 	__( 'Select Show or Hide Button ', 'omisinfobox' ),
			"group" 		=> 	'Button',
			"value" 		=> array(
				'Hide' =>  'none',
				'Show' =>  'initial',
			)
        ),

		array(
            "type" 			=> 	"textfield",
			"heading" 		=> 	__( 'Button Text', 'omisinfobox' ),
			"param_name" 	=> 	"btn_text",
			"description" 	=> 	__( 'Button text name', 'omisinfobox' ),
			"dependency" => array('element' => "btn_visibility", 'value' => 'initial'),
			"group" 		=> 	'Button',
        ),

        array(
            "type" 			=> 	"textfield",
			"heading" 		=> 	__( 'Button Url', 'omisinfobox' ),
			"param_name" 	=> 	"btn_url",
			"description" 	=> 	__( 'Write Button URL for link', 'omisinfobox' ),
			"dependency" => array('element' => "btn_visibility", 'value' => 'initial'),
			"group" 		=> 	'Button',
        ),

        array(
            "type" 			=> 	"colorpicker",
			"heading" 		=> 	__( 'Background color', 'omisinfobox' ),
			"param_name" 	=> 	"btn_bg",
			"description" 	=> 	__( 'Set Button background color', 'omisinfobox' ),
			"dependency" => array('element' => "btn_visibility", 'value' => 'initial'),
			"group" 		=> 	'Button',
        ),


	/* LINK */
        array(
            "type" 			=> 	"dropdown",
			"heading" 		=> 	__( 'Show/Hide link', 'omisinfobox' ),
			"param_name" 	=> 	"link_visibility",
			"description" 	=> 	__( 'Select Show or Hide Link ', 'omisinfobox' ),
			"group" 		=> 	'Link',
			"value" 		=> array(
				'Hide' =>  'none',
				'Show' =>  'initial',
			)
        ),

        array(
            "type" 			=> 	"textfield",
			"heading" 		=> 	__( 'Link Url', 'omisinfobox' ),
			"param_name" 	=> 	"link_url",
			"description" 	=> 	__( 'Write Link URL', 'omisinfobox' ),
			"dependency" => array('element' => "link_visibility", 'value' => 'initial'),
			"group" 		=> 	'Link',
        ),

	),
) );

