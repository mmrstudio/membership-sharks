<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_vc_omis_package_father extends WPBakeryShortCodesContainer {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
		), $atts ) );
		$content = wpb_js_remove_wpautop($content, true);

		ob_start(); ?>
		<div class="omis-package-container">
			<div class="row">
				<?php echo $content; ?>
			</div>
		</div>
		<?php return ob_get_clean();
	}
}


vc_map( array(
	"base"            => "vc_omis_package_father",
	"name"            => __( 'Omis Package', 'omispackage' ),
	"as_parent"       => array('only' => 'vc_omis_package_son'),
	"content_element" => true,
	"js_view"         => 'VcColumnView',
	"category"        => __('Omis Package'),
	"description"     => __('Packages', ''),
	"icon"            => plugin_dir_url( __FILE__ ).'../icons/carousal-slider.png',
) );
