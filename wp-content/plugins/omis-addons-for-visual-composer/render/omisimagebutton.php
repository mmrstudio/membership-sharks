<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisimagebutton extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'item_title' => '',
			'item_image_id' => '',
			'item_url' => '',
			'css' => '',
		), $atts ) );

		if ($item_image_id != '') {
			$item_image_url = wp_get_attachment_url( $item_image_id );
		}

		$item_url_obj = vc_build_link( $item_url );

		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>
                  <div class="omis-image-button <?php echo (isset($css) ? $css : ""); ?>">
                    <div class="omis-img-arrow"><i class="fa fa-angle-right"></i></div>
                    <a <?php echo (isset($item_url_obj["url"]) ? "href='".$item_url_obj["url"]."'" : ""); ?>>
                      <div class="omis-image-button-details">
                        <img class="omis-image-button-image img-fluid" src="<?php echo $item_image_url; ?>" />
                        <h1 class="omis-image-button-title"><?php echo isset($item_title) ? $item_title : ''; ?></h1>
                      </div>
                    </a>
                  </div>
		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name"                  => __( 'Omis Image Button', 'omisimagebutton' ),
	"base"                  => "mvc_omisimagebutton",
	"category"              => __('Omis Addons'),
	"description"           => __('Add button', 'omisimagebutton'),
	"icon"                  => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type"                  => "attach_image",
			"heading"               => __( 'Select Background Image', 'omisimagebutton' ),
			"param_name"            => "item_image_id",
			"group"                 => 'General',
		),
		array(
			"type"                  => "textfield",
			"heading"               => __( 'Title', 'omisimagebutton' ),
			"param_name"            => "item_title",
			"group"                 => 'General',
		),
		array(
			"type"                  => "vc_link",
			"heading"               => __( 'Package URL', 'omisimagebutton' ),
			"param_name"            => "item_url",
			"group"                 => 'General',
		),
		array(
			"type"                  => "textfield",
			"heading"               => __( 'CSS classes', 'omisimagebutton' ),
			"param_name"            => "css",
			"group"                 => 'CSS',
		),
	),
) );

