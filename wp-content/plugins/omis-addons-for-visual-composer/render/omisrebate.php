<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisrebate extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'css' => '',
		), $atts ) );

		wp_enqueue_script( 'omis-rebate', plugin_dir_url( dirname(__FILE__) ).'/js/omis-rebate.js', array('jquery') );

		$membershiptype_array = [
						"Adult"      => "price_adult", 
						"Concession" => "price_concession",
						"Junior"     => "price_junior",
					];

		$year_array = [
						"2018"   => "2018",
						"2019"   => "2019",
					];

		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>
                  <input type="hidden" id="omis_current_page_id" value="<?php echo get_the_ID(); ?>" />
                  <div class="omis-rebate-filter-container d-inline-flex">
                    <div class="omis-rebate-filter-item sharks-dropdown-arrow-style form-group">
                      <label for="omis_rebate_package" class="rebate-header text-uppercase">Seat Category</label>
                      <select id="omis_rebate_package" class="form-control">
                      <?php
			$orderby = get_field('order_package_by');
			$order = get_field('order');
			$wp_query = new WP_Query( array('posts_per_page'=>100,
							'post_type'=>'packages',
							'orderby' => $orderby,
							'order' => $order,
							'paged' => get_query_var('paged') ? get_query_var('paged') : 1)
			);

			if ($wp_query->have_posts() ) : while ($wp_query -> have_posts()) : $wp_query -> the_post();
				if(!is_null(get_field("rebate_prices", $group->ID))) {
					echo '<option value="'.get_the_ID($group->ID).'">'.get_the_title($group->ID).'</option>';
				}
				endwhile;
			endif;
                      ?>
                      </select>
                    </div>
                    <div class="omis-rebate-filter-item sharks-dropdown-arrow-style form-group">
                      <label for="omis_rebate_membershiptype" class="rebate-header text-uppercase">Membership Type</label>
                      <select id="omis_rebate_membershiptype" class="form-control">
                      <?php
			foreach($membershiptype_array as $membershiptype_index => $membershiptype_item) {
				echo '<option value="'.$membershiptype_item.'">'.$membershiptype_index.'</option>';
			}
                      ?>
                      </select>
                    </div>
                    <div class="omis-rebate-filter-item sharks-dropdown-arrow-style form-group">
                      <label for="omis_rebate_membershiptype" class="rebate-header text-uppercase">Year</label>
                      <select id="omis_rebate_year" class="form-control">
                      <?php
			foreach($year_array as $year_index => $year_item) {
				echo '<option value="'.$year_item.'">'.$year_index.'</option>';
			}
                      ?>
                      </select>
                    </div>
                  </div>
                  <div class="omis-rebate-container <?php echo (isset($css) ? $css : ""); ?>">
                    <div class="row">
                      <div class="col-lg-12 col-xs-12">
                        <div class="omis-rebate-title">REBATE</div>
                        <div class="omis-rebate-cost"></div>
                      </div>
                    </div>
                  </div>
		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name"                  => __( 'Omis Rebate', 'omisrebate' ),
	"base"                  => "mvc_omisrebate",
	"category"              => __('Sharks Omis Addons'),
	"description"           => __('Rebate', 'omisrebate'),
	"icon"                  => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type"                  => "textfield",
			"heading"               => __( 'CSS classes', 'omisrebate' ),
			"param_name"            => "css",
			"group"                 => 'CSS',
		),
	),
) );

