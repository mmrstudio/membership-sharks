<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisbutton extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'btn_style' => '',
			'css' => '',
			'btn_text' => '',
			'phone_number' => '',
			'btn_link' => '',
		), $atts ) );
		//$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>

<?php if($btn_style == "omis_phone_button") {
	echo "<a href='tel:".$phone_number."'>";
}?>
<button type="button" <?php echo (isset($css) ? "class='".$css."'" : ""); ?>><?php echo $btn_text; ?></button>
<?php if($btn_style == "omis_phone_button") {
	echo "</a>";
}?>

		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name" 			=> __( 'Omis Button', 'omisbutton' ),
	"base" 			=> "mvc_omisbutton",
	"category" 		=> __('Omis Addons'),
	"description" 	=> __('Add button', 'omisbutton'),
	"icon" => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Select Button Style', 'omisbutton' ),
			"param_name" 	=> "btn_style",
			"description" 	=> __( 'Choose button style', 'omisbutton' ),
			"group" 		=> 'General',
			"value" 		=> array(
				'Button'	=>	'omis_button',
				'Phone Button'	=>	'omis_phone_button',
				'Link Button'	=>	'omis_link_button',
			)
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Button Text', 'omisbutton' ),
			"param_name" 	=> "btn_text",
			"description" 	=> __( 'Write text for button', 'omisbutton' ),
			"group" 		=> 'General',
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Phone Number', 'omisbutton' ),
			"param_name" 	=> "phone_number",
			"description" 	=> __( 'Set the Phone Number, e.g. +1-303-499-7111', 'omisbutton' ),
			"group" 		=> 'General',
			"dependency" => array('element' => "btn_style", 'value' => 'omis_phone_button'),
		),
		array(
			'type' => 'vc_link',
			'heading' => __( 'Button Link', 'omisbutton' ),
			'param_name' => 'btn_link',
			'description' => __( 'Set the button link', 'omisbutton' ),
			"group" 		=> 'General',
			"dependency" => array('element' => "btn_style", 'value' => 'omis_link_button'),
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'CSS classes', 'omisbutton' ),
			"param_name" 	=> "css",
			"description" 	=> __( 'CSS classes', 'omisbutton' ),
			"group" 		=> 'CSS',
		),
	),
) );

