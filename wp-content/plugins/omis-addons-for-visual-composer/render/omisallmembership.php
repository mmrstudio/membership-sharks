<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omisallmembership extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'css' => '',
		), $atts ) );

		wp_enqueue_script( 'omis-all-membership', plugin_dir_url( dirname(__FILE__) ).'/js/omis-all-membership.js', array('jquery') );

		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>
                  <div class="omis-all-membership-filter-container">
                    <div class="container d-flex align-items-center">   
                    <div class="omis-all-membership-filter-item sharks-dropdown-arrow-style-all-member justify-content-lg-start justify-content-xl-center input-group">
                      <div class="input-group-prepend">
                        <label class="input-group-text text-uppercase" for="omis_all_membership_location">Location:</label>
                      </div>
                      <select id="omis_all_membership_location" class="selectpicker" data-style="omis-btn-black omis-dropdown">
                        <option value="-" selected>-</option>
                    <?php
			$terms = get_terms('location', array('hide_empty'=>false));
			foreach($terms as $term){
				echo '<option value="'.$term->slug.'" >'.$term->name.'</option>';
			}
                    ?>
                      </select>
                    </div>
                    <div class="omis-all-membership-filter-item sharks-dropdown-arrow-style-all-member justify-content-lg-start justify-content-xl-center input-group">
                      <div class="input-group-prepend">
                        <label class="input-group-text text-uppercase" for="omis_all_membership_membership">Access:</label>
                      </div>
                      <select id="omis_all_membership_membership" class="selectpicker" data-style="omis-btn-black omis-dropdown">
                        <option value="-" selected>-</option>
                    <?php
			$terms = get_terms('membership', array('hide_empty'=>false));
			foreach($terms as $term){
				echo '<option value="'.$term->slug.'" >'.$term->name.'</option>';
			}
                    ?>
                      </select>
                    </div>
                    <div class="omis-all-membership-filter-item sharks-dropdown-arrow-style-all-member justify-content-lg-start justify-content-xl-center input-group">
                      <div class="input-group-prepend">
                        <label class="input-group-text text-uppercase" for="omis_all_membership_seat">Seating:</label>
                      </div>
                      <select id="omis_all_membership_seat" class="selectpicker" data-style="omis-btn-black omis-dropdown">
                        <option value="-" selected>-</option>
                    <?php
			$terms = get_terms('seat', array('hide_empty'=>false));
			foreach($terms as $term){
				echo '<option value="'.$term->slug.'" >'.$term->name.'</option>';
			}
                    ?>
                      </select>
                    </div>
                  </div>
                  </div>
                  <div class="omis-all-membership-container container <?php echo (isset($css) ? $css : ""); ?>">
			<div class="row">
		<?php
			$orderby = get_field('order_package_by');
			$order = get_field('order');
			$wp_query = new WP_Query( array('posts_per_page'=>100,
							'post_type'=>'packages',
							'orderby' => $orderby,
							'order' => $order,
							'paged' => get_query_var('paged') ? get_query_var('paged') : 1)
			);

			if ($wp_query->have_posts() ) : while ($wp_query -> have_posts()) : $wp_query -> the_post();
		?>
                       <div class="omis-all-membership-item omis-package-item col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3" data-category="<?php
				$terms = get_the_terms( $post->ID, array( 'seat', 'membership', 'location' ) );
				if ($terms) {
					foreach($terms as $term) {
						echo ' '.$term->slug;
					}
				}
			?>">
                          <a href="<?php echo get_field('custom_page_link',$group->ID); ?>">
                            <div class="omis-package-details">
                              <h1 class="omis-package-title-1"><?php echo get_the_title($group->ID); ?></h1>
                              <h2 class="omis-package-title-2"></h2>
                              <?php $image = wp_get_attachment_image_src(get_field('panel_image',$group->ID),'panel'); ?>
                              <img class="omis-package-image" src="<?php echo $image[0]; ?>" />
                              <?php if(get_field('sold_out')) : ?>
                                <div class="sold_out"><img src="<?php bloginfo('template_url'); ?>/assets/images/sold-out.png" /></div>
                              <?php endif; ?>
                            </div>
                          </a>
                        </div>
		<?php
			endwhile;
			endif;
		?>
			</div>
                  </div>
		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name"                  => __( 'Omis All Membership', 'omisallmembership' ),
	"base"                  => "mvc_omisallmembership",
	"category"              => __('Sharks Omis Addons'),
	"description"           => __('Add all membership', 'omisallmembership'),
	"icon"                  => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type"                  => "textfield",
			"heading"               => __( 'CSS classes', 'omisallmembership' ),
			"param_name"            => "css",
			"group"                 => 'CSS',
		),
	),
) );

