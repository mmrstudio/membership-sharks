<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class WPBakeryShortCode_mvc_omislink extends WPBakeryShortCode {

	protected function content( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'css' => '',
			'link_show_icon' => '',
			'link_text' => '',
			'font_icon' => '',
			'link_url' => '',
		), $atts ) );
		//$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), $this->settings['base'], $atts );
		$link_url_obj = vc_build_link( $link_url );
		$content = wpb_js_remove_wpautop($content, true);
		ob_start(); ?>

<a <?php echo (isset($link_url_obj["url"]) ? "href='".$link_url_obj["url"]."'" : ""); ?> <?php echo (isset($css) ? "class='".$css."'" : ""); ?>>
<?php if($link_show_icon == "omis_link_show_icon") { ?>
	<i class="<?php echo $font_icon; ?>"></i>
<?php } ?>
<?php echo $link_text; ?>
</a>

		<?php
		return ob_get_clean();
	}
}


vc_map( array(
	"name" 			=> __( 'Omis Link', 'omislink' ),
	"base" 			=> "mvc_omislink",
	"category" 		=> __('Omis Addons'),
	"description" 	=> __('Add link', 'omislink'),
	"icon" => plugin_dir_url( __FILE__ ).'../icons/infobox.png',
	'params' => array(
		array(
			"type" 			=> "dropdown",
			"heading" 		=> __( 'Show Icon', 'omislink' ),
			"param_name" 	=> "link_show_icon",
			"description" 	=> __( 'Show Icon', 'omislink' ),
			"group" 		=> 'General',
			"value" 		=> array(
				'Don\'t show Icon'	=>	'omis_link_dont_show_icon',
				'Show Icon'	=>	'omis_link_show_icon',
			)
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'Link Text', 'omislink' ),
			"param_name" 	=> "link_text",
			"description" 	=> __( 'Write text for link', 'omislink' ),
			"group" 		=> 'General',
		),
		array(
			"type" 			=> "iconpicker",
			"heading" 		=> __( 'Font icon', 'omislink' ),
			"param_name" 	=> "font_icon",
			"description" 	=> __( 'Select the font icon', 'omislink' ),
			"group" 		=> 'General',
			"dependency" => array('element' => "link_show_icon", 'value' => 'omis_link_show_icon'),
		),
		array(
			'type' => 'vc_link',
			'heading' => __( 'Button Link', 'omislink' ),
			'param_name' => 'link_url',
			'description' => __( 'Set the link URL', 'omislink' ),
			"group" 		=> 'General',
		),
		array(
			"type" 			=> "textfield",
			"heading" 		=> __( 'CSS classes', 'omislink' ),
			"param_name" 	=> "css",
			"description" 	=> __( 'CSS classes', 'omislink' ),
			"group" 		=> 'CSS',
		),
	),
) );

