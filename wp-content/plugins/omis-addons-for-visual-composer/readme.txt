=== Omis Addons For WPBakery Page Builder (formerly Visual Composer) ===
Contributors: Yerbol O.
Tags: omis addons, visual composer
Requires at least: 3.5
Tested up to: 4.9
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Addons for visual composer
