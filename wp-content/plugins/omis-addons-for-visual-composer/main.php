<?php 

class VC_OMIS
{
	function __construct()
	{
		add_action( 'vc_before_init', array($this, 'vc_mega_addons' ));
		//add_action( 'wp_enqueue_scripts', array($this, 'adding_front_scripts') );
		//add_action( 'init', array( $this, 'check_if_vc_is_install' ) );
		//add_filter( 'plugin_action_links', array($this, 'action_links'), 10, 5 );
		add_action('wp_ajax_get_package_by_id', array($this, 'get_package_by_id_callback'));
		add_action('wp_ajax_nopriv_get_package_by_id', array($this, 'get_package_by_id_callback'));
		add_action('wp_ajax_get_rebate', array($this, 'get_rebate_callback'));
		add_action('wp_ajax_nopriv_get_rebate', array($this, 'get_rebate_callback'));
		add_action('wp_ajax_get_membershiptype_by_package_id', array($this, 'get_membershiptype_by_package_id_callback'));
		add_action('wp_ajax_nopriv_get_membershiptype_by_package_id', array($this, 'get_membershiptype_by_package_id_callback'));
	}


	function adding_front_scripts () {
		//wp_enqueue_style( 'image-hover-effects-css', plugins_url( 'css/ihover.css' , __FILE__ ));
		//wp_enqueue_style( 'style-css', plugins_url( 'css/style.css' , __FILE__ ));
		//wp_enqueue_style( 'font-awesome-latest', plugins_url( 'css/font-awesome/css/font-awesome.css' , __FILE__ ));
		//wp_enqueue_script( 'front-js-na', plugins_url( 'js/script.js' , __FILE__ ), array('jquery', 'jquery-ui-core'));
	}

		
	function vc_mega_addons() {
		include 'render/omismap.php';
		include 'render/omisinfobox.php';
		include 'render/omisbutton.php';
		include 'render/omislink.php';
		include 'render/omis_package_container.php';
		include 'render/omis_package_item.php';
		include 'render/omisimagebutton.php';
		include 'render/omisallmembership.php';
		include 'render/omisreservedseating.php';
		include 'render/omisrebate.php';
		include 'render/omisstadiummap.php';
		include 'render/omisreservedseating2.php';
		include 'render/omisrebatetable.php';
		include 'render/omisinfobox2.php';
	}


	function check_if_vc_is_install(){
        if ( ! defined( 'WPB_VC_VERSION' ) ) {
            // Display notice that Visual Compser is required
            add_action('admin_notices', array( $this, 'showVcVersionNotice' ));
            return;
        }			
	}

	function showVcVersionNotice(){
	    ?>
	    <div class="notice notice-warning is-dismissible">
	        <p>Please install <a href="https://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431?ref=nasir179125">Visual Composer</a> to use Mega Addons.</p>
	    </div>
	    <?php
	}

	function action_links($actions, $plugin_file){
		static $plugin;

		if (!isset($plugin))
			$plugin = plugin_basename(__FILE__);
		if ('mega-addons-for-visual-composer/index.php' == $plugin_file && defined('WPB_VC_VERSION')) {

				$site_link = array('upgrade' => '<a href="https://codecanyon.net/item/mega-addons-for-wpbakery-page-builder-formerly-visual-composer/21480176?ref=nasir179125" style="font-size: 14px;color: #11967A;" target="_blank"><b>Upgrade To Premium</b></a>');
			
					$actions = array_merge($site_link, $actions);
				
			}
			
			return $actions;
    }

	function get_package_by_id_callback() {
		$package_id     = intval($_POST["package"]);
		$membershiptype = $_POST["membershiptype"];
		$page_id        = intval($_POST["page_id"]);

		$selected_package = array();

		$membershiptype_array = [
						"Platinum"   => "price_platinum", 
						"Adult"      => "price_adult", 
						"Concession" => "price_concession",
						"Junior"     => "price_junior",
						"Family (1 Adult + 2 Juniors)" => "1A_2J",
						"Family (1 Adult + 3 Juniors)" => "1A_3J",
						"Family (1 Adult + 4 Juniors)" => "1A_4J",
						"Family (2 Adults + 1 Junior)" => "2A_1J",
						"Family (2 Adults + 2 Juniors)" => "2A_2J",
						"Family (2 Adults + 3 Juniors)" => "2A_3J",
						"Family (2 Adults + 4 Juniors)" => "2A_4J",
					];

		$response = array();

		$packages = get_field('packages', $page_id);


		$new_member_price = get_field($membershiptype, $package_id) !== '' ? '$' . number_format(get_field($membershiptype, $package_id),0,'.',',') : '';
		$renewing_member_price = get_field($membershiptype . '_renew', $package_id) !== '' ? '$' . number_format(get_field($membershiptype . '_renew', $package_id),0,'.',',') : '';
		$membership_benefits = get_field('membership_benifits', $package_id);
		$buy_now_link = get_field('buy_now_link', $package_id);
		$buy_now_text = get_field('buy_now_text', $package_id);
		$stadium_title = get_field('stadium_title', $package_id);
		$stadium_title_color = get_field('stadium_title_color', $package_id);
		$stadium_link = get_field('stadium_link', $package_id);
		$stadium_link = $stadium_link["url"];
		$stadium_map = get_field('stadium_map', $package_id);
		$stadium_map_src = wp_get_attachment_image_src($stadium_map, 'large');
		$stadium_map_src = $stadium_map_src[0];


		$response["new_member_price"] = $new_member_price;
		$response["renewing_member_price"] = $renewing_member_price;
		$response["membership_benefits"] = $membership_benefits;
		$response["buy_now_link"] = $buy_now_link;
		$response["buy_now_text"] = $buy_now_text;
		$response["stadium"] = array(
						"stadium_title" => $stadium_title,
						"stadium_title_color" => $stadium_title_color,
						"stadium_link" => $stadium_link,
						"stadium_map_src" => $stadium_map_src
						);

		echo json_encode($response);

		wp_die();
	}

	function get_rebate_callback() {
		$package_id     = intval($_POST["package"]);
		$membershiptype = $_POST["membershiptype"];
		$year           = intval($_POST["year"]);
		$page_id        = intval($_POST["page_id"]);

		$selected_package = array();

		$response = array();

		$rebate_prices = get_field('rebate_prices', $package_id);

		foreach($rebate_prices as $rebate_price) {
			if($membershiptype === $rebate_price["membership_type"]) {
				$response = $rebate_price[$year];
			}
		}

		echo json_encode($response);

		wp_die();
	}

	function get_membershiptype_by_package_id_callback() {
		$package_id     = intval($_POST["package"]);
		$page_id        = intval($_POST["page_id"]);

		$selected_package = array();

		$membershiptype_array = [
						"Platinum"   => "price_platinum", 
						"Adult"      => "price_adult", 
						"Concession" => "price_concession",
						"Junior"     => "price_junior",
						"Family (1 Adult + 2 Juniors)" => "1A_2J",
						"Family (1 Adult + 3 Juniors)" => "1A_3J",
						"Family (1 Adult + 4 Juniors)" => "1A_4J",
						"Family (2 Adults + 1 Junior)" => "2A_1J",
						"Family (2 Adults + 2 Juniors)" => "2A_2J",
						"Family (2 Adults + 3 Juniors)" => "2A_3J",
						"Family (2 Adults + 4 Juniors)" => "2A_4J",
					];

		$membershiptype_existing = array();

		$response = array();

		// membership type existing
		foreach($membershiptype_array as $membershiptype_index => $membershiptype_item) {
			if(get_field($membershiptype_item, $package_id) !== '' || get_field($membershiptype_item.'_renew', $package_id) !== '') {
				$membershiptype_existing[$membershiptype_index] = $membershiptype_item;
			}
		}

		$response["membershiptype_existing"] = $membershiptype_existing;

		echo json_encode($response);

		wp_die();
	}

}

$N_object = new VC_OMIS;
?>