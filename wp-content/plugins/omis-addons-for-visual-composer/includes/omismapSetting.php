<?php 
$omisMap_params = array(
				array(
					"type" 			=> "textfield",
					"heading" 		=> __( 'Zoom', 'om-vc' ),
					"param_name" 	=> "map_zoom",
					"description" 	=> __( 'Set the width in pixel e.g 80px', 'om-vc' ),
					"group" 		=> 'General',
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> __( 'Latitude', 'om-vc' ),
					"param_name" 	=> "map_lat",
					"description" 	=> __( 'Set the width in pixel e.g 80px', 'om-vc' ),
					"group" 		=> 'General',
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> __( 'Longitude', 'om-vc' ),
					"param_name" 	=> "map_lng",
					"description" 	=> __( 'Set the width in pixel e.g 80px', 'om-vc' ),
					"group" 		=> 'General',
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> __( 'Longitude', 'om-vc' ),
					"param_name" 	=> "map_marker_text",
					"description" 	=> __( 'Set the width in pixel e.g 80px', 'om-vc' ),
					"group" 		=> 'Markers',
				),
				array(
					"type" 			=> 	"attach_image",
					"heading" 		=> 	__( 'Image', 'om-vc' ),
					"param_name" 	=> 	"omis_marker_icon",
					"description" 	=> 	__( 'Select the image', 'om-vc' ),
					"group" 		=> 	'Markers',
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> __( 'Latitude', 'om-vc' ),
					"param_name" 	=> "map_marker_lat",
					"description" 	=> __( 'Set the width in pixel e.g 80px', 'om-vc' ),
					"group" 		=> 'Markers',
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> __( 'Longitude', 'om-vc' ),
					"param_name" 	=> "map_marker_lng",
					"description" 	=> __( 'Set the width in pixel e.g 80px', 'om-vc' ),
					"group" 		=> 'Markers',
				),
				
	);

 ?>