( function( $ ) {
	$(document).ready(function() {
		filterAllMembershipPackages();

		$("#omis_all_membership_location").change(function() {
			filterAllMembershipPackages();
		});

		$("#omis_all_membership_membership").change(function() {
			filterAllMembershipPackages();
		});

		$("#omis_all_membership_seat").change(function() {
			filterAllMembershipPackages();
		});
	});

	function filterAllMembershipPackages() {
		var location   = $("#omis_all_membership_location").val();
		var membership = $("#omis_all_membership_membership").val();
		var seat       = $("#omis_all_membership_seat").val();
		var omis_am_item = 0;

		$(".omis-all-membership-item").each(function(index, value) {
			var dataCategory = $(value).attr("data-category");

			if(dataCategory.indexOf(location) != -1 && dataCategory.indexOf(membership) != -1 && dataCategory.indexOf(seat) != -1) {
				$(value).addClass("omis-all-membership-item-active").removeClass("omis-all-membership-item-odd");

				if(isOdd(omis_am_item) == 0) {
					$(value).addClass("omis-all-membership-item-odd");
				}

				omis_am_item++;
			} else {
				$(value).removeClass("omis-all-membership-item-active");
			}
		});
	}

	function isOdd(num) { return num % 2; }
} )( jQuery );