( function( $ ) {
	$(document).ready(function() {
		var hash = location.hash.substr(1);

		if(hash !== "") {
			$("#omis_reservedseating_package option[data-name=" + hash  + "]").attr("selected", "");
		}

		//filterReservedSeatingPackages();
		filterReservedSeatingMembershiptype();

		$("#omis_reservedseating_package").change(function() {
			//filterReservedSeatingPackages();
			filterReservedSeatingMembershiptype();
		});

		$("#omis_reservedseating_membershiptype").change(function() {
			filterReservedSeatingPackages();
		});
	});

	function filterReservedSeatingMembershiptype() {
		var package        = $("#omis_reservedseating_package").val();
		var page_id        = $("#omis_current_page_id").val();
		var package_title = $("#omis_reservedseating_package option:selected").text();

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: sharksajax.url,
			data: {
				'action': 'get_membershiptype_by_package_id',
				'package': package,
				'page_id': page_id
			},
			success: function(response) {
				var membershiptype_counter = 0;

				$("#omis_reservedseating_membershiptype option").hide();

				$.each(response.membershiptype_existing, function(index, value) {
					$("#omis_reservedseating_membershiptype option[value=" + value + "]").show();

					if(membershiptype_counter == 0) {
						$("#omis_reservedseating_membershiptype").val(value);
					}

					membershiptype_counter++;
				});


				filterReservedSeatingPackages();
			}
		});

	}

	function filterReservedSeatingPackages() {
		var package        = $("#omis_reservedseating_package").val();
		var membershiptype = $("#omis_reservedseating_membershiptype").val();
		var page_id        = $("#omis_current_page_id").val();
		var package_title = $("#omis_reservedseating_package option:selected").text();
		var membershiptype_title = $("#omis_reservedseating_membershiptype option:selected").text();

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: sharksajax.url,
			data: {
				'action': 'get_package_by_id',
				'package': package,
				'membershiptype': membershiptype,
				'page_id': page_id
			},
			success: function(response) {
				$(".omis-rs-membershiptype-title").text(membershiptype_title);
				$(".omis-rs-package-title").text(package_title);
				if(response.renewing_member_price !== '') {
					$(".omis-rs-renewing-member-price").text(response.renewing_member_price);
					$(".omis-rs-renewing-member").css("cssText", "");
				} else {
					$(".omis-rs-renewing-member-price").text();
					$(".omis-rs-renewing-member").css("cssText", "display: none !important;");
				}

				if(response.new_member_price !== '') {
					$(".omis-rs-new-member-price").text(response.new_member_price);
					$(".omis-rs-new-member").css("cssText", "");
				} else {
					$(".omis-rs-new-member-price").text();
					$(".omis-rs-new-member").css("cssText", "display: none !important;");
				}
				$("#omis-rs-join-now").attr("href", response.buy_now_link);
				$("#omis-rs-join-now").text(response.buy_now_text);
				$(".omis-rs-benefits").html(response.membership_benefits);
				$(".omis-rs-seating-map-title1").text(response.stadium.stadium_title);
				$("#omis-rs-seating-map-image").attr("src", response.stadium.stadium_map_src);
				$("#omis-rs-seating-map-image-link").attr("href", response.stadium.stadium_map_src);
			}
		});

	}
} )( jQuery );