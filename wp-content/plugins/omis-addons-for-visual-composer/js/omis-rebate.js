( function( $ ) {
	$(document).ready(function() {
		filterRebatePackages();

		$("#omis_rebate_package").change(function() {
			filterRebatePackages();
		});

		$("#omis_rebate_membershiptype").change(function() {
			filterRebatePackages();
		});

		$("#omis_rebate_year").change(function() {
			filterRebatePackages();
		});
	});

	function filterRebatePackages() {
		var package        = $("#omis_rebate_package").val();
		var membershiptype = $("#omis_rebate_membershiptype").val();
		var year = $("#omis_rebate_year").val();
		var page_id        = $("#omis_current_page_id").val();

		$.ajax({
			type: 'post',
			dataType: 'json',
			url: sharksajax.url,
			data: {
				'action': 'get_rebate',
				'package': package,
				'membershiptype': membershiptype,
				'year': year,
				'page_id': page_id
			},
			success: function(response) {
				console.log(response);

				$(".omis-rebate-cost").html(response);
			}
		});

	}
} )( jQuery );