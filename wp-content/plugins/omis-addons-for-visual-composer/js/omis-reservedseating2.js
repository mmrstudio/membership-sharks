( function( $ ) {
	$(document).ready(function() {
		var hash = location.hash.substr(1);

		if(hash !== "") {
			$("#omis_reservedseating_package option[data-name=" + hash  + "]").attr("selected", "");
		}

		$("#omis-join-button a").attr("data-default-href", $("#omis-join-button a").attr("href"));

		filterReservedSeatingPackages();

		$("#omis_reservedseating_package").change(function() {
			filterReservedSeatingPackages();
		});
	});

	function filterReservedSeatingPackages() {
		var package = $("#omis_reservedseating_package").val();

		$("#membership_prices_table tr[data-package=" + package + "]").show();
		//$("#membership_prices_table tr[data-package!=" + package + "]:not(.membership-price-table-heading)").hide();
		$("#membership_prices_table tr[data-package!=" + package + "]").hide();

		$(".omis-rs-seating-map-container[data-package=" + package + "]").show();
		$(".omis-rs-seating-map-container[data-package!=" + package + "]").hide();

		$(".omis-rs-benefits-container[data-package=" + package + "]").show();
		$(".omis-rs-benefits-container[data-package!=" + package + "]").hide();

		if($(".buy_now_link[data-package=" + package + "]").val() !== '') {
			$("#omis-join-button a").attr("href", $(".buy_now_link[data-package=" + package + "]").val());
		} else {
			$("#omis-join-button a").attr("href", $("#omis-join-button a").attr("data-default-href"));
		}
	}
} )( jQuery );