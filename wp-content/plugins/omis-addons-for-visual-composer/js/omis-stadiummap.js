( function( $ ) {
	var omis_stadium_map_seats = Object;

	$(document).ready(function() {
		omis_stadium_map_seats = JSON.parse($("#omis_stadium_map_seats").val());

		$("#stadium_map_package").change(function() {
			filterStadiumMapPackages();
		});

		$.each(omis_stadium_map_seats, function(index, value) {
			$(".seat-" + index).click(function() {
				$("#stadium_map_package").val(value.package_id).trigger("change");

				$(".seat-image-container #seat_image").attr("src", value.seat_image);
				//$(".seat-image-container").show();
				$(".seat-image-container").css("cssText", "display: block !important;")
			});
		});

		$(window).mousemove(function( event ) {

			var _wx = $('.seat-image-container').offset().left;
			var _wy = $('.seat-image-container').offset().top;
			var _ww = $('.seat-image-container').width();
			var _wh = $('.seat-image-container').height();

			var _x = Math.min(Math.max(event.pageX,_wx),_wx+_ww);
			var _y = Math.min(Math.max(event.pageY,_wy),_wy+_wh);

			var _dx = -(_x-(_wx+_ww/2));
			var _dy = -(_y-(_wy+_wh/2));

			$('#seat_image').css('margin-left', _dx/100 + "%");
			$('#seat_image').css('margin-top', _dy/40 + "%");
			$('#seat_image').css('transform', 'translateX(-50%) translateY(-50%) rotateY(' + (-_dx/50) + 'deg) rotateX(' + (_dy/50) + "deg)");

		});

		$("#back_to_seat_link").click(function(e) {
			e.preventDefault();

			//$(".seat-image-container").hide();
			$(".seat-image-container").css("cssText", "")
		});
	});

	function filterStadiumMapPackages() {
		var package = $("#stadium_map_package").val();

		if(package == 'all') {
			$.each(omis_stadium_map_seats, function(index, value) {
				$(".seat-" + index).removeClass("omis-stadium-map-seat-default");
			});

			$("#membership_prices_table tbody tr").hide();

			$("#stadium_map_more_info > a").attr("href", "/package/reserved-seating/");
		} else {
			$.each(omis_stadium_map_seats, function(index, value) {
				if(package == value.package_id) {
					$(".seat-" + index).removeClass("omis-stadium-map-seat-default");
				} else {
					$(".seat-" + index).addClass("omis-stadium-map-seat-default");
				}
			});

			$("#membership_prices_table tr[data-package=" + package + "]").show();
			$("#membership_prices_table tr[data-package!=" + package + "]:not(.membership-price-table-heading)").hide();

			$("#stadium_map_more_info > a").attr("href", $("#package_link_" + package).val());
		}
	}
} )( jQuery );